﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace OxiLet.Tools.Extensions {
	public class PageResourceLinksManager {

		#region Fields

		private readonly ThreadStore<Dictionary<string, string>> _store;

		#endregion

		#region Constructor

		public PageResourceLinksManager(string storeResourceKey) {
			_store = new ThreadStore<Dictionary<string, string>>(storeResourceKey);
		}

		#endregion

		#region Methods

		public void Include(HtmlHelper htmlHelper, string path, Func<string, string> buildResource) {
			var storeContent = _store.GetContent();
			if (!storeContent.ContainsKey(path)) {
				storeContent.Add(path, buildResource(UrlHelper.GenerateContentUrl(path, htmlHelper.ViewContext.HttpContext)));
			}
		}

		/// <summary>
		/// Render resources
		/// </summary>
		/// <returns></returns>
		public MvcHtmlString Render() {
			var stringBuilder = new StringBuilder();
			var storeValues = _store.GetContent().Values;
			foreach (var storeItem in storeValues) {
				stringBuilder.AppendLine(storeItem);
			}
			return MvcHtmlString.Create(stringBuilder.ToString());
		}

		#endregion		
	}
}
