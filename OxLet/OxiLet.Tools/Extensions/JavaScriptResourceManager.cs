﻿using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace OxiLet.Tools.Extensions {
	public static class JavaScriptResourceManager {

		#region Fields

		private static readonly PageResourceLinksManager _resourceManager = new PageResourceLinksManager("Store.JavaScriptResources");

		private static readonly ThreadStore<List<string>> _linesOfCodeStore = new ThreadStore<List<string>>("Store.JavaScriptCode");

		#endregion

		#region Methods

		private static string BuildTag(string src) {
			var scriptTag = new TagBuilder("script");
			scriptTag.MergeAttribute("type", "text/javascript");
			scriptTag.MergeAttribute("src", src);
			return scriptTag.ToString();
		}

		public static void IncludeScript(this HtmlHelper htmlHelper, string path) {
			_resourceManager.Include(htmlHelper, path, BuildTag);
		}

		public static void IncludeScript(this HtmlHelper htmlHelper, string path, string debugPath) {
			string file = htmlHelper.ViewContext.HttpContext.IsDebuggingEnabled ? debugPath : path;
			_resourceManager.Include(htmlHelper, file, BuildTag);
		}

		public static void IncludeCode(this HtmlHelper htmlHelper, IEnumerable<string> linesOfCode) {
			var storeContent = _linesOfCodeStore.GetContent();
			storeContent.AddRange(linesOfCode);
		}

		public static MvcHtmlString RenderScripts(this HtmlHelper htmlHelper) {
			return _resourceManager.Render();
		}

		public static MvcHtmlString RenderScripts() {
			return _resourceManager.Render();
		}

		public static MvcHtmlString RenderCode(this HtmlHelper htmlHelper) {
			var stringBuilder = new StringBuilder();
			stringBuilder.AppendLine("<script type=\"text/javascript\">\n$(function() {");
			var storeContent = _linesOfCodeStore.GetContent();
			foreach (var item in storeContent) {
				stringBuilder.AppendLine("\t" + item);
			}
			stringBuilder.AppendLine("});");
			stringBuilder.AppendLine("</script>");
			return MvcHtmlString.Create(stringBuilder.ToString());
		}
		#endregion		
	}
}
