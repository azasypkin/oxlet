﻿using System.Web.Mvc;

namespace OxiLet.Tools.Extensions {
	public static class CssResourceManager {

		#region Fields

		private static readonly PageResourceLinksManager _resourceManager = new PageResourceLinksManager("Store.CssResources");

		#endregion

		#region Methods

		public static void IncludeStyle(this HtmlHelper htmlHelper, string path) {
			_resourceManager.Include(
				htmlHelper,
				UrlHelper.GenerateContentUrl(path, htmlHelper.ViewContext.HttpContext),
				BuildTag
			);
		}

		private static string BuildTag(string src) {
			var linkTag = new TagBuilder("link");
			linkTag.MergeAttribute("type", "text/css");
			linkTag.MergeAttribute("rel", "stylesheet");
			linkTag.MergeAttribute("href", src);
			return linkTag.ToString();
		}

		public static MvcHtmlString RenderStyles(this HtmlHelper htmlHelper) {
			return _resourceManager.Render();
		}

		#endregion
	}		

}
