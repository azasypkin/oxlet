﻿namespace OxiLet.Tools.Extensions {
	public class ThreadStore<TStoreItemType> where TStoreItemType:class, new() {

		#region Fields

		/// <summary>
		/// Returns resource store key
		/// </summary>
		/// <returns></returns>
		private readonly string _threadStoreKey;

		#endregion

		#region Methods

		public ThreadStore(string threadStoreKey) {
			_threadStoreKey = threadStoreKey;
		}

		public TStoreItemType GetContent() {
			var resourceStore = System.Web.HttpContext.Current.Items[this._threadStoreKey] as TStoreItemType;
			if (resourceStore == null) {
				resourceStore = new TStoreItemType();
				System.Web.HttpContext.Current.Items[this._threadStoreKey] = resourceStore;
			}
			return resourceStore;
		}

		#endregion
	}
}
