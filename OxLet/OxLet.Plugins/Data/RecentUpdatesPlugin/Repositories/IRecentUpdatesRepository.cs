﻿using System;
using OxLet.Core.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin.Repositories { 
	public interface IRecentUpdatesRepository {
		RecentUpdate GetRecentUpdate(int id);
		RecentUpdate GetRecentUpdate(string category, int itemId);
		RecentUpdate GetRecentUpdate(string category, string itemValue);
		RecentUpdate SaveRecentUpdate(RecentUpdate item);
		IPagedList<RecentUpdate> GetRecendUpdates(DateTime? sinceDate, PagingInfo pagingInfo);
		IPagedList<RecentUpdate> GetRecendUpdatesWithDrafts(DateTime? sinceDate, PagingInfo pagingInfo);
	}
}
