﻿using System;
using System.Linq;
using System.Transactions;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Data.SQL;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin.Repositories.SQL {
	public class RecentUpdatesRepository: IRecentUpdatesRepository{

		private readonly OxletDataContext _context;
		private readonly SiteContext _siteContext;

		public RecentUpdatesRepository(OxletDataContext context, SiteContext siteContext) {
			_context = context;
			_siteContext = siteContext;
		}

		public RecentUpdate GetRecentUpdate(int id) {
			return ProjectRecentUpdates(GetRecentUpdatesQuery(id)).FirstOrDefault();
		}

		public RecentUpdate GetRecentUpdate(string category, int itemId) {
			return ProjectRecentUpdates(GetRecentUpdatesQuery(_siteContext.Site.Id, category, itemId)).FirstOrDefault();
		}

		public RecentUpdate GetRecentUpdate(string category, string itemValue) {
			return ProjectRecentUpdates(GetRecentUpdatesQuery(_siteContext.Site.Id, category, itemValue)).FirstOrDefault();
		}

		public RecentUpdate SaveRecentUpdate(RecentUpdate item) {
			oxlet_RecentUpdate recentUpdateToSave = null;
			using (var transaction = new TransactionScope()) {
				if (item.Id != 0) {
					recentUpdateToSave = _context.oxlet_RecentUpdates.FirstOrDefault(x => x.Id == item.Id);
				}

				if (recentUpdateToSave == null) {
					recentUpdateToSave = new oxlet_RecentUpdate {
						SiteId = _siteContext.Site.Id,
						ModifiedDate = DateTime.UtcNow
					};
					_context.oxlet_RecentUpdates.InsertOnSubmit(recentUpdateToSave);
				}
				else {
					recentUpdateToSave.ModifiedDate = DateTime.UtcNow;
				}

				recentUpdateToSave.Category = item.Category;
				recentUpdateToSave.CreatorUserId = item.User.Id;
				recentUpdateToSave.ItemId = item.ItemId;
				recentUpdateToSave.ItemValue = item.ItemValue;
				recentUpdateToSave.State = (byte) item.State;
				recentUpdateToSave.PublishedDate = item.PublishedDate;
				_context.SubmitChanges();
				transaction.Complete();
			}
			return GetRecentUpdate(recentUpdateToSave.Id);
		}

		public IPagedList<RecentUpdate> GetRecendUpdates(DateTime? sinceDate, PagingInfo pagingInfo) {
			return ProjectRecentUpdates(
				ExcludeNotYetPublished(GetRecentUpdatesQuery(_siteContext.Site.Id, EntityState.Normal, sinceDate ?? new DateTime()))
			)
			.GetPage(pagingInfo);
		}

		public IPagedList<RecentUpdate> GetRecendUpdatesWithDrafts(DateTime? sinceDate, PagingInfo pagingInfo) {
			return ProjectRecentUpdates(
					GetRecentUpdatesQuery(_siteContext.Site.Id, EntityState.Normal, sinceDate ?? new DateTime())
				)
				.GetPage(pagingInfo);
		}

		private static IQueryable<oxlet_RecentUpdate> ExcludeNotYetPublished(IQueryable<oxlet_RecentUpdate> query) {
			return query.Where(p => p.PublishedDate.HasValue && p.PublishedDate.Value <= DateTime.UtcNow);
		}

		private IQueryable<oxlet_RecentUpdate> GetRecentUpdatesQuery(int siteId, EntityState state, DateTime sinceDate) {

			return
				from ru in _context.oxlet_RecentUpdates
				where ru.SiteId == siteId && ru.State == (byte)state && (ru.PublishedDate == null || ru.PublishedDate >= sinceDate)
				select ru;
		}

		private IQueryable<oxlet_RecentUpdate> GetRecentUpdatesQuery(int siteId, string category, int itemId) {

			return
				from ru in _context.oxlet_RecentUpdates
				where ru.SiteId == siteId && ru.ItemId == itemId && ru.Category == category
				select ru;
		}

		private IQueryable<oxlet_RecentUpdate> GetRecentUpdatesQuery(int siteId, string category, string itemValue) {

			return
				from ru in _context.oxlet_RecentUpdates
				where ru.SiteId == siteId && ru.ItemValue == itemValue && ru.Category == category
				select ru;
		}

		private IQueryable<oxlet_RecentUpdate> GetRecentUpdatesQuery(int id) {

			return
				from ru in _context.oxlet_RecentUpdates
				where ru.Id == id
				select ru;
		}

		private IQueryable<RecentUpdate> ProjectRecentUpdates(IQueryable<oxlet_RecentUpdate> recentUpdates) {
			return
				from ru in recentUpdates
				join u in _context.oxlet_Users on ru.CreatorUserId equals u.Id
				orderby ru.ModifiedDate descending
				select ProjectRecentUpdate(ru, u);
		}

		private static RecentUpdate ProjectRecentUpdate(oxlet_RecentUpdate recentUpdate, oxlet_User user) {

			return new RecentUpdate(
				recentUpdate.Id,
				recentUpdate.ModifiedDate,
				recentUpdate.Category,
				recentUpdate.ItemId,
				recentUpdate.ItemValue,
				(EntityState)recentUpdate.State,
				new UserDescription(user.Id) {
					DisplayName = user.DisplayName,
					Email = user.Email,
					EmailHash = user.EmailHash,
					Name = user.Name,
					Url = user.Url
				},
				recentUpdate.PublishedDate
			);
		}
	}
}
