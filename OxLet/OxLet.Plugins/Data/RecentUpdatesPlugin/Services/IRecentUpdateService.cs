﻿using System;
using OxLet.Core.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin.Services {
	public interface IRecentUpdateService {
		IPagedList<RecentUpdate> GetRecentUpdates(DateTime sinceDate, PagingInfo pagingInfo);
		ModelResult<RecentUpdate> AddRecentUpdate(RecentUpdate recentUpdate);
		ModelResult<RecentUpdate> EditRecentUpdate(RecentUpdate recentUpdate);
		void Remove(RecentUpdate recentUpdate);
	}
}
