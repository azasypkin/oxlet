﻿using System;
using System.Linq;
using OxLet.Core.Cache;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Repositories;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin.Services {
	public class RecentUpdateService : IRecentUpdateService {
		private readonly IRecentUpdatesRepository _repository;
		private readonly ICacheService _cacheService;
		private readonly SiteContext _context;

		public RecentUpdateService(IRecentUpdatesRepository repository, ICacheService cacheService, SiteContext siteContext) {
			_repository = repository;
			_cacheService = cacheService;
			_context = siteContext;
		}

		#region IRecentUpdateService Members

		public IPagedList<RecentUpdate> GetRecentUpdates(DateTime sinceDate, PagingInfo pagingInfo) {
			bool includeDrafts = _context.User.IsAuthenticated && _context.User.IsInRole("Administrator");
			return _cacheService.GetItems<IPagedList<RecentUpdate>, RecentUpdate>(
				string.Format("Site:{0}-GetRecentUpdates-IncludeDrafts:{1}", _context.Site.Id, includeDrafts),
				() => includeDrafts 
					? _repository.GetRecendUpdatesWithDrafts(sinceDate, pagingInfo) 
					: _repository.GetRecendUpdates(sinceDate, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>(),
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);
		}

		public ModelResult<RecentUpdate> AddRecentUpdate(RecentUpdate recentUpdate) {

			var update = _repository.SaveRecentUpdate(recentUpdate);

			InvalidateCachedRecentUpdateDependencies(update);

			return new ModelResult<RecentUpdate>(update, null);
		}

		public ModelResult<RecentUpdate> EditRecentUpdate(RecentUpdate recentUpdate) {

			// if id isn't set then try to find the appropriate recent update entry
			if(recentUpdate.Id == 0) {
				var existentRecentUpdate = recentUpdate.ItemId.HasValue && recentUpdate.ItemId > 0
					? _repository.GetRecentUpdate(recentUpdate.Category, recentUpdate.ItemId.Value)
					: _repository.GetRecentUpdate(recentUpdate.Category, recentUpdate.ItemValue);
				if (existentRecentUpdate != null) {
					recentUpdate = new RecentUpdate(
						existentRecentUpdate.Id,
						recentUpdate.Date,
						recentUpdate.Category,
						recentUpdate.ItemId,
						recentUpdate.ItemValue,
						recentUpdate.State,
						recentUpdate.User,
						recentUpdate.PublishedDate
					);
				}
			}

			var update = _repository.SaveRecentUpdate(recentUpdate);

			InvalidateCachedRecentUpdateDependencies(update);

			return new ModelResult<RecentUpdate>(update, null);
		}

		public void Remove(RecentUpdate recentUpdate) {

			if(recentUpdate.Id == 0) {
				
				// try to find recent update by category and itemId
				if(recentUpdate.ItemId != null) {
					var updateByItemId = _repository.GetRecentUpdate(recentUpdate.Category, recentUpdate.ItemId.Value);
					if(updateByItemId != null) {
						recentUpdate = new RecentUpdate(
							updateByItemId.Id,
							recentUpdate.Date,
							recentUpdate.Category,
							recentUpdate.ItemId,
							recentUpdate.ItemValue,
							recentUpdate.State, 
							recentUpdate.User,
							recentUpdate.PublishedDate
						);
					}
				}

				if(recentUpdate.Id == 0 && !string.IsNullOrEmpty(recentUpdate.ItemValue)) {
					var updateByItemValue= _repository.GetRecentUpdate(recentUpdate.Category, recentUpdate.ItemValue);
					if (updateByItemValue != null) {
						recentUpdate = new RecentUpdate(
							updateByItemValue.Id,
							recentUpdate.Date,
							recentUpdate.Category,
							recentUpdate.ItemId,
							recentUpdate.ItemValue,
							recentUpdate.State,
							recentUpdate.User,
							recentUpdate.PublishedDate
						);
					}
				}
			}

			var update = _repository.SaveRecentUpdate(recentUpdate);

			InvalidateCachedRecentUpdateDependencies(update);
		}

		#endregion

		#region Private Methods

		private void InvalidateCachedRecentUpdateDependencies(RecentUpdate recentUpdate) {
			_cacheService.InvalidateItem(recentUpdate);
			_cacheService.Invalidate(string.Format("Site:{0}-GetRecentUpdates-IncludeDrafts:True", _context.Site.Id));
			_cacheService.Invalidate(string.Format("Site:{0}-GetRecentUpdates-IncludeDrafts:False", _context.Site.Id));
		}

		#endregion

		
	}
}
