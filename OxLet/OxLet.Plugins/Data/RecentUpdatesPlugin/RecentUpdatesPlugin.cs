﻿using System.Collections.Generic;
using OxLet.Core.Plugins.Interfaces;
using OxLet.Plugins.Data.RecentUpdatesPlugin.ModelBinders;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Services;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin {
	public class RecentUpdatesPlugin : IPlugin, ISubscribeable {

		private readonly IRecentUpdateModelBinderRegistry _binderRegistry;
		private readonly IRecentUpdateService _service;

		public RecentUpdatesPlugin(IRecentUpdateModelBinderRegistry binderRegistry, IRecentUpdateService service) {
			_binderRegistry = binderRegistry;
			_service = service;
		}

		public string Description {
			get { return "RecentUpdates plugin collects information about all updates which has occured."; }
		}

		public string Name {
			get { return "RecentUpdatesPlugin"; }
		}

		public bool Enabled { get; set; }
		public IEnumerable<string> GetEvents() {
			return new[] {
				"POST:AFTERADD",
				"POST:AFTERREMOVE",
				"POST:AFTEREDIT"
			};
		}

		public void Fire(string targetEvent, object parameters) {
			var binder = _binderRegistry.GetRecentUpdateModelBinder(targetEvent);
			switch (targetEvent) {
				case "POST:AFTERADD":
					if (binder != null) {
						var recentUpdate = binder.Bind(parameters);
						if (recentUpdate != null) {
							_service.AddRecentUpdate(recentUpdate);
						}
					}
					break;
				case "POST:AFTERREMOVE":
					if (binder != null) {
						var recentUpdate = binder.Bind(parameters);
						if (recentUpdate != null) {
							_service.Remove(recentUpdate);
						}
					}
					break;
				case "POST:AFTEREDIT":
					if (binder != null) {
						var recentUpdate = binder.Bind(parameters);
						if (recentUpdate != null) {
							_service.EditRecentUpdate(recentUpdate);
						}
					}
					break;
			}
		}
	}
}
