﻿using System;
using System.Collections.Generic;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin.Models {
	public class RecentUpdate :EntityBase {

		#region Properties

		public RecentUpdate(int id, DateTime date, string category, int? itemId, string itemValue, EntityState state, UserDescription user, DateTime? publishedDate) : base(id) {
			Date = date;
			Category = category;
			ItemId = itemId;
			ItemValue = itemValue;
			State = state;
			User = user;
			PublishedDate = publishedDate;
		}

		public DateTime Date { get; private set; }
		public string Category { get;private set; }
		public int? ItemId { get; private set; }
		public string ItemValue { get; private set; }
		public EntityState State { get; private set; }
		public UserDescription User { get; private set; }
		public DateTime? PublishedDate { get; private set; }

		#endregion

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			return new[] {this};
		}
	}
}
