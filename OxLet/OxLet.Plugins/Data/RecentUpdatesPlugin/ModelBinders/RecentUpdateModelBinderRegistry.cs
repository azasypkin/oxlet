﻿using System;
using System.Collections.Generic;
using OxLet.Core.Plugins;
using OxLet.Core.Plugins.Interfaces;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin.ModelBinders {
	public class RecentUpdateModelBinderRegistry : IRecentUpdateModelBinderRegistry {

		private readonly Dictionary<string, IPluginParametersBinder<RecentUpdate>> _binders = new Dictionary<string, IPluginParametersBinder<RecentUpdate>>();

		public void Clear() {
			_binders.Clear();
		}

		public void Add(string operation, IPluginParametersBinder<RecentUpdate> binder) {
			_binders[operation.ToUpper()] = binder;
		}

		public void Add(string operation, Func<object, RecentUpdate> lambdaBinder) {
			_binders[operation.ToUpper()] = new LambdaPluginParametersBinder<RecentUpdate>(lambdaBinder);
		}

		public IPluginParametersBinder<RecentUpdate> GetRecentUpdateModelBinder(string operation) {
			if(_binders.ContainsKey(operation.ToUpper())) {
				return _binders[operation.ToUpper()];
			}
			return null;
		}
	}
}
