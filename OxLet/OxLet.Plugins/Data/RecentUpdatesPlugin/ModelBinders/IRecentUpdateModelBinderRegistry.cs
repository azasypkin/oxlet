﻿using System;
using OxLet.Core.Plugins.Interfaces;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Plugins.Data.RecentUpdatesPlugin.ModelBinders {
	public interface IRecentUpdateModelBinderRegistry {
		void Clear();

		void Add(string operation, IPluginParametersBinder<RecentUpdate> binder);

		void Add(string operation, Func<object, RecentUpdate> lambdaBinder);

		IPluginParametersBinder<RecentUpdate> GetRecentUpdateModelBinder(string operation);
	}
}
