﻿using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Filters {
	public class TagStatisticActionFilter : IActionFilter {
		private readonly ITagService _tagService;
		private readonly IBlogService _blogService;

		public TagStatisticActionFilter(ITagService tagService, IBlogService blogService) {
			_tagService = tagService;
			_blogService = blogService;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model == null) {
				return;
			}

			model.AddModelItem(_tagService.GetTagsWithPostCount(_blogService.GetBlogs(new PagingInfo(0,1)).FirstOrDefault()));
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
