﻿using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Configuration;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Filters {
	public class BlogActionFilter : IActionFilter {
		private readonly IBlogService _blogService;
		private readonly OxLetSection _configuration;

		public BlogActionFilter(IBlogService blogService, OxLetSection configuration) {
			_blogService = blogService;
			_configuration = configuration;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model == null) {
				return;
			}

			var blogName = filterContext.RouteData.Values["blogName"] as string;

			var blog = string.IsNullOrEmpty(blogName) 
				? _blogService.GetBlog(_configuration.GetValue<string>("BlogName"))
				: _blogService.GetBlog(blogName);

			model.AddModelItem(blog);
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
