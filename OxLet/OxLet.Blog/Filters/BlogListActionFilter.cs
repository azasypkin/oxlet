﻿using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Blog.ViewModels;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Filters {
	public class BlogListActionFilter : IActionFilter {
		private readonly IBlogService _blogService;

		public BlogListActionFilter(IBlogService blogService) {
			_blogService = blogService;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model == null){ return;
			}

			model.AddModelItem(new BlogListViewModel(_blogService.GetBlogs(new PagingInfo(0, 10))));
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
