﻿using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Blog.ViewModels;
using OxLet.Core.Configuration;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Filters {
	public class ArchiveListActionFilter : IActionFilter {
		private readonly IPostService _postService;
		private readonly IBlogService _blogService;
		private readonly OxLetSection _configuration;

		public ArchiveListActionFilter(IPostService postService, IBlogService blogService, OxLetSection configuration) {
			_postService = postService;
			_blogService = blogService;
			_configuration = configuration;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model == null) {
				return;
			}

			var blogName = filterContext.RouteData.Values["blogName"] as string;

			var blog = string.IsNullOrEmpty(blogName) 
				? _blogService.GetBlog(_configuration.GetValue<string>("BlogName"))
				: _blogService.GetBlog(blogName);

			model.AddModelItem(new ArchiveViewModel(_postService.GetArchives(blog).ToList(), blog));
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
