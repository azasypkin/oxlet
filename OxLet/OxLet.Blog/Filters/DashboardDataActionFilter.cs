﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Blog.Services.Interfaces;
using OxLet.Blog.ViewModels;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;


namespace OxLet.Blog.Filters {
	public class DashboardDataActionFilter : IActionFilter {
		private readonly IPostService _postService;
		private readonly IBlogService _blogService;
		private readonly ICommentService _commentService;
		public DashboardDataActionFilter(IPostService postService, ICommentService commentService, IBlogService blogService) {
			_postService = postService;
			_blogService = blogService;
			_commentService = commentService;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model == null) {
				return;
			}

			//recent posts - all up
			IList<Post> posts = _postService.GetPosts(_blogService.GetBlogs(new PagingInfo(0,1)).First(),new PagingInfo(0, 5));

			//recent comments - all up
			IList<Comment> comments = _commentService.GetComments(new PagingInfo(0, 10), true, true);

			IList<Models.Blog> blogs = _blogService.GetBlogs(new PagingInfo(0, 10)).ToList();

			model.AddModelItem(new AdminDataViewModel(posts, comments, blogs));
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
