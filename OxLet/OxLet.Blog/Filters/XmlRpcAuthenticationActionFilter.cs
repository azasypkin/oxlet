﻿using System.Web.Mvc;
using OxLet.Core.Infrastructure;
using OxLet.Core.Infrastructure.XmlRpc;

namespace OxLet.Blog.Filters {
	public class XmlRpcAuthenticationActionFilter : IAuthorizationFilter {
		private readonly SiteContext _context;
		public XmlRpcAuthenticationActionFilter(SiteContext context) {
			_context = context;
		}

		public void OnAuthorization(AuthorizationContext filterContext) {
			if (!_context.User.IsAuthenticated) {
				filterContext.Result = new XmlRpcFaultResult(2041, "Invalid credentials were provided");
			}
		}
	}
}
