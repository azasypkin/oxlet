﻿using System.Web.Helpers;
using System.Web.Mvc;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Filters {
	public class SearchCriteriaActionFilter : IActionFilter {

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model == null) {
				return;
			}

			model.AddModelItem(new SearchCriteria{Term = filterContext.RequestContext.HttpContext.Request.Unvalidated().QueryString["term"] ?? ""});
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
