﻿using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Filters {
	public class CommentingDisabledActionFilter : IActionFilter {
		private readonly Models.Blog _blog;

		public CommentingDisabledActionFilter(Models.Blog blog) {
			_blog = blog;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var postModel = filterContext.Controller.ViewData.Model as OxletModelItem<Post>;

			//if (postModel != null) {
			//    postModel.CommentingDisabled = _blog.CommentingDisabled
			//        || ((Models.Blog)postModel.Container).CommentingDisabled 
			//        || postModel.Item.CommentingDisabled;
			//}

			//TODO: (erikpo) Once comments are added to pages, add code similar to above to set allow comments for pages
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
