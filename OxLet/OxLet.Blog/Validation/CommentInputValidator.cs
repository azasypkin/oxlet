﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Models.Input;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;

namespace OxLet.Blog.Validation {
	public class CommentInputValidator : ModelValidatorBase<CommentInput> {

		private const string LocalizationStore = "CommentValidationMessages";


		public CommentInputValidator(ILocalizationService localizationService, IRegularExpressionService regularExpressionService) 
			: base(localizationService, regularExpressionService) {
		}

		public override IEnumerable<ModelValidationResult> Validate(CommentInput comment) {
			if (comment == null) {
				throw new ArgumentNullException("comment");
			}

			if (string.IsNullOrEmpty(comment.Body)) {
				yield return new ModelValidationResult {
					MemberName = "Item.Body",
					Message = LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Body_ValidationMessage").Value
				};
			}

			if (comment.CreatorId == 0) {
				if (string.IsNullOrEmpty(comment.Name)) {
					yield return new ModelValidationResult {
						MemberName = "Item.Name",
						Message = LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Name_ValidationMessage").Value
					};
				}

				if (!string.IsNullOrEmpty(comment.Email) && !RegularExpressionService.IsMatch("IsEmail", comment.Email)) {
					yield return new ModelValidationResult {
						MemberName = "Item.Email",
						Message = LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Email_ValidationMessage").Value
					};
				}

				if (!string.IsNullOrEmpty(comment.Url) && !RegularExpressionService.IsMatch("IsUrl", comment.Url)) {
					yield return new ModelValidationResult {
						MemberName = "Item.Url",
						Message = LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_URL_ValidationMessage").Value
					};
				}
			}

		}

		public override IEnumerable<ModelClientValidationRule> GetClientValidationRules(string modelPropertyName) {
			if (modelPropertyName == "Name") {
				return new ModelClientValidationRule[] {
					new ModelClientValidationRequiredRule(
						LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Name_ValidationMessage").Value
					)
				};
			}
			if (modelPropertyName == "Email") {
				var message = LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Email_ValidationMessage").Value;
				return new ModelClientValidationRule[]{
					new ModelClientValidationRegexRule(
						message,
						RegularExpressionService.GetExpression("IsEmail").ToString()
					)
				};
			}
			if (modelPropertyName == "Url") {
				return new ModelClientValidationRule[]{
					new ModelClientValidationRegexRule(
						LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_URL_ValidationMessage").Value,
						RegularExpressionService.GetExpression("IsUrl").ToString()
					)
				};
			}
			if (modelPropertyName == "Body") {
				return new ModelClientValidationRule[] {
					new ModelClientValidationRequiredRule(
						LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Body_ValidationMessage").Value
					)
				};
			}
			return Enumerable.Empty<ModelClientValidationRule>();
		}
	}
}
