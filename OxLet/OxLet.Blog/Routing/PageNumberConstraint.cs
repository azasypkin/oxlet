﻿using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;

namespace OxLet.Blog.Routing {
	public class PageNumberConstraint : IRouteConstraint {
		private readonly Regex _pageNumberRegex = new Regex(
			@"(?:(?<=^|/)Page(?<number>\d+)(?=$|/))?",
			RegexOptions.Compiled | RegexOptions.IgnoreCase
		);

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			var pageNumber = values[parameterName] as string;

			if (!string.IsNullOrEmpty(pageNumber)){
				return _pageNumberRegex.Match(pageNumber).Groups["number"].Success;
			}

			return true;
		}
	}
}
