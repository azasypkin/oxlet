﻿using System.Web;
using System.Web.Routing;
using OxLet.Blog.Models;

namespace OxLet.Blog.Routing {
	public class ArchiveDataConstraint : IRouteConstraint {
		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			return values[parameterName] != null && (new ArchiveData(values[parameterName].ToString())).Year > 0;
		}
	}
}