﻿using System.Web;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Configuration;

namespace OxLet.Blog.Routing {
	public class SlugConstraint : IRouteConstraint {
		private readonly IPostService _postService;
		private readonly IBlogService _blogService;
		private readonly string _currentBlogName;

		public SlugConstraint(IUnityContainer container) {
			_postService = container.Resolve<IPostService>();
			_blogService = container.Resolve<IBlogService>();
			_currentBlogName = container.Resolve<OxLetSection>().GetValue<string>("BlogName");
		}

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			if(_blogService.IsBlogExists(_currentBlogName)) {
				var post = _postService.GetPost(_blogService.GetBlog(_currentBlogName), values[parameterName].ToString());
				return post != null;
			}
			return false;
		}
	}
}
