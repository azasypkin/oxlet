﻿using System;
using System.Web;
using System.Web.Routing;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models;
using OxLet.Core.Models;

namespace OxLet.Blog.Routing {
	public class AbsolutePathHelper {
		private readonly Site _site;
		private readonly RouteCollection _routes;

		public AbsolutePathHelper(Site site, RouteCollection routes) {
			_site = site;
			_routes = routes;
		}

		public string GetAbsolutePath(Post post) {
			if (post == null) {
				throw new ArgumentNullException("post");
			}
			if (string.IsNullOrEmpty(post.Slug) || post.Blog == null || string.IsNullOrEmpty(post.Blog.Name)) {
				throw new ArgumentException();
			}
			var builder = new UriBuilder(_site.Host.Scheme, _site.Host.Host, _site.Host.Port, _site.Host.AbsolutePath) {
				Path = post.GetUrl(new RequestContext(new DummyHttpContext(null, _site.Host), new RouteData()),_routes)
			};

			return builder.Uri.ToString();
		}

		public string GetAbsolutePath(Post post, Comment comment) {
			if (post == null) {
				throw new ArgumentNullException("post");
			}
			if (comment == null) {
				throw new ArgumentNullException("comment");
			}
			if (string.IsNullOrEmpty(post.Slug) || post.Blog == null || string.IsNullOrEmpty(post.Blog.Name)) {
				throw new ArgumentException();
			}
			var builder = new UriBuilder(_site.Host) {
				Path = comment.GetUrl(
					post,
					new RequestContext(new DummyHttpContext(null, _site.Host),new RouteData()), _routes)
			};

			return builder.Uri.ToString();
		}

		public Post GetPostFromUri(Uri permalink) {
			//if (permalink == null){
			//    throw new ArgumentNullException();
			//}

			//if (!permalink.ToString().StartsWith(_site.Host.ToString(), StringComparison.OrdinalIgnoreCase)) {
			//    return null;
			//}
			//RouteData data = _routes["Post"].GetRouteData(new DummyHttpContext(permalink, _site.Host));

			//if (data != null) {
			//    return new Post {Slug = data.GetRequiredString("slug"), Blog = new Models.Blog {Name = data.GetRequiredString("blogName")}};
			//}

			//return null;
			throw new NotImplementedException();
		}

		private class DummyHttpContext : HttpContextBase {
			private readonly Uri _requestUrl;
			private readonly Uri _hostUrl;

			public DummyHttpContext(Uri requestUrl, Uri hostUrl) {
				_requestUrl = requestUrl;
				_hostUrl = hostUrl;
			}

			public override HttpRequestBase Request {
				get { return new DummyHttpRequest(_requestUrl, _hostUrl); }
			}
		}

		private class DummyHttpRequest : HttpRequestBase {
			private readonly Uri _requestUrl;
			private readonly Uri _hostUrl;

			public DummyHttpRequest(Uri requestUrl, Uri hostUrl) {
				_requestUrl = requestUrl;
				_hostUrl = hostUrl;
			}

			public override Uri Url {
				get { return _requestUrl; }
			}

			public override string ApplicationPath {
				get { return _hostUrl.AbsolutePath; }
			}

			public override string AppRelativeCurrentExecutionFilePath {
				get {
					if (_hostUrl.AbsolutePath.Length > 1)
						return "~" + _requestUrl.AbsolutePath.Remove(0, _hostUrl.AbsolutePath.Length);

					return "~" + _requestUrl.AbsolutePath;
				}
			}

			public override string PathInfo {
				get { return ""; }
			}
		}
	}
}
