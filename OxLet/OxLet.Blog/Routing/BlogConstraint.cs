﻿using System.Web;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Blog.Services.Interfaces;

namespace OxLet.Blog.Routing {
	public class BlogConstraint : IRouteConstraint {
		private readonly IUnityContainer _container;

		public BlogConstraint(IUnityContainer container) {
			_container = container;
		}

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			var blogService = _container.Resolve<IBlogService>();
			string blogName = values[parameterName].ToString();

			return blogService.IsBlogExists(blogName);
		}
	}
}
