﻿using System.Linq;
using System.Web;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Configuration;
using OxLet.Core.Models;

namespace OxLet.Blog.Routing {
	public class WindowsLiveWriterTempPostConstraint: IRouteConstraint {

		private readonly IPostService _postService;
		private readonly IBlogService _blogService;
		private readonly string _currentBlogName;

		public WindowsLiveWriterTempPostConstraint(IUnityContainer container) {
			_postService = container.Resolve<IPostService>();
			_blogService = container.Resolve<IBlogService>();
			_currentBlogName = container.Resolve<OxLetSection>().GetValue<string>("BlogName");
		}

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			// check user agent of the WLW, check if there is temp post in the repository 
			if (httpContext.Request.UserAgent != null && httpContext.Request.UserAgent.Contains("Windows Live Writer") && _blogService.IsBlogExists(_currentBlogName)) {
				var post = _postService.GetPosts(
					_blogService.GetBlog(_currentBlogName),
					new SearchCriteria() { Term = "Temporary Post Used For Theme Detection" },
					new PagingInfo(0,1),
					true
				).FirstOrDefault();

				if(post != null) {
					values.Add("slug", post.Slug);
					return true;
				}
			}

			return false;
		}
	}
}
