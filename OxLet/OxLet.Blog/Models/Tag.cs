﻿using System;
using OxLet.Core.Models;

namespace OxLet.Blog.Models {
	public class Tag : NamedEntity {
		public Tag(int id, string name, string displayName, DateTime? created, bool isService) : base(id, name, displayName) {
			Created = created;
			IsService = isService;
		}

		public DateTime? Created { get; private set; }
		public bool IsService { get; private set; }
	}
}
