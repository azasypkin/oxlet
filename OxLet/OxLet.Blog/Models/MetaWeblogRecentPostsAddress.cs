﻿namespace OxLet.Blog.Models {
	public class MetaWeblogRecentPostsAddress {
		public MetaWeblogRecentPostsAddress(Blog blog) {
			Blog = blog;
		}

		public Blog Blog{ get; private set; }
	}
}
