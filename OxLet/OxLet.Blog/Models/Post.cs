﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Syndication;
using OxLet.Blog.Models.Projections;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Web.Syndication;

namespace OxLet.Blog.Models {
	public class Post : EntityBase, INamedEntity {
		public Post(int id, UserDescription creator, string title, string body, string bodyShort, EntityState state, string slug, DateTime? created, DateTime? modified, DateTime? published, IList<Comment> comments, bool commentingDisabled, BlogProjection blog, IList<Tag> tags, Language language) : base(id) {
			Creator = creator;
			Title = title;
			Body = body;
			BodyShort = bodyShort;
			State = state;
			Slug = slug;
			Created = created;
			Modified = modified;
			Published = published;
			Comments = comments ?? new List<Comment>(0);
			CommentingDisabled = commentingDisabled;
			Blog = blog;
			Tags = tags;
			Language = language;
		}

		public UserDescription Creator { get; private set; }
		public string Title { get; private set; }
		public string Body { get; private set; }
		public string BodyShort { get; private set; }
		public EntityState State { get; private set; }
		public string Slug { get; private set; }
		public DateTime? Created { get; private set; }
		public DateTime? Modified { get; private set; }
		public DateTime? Published { get; private set; }
		public IList<Comment> Comments { get; private set; }
		public bool CommentingDisabled { get; private set; }
		public BlogProjection Blog { get; private set; }
		public IList<Tag> Tags { get; private set; }
		public Language Language { get; private set; }

		public string Name {
			get { return Slug; }
		}

		public string DisplayName {
			get { return Title; }
		}

		public void SetComments(IList<Comment> comments) {
			Comments = comments;
		}

		public void SetTags(IList<Tag> tags) {
			Tags = tags;
		}

		public void SetCreator(UserDescription creator) {
			Creator = creator;
		}

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			return new List<ICacheable> {this, Blog};
		}
	}
}
