﻿namespace OxLet.Blog.Models {
	public class MetaWeblogPostAddress {
		public MetaWeblogPostAddress(int postId) {
			PostId = postId;
		}

		public int PostId { get; private set; }
	}
}
