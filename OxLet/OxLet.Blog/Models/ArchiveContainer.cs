﻿using System;
using OxLet.Core.Models;

namespace OxLet.Blog.Models {
	public class ArchiveContainer : NamedEntity {
		public ArchiveData ArchiveData { get; set; }
		public ArchiveContainer(int id, string name, string displayName, ArchiveData archiveData)
			: base(id, name, displayName) {
			ArchiveData = archiveData;
		}
	}
}
