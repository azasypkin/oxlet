﻿using System;
using System.Collections.Generic;
using System.Linq;
using OxLet.Core.Cache.Interfaces;

namespace OxLet.Blog.Models.Projections {
	public class BlogProjection : ICacheable {
		public BlogProjection(int id, bool commentingDisabled, string name) {
			Id = id;
			CommentingDisabled = commentingDisabled;
			Name = name;
		}

		public int Id { get; private set; }

		public bool CommentingDisabled { get; private set; }

		public string Name { get; private set; }

		public virtual string GetCacheItemKey() {
			return string.Format("{0}:{1}", typeof(Blog).Name, Id.ToString());
		}

		public virtual IEnumerable<ICacheable> GetCacheDependencyItems() {
			return Enumerable.Empty<ICacheable>();
		}
	}
}
