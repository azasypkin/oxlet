﻿using System;
using System.Collections.Generic;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Models.Projections;

namespace OxLet.Blog.Models {
	public class Blog : EntityBase, INamedEntity, ICacheable {

		public Blog(int id) : this(id, null) {
		}

		public Blog(int id, string name)
			: this(id, null, name, null, null, EntityState.NotSet, false, 0, null, false, null, null) {
		}

		public Blog(int id, SiteProjection site, string name, string displayName, string description, EntityState commentStateDefault, bool authorAutoSubscribe, short postEditTimeout, string skinDefault, bool commentingDisabled, DateTime? created, DateTime? modified) : base(id) {
			Site = site;
			Description = description;
			CommentStateDefault = commentStateDefault;
			AuthorAutoSubscribe = authorAutoSubscribe;
			PostEditTimeout = postEditTimeout;
			SkinDefault = skinDefault;
			CommentingDisabled = commentingDisabled;
			Created = created;
			Modified = modified;
			Name = name;
			DisplayName = displayName;
		}

		public SiteProjection Site { get; private set; }
		public string Description { get; private set; }
		public EntityState CommentStateDefault { get; private set; }
		public bool AuthorAutoSubscribe { get; private set; }
		public short PostEditTimeout { get; private set; }
		public string SkinDefault { get; private set; }
		public bool CommentingDisabled { get; private set; }
		public DateTime? Created { get; private set; }
		public DateTime? Modified { get; private set; }

		public string Name { get; private set; }

		public string DisplayName { get; private set; }

		IEnumerable<ICacheable> ICacheable.GetCacheDependencyItems() {
			var dependencies = new List<ICacheable> {Site};

			return dependencies;
		}
	}
}
