﻿using OxLet.Core.Models;

namespace OxLet.Blog.Models.Input {
	public class CommentInput {

		public int Id { get; set; }
		public int ParentId { get; set; }
		public string Body { get; set; }
		public bool IsRemember { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string EmailHash { get; set; }
		public string Url { get; set; }
		public long CreatorIp { get; set; }
		public string CreatorUserAgent { get; set; }
		public int Language { get; set; }
		public int CreatorId { get; set; }

		public Comment ToComment(EntityState state, Post post, string slug) {
			return new Comment(
				0,
				new UserDescription(CreatorId){
					Email = Email,
					EmailHash = EmailHash,
					Name = Name,
					Url = Url
				},
				CreatorIp,
				CreatorUserAgent,
				Body,
				state,
				null,
				null,
				post,
				new Language(Language, null, null), 
				slug
			);
		}
	}
}
