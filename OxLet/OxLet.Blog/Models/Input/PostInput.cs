﻿using System.Collections.Generic;
using System;
using System.Linq;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models.Projections;
using OxLet.Core.Models;

namespace OxLet.Blog.Models.Input {
	public class PostInput {
		public IList<string> Tags { get; set; }
		public string Title { get; set; }
		public string Body { get; set; }
		public string BodyShort { get; set; }
		public string Slug { get; set; }
		public DateTime? Published { get; set; }
		public bool CommentingDisabled { get; set; }
		public Language Language { get; set; }

		public Post ToPost(Blog blog, EntityState state, UserDescription creator, Func<string, string> normalizeTag) {
			return new Post(
				0,
				creator,
				Title,
				Body,
				BodyShort,
				state,
				Slug,
				null,
				null,
				Published,
				null,
				CommentingDisabled,
				new BlogProjection(blog.Id, blog.CommentingDisabled, blog.Name),
				Tags.Select(x => new Tag(0, normalizeTag(x), x, Published, x.IsServiceTag())).ToList(),
				Language
			);
		}
	}
}
