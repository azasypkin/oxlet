﻿using System;
using OxLet.Core.Models;
using OxLet.Core.Models.Projections;

namespace OxLet.Blog.Models.Input {
	public class BlogInput {
		public string Name { get; set; }
		public string DisplayName { get; set; }
		public string Description { get; set; }
		public EntityState CommentStateDefault { get; set; }
		public bool AuthorAutoSubscribe { get; set; }
		public short PostEditTimeout { get; set; }
		public string SkinDefault { get; set; }
		public bool CommentingDisabled { get; set; }

		public Blog ToBlog(SiteProjection site, DateTime? created, DateTime? modified) {
			return new Blog(
				0,
				site,
				Name,
				DisplayName,
				Description,
				EntityState.NotSet,
				AuthorAutoSubscribe,
				PostEditTimeout,
				SkinDefault,
				CommentingDisabled,
				created,
				modified
			) ;
		}
	}
}
