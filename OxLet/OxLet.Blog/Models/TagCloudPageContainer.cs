﻿using System;
using OxLet.Core.Models;

namespace OxLet.Blog.Models {
	public class TagCloudPageContainer : NamedEntity {
		public TagCloudPageContainer(int id, string name, string displayName) : base(id, name, displayName) {
		}

		public TagCloudPageContainer() : this(0, null, null) {
		}
	}
}
