﻿using System;
using System.Collections.Generic;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;

namespace OxLet.Blog.Models {
	public class Comment : EntityBase {
		public Comment(int id) : this(id, null, 0, null, null, EntityState.NotSet, null, null, null, null, null) {
		}

		public Comment(int id, UserDescription creator, long creatorIp, string userAgent,
			string body, EntityState state, DateTime? created, DateTime? modified,Post post, Language language, string slug)
			: base(id) {
			Creator = creator;
			CreatorIp = creatorIp;
			CreatorUserAgent = userAgent;
			Body = body;
			State = state;
			Created = created;
			Modified = modified;
			Post = post;
			Language = language;
			Slug = slug;
		}

		public UserDescription Creator { get; private set; }
		public long CreatorIp { get; private set; }
		public string CreatorUserAgent { get; private set; }
		public Language Language { get; private set; }
		public string Body { get; private set; }
		public EntityState State { get; private set; }
		public DateTime? Created { get; private set; }
		public DateTime? Modified { get; private set; }
		public Post Post { get; private set; }
		public string Slug { get; private set; }

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			return new ICacheable[] {this, Post};
		}
	}
}
