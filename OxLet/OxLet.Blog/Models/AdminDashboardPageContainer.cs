﻿using System;
using OxLet.Core.Models;

namespace OxLet.Blog.Models {
	public class AdminDashboardPageContainer : NamedEntity {
		public AdminDashboardPageContainer(int id, string name, string displayName) : base(id, name, displayName) {
		}

		public AdminDashboardPageContainer() : this(0, null, null) {
		}
	}
}
