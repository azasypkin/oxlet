﻿using System;
using System.Collections.Generic;
using OxLet.Blog.Models;
using OxLet.Core.Models;

namespace OxLet.Blog.Repositories.Interfaces {
	public interface IPostRepository {
		IPagedList<Post> GetPosts(Models.Blog blog, Tag tag, PagingInfo pagingInfo);
		IPagedList<Post> GetPosts(Models.Blog blog, Language language, PagingInfo pagingInfo);
		IPagedList<Post> GetPosts(Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo);
		IPagedList<Post> GetPostsWithDrafts(Models.Blog blog, PagingInfo pagingInfo);
		IPagedList<Post> GetPostsWithDrafts(Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo);
		IPagedList<Post> GetPosts(Models.Blog blog, PagingInfo pagingInfo);
		IPagedList<Post> GetPostsByArchive(Models.Blog blog, int year, int month, int day, PagingInfo pagingInfo);

		IEnumerable<DateTime> GetPostDateGroups(Models.Blog blog);

		Post GetPost(Models.Blog blog, string slug);
		Post GetPost(int id);
		Post GetRandomPost(Models.Blog blog);
		Post Save(Post post);

		bool RemovePost(Post post);
		void RemoveAllPosts(Models.Blog blog);

		IEnumerable<KeyValuePair<ArchiveData, int>> GetArchives(Models.Blog blog);
		//IEnumerable<KeyValuePair<ArchiveData, int>> GetArchives(string blogName);

		Tag Save(Post post, Tag tag);
	}
}
