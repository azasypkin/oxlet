﻿using System;
using OxLet.Blog.Models;
using OxLet.Core.Models;

namespace OxLet.Blog.Repositories.Interfaces {
	public interface ICommentRepository {

		/// <summary>
		/// Get a specific comment
		/// </summary>
		/// <param name="blogName"></param>
		/// <param name="postSlug"></param>
		/// <param name="commentSlug"></param>
		/// <returns></returns>
		Comment GetComment(string blogName, string postSlug, string commentSlug);

		/// <summary>
		/// Get a specific comment
		/// </summary>
		/// <returns></returns>
		Comment GetComment(int id);

		/// <summary>
		/// Get all coments for a blog with pendings
		/// </summary>
		/// <param name="pagingInfo"></param>
		/// <param name="includePending"></param>
		/// <param name="sortDescending"></param>
		/// <returns></returns>
		IPagedList<Comment> GetComments(PagingInfo pagingInfo, bool includePending, bool sortDescending);

		/// <summary>
		/// All comment for a blog
		/// </summary>
		/// <param name="pagingInfo"></param>
		/// <param name="blog"></param>
		/// <returns></returns>
		IPagedList<Comment> GetComments(PagingInfo pagingInfo, Models.Blog blog);

		/// <summary>
		/// All comments for a post
		/// </summary>
		/// <param name="pagingInfo"></param>
		/// <param name="post"></param>
		/// <param name="includeUnapproved"></param>
		/// <returns></returns>
		IPagedList<Comment> GetComments(PagingInfo pagingInfo, Post post, bool includeUnapproved);

		/// <summary>
		/// All comments for a tag
		/// </summary>
		/// <param name="pagingInfo"></param>
		/// <param name="tag"></param>
		/// <returns></returns>
		IPagedList<Comment> GetComments(PagingInfo pagingInfo, Tag tag);

		/// <summary>
		/// Save comment
		/// </summary>
		/// <param name="comment"></param>
		/// <param name="blogName"></param>
		/// <param name="postSlug"></param>
		/// <returns></returns>
		Comment Save(Comment comment, string blogName, string postSlug);

		void ChangeState(int commentId, EntityState state);
	}
}
