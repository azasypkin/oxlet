﻿using System;
using System.Collections.Generic;
using System.Linq;
using OxLet.Blog.Models;

namespace OxLet.Blog.Repositories.Interfaces {
	public interface ITagRepository {
		IQueryable<Tag> GetTags(bool includeServiceTags);
		IQueryable<KeyValuePair<Tag, int>> GetTagsWithPostCount(bool includeServiceTags);
		Tag GetTag(string urlName);
		Tag GetTag(int id);

		Tag SaveTag(Tag tag);
	}
}
