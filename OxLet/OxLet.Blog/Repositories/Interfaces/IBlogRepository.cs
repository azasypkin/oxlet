﻿using System;
using OxLet.Core.Models;

namespace OxLet.Blog.Repositories.Interfaces {
	public interface IBlogRepository {
		Models.Blog GetBlog(string name);
		Models.Blog GetBlog(int id);
		Models.Blog Save(Models.Blog blog);
		IPagedList<Models.Blog> GetBlogs(PagingInfo pagingInfo);
	}
}
