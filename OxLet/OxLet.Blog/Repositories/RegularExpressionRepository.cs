﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Blog.Repositories {
	class RegularExpressionRepository:IRegularExpressionRepository {
		public IEnumerable<RegularExpressionDescription> GetRegularExpressions() {
			return new[] {
				new RegularExpressionDescription {
					Name = "IsSlug",
					Expression = "^[a-zа-я0-9-_]+$",
					Options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline
				},
				new RegularExpressionDescription {
					Name = "SlugReplace",
					Expression = "([^a-zа-я0-9-_]?)",
					Options = RegexOptions.IgnoreCase | RegexOptions.Compiled
				},
				new RegularExpressionDescription {
					Name = "IsTag",
					Expression = "^[a-zа-я0-9-_ ]+$",
					Options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline
				},
				new RegularExpressionDescription {
					Name = "TagReplace",
					Expression = "([^a-zа-я0-9:\\[\\]]?)",
					Options = RegexOptions.IgnoreCase | RegexOptions.Compiled
				},
				new RegularExpressionDescription {
					Name = "PostblogName",
					Expression = "[^a-zа-я0-9]",
					Options = RegexOptions.Compiled
				},
			};
		}

		public void SaveRegularExpression(RegularExpressionDescription regularExpressionDescription) {
			throw new NotImplementedException();
		}
	}
}
