﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Blog.Controllers;
using OxLet.Blog.Filters;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Blog.Routing;
using OxLet.Core.Configuration;
using OxLet.Core.Infrastructure.XmlRpc;
using OxLet.Core.Infrastructure.XmlRpc.ModelBinders;
using OxLet.Core.Infrastructure.XmlRpc.Models;
using OxLet.Core.Modules;
using OxLet.Core.Routing;
using Microsoft.Practices.Unity;
using OxLet.Core.Web.Filters;
using OxLet.Core.Web.Filters.Interfaces;
using OxLet.Core.Web.Syndication;

namespace OxLet.Blog.Modules {
	public class OxLetBlogSiteModule : SiteModuleBase {

		public OxLetBlogSiteModule(IUnityContainer container, OxLetSection section)
			: base(container, section) {
		}

		public override void RegisterRoutes(RouteCollection routes) {

			RegisterAdminRoutes(routes);

			RegisterMetaWeblogRoutes(routes);

			RegisterPostRoutes(routes);
		}

		private void RegisterMetaWeblogRoutes(RouteCollection routes) {

			routes.Add(
				"MetaWeblog",
				new Route(
					"{area}/metaweblog",
					new RouteValueDictionary(new Dictionary<string, object> { { "controller", "MetaWeblog" } }),
					new RouteValueDictionary(new Dictionary<string, object> { { "area", AreaName } }),
					new XmlRpcRouteHandler()
				)
			);

			MapRoute(
				routes,
				"Rsd",
				"{area}/metaweblog/rsd",
				new { controller = "MetaWeblog", action = "Rsd" },
				new { area = AreaName }
			);

			MapRoute(
				routes,
				"BlogRsd",
				"{area}/metaweblog/{blogName}/rsd",
				new { controller = "MetaWeblog", action = "Rsd" },
				new { area = AreaName, blogName = new BlogConstraint(Container) }
			);

			MapRoute(
				routes,
				"LiveWriterManifest",
				"livewritermanifest.xml",
				new { area = AreaName, controller = "MetaWeblog", action = "LiveWriterManifest" }
			);

			MapRoute(
				routes,
				"LiveWriterManifestRoot",
				"wlwmanifest.xml",
				new { area = AreaName, controller = "MetaWeblog", action = "LiveWriterManifest" }
			);

			// Register special route to display temporal post for WLW
			// to update post theme
			MapRoute(
				routes,
				"WindowsLiveWriterTempPost",
				"{area}",
				new { area = AreaName, controller = "Post", action = "Item" },
				new { area = AreaName, wlwAgent = new WindowsLiveWriterTempPostConstraint(Container) }
			);
		}

		private void RegisterAdminRoutes(RouteCollection routes) {
			MapRoute(
				routes,
				"Admin",
				"{area}/admin",
				new { area = AreaName, controller = "Blog", action = "Dashboard" },
				new { area = AreaName },
				true
			);

			MapRoute(
				routes,
				"ManageBlog",
				"{area}/admin/",
				new { area = AreaName, controller = "Blog", action = "Dashboard" },
				new { area = AreaName, routeDirection = new RouteDirectionConstraint(RouteDirection.UrlGeneration) },
				true
			);

			MapRoute(
				routes,
				"Blog",
				"{area}/admin/setup/{blogName}",
				new { area = AreaName, controller = "Blog", action = "ItemEdit", validateAntiForgeryToken = true },
				new { blogName = new BlogConstraint(Container) },
				true
			);

			MapRoute(
				routes,
				"AddPostToSite",
				"{area}/admin/addpost",
				new { area = AreaName, controller = "Post", action = "ItemAdd", validateAntiForgeryToken = true },
				new { area = AreaName },
				true
			);

			MapRoute(
				routes,
				"AllComments",
				"{area}/admin/comments/{pageNumber}",
				new { area = AreaName, controller = "Comment", action = "ListForAdmin", pageNumber = "" },
				new { area = AreaName, pageNumber = new PageNumberConstraint()},
				true
			);

			MapRoute(
				routes,
				"AllCommentsPermalink",
				"{area}/admin/comments",
				new { area = AreaName },
				new { area = AreaName, routeDirection = new RouteDirectionConstraint(RouteDirection.UrlGeneration) },
				true
			);

			MapRoute(
				routes,
				"RemoveComment",
				"{area}/admin/{blogName}/{slug}/removeComment",
				new { area = AreaName, controller = "Comment", action = "Remove", validateAntiForgeryToken = true },
				new { area= AreaName, blogName = new BlogConstraint(Container), httpMethod = new HttpMethodConstraint("POST") }
			);

			MapRoute(
				routes,
				"ApproveComment",
				"{area}/admin/{blogName}/{slug}/approve",
				new { area = AreaName, controller = "Comment", action = "Approve", validateAntiForgeryToken = true },
				new { area = AreaName, blogName = new BlogConstraint(Container), httpMethod = new HttpMethodConstraint("POST") }
			);

			MapRoute(
				routes,
				"AddPostToBlog",
				"{area}/admin/{blogName}/add",
				new { area = AreaName, controller = "Post", action = "ItemAdd", validateAntiForgeryToken = true },
				new { area = AreaName, blogName = new BlogConstraint(Container) },
				true
			);

			MapRoute(
				routes,
				"EditPost",
				"{area}/admin/{blogName}/{slug}/edit",
				new { area = AreaName, controller = "Post", action = "ItemEdit", validateAntiForgeryToken = true },
				new { area = AreaName, blogName = new BlogConstraint(Container) },
				true
			);

			MapRoute(
				routes,
				"RemovePost",
				"{area}/admin/{blogName}/{slug}/remove",
				new { area = AreaName, controller = "Post", action = "Remove", validateAntiForgeryToken = true },
				new { area = AreaName, blogName = new BlogConstraint(Container), httpMethod = new HttpMethodConstraint("POST") }
			);

			MapRoute(
				routes,
				"PostsWithDrafts",
				"{area}/admin/posts",
				new { area = AreaName, controller = "Post", action = "ListWithDrafts" },
				new { area = AreaName },
				true
			);

			// resource controller

			MapRoute(
				routes,
				"ForwardAreaResources",
				"areas/{area}/skins/{skin}/{type}/{*path}",
				new { area = AreaName, controller = "ResourceForwarder", action = "Forward" },
				new { area = AreaName, skin = new SkinConstraint(Container), type = "(Scripts|Styles|Images)", httpMethod = new HttpMethodConstraint("GET") }
			);

		}

		private void RegisterPostRoutes(RouteCollection routes) {

			MapRoute(
				routes,
				"PostsByArchive",
				"{area}/archive/{*archiveData}",
				new { area = AreaName, controller = "Post", action = "ListByArchive" },
				new { area = AreaName, archiveData = new ArchiveDataConstraint() },
				true
			);

			MapRoute(
				routes,
				"PostsBySearch",
				"{area}/search",
				new { area = AreaName, controller = "Post", action = "ListBySearch"},
				new { area = AreaName},
				true
			);

			MapRoute(
				routes,
				"PostsByTag",
				"{area}/tags/{tagName}/{dataFormat}",
				new { area = AreaName, controller = "Post", action = "ListByTag", dataFormat = UrlParameter.Optional },
				new { area = AreaName, dataFormat = "(|rss|atom)" },
				true
			);

			MapRoute(
				routes,
				"PostsByLanguage",
				"{area}/language/{langName}",
				new { area = AreaName, controller = "Post", action = "ListByLanguage", langName = UrlParameter.Optional},
				new { area = AreaName, langName = new LanguageConstraint(Container)},
				true
			);

			MapRoute(
				routes,
				"CommentsByTag",
				"{area}/tags/{tagName}/comments/{dataFormat}",
				new { area = AreaName, controller = "Comment", action = "ListByTag", dataFormat = UrlParameter.Optional },
				new { area = AreaName, dataFormat = "(|rss|atom)" },
				true
			);

			MapRoute(
				routes,
				"PageOfPosts",
				"{area}/page{pageNumber}",
				new { area = AreaName, controller = "Post", action = "List" },
				new { area = AreaName, pageNumber = new IsIntegerConstraint() },
				true
			);

			MapRoute(
				routes,
				"PostCommentPermalink",
				"{area}/{slug}",
				new { area = AreaName },
				new { area = AreaName, slug = new SlugConstraint(Container), routeDirection = new RouteDirectionConstraint(RouteDirection.UrlGeneration) },
				true
			);

			MapRoute(
				routes,
				"AddCommentToPost",
				"{area}/{slug}",
				new { area = AreaName, controller = "Post", action = "Item" },
				new { area = AreaName, slug = new SlugConstraint(Container), httpMethod = new HttpMethodConstraint("POST") }
			);

			MapRoute(
				routes,
				"Post",
				"{area}/{slug}/{dataFormat}",
				new { area = AreaName, controller = "Post", action = "Item", dataFormat = UrlParameter.Optional },
				new { area = AreaName, slug = new SlugConstraint(Container) },
				true
			);

			//MapRoute(
			//    routes,
			//    "Post",
			//    "{area}/{slug}/{dataFormat}",
			//    new { controller = "Post", action = "Item", dataFormat = "" },
			//    new {area = AreaName}
			//);

			

			MapRoute(
				routes,
				"PostsByBlog",
				"{area}/{blogName}",
				new { area = AreaName, controller = "Post", action = "List", dataFormat = "" },
				new { area = AreaName, blogName = new BlogConstraint(Container) },
				true
			);


			MapRoute(
				routes,
				"Posts",
				"{area}/{dataFormat}",
				new { controller = "Post", action = "List", dataFormat=UrlParameter.Optional },
				new { area = AreaName, dataFormat = "(|rss|atom)" },
				true
			);

			MapRoute(
				routes,
				"PostsForGeneration",
				AreaName,
				new { controller = "Post", action = "List", routeDirection = new RouteDirectionConstraint(RouteDirection.UrlGeneration) },
				null,
				true
			);

			//redirect redirect root

			MapRoute(
				routes,
				"PostsByArchiveEmpty",
				"{area}/archive/",
				new { area = AreaName, controller = "Utility", action = "RedirectToRoot", rootName = "Posts" },
				new { area = AreaName },
				true
			);

			MapRoute(
				routes,
				"PostsByTagsEmpty",
				"{area}/tags/",
				new { area = AreaName, controller = "Utility", action = "RedirectToRoot", rootName = "Posts" },
				new { area = AreaName },
				true
			);

			MapRoute(
				routes,
				"PostsEmpty",
				"",
				new { area = AreaName, controller = "Utility", action = "RedirectToRoot", rootName = "Posts" },
				new { area = AreaName },
				true
			);
		}

		public override void RegisterActionFilters(IActionFilterRegistry registry) {

			registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(BlogActionFilter));

			var listActionsCriteria = new ControllerActionCriteria();
			
			listActionsCriteria.AddMethod<BlogController>(a => a.Find());
			listActionsCriteria.AddMethod<BlogController>(a => a.FindQuery(null));
			listActionsCriteria.AddMethod<CommentController>(c => c.List(null));
			listActionsCriteria.AddMethod<PostController>(p => p.List(null, null, 0, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListByArchive(null, 0, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListBySearch(null, null, 0, null, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListByLanguage(null, null, 0, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListByTag(null, null, 0, null, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListWithDrafts(null, null, 0));
			registry.Add(new[] { listActionsCriteria }, typeof(ArchiveListActionFilter));
			registry.Add(new[] { listActionsCriteria }, typeof(PageSizeActionFilter));

			var itemActionCriteria = new ControllerActionCriteria();
			itemActionCriteria.AddMethod<PostController>(p => p.Item(null, null));
			itemActionCriteria.AddMethod<PostController>(p => p.AddComment(null, null, null));
			registry.Add(new[] { itemActionCriteria }, typeof(CommentingDisabledActionFilter));

			var tagCloudActionCriteria = new ControllerActionCriteria();
			tagCloudActionCriteria.AddMethod<BlogController>(a => a.Dashboard());
			tagCloudActionCriteria.AddMethod<BlogController>(a => a.ItemEdit(null));
			tagCloudActionCriteria.AddMethod<PostController>(p => p.Item(null, null));
			tagCloudActionCriteria.AddMethod<TagController>(t => t.Cloud(null));
			registry.Add(new[] { tagCloudActionCriteria }, typeof(ArchiveListActionFilter));

			var areaListActionCriteria = new ControllerActionCriteria();
			areaListActionCriteria.AddMethod<PostController>(p => p.Add(null, null));
			areaListActionCriteria.AddMethod<PostController>(p => p.SaveAdd(null, null, null));
			areaListActionCriteria.AddMethod<PostController>(p => p.Edit(null, null));
			areaListActionCriteria.AddMethod<PostController>(p => p.SaveEdit(null, null, null));
			registry.Add(new[] { areaListActionCriteria }, typeof(BlogListActionFilter));

			var tagStatisticActionCriteria = new ControllerActionCriteria();
			tagStatisticActionCriteria.AddMethod<BlogController>(a => a.Dashboard());
			tagStatisticActionCriteria.AddMethod<BlogController>(a => a.ItemEdit(null));
			tagStatisticActionCriteria.AddMethod<PostController>(p => p.Item(null, null));
			tagStatisticActionCriteria.AddMethod<PostController>(p => p.List(null, null, 0, null));
			tagStatisticActionCriteria.AddMethod<PostController>(p => p.ListByArchive(null, 0, null));
			tagStatisticActionCriteria.AddMethod<PostController>(p => p.ListByTag(null, null, 0, null, null));
			tagStatisticActionCriteria.AddMethod<PostController>(p => p.ListByLanguage(null, null, 0, null));
			tagStatisticActionCriteria.AddMethod<PostController>(p => p.Item(null, null));
			tagStatisticActionCriteria.AddMethod<PostController>(p => p.AddComment(null, null, null));
			registry.Add(new[] { tagStatisticActionCriteria }, typeof(TagStatisticActionFilter));

			//var pageListActionCriteria = new ControllerActionCriteria();
			//pageListActionCriteria.AddMethod<PostPageController>(p => p.Add(null));
			//pageListActionCriteria.AddMethod<PostPageController>(p => p.SaveAdd(null, null, null));
			//pageListActionCriteria.AddMethod<PostPageController>(p => p.Edit(null));
			//pageListActionCriteria.AddMethod<PostPageController>(p => p.SaveEdit(null, null, null));
			//registry.Add(new[] { pageListActionCriteria }, typeof(PageListActionFilter));

			var adminActionsCriteria = new ControllerActionCriteria();
			adminActionsCriteria.AddMethod<BlogController>(a => a.Find());
			adminActionsCriteria.AddMethod<BlogController>(a => a.FindQuery(null));
			adminActionsCriteria.AddMethod<BlogController>(a => a.ItemEdit(null));
			//adminActionsCriteria.AddMethod<BlogController>(a => a.ItemSave(null, null));
			adminActionsCriteria.AddMethod<BlogController>(a => a.Dashboard());
			//adminActionsCriteria.AddMethod<CommentController>(c => c.List(0, 0));
			adminActionsCriteria.AddMethod<CommentController>(c => c.Remove(null, null, null));
			adminActionsCriteria.AddMethod<CommentController>(c => c.Approve(null, null, null));
			adminActionsCriteria.AddMethod<PostController>(p => p.Add(null, null));
			adminActionsCriteria.AddMethod<PostController>(p => p.SaveAdd(null, null, null));
			adminActionsCriteria.AddMethod<PostController>(p => p.Edit(null, null));
			adminActionsCriteria.AddMethod<PostController>(p => p.SaveEdit(null, null, null));
			adminActionsCriteria.AddMethod<PostController>(p => p.Remove(null, null, null));
			adminActionsCriteria.AddMethod<PostController>(p => p.ListWithDrafts(null, null, 0));
			//adminActionsCriteria.AddMethod<PostController>(p => p.List(null, null, 0, null));
			registry.Add(new[] { adminActionsCriteria }, typeof(AuthorizationFilter));

			var dashboardDataActionCriteria = new ControllerActionCriteria();
			dashboardDataActionCriteria.AddMethod<BlogController>(s => s.Dashboard());
			registry.Add(new[] { dashboardDataActionCriteria }, typeof(DashboardDataActionFilter));

			var authCriteria = new ControllerActionCriteria();
			authCriteria.AddMethod<MetaWeblogController>(m => m.NewPost(null, null));
			authCriteria.AddMethod<MetaWeblogController>(m => m.NewMediaObject(null, null));
			authCriteria.AddMethod<MetaWeblogController>(m => m.GetCategories(null));
			authCriteria.AddMethod<MetaWeblogController>(m => m.GetRecentPosts(null));
			authCriteria.AddMethod<MetaWeblogController>(m => m.GetUsersBlogs(null));
			authCriteria.AddMethod<MetaWeblogController>(m => m.DeletePost(null));
			authCriteria.AddMethod<MetaWeblogController>(m => m.EditPost(null));
			authCriteria.AddMethod<MetaWeblogController>(m => m.GetPost(null));
			registry.Add(new[] { authCriteria }, typeof(XmlRpcAuthenticationActionFilter));

			var searchActionsCriteria = new ControllerActionCriteria();
			searchActionsCriteria.AddMethod<PostController>(a => a.ListBySearch(null, null, 0, null, null));
			registry.Add(new[] { searchActionsCriteria }, typeof(SearchCriteriaActionFilter));

			registry.Add(new[] { new DataFormatFilterCriteria("RSS") }, typeof(RssResultActionFilter));
			registry.Add(new[] { new DataFormatFilterCriteria("Atom") }, typeof(AtomResultActionFilter));
		}

		public override void RegisterModelBinders(ModelBinderDictionary binders) {
			binders[typeof(ArchiveData)] = Container.Resolve<IModelBinder>("ArchiveDataModelBinder");
			binders[typeof(Models.Blog)] = Container.Resolve<IModelBinder>("BlogModelBinder");
			binders[typeof(BlogInput)] = Container.Resolve<IModelBinder>("BlogInputModelBinder");
			binders[typeof(Post)] = Container.Resolve<IModelBinder>("PostModelBinder");
			binders[typeof(PostInput)] = Container.Resolve<IModelBinder>("PostInputModelBinder");
			//binders[typeof(PostPage)] = Container.Resolve<IModelBinder>("PostPageModelBinder");
			binders[typeof(Comment)] = Container.Resolve<IModelBinder>("CommentModelBinder");
			binders[typeof(CommentInput)] = Container.Resolve<IModelBinder>("CommentInputModelBinder");
			binders[typeof(Tag)] = Container.Resolve<IModelBinder>("TagModelBinder");
			binders[typeof(BlogSearchCriteria)] = Container.Resolve<IModelBinder>("BlogSearchCriteriaModelBinder");

			//Metaweblog
			binders[typeof(XmlRpcGetUserBlogs)] = new XmlRpcRequestModelBinder<XmlRpcGetUserBlogs>();
			binders[typeof(XmlRpcGetCategories)] = new XmlRpcRequestModelBinder<XmlRpcGetCategories>();
			binders[typeof(XmlRpcNewPost)] = new XmlRpcRequestModelBinder<XmlRpcNewPost>();
			binders[typeof(XmlRpcNewMediaObject)] = new XmlRpcRequestModelBinder<XmlRpcNewMediaObject>();
			binders[typeof(XmlRpcGetRecentPosts)] = new XmlRpcRequestModelBinder<XmlRpcGetRecentPosts>();
			binders[typeof(XmlRpcGetPost)] = new XmlRpcRequestModelBinder<XmlRpcGetPost>();
			binders[typeof(XmlRpcEditPost)] = new XmlRpcRequestModelBinder<XmlRpcEditPost>();
			binders[typeof(XmlRpcDeletePost)] = new XmlRpcRequestModelBinder<XmlRpcDeletePost>();
		}

		public override string CongfigurationName {
			get { return "OxLetBlogSiteModule"; }
		}

		public override void RegisterInContainer(IUnityContainer container) {
			var repository = container.Resolve<IBlogRepository>();
			container.RegisterInstance(repository.GetBlog(Configuration.GetValue<string>("BlogName")));
		}
	}
}
