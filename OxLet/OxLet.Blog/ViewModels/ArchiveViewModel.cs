﻿using System.Collections.Generic;
using OxLet.Blog.Models;
using OxLet.Core.Models;

namespace OxLet.Blog.ViewModels {
	public class ArchiveViewModel {
		public ArchiveViewModel(IList<KeyValuePair<ArchiveData, int>> archives, INamedEntity container) {
			Archives = archives;
			Container = container;
		}

		public IList<KeyValuePair<ArchiveData, int>> Archives { get; private set; }
		public INamedEntity Container { get; private set; }
	}
}
