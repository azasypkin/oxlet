﻿using System.Collections.Generic;

namespace OxLet.Blog.ViewModels {
	public class BlogListViewModel {
		public BlogListViewModel(IEnumerable<Models.Blog> blogs) {
			Blogs = blogs;
		}

		public IEnumerable<Models.Blog> Blogs { get; private set; }
	}
}
