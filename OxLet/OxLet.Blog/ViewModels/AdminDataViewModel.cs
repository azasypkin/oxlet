﻿using System.Collections.Generic;
using OxLet.Blog.Models;

namespace OxLet.Blog.ViewModels {
	public class AdminDataViewModel {
		public AdminDataViewModel(IList<Post> posts, IList<Comment> comments, IList<Models.Blog> blogs) {
			Posts = posts;
			Comments = comments;
			Blogs = blogs;
		}

		public IList<Post> Posts { get; private set; }
		public IList<Comment> Comments { get; private set; }
		public IList<Models.Blog> Blogs { get; private set; }
	}
}
