﻿using System;

namespace OxLet.Blog.ViewModels {
	public class BlogViewModel {
		public BlogViewModel(Models.Blog currentBlog) {
			Id = currentBlog.Id;
			DisplayName = currentBlog.DisplayName ?? "My OxLet Blog";
			Description = currentBlog.Description;
			SkinDefault = currentBlog.SkinDefault ?? "Default";
			CommentingDisabled = currentBlog.CommentingDisabled;
			PostEditTimeout = currentBlog.PostEditTimeout;
		}

		internal int Id { get; private set; }
		public string DisplayName { get; private set; }
		public string Description { get; private set; }
		public short PostEditTimeout { get; set; }
		public string SkinDefault { get; set; }
		public bool HasMultipleAreas { get; set; }
		public bool CommentingDisabled { get; set; }
	}
}