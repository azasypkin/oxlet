﻿using System.Text.RegularExpressions;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Blog.Extensions {
	public static class IRegularExpressionServiceExtensions {
		public static string Slugify(this IRegularExpressionService expressionService, string value) {
			string slug = "";

			if (!string.IsNullOrEmpty(value)) {
				Regex regex = expressionService.GetExpression("SlugReplace");

				slug = value.Trim();
				slug = slug.Replace(' ', '-');
				slug = slug.Replace("---", "-");
				slug = slug.Replace("--", "-");
				if (regex != null) {
					slug = regex.Replace(slug, "");
				}

				if (slug.Length*2 < value.Length) {
					return "";
				}

				if (slug.Length > 100) {
					slug = slug.Substring(0, 100);
				}
			}

			return slug;
		}
	}
}
