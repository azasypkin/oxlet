﻿using System.Web.Mvc;
using OxLet.Blog.Infrastructure;
using OxLet.Blog.Models;
using OxLet.Core.Models;
using OxLet.Core.Routing;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.Extensions {
	public static class UrlHelperExtensions {

		public static string Blog(this UrlHelper urlHelper, Models.Blog blog) {
			return urlHelper.RouteOxiLetUrl("Blog", new {blogName = blog.Name.ToLower()});
		}

		public static string BlogHome(this UrlHelper urlHelper) {
			return urlHelper.Posts();
		}

		public static string Container(this UrlHelper urlHelper, INamedEntity entity) {
			return new PostVisitor(urlHelper).Visit<string>(entity);
		}

		public static string Container(this UrlHelper urlHelper, INamedEntity entity, string dataFormat) {
			return new PostVisitor(urlHelper).Visit<string>(entity, dataFormat);
		}

		public static string ContainerComments(this UrlHelper urlHelper, INamedEntity entity, string dataFormat) {
			return new CommentVisitor(urlHelper).Visit<string>(entity, dataFormat);
		}

		public static string Posts(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("PostsForGeneration");
		}

		public static string PostsByBlog(this UrlHelper urlHelper, string blogArea) {
			return urlHelper.RouteOxiLetUrl("Posts", new {area = blogArea.ToLower()});
		}

		public static string PostsWithDrafts(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("PostsWithDrafts");
		}

		//public static string Posts(this UrlHelper urlHelper, string dataFormat) {
		//    return urlHelper.RouteUrl("Posts", new {dataFormat});
		//}

		public static string Posts(this UrlHelper urlHelper, string blogName) {
			return urlHelper.RouteCollection.GetUrl(urlHelper.RequestContext, "PostsByBlog", new {blogName = blogName.ToLower()});
		}

		public static string Posts(this UrlHelper urlHelper, Models.Blog blog, string dataFormat) {
			return urlHelper.RouteOxiLetUrl("PostsByArea", new {blogName = blog.Name.ToLower(), dataFormat});
		}

		public static string Tags(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("Tags");
		}

		public static string Posts(this UrlHelper urlHelper, Tag tag) {
			//return tag.GetUrl(urlHelper.RequestContext, urlHelper.RouteCollection);
			return urlHelper.RouteOxiLetUrl("PostsByTag", new { tagName = tag.Name.ToLower()});
		}

		public static string Posts(this UrlHelper urlHelper, Tag tag, string dataFormat) {
			return urlHelper.RouteOxiLetUrl("PostsByTag", new {tagName = tag.Name.ToLower(), dataFormat});
		}

		public static string Posts(this UrlHelper urlHelper, int year) {
			return urlHelper.RouteOxiLetUrl("PostsByArchive", new {archiveData = string.Format("{0}", year)});
		}

		public static string Posts(this UrlHelper urlHelper, int year, int month, int day) {
			return urlHelper.RouteOxiLetUrl("PostsByArchive", new {archiveData = string.Format("{0}/{1}/{2}", year, month, day)});
		}

		public static string Posts(this UrlHelper urlHelper, int year, int month) {
			return urlHelper.RouteOxiLetUrl("PostsByArchive", new {archiveData = string.Format("{0}/{1}", year, month)});
		}

		public static string Post(this UrlHelper urlHelper, Post post) {
			return urlHelper.RouteOxiLetUrl("Post", new {slug = post.Slug.ToLower(), dataFormat = "" });
			//return post.GetUrl(urlHelper.RequestContext, urlHelper.RouteCollection);
		}

		public static string AddPost(this UrlHelper urlHelper, Models.Blog blog) {
			return blog != null
				? urlHelper.RouteOxiLetUrl("AddPostToBlog", new {blogName = blog.Name.ToLower()})
				: urlHelper.RouteOxiLetUrl("AddPostToSite");
		}

		public static string EditPost(this UrlHelper urlHelper, Post post) {
			return urlHelper.RouteOxiLetUrl("EditPost", new { blogName = post.Blog.Name.ToLower(), slug = post.Slug.ToLower() });
		}

		public static string RemovePost(this UrlHelper urlHelper, Post post) {
			return urlHelper.RouteUrl("RemovePost", new {blogName = post.Blog.Name.ToLower(), slug = post.Slug.ToLower()});
		}

		public static string Comments(this UrlHelper urlHelper, string dataFormat) {
			return urlHelper.RouteOxiLetUrl("Comments", new {dataFormat});
		}

		public static string Comments(this UrlHelper urlHelper, Post post, string dataFormat) {
			return urlHelper.RouteOxiLetUrl("CommentsByPost", new {blogName = post.Blog.Name.ToLower(), slug = post.Slug.ToLower(), dataFormat});
		}

		public static string Comments(this UrlHelper urlHelper, Models.Blog blog, string dataFormat) {
			return urlHelper.RouteOxiLetUrl("CommentsByArea", new {blogName = blog.Name.ToLower(), dataFormat});
		}

		public static string Comments(this UrlHelper urlHelper, Tag tag, string dataFormat) {
			return urlHelper.RouteOxiLetUrl("CommentsByTag", new {tagName = tag.Name.ToLower(), dataFormat});
		}

		public static string Comment(this UrlHelper urlHelper, Post post, Comment comment) {
			//return comment.GetUrl(post, urlHelper.RequestContext, urlHelper.RouteCollection);
			return urlHelper.RouteOxiLetUrl("PostCommentPermalink", new { slug = post.Slug.ToLower() }) + "#" + comment.GetSlug().ToLower();
		}

		public static string CommentPending(this UrlHelper urlHelper, Post post, Comment comment) {
			//return comment.GetPendingUrl(post, urlHelper.RequestContext, urlHelper.RouteCollection);
			//todo: (nheskew) really want PostCommentForm w/ a query string inserted but that's not going to happen in the near term so hacking together the URL
			return string.Format(
				"{0}#comment",
				urlHelper.RouteOxiLetUrl("Post", new {slug = post.Slug.ToLower(), pending = bool.TrueString })
			);
		}

		public static string ManageComments(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("AllComments");
		}

		public static string ManageComment(this UrlHelper urlHelper, Comment comment) {
			return urlHelper.RouteOxiLetUrl("AllCommentsPermalink") + "#" + comment.GetSlug().ToLower();
		}

		public static string AddCommentToPost(this UrlHelper urlHelper, Post post) {
			return urlHelper.RouteUrl("AddCommentToPost", new {slug = post.Slug.ToLower()});
		}

		public static string RemoveComment(this UrlHelper urlHelper, Post post) {
			return urlHelper.RouteUrl("RemoveComment", new {blogName = post.Blog.Name.ToLower(), slug = post.Slug.ToLower()});
		}

		public static string ApproveComment(this UrlHelper urlHelper, Post post) {
			return urlHelper.RouteUrl("ApproveComment", new {blogName = post.Blog.Name.ToLower(), slug = post.Slug.ToLower()});
		}

		public static string PostsByLanguage(this UrlHelper urlHelper, string languageName) {
			return urlHelper.RouteOxiLetUrl("PostsByLanguage", new {langName = languageName});
		}
		public static string PostsByLanguage(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("PostsByLanguage");
		}

		public static string PostSearch(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("PostsBySearch");
		}

		public static string PostSearch(this UrlHelper urlHelper, string term) {
			return urlHelper.RouteOxiLetUrl("PostsBySearch", new {term = term.ToLower()});
		}

		public static string PostSearch(this UrlHelper urlHelper, string dataFormat, string term) {
			return urlHelper.RouteOxiLetUrl("PostsBySearch", new {dataFormat, term = term.ToLower()});
		}

		public static string Rsd(this UrlHelper urlHelper) {
			return urlHelper.Rsd((Models.Blog)null);
		}

		public static string Rsd(this UrlHelper urlHelper, Models.Blog blog) {
			if (blog != null)
				return urlHelper.Rsd(blog.Name);

			return urlHelper.Rsd((string) null);
		}

		public static string Rsd(this UrlHelper urlHelper, string blogName) {
			if (!string.IsNullOrEmpty(blogName)){
				return urlHelper.RouteUrl("AreaRsd", new { blogName = blogName.ToLower()});
			}

			return urlHelper.RouteUrl("Rsd");
		}

		public static string ManageBlogs(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("ManageAreas");
		}

		public static string BlogFind(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("AreaFind");
		}

		public static string BlogAdd(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("AreaAdd");
		}

		public static string BlogEdit(this UrlHelper urlHelper, Models.Blog blog) {
			return urlHelper.RouteOxiLetUrl("AreaEdit", new {areaID = blog.Id});
		}

		public static bool IsPagePath(this UrlHelper urlHelper, string pagePath) {
			var controller = urlHelper.RequestContext.RouteData.Values["controller"] as string;
			var pagePathValue = urlHelper.RequestContext.RouteData.Values["pagePath"] as string;

			return
				!string.IsNullOrEmpty(controller) &&
				string.Compare(controller, "Page", true) == 0 &&
				!string.IsNullOrEmpty(pagePath) &&
				string.Compare(pagePathValue, pagePath, true) == 0;
		}

		public static string MetaWeblog(this UrlHelper urlHelper, string blogArea) {
			return urlHelper.AppPath(string.Format("/{0}/MetaWeblog", blogArea.ToLower()));
		}
	}
}
