﻿using System.Web.Routing;
using OxLet.Blog.Models.Input;
using OxLet.Core.Routing;

namespace OxLet.Blog.Extensions {
	public static class BlogExtensions {
		public static string GetUrl(this Models.Blog blog, RequestContext context, RouteCollection routes) {
			return routes.GetUrl(context, "PostsByBlog", new {blogName = blog.Name});
		}

		public static Models.Blog Apply(this Models.Blog blog, BlogInput input) {

			return new Models.Blog(
				blog.Id,
				blog.Site,
				input.Name,
				input.DisplayName,
				input.Description,
				input.CommentStateDefault,
				input.AuthorAutoSubscribe,
				input.PostEditTimeout,
				input.SkinDefault,
				input.CommentingDisabled,
				blog.Created,
				blog.Modified
			);
		}
	}
}
