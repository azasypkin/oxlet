﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Blog.Models;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Extensions {
	public static class HtmlHelperExtensions {

		#region Pager

		public static string PostListPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, string nextTemplate, string previousTemplate) {
			return PostListPager(htmlHelper, pageOfAList, null, previousTemplate, nextTemplate, false);
		}

		public static string PostListPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, object values, string previousText, string nextText, bool alwaysShowPreviousAndNext) {
			return htmlHelper.SimplePager(pageOfAList, "PageOfPosts", values, previousText, nextText, alwaysShowPreviousAndNext);
		}

		public static string PostListByTagPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList,
			string previousText, string nextText, string tagName) {
			return PostListByTagPager(htmlHelper, pageOfAList, new {tagName}, nextText, previousText, false);
		}

		public static string PostListByTagPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, object values, string previousText, string nextText, bool alwaysShowPreviousAndNext) {
			return htmlHelper.SimplePager(pageOfAList, "PageOfPostsByTag", values, previousText, nextText, alwaysShowPreviousAndNext);
		}

		public static string PostListBySearchPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, string previousText, string nextText) {
			return PostListBySearchPager(htmlHelper, pageOfAList, null, previousText, nextText, false);
		}

		public static string PostListBySearchPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, object values,
			string previousText, string nextText, bool alwaysShowPreviousAndNext) {

				return htmlHelper.SimplePager(
					pageOfAList,
					"PageOfPostsBySearch",
					values,
					previousText,
					nextText,
					alwaysShowPreviousAndNext
				);
		}

		public static string CommentListPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList,
			string previousText, string nextText) {
			return CommentListPager(htmlHelper, pageOfAList, null, previousText,
			                        nextText, false);
		}

		public static string CommentListPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, object values, string previousText, string nextText, bool alwaysShowPreviousAndNext) {
			return htmlHelper.SimplePager(
				pageOfAList,
				"PageOfAllComments",
				values,
				previousText,
				nextText,
				alwaysShowPreviousAndNext
			);
		}

		public static string PostArchiveListPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, string previousText, string nextText) {
			return PostArchiveListPager(htmlHelper, pageOfAList, null, previousText, nextText, false);
		}

		public static string PostArchiveListPager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, object values, string previousText,
			string nextText, bool alwaysShowPreviousAndNext) {

			return SimpleArchivePager(
				htmlHelper,
				pageOfAList,
				"PostsByArchive",
				values,
				previousText,
				nextText,
				alwaysShowPreviousAndNext
			);
		}

		public static string SimpleArchivePager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, string routeName,
			object values, string previousText, string nextText, bool alwaysShowPreviousAndNext) {

			if (pageOfAList.TotalPageCount < 2) {
				return "";
			}

			var sb = new StringBuilder(50);
			ViewContext viewContext = htmlHelper.ViewContext;
			var rvd = new RouteValueDictionary();

			foreach (KeyValuePair<string, object> item in viewContext.RouteData.Values) {
				rvd.Add(item.Key, item.Value);
			}

			var urlHelper = new UrlHelper(viewContext.RequestContext);

			rvd.Remove("controller");
			rvd.Remove("action");
			rvd.Remove("id");

			if (values != null) {
				var rvd2 = new RouteValueDictionary(values);

				foreach (KeyValuePair<string, object> item in rvd2) {
					rvd[item.Key] = item.Value;
				}
			}

			var archiveData = new ArchiveData(rvd["archiveData"] as string);

			sb.Append("<div class=\"pager\">");

			if (pageOfAList.PageIndex < pageOfAList.TotalPageCount - 1 || alwaysShowPreviousAndNext) {
				archiveData.Page = pageOfAList.PageIndex + 2;
				rvd["archiveData"] = archiveData.ToString();

				sb.AppendFormat("<a href=\"{1}{2}\" class=\"next\">{0}</a>", nextText,
				                urlHelper.RouteUrl(routeName, rvd),
				                viewContext.HttpContext.Request.QueryString.ToQueryString());
			}

			if (pageOfAList.PageIndex > 0 || alwaysShowPreviousAndNext) {
				archiveData.Page = pageOfAList.PageIndex;
				rvd["archiveData"] = archiveData.ToString();

				sb.AppendFormat("<a href=\"{1}{2}\" class=\"previous\">{0}</a>", previousText,
				                urlHelper.RouteUrl(routeName, rvd),
				                viewContext.HttpContext.Request.QueryString.ToQueryString());
			}

			sb.Append("</div>");

			return sb.ToString();
		}

		#endregion

		#region Published

		public static string Published<TModel>(this HtmlHelper<TModel> htmlHelper, Post post, string removedStatus, string draftStatus) where TModel : OxletModel {

			if (post.State == EntityState.Removed)
				return removedStatus;

			if (post.Published.HasValue){
				return ConvertToLocalTime(htmlHelper, post.Published.Value, htmlHelper.ViewData.Model).ToLongDateString();
			}

			return draftStatus;
		}

		#endregion

		#region ConvertToLocalTime

		private static DateTime ConvertToLocalTime<TModel>(this HtmlHelper<TModel> htmlHelper, DateTime dateTime)
			where TModel : OxletModel {
			return ConvertToLocalTime(htmlHelper, dateTime, htmlHelper.ViewData.Model);
		}

		private static DateTime ConvertToLocalTime(this HtmlHelper htmlHelper, DateTime dateTime, OxletModel model) {
			if (model.Context.User == null) {
				if (model.Context.Site.TimeZoneOffset != 0)
					return dateTime.Add(TimeSpan.FromHours(model.Context.Site.TimeZoneOffset));

				return dateTime;
			}

			return dateTime; //TODO: (erikpo) Get the timezone offset from the current user and apply it
		}

		#endregion

		#region RenderRsd

		public static void RenderRsd(this HtmlHelper htmlHelper) {
			htmlHelper.RenderRsd((Models.Blog)null);
		}

		public static void RenderRsd(this HtmlHelper htmlHelper, Models.Blog blog) {
			if (blog != null) {
				htmlHelper.RenderRsd(blog.Name);
			} else {
				htmlHelper.RenderRsd((string)null);
			}
		}

		public static void RenderRsd(this HtmlHelper htmlHelper, string blogName) {
			var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

			htmlHelper.ViewContext.HttpContext.Response.Write(
				htmlHelper.HeadLink(
					"EditURI",
					urlHelper.AbsolutePath(urlHelper.Rsd(blogName)),
					"application/rsd+xml",
					"RSD"
				)
			);
		}

		#endregion
	}
}
