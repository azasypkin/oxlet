﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Routing;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Blog.Extensions {
	public static class PostExtensions {
		public static string GetUrl(this Post post, RequestContext context, RouteCollection routes) {
			return routes.GetUrl(context, "Post", new {slug = post.Slug, dataFormat = ""});
		}

		public static IPagedList<Post> FillTags(this IPagedList<Post> posts, ITagService tagService) {
			return FillTags(posts, tagService, false);
		}

		public static IPagedList<Post> FillTags(this IPagedList<Post> posts, ITagService tagService, bool includeServiceTags) {
			if (posts != null) {
				foreach (var post in posts) {
					post.SetTags(tagService.GetTags(post, includeServiceTags));
				}
			}
			return posts;
		}

		public static IPagedList<Post> FillComments(this IPagedList<Post> posts, ICommentService commentService) {
			if (posts != null) {
				foreach (var post in posts) {
					post.SetComments(commentService.GetComments(new PagingInfo(0, 10), post, true));
				}
			}
			return posts;
		}

		public static IPagedList<Post> FillCreator(this IPagedList<Post> posts, IUserService userService) {
			if (posts != null) {
				foreach (var post in posts) {
					post.SetCreator(userService.Get(post.Creator.Id).GetDescription());
				}
			}
			return posts;
		}

		public static Post FillTags(this Post post, ITagService tagService) {
			return FillTags(post, tagService, false);
		}

		public static Post FillTags(this Post post, ITagService tagService, bool includeServiceTags) {
			if (post != null) {
				post.SetTags(tagService.GetTags(post, includeServiceTags));
			}

			return post;
		}

		public static Post FillComments(this Post post, ICommentService commentService) {
			if (post != null) {
				post.SetComments(commentService.GetComments(new PagingInfo(0, 10), post, true));
			}
			return post;
		}

		public static Post FillCreator(this Post post, IUserService userService) {
			if (post != null) {
				post.SetCreator(userService.Get(post.Creator.Id).GetDescription());
			}
			return post;
		}

		public static Post Apply(this Post post, PostInput input, EntityState state, UserDescription creator, Func<string, string> normalizeTag) {
			return new Post(
				post.Id, 
				creator,
				input.Title,
				input.Body,
				input.BodyShort,
				state,
				input.Slug,
				post.Created,
				post.Modified,
				input.Published,
				new List<Comment>(0),
				input.CommentingDisabled,
				post.Blog,
				input.Tags.Select(tagName => new Tag(0, normalizeTag(tagName), tagName, null, tagName.IsServiceTag())).ToList(),
				input.Language
			);
		}

		public static string GetBodyShort(this Post postBase) {
			return !string.IsNullOrEmpty(postBase.BodyShort)
					? postBase.BodyShort
					: postBase.GetBodyShort(100);
		}

		public static string GetBodyShort(this Post postBase, int wordCount) {
			string previewText = !string.IsNullOrEmpty(postBase.Body)
				? postBase.Body.CleanHtmlTags().CleanWhitespace()
				: string.Empty;

			if (!string.IsNullOrEmpty(previewText)) {
				previewText = string.Join(" ", previewText.Split(' ').Take(wordCount).ToArray());
			}

			return previewText;
		}

		//public static IEnumerable<ICacheable> GetDependencies(this Post post) {
		//    var dependencies = new List<ICacheable>();

		//    if (post == null){
		//        return dependencies;
		//    }

		//    dependencies.Add(post);

		//    dependencies.Add(post.Blog);

		//    dependencies.AddRange(post.Tags);

		//    return dependencies;
		//}
	}
}
