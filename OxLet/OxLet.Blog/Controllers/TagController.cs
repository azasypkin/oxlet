﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Controllers {
	public class TagController : Controller {
		private readonly ITagService _tagService;

		public TagController(ITagService tagService) {
			_tagService = tagService;
		}

		public virtual OxletModelList<KeyValuePair<Tag, int>> Cloud(Models.Blog blog) {
			return GetTagList(new TagCloudPageContainer(), () => _tagService.GetTagsWithPostCount(blog));
		}

		private static OxletModelList<KeyValuePair<Tag, int>> GetTagList(INamedEntity container, Func<IList<KeyValuePair<Tag, int>>> serviceCall) {
			var result = new OxletModelList<KeyValuePair<Tag, int>> {
				Container = container,
				List = serviceCall()
			};

			return result;
		}
	}
}