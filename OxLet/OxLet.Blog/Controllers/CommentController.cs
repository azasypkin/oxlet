﻿using System;
using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Controllers {
	public class CommentController : Controller {
		private readonly IPostService _postService;
		private readonly ITagService _tagService;
		private readonly IBlogService _blogService;
		private readonly ICommentService _commentService;

		public CommentController(ICommentService commentService, IPostService postService, ITagService tagService, IBlogService blogService) {
			_postService = postService;
			_tagService = tagService;
			_blogService = blogService;
			_commentService = commentService;
		}

		public virtual OxletModelList<Tree<Post, Comment>> List() {
			return new OxletModelList<Tree<Post, Comment>> {
				//List = _postService.GetComments(),
				Container = new SiteContainer()
			};
		}

		[ActionName("ListForAdmin")]
		public virtual OxletModelList<Comment> List(PagingInfo pagingInfo) {
			IPagedList<Comment> comments = _commentService.GetComments(pagingInfo, true, true);

			return new OxletModelList<Comment> {
				List = comments,
				Container = new SiteContainer()
			};
		}

		public virtual OxletModelList<Tree<Post, Comment>> ListByTag(Tag tagInput) {
			Tag tag = _tagService.GetTag(tagInput.Name);

			if (tag == null) {
				return null;
			}

			return new OxletModelList<Tree<Post, Comment>> {
				//List = _postService.GetComments(tag),
				Container = tag
			};
		}

		public virtual OxletModelList<Tree<Post, Comment>> ListByArea(Models.Blog blogInput) {
			Models.Blog blog = _blogService.GetBlog(blogInput.Name);

			if (blog == null)
				return null;

			return new OxletModelList<Tree<Post, Comment>> {
				//List = _postService.GetComments(blog),
				Container = blog
			};
		}

		public virtual OxletModelList<Tree<Post, Comment>> ListByPost(Models.Blog blogInput, Post postInput) {
			//Models.Blog blog = _blogService.GetBlog(blogInput.Name);

			//if (blog == null) {
			//    return null;
			//}

			//Post post = _postService.GetPost(blog, postInput.Slug);

			//if (post == null)
			//    return null;

			//return new OxletModelList<Tree<Post, Comment>> {
			//    List = _postService.GetComments(post)
			//        .Select(c => new Tree<Post, Comment> { Parent = post, Child = c })
			//        .ToList(),
			//    Container = post
			//};
			return null;
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public virtual ActionResult Approve(Post post, Comment comment, string returnUri) {

			if (post == null || post.Blog == null) {
				return null;
			}

			try {
				var result = _commentService.ApproveComment(comment);

				if (!string.IsNullOrEmpty(returnUri)){
					return new RedirectResult(returnUri);
				}
				return new JsonResult { Data = result };
			} catch {
				return new JsonResult { Data = false };
			}
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public virtual ActionResult Remove(Post post, Comment comment, string returnUri) {
			if (post == null || post.Blog == null) {
				return null;
			}

			try {
				_commentService.RemoveComment(comment);
			} catch {
				return new JsonResult { Data = false };
			}

			if (!string.IsNullOrEmpty(returnUri)){
				return new RedirectResult(returnUri);
			}

			return new JsonResult { Data = true };
		}

		private static OxletModelList<Tree<Post, Comment>> GetCommentList(INamedEntity container, Func<IPagedList<Tree<Post, Comment>>> serviceCall) {
			var result = new OxletModelList<Tree<Post, Comment>> {
				Container = container,
				List = serviceCall()
			};

			return result;
		}
	}
}
