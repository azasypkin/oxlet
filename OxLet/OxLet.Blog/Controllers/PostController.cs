﻿using System;
using System.Web.Mvc;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Secuity;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.Syndication;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Controllers {
	public class PostController : Controller {
		private readonly IPostService _postService;
		private readonly ITagService _tagService;
		private readonly ICommentService _commentService;
		private readonly IRegularExpressionService _expressionService;
		private readonly SiteContext _context;

		public PostController(
			IPostService postService,
			ITagService tagService,
			ICommentService commentService,
			IBlogService blogService,
			SiteContext siteContext, 
			IRegularExpressionService expressionService
			) {

			_postService = postService;
			_tagService = tagService;
			_context = siteContext;
			_commentService = commentService;
			_expressionService = expressionService;
			ValidateRequest = false;
		}

		[FeedViewSettings("Rss", FeedType.Rss)]
		[FeedViewSettings("Atom", FeedType.Atom)]
		public virtual OxletModelList<Post> List(Models.Blog blog, int? pageNumber, int pageSize, DateTime? ifModifiedSince) {
			int pageIndex = pageNumber.HasValue ? pageNumber.Value - 1 : 0;
			return GetPostList(blog, new SiteContainer(0, "Blog", null), () => _postService.GetPosts(blog, new PagingInfo(pageIndex, pageSize)));
		}

		[FeedViewSettings("Rss", FeedType.Rss)]
		[FeedViewSettings("Atom", FeedType.Atom)]
		public virtual OxletModelList<Post> ListByTag(Models.Blog blog, int? pageNumber, int pageSize, Tag tagInput, DateTime? ifModifiedSince) {
			int pageIndex = pageNumber.HasValue ? pageNumber.Value - 1 : 0;

			Tag tag = _tagService.GetTag(tagInput.Name);

			if (tag == null){
				return null;
			}

			return GetPostList(blog, tag, () => _postService.GetPosts(blog, tag, new PagingInfo(pageIndex, pageSize)));
		}

		public virtual OxletModelList<Post> ListByArchive(Models.Blog blog, int pageSize, ArchiveData archiveData) {
			return GetPostList(blog, new ArchiveContainer(0, null, null, archiveData), () => _postService.GetPosts(blog, archiveData, new PagingInfo(archiveData.Page - 1, pageSize)));
		}

		public virtual OxletModelList<Post> ListByLanguage(Models.Blog blog, int? pageNumber, int pageSize, Language language) {
			int pageIndex = pageNumber.HasValue ? pageNumber.Value - 1 : 0;
			var model = GetPostList(
				blog,
				new SiteContainer(0, "Blog", null),
				() => language != null 
					? _postService.GetPosts(blog, language, new PagingInfo(pageIndex, pageSize))
					: _postService.GetPosts(blog, new PagingInfo(pageIndex, pageSize))
			);
			model.AddModelItem(language);
			return model;
		}

		public virtual OxletModelList<Post> ListBySearch(Models.Blog blog, int? pageNumber, int pageSize, SearchCriteria criteria, DateTime? ifModifiedSince) {
			int pageIndex = pageNumber.HasValue ? pageNumber.Value - 1 : 0;

			if (!criteria.HasCriteria()){
				return List(blog, pageNumber, pageSize, ifModifiedSince);
			}

			return GetPostList(blog, new SearchPageContainer(0, criteria.Term, criteria.Term), () => _postService.GetPosts(blog, criteria, new PagingInfo(pageIndex, pageSize)));
		}

		public virtual OxletModelList<Post> ListWithDrafts(Models.Blog blog, int? pageNumber, int pageSize) {
			int pageIndex = pageNumber.HasValue ? pageNumber.Value - 1 : 0;

			return GetPostList(blog, new SiteContainer(), () => _postService.GetPosts(blog, new PagingInfo(pageIndex, pageSize)));
		}

		public virtual OxletModelItem<Post> Item(Models.Blog blog, Post post) {

			if (post == null || blog == null) {
				return null;
			}

			return new OxletModelItem<Post> {
				Container = blog,
				Item = _postService.GetPost(post.Id)
			};
		}

		[ActionName("Item"), AcceptVerbs(HttpVerbs.Post)]
		public object AddComment(Models.Blog blog, Post post, CommentInput commentInput) {
			
			if (post == null || post.CommentingDisabled) {
				return null;
			}

			if (post.Blog.CommentingDisabled) {
				return null;
			}

			ModelResult<Comment> addCommentResults = _commentService.AddComment(blog, post, commentInput);

			if (!addCommentResults.IsValid) {
				ModelState.AddModelErrors(addCommentResults.ValidationResults);

				return Item(blog, post);
			}

			if (!_context.User.IsAuthenticated) {
				if (commentInput.IsRemember){
					Response.Cookies.SetAnonymousUser(new UserAnonymous(new UserDescription(0) { Name = commentInput.Name, Email = commentInput.Email, Url = commentInput.Url, EmailHash = commentInput.EmailHash }));
				}
				else if (Request.Cookies.GetAnonymousUser() != null){
					Response.Cookies.ClearAnonymousUser();
				}
			}

			return new RedirectResult( addCommentResults.Item.State != EntityState.PendingApproval
				? Url.Comment(post, addCommentResults.Item)
				: Url.CommentPending(post, addCommentResults.Item)
			);
		}

		[ActionName("ItemAdd"), AcceptVerbs(HttpVerbs.Get)]
		public virtual OxletModelItem<Post> Add(Models.Blog blogInput, Post post) {

			return new OxletModelItem<Post> {
				Container = blogInput,
				Item = post
			};
		}

		[ActionName("ItemAdd"), AcceptVerbs(HttpVerbs.Post)]
		public virtual object SaveAdd(Models.Blog blog, Models.Blog blogInput, PostInput postInput) {

			var result = _postService.AddPost(blog, postInput, EntityState.Normal);

			//todo: (nheskew) need to do more than just return another action method because it's likely different actions will need different filters applied to it
			if (!result.IsValid) {
				ModelState.AddModelErrors(result.ValidationResults);
				return Add(blogInput, postInput.ToPost(blogInput, EntityState.Normal, _context.User.GetDescription(), t => _expressionService.Clean("TagReplace", t)));
			}

			return Redirect(Url.Post(result.Item));
		}

		[ActionName("ItemEdit"), AcceptVerbs(HttpVerbs.Get)]
		public virtual OxletModelItem<Post> Edit(Models.Blog blog, Post post) {

			if (post == null || blog == null) {
				return null;
			}

			return new OxletModelItem<Post> {
				Container = blog,
				Item = post
			};
		}

		[ValidateInput(false)]
		[ActionName("ItemEdit"), AcceptVerbs(HttpVerbs.Post)]
		public virtual object SaveEdit(Models.Blog blog, Post post, PostInput postInput) {

			if (post == null || blog == null) {
				return null;
			}

			var result = _postService.EditPost(blog, post, postInput, EntityState.Normal);

			//todo: (nheskew) need to do more than just return another action method because it's likely different actions will need different filters applied to it
			if (!result.IsValid) {
				ModelState.AddModelErrors(result.ValidationResults);
				return Edit(blog, result.Item);
			}

			return Redirect(Url.Post(post));
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public virtual ActionResult Remove(Models.Blog blog, Post postBaseInput, string returnUri) {

			Post post = _postService.GetPost(blog, postBaseInput.Slug);

			_postService.Remove(post);

			return Redirect(returnUri);
		}

		private static OxletModelList<Post> GetPostList(Models.Blog blog, INamedEntity container, Func<IPagedList<Post>> serviceCall) {
			return new OxletModelList<Post> {
				Container = container,
				List = serviceCall()
			};
		}
	}
}
