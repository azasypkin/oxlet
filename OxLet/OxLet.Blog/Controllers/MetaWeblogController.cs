using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Infrastructure;
using OxLet.Core.Infrastructure.XmlRpc;
using OxLet.Core.Infrastructure.XmlRpc.Models;
using OxLet.Core.Models;
using OxLet.Core.Models.Input;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.Controllers {
	public class MetaWeblogController : Controller {
		private readonly IBlogService _blogService;
		private readonly IPostService _postService;
		private readonly ITagService _tagService;
		private readonly IRegularExpressionService _expressionService;
		private readonly IFileResourceService _fileService;
		private readonly ILanguageService _languageService;
		private readonly SiteContext _context;

		public MetaWeblogController(
			SiteContext context,
			IBlogService blogService,
			IPostService postService,
			ITagService tagService,
			IRegularExpressionService expressionService,
			IFileResourceService fileService,
			ILanguageService languageService) {

			_blogService = blogService;
			_postService = postService;
			_tagService = tagService;
			_expressionService = expressionService;
			_fileService = fileService;
			_context = context;
			_languageService = languageService;
		}

		public ContentResult Rsd(SiteContext context, Models.Blog blog) {
			return Content(GenerateRsd(context, blog).ToString(), "text/xml");
		}

		protected XDocument GenerateRsd(SiteContext context, Models.Blog blog) {
			XNamespace rsdNamespace = "http://archipelago.phrasewise.com/rsd";
			var rsd = new XDocument(
				new XElement(rsdNamespace + "rsd", new XAttribute("version", "1.0"),
					new XElement(rsdNamespace + "service",
						new XElement(rsdNamespace + "engineName", "OxLet"),
						new XElement(rsdNamespace + "engineLink", Url.OxiLet()),
						new XElement(rsdNamespace + "homePageLink", Url.AbsolutePath(Url.Home())),
						new XElement(rsdNamespace + "apis", GenerateRsdApiList(context, blog, rsdNamespace))
					)
				)
			);

			return rsd;
		}

		protected XElement[] GenerateRsdApiList(SiteContext context, Models.Blog blog, XNamespace rsdNamespace) {
			IEnumerable<Models.Blog> blogs = blog != null && !string.IsNullOrEmpty(blog.Name)
				? new[] {blog} 
				: _blogService.GetBlogs(new PagingInfo(0,10)).ToArray();

			var elements = new List<XElement>(blogs.Count());

			string apiLink = new UriBuilder(context.Site.Host) { Path = Url.MetaWeblog(context.Configuration.GetValue<string>("BlogName")) }.Uri.ToString();

			foreach (Models.Blog b in blogs) {
				elements.Add(
					new XElement(
						rsdNamespace + "api",
						new XAttribute("name", "MetaWeblog"),
						new XAttribute("blogID", b.Name),
						new XAttribute(
							"preferred",
							(blogs.Count() == 1 || string.Compare(b.Name, blog.Name, true) == 0).ToString().ToLower()
							),
						new XAttribute("apiLink", apiLink)
						)
					);
			}

			return elements.ToArray();
		}

		[ActionName("metaWeblog.newPost")]
		public ActionResult NewPost(SiteContext context, XmlRpcNewPost post) {

			Models.Blog blog = _blogService.GetBlog(post.BlogId);

			if(blog == null) {
				throw new ArgumentException(string.Format("Blog with name \"{0}\" isn't found.", post.BlogId));
			}

			var result = _postService.AddPost(blog, XmlRpcPostToModelPost(blog, post), EntityState.Normal);

			//todo: (nheskew) need to do more than just return another action method because it's likely different actions will need different filters applied to it
			if (!result.IsValid) {
				ModelState.AddModelErrors(result.ValidationResults);
				return new XmlRpcFaultResult(0, "Error");
			}
			return new XmlRpcResult(result.Item.Id);
		}

		[ActionName("metaWeblog.editPost")]
		public ActionResult EditPost(XmlRpcEditPost post) {
			if (string.IsNullOrEmpty(post.Id)) {
				throw new ArgumentException();
			}

			// get existing post
			var originalPost = _postService.GetPost(new MetaWeblogPostAddress(Int32.Parse(post.Id)));

			Models.Blog blog = _blogService.GetBlog(originalPost.Blog.Id);

			if (blog == null) {
				throw new ArgumentException(string.Format("Blog with Id \"{0}\" isn't found.", originalPost.Blog.Id));
			}

			var result = _postService.EditPost(blog, originalPost, XmlRpcPostToModelPost(blog, post), EntityState.Normal);

			//todo: (nheskew) need to do more than just return another action method because it's likely different actions will need different filters applied to it)
			if (!result.IsValid) {
				ModelState.AddModelErrors(result.ValidationResults);
				return new XmlRpcFaultResult(0, "Error");
			}
			return new XmlRpcResult(result.Item.Id);
		}

		[ActionName("metaWeblog.getPost")]
		public ActionResult GetPost(XmlRpcGetPost request) {
			Post post = _postService.GetPost(new MetaWeblogPostAddress(Int32.Parse(request.PostId)));

			if (post == null) {
				throw new ArgumentOutOfRangeException();
			}

			return new XmlRpcResult(ModelPostToServicePost(post));
		}

		//TODO: (erikpo) Need to implement this method if the current setup supports writing to the file system
		[ActionName("metaWeblog.newMediaObject")]
		public ActionResult NewMediaObject(SiteContext siteContext, XmlRpcNewMediaObject file) {

			var fileResourceInput = new FileResourceInput(0, file.Name, file.MimeType, file.Content);

			var result = _fileService.AddFileResource(fileResourceInput);

			//todo: (nheskew) need to do more than just return another action method because it's likely different actions will need different filters applied to it)
			if (!result.IsValid) {
				ModelState.AddModelErrors(result.ValidationResults);
				return new XmlRpcFaultResult(0, "Error");
			}
			return new XmlRpcResult(new Dictionary<string, object> { {
				"url",
				string.Format("/File/{0}", result.Item.Id)
				} }
			);

		}

		[ActionName("metaWeblog.getCategories")]
		public ActionResult GetCategories(XmlRpcGetCategories request) {
			Models.Blog blog = _blogService.GetBlog(request.BlogId);

			if(blog == null) {
				throw new ArgumentException(string.Format("Blog with name \"{0}\" isn't found.", request.BlogId));
			}

			return new XmlRpcResult(
				_tagService.GetTags(blog, true).Select(
					t => new Dictionary<string, object> {{"description", t.Name}, {"htmlUrl", string.Empty}, {"rssUrl", string.Empty}})
				.ToArray());
		}

		[ActionName("metaWeblog.getRecentPosts")]
		public ActionResult GetRecentPosts(XmlRpcGetRecentPosts request) {
			var blog = _blogService.GetBlog(request.BlogName);
			return new XmlRpcResult(
				_postService.GetPosts( new MetaWeblogRecentPostsAddress(blog),
					new PagingInfo(0, request.PageSize.HasValue ? request.PageSize.Value : 10)
				).Select(ModelPostToServicePost).ToArray()
			);
		}

		[ActionName("blogger.getUsersBlogs")]
		public ActionResult GetUsersBlogs(XmlRpcGetUserBlogs request) {
			//TODO: (erikpo) Make this return only the list of blogs the current user has explicit access to
			return
				new XmlRpcResult(
					_blogService.GetBlogs(new PagingInfo(0, 10)).Select(
						b => new Dictionary<string, object> {{"url", string.Empty}, {"blogid", b.Name}, {"blogName", b.DisplayName}}).
						ToArray());
		}

		[ActionName("blogger.deletePost")]
		public ActionResult DeletePost(XmlRpcDeletePost request) {
			_postService.RemovePost(new MetaWeblogPostAddress(Int32.Parse(request.Id)));
			return new XmlRpcResult(true);
		}

		public ContentResult LiveWriterManifest() {
			XNamespace ns = "http://schemas.microsoft.com/wlw/manifest/weblog";

			//TODO: (erikpo) Make these settings dynamic based on capabilities

			var doc =
				new XDocument(
					new XDeclaration("1.0", "utf-8", "no"),
					new XElement(
						ns + "manifest",
						new XElement(
							ns + "options",
							new[] {
								new XElement(ns + "clientType", "Metaweblog"),
								new XElement(ns + "supportsExcerpt", "Yes"),
								new XElement(ns + "supportsNewCategories", "Yes"),
								new XElement(ns + "supportsNewCategoriesInline", "Yes"),
								new XElement(ns + "requiresHtmlTitles", "No"),
								new XElement(ns + "requiresXHTML", "Yes"),
								new XElement(ns + "supportsScripts", "Yes"),
								new XElement(ns + "supportsEmbeds", "Yes"),
								new XElement(ns + "supportsSlug", "Yes"),
								new XElement(ns + "supportsFileUpload", "Yes")
							}
							)
						)
					);

			return Content(doc.ToString(), "text/xml");
		}

		private static IDictionary<string, object> ModelPostToServicePost(Post post) {
			return new Dictionary<string, object> {
				{"categories", post.Tags.Select(t => t.Name).ToArray()},
				{"dateCreated", post.Created},
				{"description", post.Body},
				{"mt_basename", post.Slug},
				{"mt_excerpt", post.BodyShort},
				{"postid", post.Id.ToString()},
				{"title", post.Title},
				{"userid", post.Creator.Id.ToString()},
			};
		}

		private PostInput XmlRpcPostToModelPost(Models.Blog blog, XmlRpcNewPost post) {
			var languageCategory = post.Categories.FirstOrDefault(x => x.IsServiceTag() && x.Contains("[Language]"));

			Language language = !string.IsNullOrEmpty(languageCategory) 
				? _languageService.GetLanguage(languageCategory.GetServiceTagLanguage() ?? _context.Site.DefaultLanguage.Name)
				: _context.Site.DefaultLanguage;

			return new PostInput {
				Title = post.Title,
				Body = post.Description,
				BodyShort = post.Excerpt,
				Tags = post.Categories,
				Slug = !string.IsNullOrEmpty(post.Excerpt) && !string.IsNullOrEmpty(post.BaseName)
					? post.BaseName
					: _expressionService.Slugify(post.Title),
				CommentingDisabled = blog.CommentingDisabled,
				Published = post.Created,
				Language = language
			};
		}

		private PostInput XmlRpcPostToModelPost(Models.Blog blog, XmlRpcEditPost post) {

			Language language = null;

			var languageCategory = post.Categories.FirstOrDefault(x => x.IsServiceTag() && x.Contains("[Language]"));

			if (!string.IsNullOrEmpty(languageCategory) && languageCategory.GetServiceTagLanguage() != null) {
				language = _languageService.GetLanguage(languageCategory.GetServiceTagLanguage());
			}
			if(language == null) {
				language = _languageService.GetLanguage(_context.Site.DefaultLanguage.Name);
			}
			return new PostInput {
				Title = post.Title,
				Body = post.Description,
				BodyShort = post.Excerpt,
				Tags = post.Categories,
				Slug = !string.IsNullOrEmpty(post.Excerpt) && !string.IsNullOrEmpty(post.BaseName)
					? post.BaseName
					: _expressionService.Slugify(post.Title),
				CommentingDisabled = blog.CommentingDisabled,
				Published = post.Created,
				Language = language
			};
		}
	}
}
