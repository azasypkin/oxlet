﻿using System.Collections.Generic;
using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Blog.Controllers {
	public class BlogController : Controller {
		private readonly IBlogService _blogService;

		public BlogController(IBlogService blogService) {
			_blogService = blogService;
		}

		public virtual OxletModel Dashboard() {
			return new OxletModel {Container = new AdminDashboardPageContainer()};
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public virtual OxletModelList<Models.Blog> Find() {
			return new OxletModelList<Models.Blog>();
		}

		[ActionName("Find"), AcceptVerbs(HttpVerbs.Post)]
		public virtual OxletModelList<Models.Blog> FindQuery(BlogSearchCriteria searchCriteria) {
			IList<Models.Blog> foundBlogs = _blogService.FindBlogs(searchCriteria);
			var model = new OxletModelList<Models.Blog> { List = foundBlogs };

			model.AddModelItem(searchCriteria);

			return model;
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public virtual OxletModelItem<Models.Blog> ItemEdit(Models.Blog blog) {
			if(blog == null) {
				return null;
			}
			return new OxletModelItem<Models.Blog> { Container = new AdminDashboardPageContainer(), Item = blog };
		}

		//[ActionName("ItemEdit"), AcceptVerbs(HttpVerbs.Post)]
		//public virtual object ItemSave(Models.Blog blog, BlogInput blogInput) {

		//    ValidationStateDictionary validationState = blog == null
		//        ? _blogService.AddBlog(blogInput).ValidationResults 
		//        : _blogService.EditBlog(blog, blogInput).ValidationResults;

		//    if (!validationState.IsValid) {
		//        ModelState.AddModelErrors(validationState);

		//        return ItemEdit(blog);
		//    }

		//    return Redirect(Url.AppPath(Url.ManageBlog()));
		//}


	}
}
