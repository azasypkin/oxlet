﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.ModelBinders {
	public class PostModelBinder : IModelBinder {

		private readonly IUnityContainer _container;

		public PostModelBinder(IUnityContainer container) {
			_container = container;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var blogService = _container.Resolve<IBlogService>();
			ValueProviderResult providerResult = bindingContext.ValueProvider.GetValue("blogName");

			var blog = providerResult != null && !string.IsNullOrEmpty(providerResult.GetValue<string>())
				? blogService.GetBlog(providerResult.GetValue<string>())
				: blogService.GetBlogs(new PagingInfo(0, 1)).FirstOrDefault();

			var postService = _container.Resolve<IPostService>();

			providerResult = bindingContext.ValueProvider.GetValue("slug");
			if (providerResult != null) {
				return postService.GetPost(blog, providerResult.GetValue<string>());
			}

			return null;
		}
	}
}
