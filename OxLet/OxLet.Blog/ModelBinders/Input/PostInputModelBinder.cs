﻿using System;
using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Models.Input;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.ModelBinders.Input {
	public class PostInputModelBinder : IModelBinder {
		private readonly ILanguageService _languageService;
		public PostInputModelBinder(ILanguageService languageService) {
			_languageService = languageService;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			//return post;
			var valueProvider = bindingContext.ValueProvider;
			//bindingContext.Equals()

			var postInput = new PostInput();

			ValueProviderResult providerResult = valueProvider.GetValue("title");
			if (providerResult != null) {
				postInput.Title = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("body");
			if (providerResult != null) {
				postInput.Body = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("bodyShort");
			if (providerResult != null) {
				postInput.BodyShort = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("slug");
			if (providerResult != null) {
				postInput.Slug = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("commentingDisabled");
			if (providerResult != null) {
				postInput.CommentingDisabled = providerResult.GetValue<string>().Contains("true");
			}

			providerResult = valueProvider.GetValue("tags");
			if (providerResult != null) {
				postInput.Tags = providerResult.GetValue<string>().Split(',').Select(x=>x.Trim()).ToArray();
			}

			providerResult = valueProvider.GetValue("isPublished");
			if (providerResult != null && providerResult.GetValue<string>().Contains("true")) {
				providerResult = valueProvider.GetValue("published");
				if(providerResult != null) {
					postInput.Published = providerResult.GetValue(DateTime.UtcNow);
				}
			}

			providerResult = valueProvider.GetValue("language");

			postInput.Language = _languageService.GetLanguage(providerResult != null ? providerResult.GetValue<string>() : "en");

			return postInput;
		}
	}
}
