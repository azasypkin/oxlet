﻿using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using OxLet.Blog.Models.Input;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.ModelBinders.Input {
	public class CommentInputModelBinder : IModelBinder {

		private readonly ILanguageService _languageService;
		private readonly IUnityContainer _container;
		public CommentInputModelBinder(IUnityContainer container, ILanguageService languageService) {
			_languageService = languageService;
			_container = container;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var valueProvider = bindingContext.ValueProvider;

			var comment = new CommentInput();
			ValueProviderResult providerResult;

			comment.Body = controllerContext.RequestContext.HttpContext.Request.Unvalidated("Item.Body");

			bool isAuthenticated = controllerContext.HttpContext.User.Identity.IsAuthenticated;

			// if comment is posted by anonymous user let's extract it's info
			if (!isAuthenticated) {
				string creatorName = null;
				providerResult = valueProvider.GetValue("Item.Name");
				if (providerResult != null) {
					creatorName = providerResult.GetValue<string>();
				}

				string creatorEmailHash = null;
				string creatorEmail = null;
				providerResult = valueProvider.GetValue("Item.Email");
				if (providerResult != null) {
					creatorEmail = providerResult.GetValue<string>();

					providerResult = valueProvider.GetValue("Item.EmailHash");

					if (providerResult != null) {
						creatorEmailHash = providerResult.GetValue<string>();
					}

					if (!string.IsNullOrEmpty(creatorEmail) && string.IsNullOrEmpty(creatorEmailHash)) {
						creatorEmailHash = creatorEmail.ComputeHash();
					}
				}

				string creatorUrl = null;
				providerResult = valueProvider.GetValue("Item.Url");
				if (providerResult != null) {
					creatorUrl = providerResult.GetValue<string>();
				}

				providerResult = valueProvider.GetValue("Item.IsRemember");
				if (providerResult != null) {
					comment.IsRemember = providerResult.GetValue<string>().Contains("true");
				}
				comment.Email = creatorEmail;
				comment.EmailHash = creatorEmailHash;
				comment.Name = creatorName;
				comment.Url = creatorUrl;
			}
			else {
				comment.CreatorId = _container.Resolve<SiteContext>().User.GetDescription().Id;
			}

			//int parentId = 0;

			//providerResult = valueProvider.GetValue("parentId");
			//if (providerResult != null) {
			//    var resultString = providerResult.GetValue<string>();
			//    if (!string.IsNullOrEmpty(resultString)) {
			//        comment.ParentId = new int(resultString);
			//    }
			//}

			providerResult = valueProvider.GetValue("Item.Id");
			if (providerResult != null) {
				var id = providerResult.GetValue<int>();
				if (id != 0) {
					comment.Id = id;
				}
			}

			comment.CreatorIp = controllerContext.HttpContext.Request.GetUserIpAddress().ToLong();

			comment.CreatorUserAgent = controllerContext.HttpContext.Request.UserAgent;

			comment.Language = _languageService.GetLanguage("en-US").Id;

			return comment;
		}
	}
}
