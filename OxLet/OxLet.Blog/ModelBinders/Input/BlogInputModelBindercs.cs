﻿using System.Web.Mvc;
using OxLet.Blog.Models.Input;
using OxLet.Core.Models;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.ModelBinders.Input {
	public class BlogInputModelBinder : IModelBinder {

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var valueProvider = bindingContext.ValueProvider;

			var blog = new BlogInput();

			ValueProviderResult providerResult = valueProvider.GetValue("blogName");
			if (providerResult != null) {
				blog.Name = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("blogDisplayName");
			if (providerResult != null) {
				blog.DisplayName = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("blogDescription");
			if (providerResult != null) {
				blog.Description = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("skinDefault");
			if (providerResult != null) {
				blog.SkinDefault = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("postEditTimeout");
			if (providerResult != null) {
				blog.PostEditTimeout = providerResult.GetValue<short>();
			}

			providerResult = valueProvider.GetValue("authorAutoSubscribe");
			if (providerResult != null) {
				blog.AuthorAutoSubscribe = providerResult.GetValue<string>().Contains("true");
			}

			providerResult = valueProvider.GetValue("commentingDisabled");
			if (providerResult != null) {
				blog.CommentingDisabled = providerResult.GetValue<string>().Contains("true");
			}

			providerResult = valueProvider.GetValue("Item.CommentStateDefault");
			if (providerResult != null) {
				blog.CommentStateDefault = providerResult.GetValue<EntityState>();
			}

			return blog;
		}


	}
}
