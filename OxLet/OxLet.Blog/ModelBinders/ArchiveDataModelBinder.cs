﻿using System.Web.Mvc;
using OxLet.Blog.Models;

namespace OxLet.Blog.ModelBinders {
	public class ArchiveDataModelBinder : IModelBinder {
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			return new ArchiveData(controllerContext.RouteData.Values["archiveData"] as string);
		}
	}
}