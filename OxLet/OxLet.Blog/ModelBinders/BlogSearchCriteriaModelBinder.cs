﻿using System.Web.Mvc;
using OxLet.Blog.Models;

namespace OxLet.Blog.ModelBinders {
	public class BlogSearchCriteriaModelBinder : IModelBinder {
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			return new BlogSearchCriteria {
				Name = controllerContext.HttpContext.Request.Form.Get("blogNameSearch")
			};
		}
	}
}
