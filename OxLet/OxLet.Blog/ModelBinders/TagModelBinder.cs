﻿using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.ModelBinders {
	public class TagModelBinder : IModelBinder {
		private readonly ITagService _tagService;
		public TagModelBinder(ITagService tagService) {
			_tagService = tagService;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			ValueProviderResult providerResult = bindingContext.ValueProvider.GetValue("tagName");

			if (providerResult != null && !string.IsNullOrEmpty(providerResult.GetValue<string>())) {
				return _tagService.GetTag(providerResult.GetValue<string>());
			}

			providerResult = bindingContext.ValueProvider.GetValue("tagId");

			if (providerResult != null && !string.IsNullOrEmpty(providerResult.GetValue<string>())) {
				return _tagService.GetTag(providerResult.GetValue<int>());
			}

			return null;
		}
	}
}
