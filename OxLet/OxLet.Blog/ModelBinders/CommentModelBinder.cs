﻿using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.ModelBinders {
	public class CommentModelBinder : IModelBinder {

		private readonly ICommentService _commentService;

		public CommentModelBinder(ICommentService commentService) {
			_commentService = commentService;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			ValueProviderResult commentIdProviderResult = bindingContext.ValueProvider.GetValue("id");

			if(commentIdProviderResult != null) {
				return _commentService.GetComment(commentIdProviderResult.GetValue<int>());
			}

			ValueProviderResult blogProviderResult = bindingContext.ValueProvider.GetValue("blogName");
			ValueProviderResult postProviderResult = bindingContext.ValueProvider.GetValue("slug");
			ValueProviderResult commentProviderResult = bindingContext.ValueProvider.GetValue("comment");

			if(blogProviderResult != null && postProviderResult != null && commentProviderResult != null) {
				return _commentService.GetComment(
					blogProviderResult.GetValue<string>(),
					postProviderResult.GetValue<string>(),
					commentProviderResult.GetValue<string>()
				);
			}
			return null;
		}
	}
}
