﻿using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Configuration;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.ModelBinders {
	public class BlogModelBinder : IModelBinder {
		private readonly IBlogService _blogService;
		private readonly OxLetSection _section;

		public BlogModelBinder(IBlogService blogService, OxLetSection section) {
			_blogService = blogService;
			_section = section;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			
			ValueProviderResult providerResult = bindingContext.ValueProvider.GetValue("blogName");


			if (providerResult != null && !string.IsNullOrEmpty(providerResult.GetValue<string>())) {
				return _blogService.GetBlog(providerResult.GetValue<string>());
			}

			providerResult = bindingContext.ValueProvider.GetValue("blogId");

			if (providerResult != null && !string.IsNullOrEmpty(providerResult.GetValue<string>())) {
				return _blogService.GetBlog(providerResult.GetValue<string>());
			}

			return _blogService.GetBlog(_section.GetValue<string>("BlogName"));
		}
	}
}
