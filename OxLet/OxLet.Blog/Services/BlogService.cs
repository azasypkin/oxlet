﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Cache;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Models.Projections;
using OxLet.Core.Validation;

namespace OxLet.Blog.Services {
	public class BlogService : IBlogService {
		private readonly IBlogRepository _repository;
		private readonly IValidationService _validator;
		private readonly ICacheService _cacheService;
		private readonly SiteContext _context;

		public BlogService(IBlogRepository repository, IValidationService validator, ICacheService cacheService, SiteContext siteContext) {
			_repository = repository;
			_validator = validator;
			_cacheService = cacheService;
			_context = siteContext;
		}

		#region IBlogService Members

		public Models.Blog GetBlog(string name) {
			if (string.IsNullOrEmpty(name)) {
				throw new ArgumentNullException("name");
			}

			return _cacheService.GetItem(
				string.Format("GetBlog-Name:{0}", name),
				()=>_repository.GetBlog(name),
				GetBlogDependencies
			);
		}

		public Models.Blog GetBlog(int id) {
			return _cacheService.GetItem(
				string.Format("GetBlog-Id:{0}", id),
				() => _repository.GetBlog(id),
				GetBlogDependencies
			);
		}

		public ModelResult<Models.Blog> AddBlog(BlogInput blogInput) {
			//var validationState = new ValidationStateDictionary {{typeof (BlogInput), _validator.Validate(blogInput)}};

			//if (!validationState.IsValid) {
			//    return new ModelResult<Models.Blog>(validationState);
			//}

			Models.Blog blog;

			using (var transaction = new TransactionScope()) {
				blog = blogInput.ToBlog(new SiteProjection(_context.Site.Id), DateTime.UtcNow, DateTime.UtcNow);

				//ValidateBlog(blog, validationState);

				//if (!validationState.IsValid) {
				//    return new ModelResult<Models.Blog>(validationState);
				//}

				blog = _repository.Save(blog);

				InvalidateCachedBlogDependencies(blog);

				transaction.Complete();
			}


			return new ModelResult<Models.Blog>(blog, Enumerable.Empty<ModelValidationResult>() );//validationState);
		}

		public ModelResult<Models.Blog> EditBlog(Models.Blog blog, BlogInput blogInput) {
			//var validationState = new ValidationStateDictionary {{typeof (BlogInput), _validator.Validate(blogInput)}};

			//if (!validationState.IsValid) {
			//    return new ModelResult<Models.Blog>(validationState);
			//}

			Models.Blog originalBlog = blog;
			Models.Blog newBlog;

			using (var transaction = new TransactionScope()) {
				newBlog = originalBlog.Apply(blogInput);

				//ValidateBlog(newBlog, originalBlog, validationState);

				//if (!validationState.IsValid) {
				//    return new ModelResult<Models.Blog>(validationState);
				//}

				newBlog = _repository.Save(newBlog);

				InvalidateCachedBlogForEdit(newBlog, originalBlog);

				transaction.Complete();
			}

			return new ModelResult<Models.Blog>(newBlog, Enumerable.Empty<ModelValidationResult>());//validationState);
		}

		public IPagedList<Models.Blog> GetBlogs(PagingInfo pagingInfo) {
			return _cacheService.GetItems<IPagedList<Models.Blog>, Models.Blog>(
				"GetBlogs",
				() => _repository.GetBlogs(pagingInfo),
				b => b.GetCacheDependencyItems(),
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);
		}

		public bool IsBlogExists(string name) {
			return _cacheService.GetItem<bool?>(
				string.Format("GetBlogExists-Name:{0}", name),
				() => _repository.GetBlog(name) != null,
				null).GetValueOrDefault(false);
		}

		public IPagedList<Models.Blog> FindBlogs(BlogSearchCriteria criteria) {

			throw new NotImplementedException();
			//return _cacheService.GetItems<IPagedList<Models.Blog>, Models.Blog>(
			//    string.Format("FindBlogs:{0}", criteria.Name),
			//    () => _repository.GetBlogs().ToList()
			//            .Where(b =>
			//                b.Name.IndexOf(criteria.Name, StringComparison.OrdinalIgnoreCase) != -1 ||
			//                b.DisplayName.IndexOf(criteria.Name, StringComparison.OrdinalIgnoreCase) != -1
			//            ).ToList(),
			//    GetBlogDependencies
			//));

			//return
			//    ;
		}

		//private void ValidateBlog(Models.Blog newBlog, ValidationStateDictionary validationState) {
		//    ValidateBlog(newBlog, newBlog, validationState);
		//}

		//private void ValidateBlog(Models.Blog newBlog, Models.Blog originalBlog, ValidationStateDictionary validationState) {
		//    var state = new ValidationState();

		//    validationState.Add(typeof(Models.Blog), state);

		//    Models.Blog foundBlog = _repository.GetBlog(newBlog.Name);

		//    if (foundBlog != null && newBlog.Name != originalBlog.Name){
		//        state.Errors.Add(new ValidationError("Blogs.NameNotUnique", newBlog.Name, "A blog already exists with the supplied name"));
		//    }
		//}

		private static IEnumerable<ICacheable> GetBlogDependencies(Models.Blog blog) {
			return new List<ICacheable> {blog.Site, blog};
		}

		private void InvalidateCachedBlogDependencies(Models.Blog blog) {
			_cacheService.InvalidateItem(blog.Site);
			_cacheService.Invalidate(string.Format("GetBlogExists-Name:{0}", blog.Name));
		}

		private void InvalidateCachedBlogForEdit(Models.Blog newBlog, Models.Blog originalBlog) {
			_cacheService.InvalidateItem(newBlog);
			_cacheService.Invalidate(string.Format("GetBlog-Name:{0}", originalBlog.Name));
			_cacheService.Invalidate(string.Format("GetBlog-Id:{0}", originalBlog.Id));
			if(originalBlog.Name != newBlog.Name) {
				_cacheService.Invalidate(string.Format("GetBlogExists-Name:{0}", originalBlog.Name));
			}
		}

		#endregion
	}
}
