﻿using System.Collections.Generic;
using OxLet.Blog.Models;
using OxLet.Core.Models;

namespace OxLet.Blog.Services.Interfaces {
	public interface ITagService {
		IList<Tag> GetTags(Models.Blog blog);
		IList<Tag> GetTags(Models.Blog blog, bool includeServiceTags);
		IList<KeyValuePair<Tag, int>> GetTagsWithPostCount(Models.Blog blog);
		IList<KeyValuePair<Tag, int>> GetTagsWithPostCount(Models.Blog blog, bool includeServiceTags);

		Tag GetTag(string name);
		Tag GetTag(int id);

		ModelResult<Tag> AddTag(Tag tag);

		IList<Tag> GetTags(Post post, bool includeServiceTags);
		IList<Tag> GetTags(Post post);
	}
}
