﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Core.Models;

namespace OxLet.Blog.Services.Interfaces {
	public interface ICommentService {
		Comment GetComment(int id);
		Comment GetComment(string blogName, string postSlug, string commentSlug);

		IPagedList<Comment> GetComments(PagingInfo pagingInfo, bool includePending, bool sortDescending);
		IPagedList<Comment> GetComments(PagingInfo pagingInfo, Models.Blog blog);
		IPagedList<Comment> GetComments(PagingInfo pagingInfo, Post post, bool includeUnapproved);
		IPagedList<Comment> GetComments(PagingInfo pagingInfo, Tag tag);

		IEnumerable<ModelValidationResult> ValidateCommentInput(CommentInput commentInput);

		ModelResult<Comment> AddComment(Models.Blog blog, Post post, CommentInput commentInput);
		ModelResult<Comment> EditComment(Comment comment, CommentInput commentInput);

		bool RemoveComment(Comment comment);
		bool ApproveComment(Comment comment);
	}
}
