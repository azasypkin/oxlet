﻿using System;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Core.Models;

namespace OxLet.Blog.Services.Interfaces {
	public interface IBlogService {
		Models.Blog GetBlog(string name);
		Models.Blog GetBlog(int id);
		ModelResult<Models.Blog> AddBlog(BlogInput blogInput);
		ModelResult<Models.Blog> EditBlog(Models.Blog blog, BlogInput blogInput);
		IPagedList<Models.Blog> GetBlogs(PagingInfo pagingInfo);

		bool IsBlogExists(string name);

		IPagedList<Models.Blog> FindBlogs(BlogSearchCriteria criteria);
	}
}
