﻿using System;
using System.Collections.Generic;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Core.Models;
using OxLet.Core.Validation;

namespace OxLet.Blog.Services.Interfaces {
	public interface IPostService {
		IPagedList<Post> GetPosts(Models.Blog blog, PagingInfo pagingInfo);

		IPagedList<Post> GetPosts(Models.Blog blog, Tag tag, PagingInfo pagingInfo);
		IPagedList<Post> GetPosts(Models.Blog blog, Language language, PagingInfo pagingInfo);
		IPagedList<Post> GetPosts(Models.Blog blog, ArchiveData archive, PagingInfo pagingInfo);
		IPagedList<Post> GetPosts(Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo);
		IPagedList<Post> GetPosts(Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo, bool includeDrafts);

		IEnumerable<DateTime> GetPostDateGroups(Models.Blog blog);

		Post GetPost(Models.Blog blog, string postSlug);
		Post GetPost(int id);

		Post GetRandomPost(Models.Blog blog);


		//ValidationStateDictionary ValidatePostInput(PostInput postInput);

		ModelResult<Post> AddPost(Models.Blog blog, PostInput postInput, EntityState state);
		ModelResult<Post> EditPost(Models.Blog blog, Post post, PostInput postInput, EntityState state);

		void Remove(Post post);
		void RemoveAll(Models.Blog blog);

		IEnumerable<KeyValuePair<ArchiveData, int>> GetArchives(Models.Blog blog);

		IPagedList<Post> GetPosts(MetaWeblogRecentPostsAddress adress, PagingInfo pagingInfo);
		Post GetPost(MetaWeblogPostAddress postAddress);
		void RemovePost(MetaWeblogPostAddress postAddress);
	}
}
