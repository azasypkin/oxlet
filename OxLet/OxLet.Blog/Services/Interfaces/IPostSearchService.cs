﻿using OxLet.Blog.Models;
using OxLet.Core.Models;

namespace OxLet.Blog.Services.Interfaces {
	public interface IPostSearchService {
		IPagedList<Post> GetPosts(int pageIndex, int pageSize, SearchCriteria criteria);
	}
}
