﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Cache;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Plugins.Interfaces;
using OxLet.Core.Secuity.Interfaces;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;
using OxLet.Core.Web.Extensions;

namespace OxLet.Blog.Services {
	public class PostService : IPostService {
		private readonly IPostRepository _repository;
		private readonly IRegularExpressionService _expressionService;
		private readonly IValidationService _validator;
		private readonly ITagService _tagService;
		private readonly ICommentService _commentService;
		private readonly IUserService _userService;
		private readonly ICacheService _cacheService;
		private readonly SiteContext _context;
		private readonly IPluginEngine _pluginEngine;

		public PostService(
			IPostRepository repository,
			ITagService tagService,
			ICommentService commentService,
			IValidationService validationService,
			IRegularExpressionService expressionService,
			ICacheService cacheService,
			IUserService userService,
			SiteContext siteContext,
			IPluginEngine pluginEngine) {

			_repository = repository;
			_validator = validationService;
			_tagService = tagService;
			_commentService = commentService;
			_expressionService = expressionService;
			_userService = userService;
			_cacheService = cacheService;
			_context = siteContext;
			_pluginEngine = pluginEngine;
		}

		#region IPostService Members

		public IPagedList<Post> GetPosts(Models.Blog blog, PagingInfo pagingInfo) {

			bool includeDrafts = _context.User.IsAuthenticated && _context.User.IsInRole("Administrator");
			IPagedList<Post> posts = _cacheService.GetItems<IPagedList<Post>, Post>(
				string.Format("Blog:{0}-GetPosts-IncludeDrafts:{1}", blog.Name, includeDrafts),
				() => includeDrafts ? _repository.GetPostsWithDrafts(blog, pagingInfo) : _repository.GetPosts(blog, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : new[] { blog },
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);
		
			if (!includeDrafts){
				posts = posts.Since(p => p.Published.Value, _context.HttpContext.Request.IfModifiedSince());
			}

			return posts.FillTags(_tagService).FillComments(_commentService).FillCreator(_userService);
		}

		public IPagedList<Post> GetPosts(MetaWeblogRecentPostsAddress address, PagingInfo pagingInfo) {

			bool includeDrafts = _context.User.IsAuthenticated && _context.User.IsInRole("Administrator");
			IPagedList<Post> posts = _cacheService.GetItems<IPagedList<Post>, Post>(
				string.Format("Blog:{0}-GetPosts-IncludeDrafts:{1}", address.Blog.Name, includeDrafts),
				() => includeDrafts ? _repository.GetPostsWithDrafts(address.Blog, pagingInfo) : _repository.GetPosts(address.Blog, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : new[] { address.Blog },
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);

			if (!includeDrafts) {
				posts = posts.Since(p => p.Published.Value, _context.HttpContext.Request.IfModifiedSince());
			}

			return posts.FillTags(_tagService, true).FillComments(_commentService).FillCreator(_userService);
		}

		public IPagedList<Post> GetPosts(Models.Blog blog, Language language, PagingInfo pagingInfo) {

			return _cacheService.GetItems<IPagedList<Post>, Post>(
				string.Format("Blog:{0}-GetPosts-Language:{1}", blog.Name, language.Id),
				() => _repository.GetPosts(blog, language, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : new[] { blog },
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			).FillTags(_tagService).FillComments(_commentService).FillCreator(_userService);
		}

		public IPagedList<Post> GetPosts(Models.Blog blog, Tag tag, PagingInfo pagingInfo) {

			return _cacheService.GetItems<IPagedList<Post>, Post>(
				string.Format("Blog:{0}-GetPosts-Tag:{1}", blog.Name, tag.Name),
				() => _repository.GetPosts(blog, tag, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : new[] { blog },
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			).FillTags(_tagService).FillComments(_commentService).FillCreator(_userService);
		}

		public IPagedList<Post> GetPosts(Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo) {
			return GetPosts(blog, searchCriteria, pagingInfo, _context.User.IsAuthenticated && _context.User.IsInRole("Administrator"));
		}

		public IPagedList<Post> GetPosts(Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo, bool includeDrafts) {
			return _cacheService.GetItems<IPagedList<Post>, Post>(
				string.Format("Blog:{0}-GetPosts-IncludeDrafts:{1}-Search:{2}", blog.Name, includeDrafts, searchCriteria.Term),
				() =>includeDrafts ? _repository.GetPostsWithDrafts(blog, searchCriteria, pagingInfo) : _repository.GetPosts(blog, searchCriteria, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : new[] { blog },
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			).FillTags(_tagService).FillComments(_commentService).FillCreator(_userService);
		}

		public IPagedList<Post> GetPosts(Models.Blog blog, ArchiveData archive,  PagingInfo pagingInfo) {

			return _cacheService.GetItems<IPagedList<Post>, Post>(
				string.Format("Blog:{0}-GetPosts-Year:{1}, Month:{2}, Day:{3}", blog.Name, archive.Year, archive.Month, archive.Day),
				() => _repository.GetPostsByArchive(blog, archive.Year, archive.Month, archive.Day, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : new[] { blog },
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			).FillTags(_tagService).FillComments(_commentService).FillCreator(_userService);
		}

		//TODO: (erikpo) Need to change the query to return back data about the posts so they can be added as cache dependencies
		public IEnumerable<DateTime> GetPostDateGroups(Models.Blog blog) {
			//return cache.GetItems<IEnumerable<DateTime>, DateTime>(
			//    "GetPostDateGroups",
			//    () => _repository.GetPostDateGroups(),
			//    null
			//    );
			return null;
		}

		public Post GetPost(Models.Blog blog, string postSlug) {
			return _cacheService.GetItem(
				string.Format("Blog:{0}-GetPost:{1}", blog.Name, postSlug),
				() => _repository.GetPost(blog, postSlug),
				p => p != null ? p.GetCacheDependencyItems() : new[] { blog }
			).FillTags(_tagService).FillComments(_commentService).FillCreator(_userService);
		}

		public Post GetPost(int id) {
			return _cacheService.GetItem(
				string.Format("GetPost:{0}", id),
				() => _repository.GetPost(id),
				p => p != null ? p.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
			).FillTags(_tagService).FillComments(_commentService).FillCreator(_userService);
		}

		public Post GetRandomPost(Models.Blog blog) {
			//TODO: (erikpo) Add caching

			//return _repository.GetRandomPost().FillTags(_tagService);
			return null;
		}

		//public ValidationStateDictionary ValidatePostInput(PostInput postInput) {
		//    return new ValidationStateDictionary {{typeof (PostInput), _validator.Validate(postInput)}};
		//}

		public ModelResult<Post> AddPost(Models.Blog blog, PostInput post, EntityState state) {
			return AddPost(
				blog,
				post,
				() => post.ToPost(blog, state, _context.User.GetDescription(), t => _expressionService.Clean("TagReplace", t))
			);
		}

		public ModelResult<Post> EditPost(Models.Blog blog, Post post, PostInput postInput, EntityState postState) {
			//ValidationStateDictionary validationState = ValidatePostInput(postInput);

			//if (!validationState.IsValid) {
			//    return new ModelResult<Post>(validationState);
			//}

			Post newPost;
			Post originalPost = post;

			newPost = originalPost.Apply(
					postInput,
					postState,
					_context.User.GetDescription(),
					t => _expressionService.Clean("TagReplace", t)
				);

			//ValidatePost(blog, newPost, originalPost, validationState);

			//if (!validationState.IsValid) {
			//    return new ModelResult<Post>(validationState);
			//}

			for (int i = 0; i < newPost.Tags.Count; i++) {
				newPost.Tags[i] = _tagService.GetTag(newPost.Tags[i].Name) ?? _tagService.AddTag(newPost.Tags[i]).Item;
			}

			using (var transaction = new TransactionScope()) {

				newPost = _repository.Save(newPost);

				InvalidateCachedPostForEdit(newPost, originalPost);

				transaction.Complete();
			}

			// signal that post has been added
			_pluginEngine.Fire("Post:AfterEdit", new object[] { _context, newPost });

			return new ModelResult<Post>(newPost, null);//validationState);
		}

		public void Remove(Post post) {
			if (post == null) {
				throw new ArgumentNullException("post");
			}

			using (var transaction = new TransactionScope()) {
				if (_repository.RemovePost(post)) {
					InvalidateCachedPostForRemove(post);
				}
				transaction.Complete();
			}

			// signal that post has been added
			_pluginEngine.Fire("Post:AfterRemove", new object[] { _context, post });
		}

		public void RemoveAll(Models.Blog blog) {
			_repository.RemoveAllPosts(blog);
		}

		public IEnumerable<KeyValuePair<ArchiveData, int>> GetArchives(Models.Blog blog) {
			var dependencies = new ICacheable[] {blog};
			return _cacheService.GetItems<IEnumerable<KeyValuePair<ArchiveData, int>>, KeyValuePair<ArchiveData, int>>(
				string.Format("GetArchives-Blog:{0}", blog.Id),
				() => _repository.GetArchives(blog),
				x => dependencies
			);
		}

		public Post GetPost(MetaWeblogPostAddress postAddress) {
			return _cacheService.GetItem(
				string.Format("GetPost:{0}", postAddress.PostId),
				() => _repository.GetPost(postAddress.PostId),
				p => p != null ? p.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
			).FillTags(_tagService, true).FillComments(_commentService).FillCreator(_userService);
		}

		public void RemovePost(MetaWeblogPostAddress postAddress) {

			var post = _repository.GetPost(postAddress.PostId);

			if (post == null) {
				throw new ArgumentNullException("post");
			}

			Remove(post);
		}

		#endregion

		#region Private Methods

		//private void ValidatePost(Models.Blog blog, Post newPost, ValidationStateDictionary validationState) {
		//    ValidatePost(blog, newPost, null, validationState);
		//}

		//private void ValidatePost(Models.Blog blog, Post newPost, Post originalPost, ValidationStateDictionary validationState) {
		//    var state = new ValidationState();

		//    validationState.Add(typeof(Post), state);

		//    Post foundPost = _repository.GetPost(blog, newPost.Slug);

		//    if (foundPost != null && blog.Id == foundPost.Blog.Id && newPost.Slug != originalPost.Slug) {
		//        state.Errors.Add("Post.SlugNotUnique", newPost.Slug, "A post already exists with the supplied blog and slug");
		//    }
		//}

		private ModelResult<Post> AddPost<T>(Models.Blog blog, T postInput, Func<Post> generatePost) {
			//var validationState = new ValidationStateDictionary {{typeof (T), _validator.Validate(postInput)}};

			//if (!validationState.IsValid) {
			//    return new ModelResult<Post>(validationState);
			//}

			Post post = generatePost();

			//ValidatePost(blog, post, validationState);

			//if (!validationState.IsValid) {
			//    return new ModelResult<Post>(validationState);
			//}

			for (int i = 0; i < post.Tags.Count; i++) {
				post.Tags[i] = _tagService.AddTag(post.Tags[i]).Item;
			}

			post = _repository.Save(post);

			InvalidateCachedPostDependencies(blog, post);

			// signal that post has been added
			_pluginEngine.Fire("Post:AfterAdd", new object[] { _context, post });

			return new ModelResult<Post>(post, null);
		}

		private void InvalidateCachedPostDependencies(Models.Blog blog, Post post) {
			_cacheService.InvalidateItem(post);
			_cacheService.InvalidateItem(blog);

			//post.Tags.ToList().ForEach(t => _cacheService.InvalidateItem(t));
		}

		private void InvalidateCachedPostForEdit(Post newPost, Post originalPost) {

			// invalidate any tags that have changed since the edit
			//originalPost.Tags.Except(newPost.Tags).Union(newPost.Tags.Except(originalPost.Tags)).ToList().ForEach(t => _cacheService.InvalidateItem(t));
			_cacheService.InvalidateItem(originalPost);
			_cacheService.InvalidateItem(newPost);
		}


		private void InvalidateCachedPostForRemove(Post postToRemove) {

			//postToRemove.Tags.Except(postToRemove.Tags).Union(postToRemove.Tags.Except(postToRemove.Tags)).ToList().ForEach(t => _cacheService.InvalidateItem(t));

			_cacheService.InvalidateItem(postToRemove);
		}

		#endregion

	}
}
