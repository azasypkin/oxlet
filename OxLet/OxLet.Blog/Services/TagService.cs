﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;

namespace OxLet.Blog.Services {
	public class TagService : ITagService {
		private readonly ITagRepository _repository;
		private readonly ICacheService _cacheService;

		public TagService(ITagRepository repository, ICacheService cacheService) {
			_repository = repository;
			_cacheService = cacheService;
		}

		public IList<Tag> GetTags(Models.Blog blog, bool includeServiceTags) {
			var dependencies = new ICacheable[] { blog };
			return _cacheService.GetItems<IList<Tag>, Tag>(
				string.Format("Blog:{0}-Tags-IncludeService:{1}", blog.Id, includeServiceTags),
				() => _repository.GetTags(includeServiceTags).ToList(),
				x => dependencies
			);
		}

		public IList<Tag> GetTags(Models.Blog blog) {
			return GetTags(blog, false);
		}

		public IList<KeyValuePair<Tag, int>> GetTagsWithPostCount(Models.Blog blog) {
			return GetTagsWithPostCount(blog, false);
		}

		public IList<KeyValuePair<Tag, int>> GetTagsWithPostCount(Models.Blog blog, bool includeServiceTags) {
			var dependencies = new ICacheable[] { blog };
			return _cacheService.GetItems<IList<KeyValuePair<Tag, int>>, KeyValuePair<Tag, int>>(
				string.Format("Blog:{0}-TagsWithCount-IncludeService:{1}", blog.Id, includeServiceTags),
				() => _repository.GetTagsWithPostCount(includeServiceTags).ToList(),
				x => dependencies
			);
		}

		public Tag GetTag(string name) {
			return _cacheService.GetItem(
				string.Format("TagByName:{0}", name),
				() => _repository.GetTag(name)
			);
		}

		public Tag GetTag(int id) {
			return _cacheService.GetItem(
				string.Format("TagById:{0}", id),
				()=>_repository.GetTag(id)
			);
		}

		public IList<Tag> GetTags(Post post) {
			return GetTags(post, false);
		}

		public IList<Tag> GetTags(Post post, bool includeServiceTags) {
			return _cacheService.GetItems<IList<Tag>, Tag>(
				string.Format("TagsByPost:{0}-IncludeService:{1}", post.Id, includeServiceTags),
				() => (includeServiceTags ? post.Tags.Select(x => GetTag(x.Id)) : post.Tags.Select(x => GetTag(x.Id)).Where(x=>!x.IsService)).ToList(),
				x => new[] { post }
			);
		}

		public ModelResult<Tag> AddTag(Tag tagInput) {

			var tag = _repository.SaveTag(tagInput);

			InvalidateCacheForTagAdd(tag);

			return new ModelResult<Tag>(tag, Enumerable.Empty<ModelValidationResult>());
		}

		private void InvalidateCacheForTagAdd(Tag tag) {
			_cacheService.InvalidateItem(tag);
			_cacheService.Invalidate(string.Format("TagById:{0}", tag.Id));
			_cacheService.Invalidate(string.Format("TagByName:{0}", tag.Name));
		}
	}
}
