﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Input;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Cache;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Validation;

namespace OxLet.Blog.Services {
	public class CommentService : ICommentService {

		private readonly ICommentRepository _repository;
		private readonly IValidationService _validator;
		private readonly ICacheService _cacheService;
		private readonly SiteContext _context;

		public CommentService(SiteContext context, ICommentRepository repository, IValidationService validator, ICacheService cacheService) {
			_repository = repository;
			_validator = validator;
			_context = context;
			_cacheService = cacheService;
		}

		public Comment GetComment(int id) {
			return _cacheService.GetItem(
				string.Format("CommentById:{0}", id),
				() => _repository.GetComment(id),
				x => x != null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
			);
		}

		public Comment GetComment(string blogName, string postSlug, string commentSlug) {
			return _cacheService.GetItem(
				string.Format("CommentBy-BlogName:{0}-PostSlug:{1}-CommentSlug{2}", blogName, postSlug, commentSlug),
				() => _repository.GetComment(blogName, postSlug, commentSlug),
				x => x != null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
			);
		}

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, bool includePending, bool sortDescending) {
			return _cacheService.GetItems<IPagedList<Comment>, Comment>(
				string.Format("GetComments-IncludePending:{0}-Descending:{1}", includePending, sortDescending),
				() => _repository.GetComments(pagingInfo, includePending, sortDescending),
				x => x != null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>(),
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);
		}

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, Models.Blog blog) {
			throw new NotImplementedException();
		}

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, Post post, bool includeUnapproved) {
			return _cacheService.GetItems<IPagedList<Comment>, Comment>(
				string.Format("GetComments-Post:{0}-IncludeUnapproved:{1}", post.Id, includeUnapproved),
				() => _repository.GetComments(pagingInfo, post, includeUnapproved),
				x => x != null ? x.GetCacheDependencyItems() : new [] {post},
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);
		}

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, Tag tag) {
			throw new NotImplementedException();
		}

		public IEnumerable<ModelValidationResult> ValidateCommentInput(CommentInput commentInput) {
			return _validator.Validate(commentInput);
		}

		public ModelResult<Comment> AddComment(Models.Blog blog, Post post, CommentInput commentInput) {

			IEnumerable<ModelValidationResult> validationResults = ValidateCommentInput(commentInput);

			if (!validationResults.IsValid()) {
				return new ModelResult<Comment>(validationResults);
			}

			EntityState commentState;

			try {
				commentState = _context.User.IsAuthenticated 
					? EntityState.Normal 
					: blog.CommentStateDefault;
			} catch {
				commentState = EntityState.PendingApproval;
			}

			Comment comment;

			using (var transaction = new TransactionScope()) {
				string commentSlug = GenerateUniqueCommentSlug(post);

				comment = commentInput.ToComment(commentState, post, commentSlug);

				comment = _repository.Save(comment, post.Blog.Name, post.Slug);

				InvalidateCacheForComment(comment);

				transaction.Complete();
			}

			return new ModelResult<Comment>(comment, validationResults);
		}

		public ModelResult<Comment> EditComment(Comment comment, CommentInput commentInput) {
			throw new NotImplementedException();
		}

		public bool RemoveComment(Comment comment) {
			return ChangeState(comment, EntityState.Removed);
		}

		public bool ApproveComment(Comment comment) {
			return ChangeState(comment, EntityState.Normal);
		}

		#region Private methods

		private string GenerateUniqueCommentSlug(Post post) {
			string commentSlug = null;
			bool isUnique = false;
			int i = 1;
			while (!isUnique) {
				commentSlug = (post.Comments.Count + i).ToString();

				Comment foundComment = GetComment(post.Blog.Name, post.Slug, commentSlug);

				isUnique = foundComment == null;

				i++;
			}

			return commentSlug;
		}

		private bool ChangeState(Comment comment, EntityState state) {
			bool commentStateChanged = false;

			using (var transaction = new TransactionScope()) {
				if (comment != null && comment.State != state) {
					_repository.ChangeState(comment.Id, state);

					commentStateChanged = _repository.GetComment(comment.Id).State == state;
				}

				InvalidateCacheForComment(comment);

				transaction.Complete();
			}

			return commentStateChanged;
		}

		private void InvalidateCacheForComment(Comment comment) {
			_cacheService.InvalidateItem(comment.Post);
			_cacheService.InvalidateItem(comment);
		}

		#endregion
	}
}
