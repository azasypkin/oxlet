﻿using System.Web.Mvc;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models;
using OxLet.Core.Models;
using OxLet.Core.Infrastructure;

namespace OxLet.Blog.Infrastructure {
	public class CommentVisitor : Visitor {
		private readonly UrlHelper _urlHelper;

		public CommentVisitor(UrlHelper urlHelper) {
			_urlHelper = urlHelper;
		}

		public string Visit(SiteContainer container, string dataFormat) {
			return _urlHelper.Posts();
		}

		public string Visit(Models.Blog blog, string dataFormat) {
			return _urlHelper.Comments(blog, dataFormat);
		}

		public string Visit(Tag tag, string dataFormat) {
			return _urlHelper.Comments(tag, dataFormat);
		}
	}
}
