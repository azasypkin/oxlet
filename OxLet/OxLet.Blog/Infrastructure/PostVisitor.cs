﻿using System.Web.Mvc;
using OxLet.Blog.Extensions;
using OxLet.Blog.Models;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;

namespace OxLet.Blog.Infrastructure {
	public class PostVisitor : Visitor {
		private readonly UrlHelper _urlHelper;

		public PostVisitor(UrlHelper urlHelper) {
			_urlHelper = urlHelper;
		}

		public string Visit(SiteContainer container) {
			return _urlHelper.Posts();
		}

		public string Visit(SearchPageContainer container) {
			return "Search";
		}

		public string Visit(Post post) {
			return _urlHelper.Post(post);
		}

		//public string Visit(PostPage postPage) {
		//    throw new System.NotImplementedException();
		//}

		public string Visit(Models.Blog blog) {
			return _urlHelper.Posts(blog.Name);
		}

		public string Visit(Models.Blog blog, string dataFormat) {
			return _urlHelper.Posts(blog, dataFormat);
		}

		public string Visit(Tag tag) {
			return _urlHelper.Posts(tag);
		}

		public string Visit(Tag tag, string dataFormat) {
			return _urlHelper.Posts(tag, dataFormat);
		}
	}
}
