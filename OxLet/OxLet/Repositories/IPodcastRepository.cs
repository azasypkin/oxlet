﻿using OxLet.Core.Models;
using OxLet.Models;

namespace OxLet.Repositories {
	public interface IPodcastRepository {
		IPagedList<Podcast> GetPodcasts(ContentItemPath path, PagingInfo pagingInfo);
	}
}