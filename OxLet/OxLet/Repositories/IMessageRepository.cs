﻿using OxLet.Core.Models;
using OxLet.Models;

namespace OxLet.Repositories {
	public interface IMessageRepository {

		/// <summary>
		/// Get a specific message
		/// </summary>
		/// <returns></returns>
		Message GetMessage(int id);

		/// <summary>
		/// Get all messages
		/// </summary>
		/// <param name="pagingInfo"></param>
		/// <param name="sortDescending"></param>
		/// <returns></returns>
		IPagedList<Message> GetMessages(PagingInfo pagingInfo, bool sortDescending);

		/// <summary>
		/// Save message
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		Message Save(Message message);
	}
}
