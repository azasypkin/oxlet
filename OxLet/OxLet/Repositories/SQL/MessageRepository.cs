﻿using System;
using System.Linq;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Data.SQL;
using OxLet.Models;

namespace OxLet.Repositories.SQL {
	public class MessageRepository : IMessageRepository {
		private readonly OxletDataContext _context;

		public MessageRepository(OxletDataContext context) {
			_context = context;
		}

		public IPagedList<Message> GetMessages(PagingInfo pagingInfo, bool sortDescending) {
			var query =
				from m in _context.oxlet_Messages
				join u in _context.oxlet_Users on m.CreatorUserId equals u.Id into ucr
				from uc in ucr.DefaultIfEmpty()
				select new {Message = m, User = uc};

			query = sortDescending
				? query.OrderByDescending(q => q.Message.CreatedDate)
				: query.OrderBy(q => q.Message.CreatedDate);

			return query.Select(x => ProjectMessage(x.Message, x.User)).GetPage(pagingInfo);

		}

		public Message GetMessage(int id) {
			return (
				from m in _context.oxlet_Messages
				join u in _context.oxlet_Users on m.CreatorUserId equals u.Id into ucr
				from uc in ucr.DefaultIfEmpty()
				where m.Id == id
				select ProjectMessage(m, uc)
			 ).FirstOrDefault();
		}

		public Message Save(Message message) {
			oxlet_Message messageToSave = null;

			if (message.Id != 0) {
				messageToSave = _context.oxlet_Messages.FirstOrDefault(m => m.Id == message.Id);
			}

			if (messageToSave == null) {
				messageToSave = new oxlet_Message {
					Id = message.Id != 0 ? message.Id : 0,
					CreatedDate = DateTime.UtcNow
				};

				_context.oxlet_Messages.InsertOnSubmit(messageToSave);
			}

			messageToSave.Body = message.Body;
			messageToSave.CreatorIp = message.CreatorIp;
			messageToSave.UserAgent = message.CreatorUserAgent;

			if (message.Creator != null && message.Creator.Id != 0) {
				messageToSave.CreatorUserId = message.Creator.Id;
			} else if (message.Creator != null) {
				messageToSave.CreatorName = message.Creator.Name;
				messageToSave.CreatorEmail = message.Creator.Email;
				messageToSave.CreatorUrl = message.Creator.Url;
			}

			_context.SubmitChanges();

			return GetMessage(messageToSave.Id);
		}

		#region Private methods

		private static Message ProjectMessage(oxlet_Message message, oxlet_User user) {
			return new Message(
				message.Id,
				user == null
					? new UserDescription(0) {
						Email = message.CreatorEmail,
						Name = message.CreatorName,
						Url = message.CreatorUrl
					}
					: new UserDescription(user.Id) { Email = user.Email, EmailHash = user.EmailHash, Name = user.Name, Url = user.Url },
				message.CreatorIp,
				message.UserAgent,
				message.Body,
				message.CreatedDate
			);
		}

		#endregion

	}
}
