﻿using System;
using System.Linq;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;
using OxLet.Data.SQL;
using OxLet.Models;

namespace OxLet.Repositories.SQL {
	public class PodcastRepository : IPodcastRepository {
		private readonly OxletDataContext _context;
		private readonly IContentItemRepository _contentItemRepository;

		public PodcastRepository(OxletDataContext context, IContentItemRepository contentItemRepository) {
			_context = context;
			_contentItemRepository = contentItemRepository;
		}

		public IPagedList<Podcast> GetPodcasts(ContentItemPath path, PagingInfo pagingInfo) {
			throw new NotImplementedException();
		}

		public IPagedList<Message> GetMessages(PagingInfo pagingInfo, bool sortDescending) {
			var query =
				from m in _context.oxlet_Messages
				join u in _context.oxlet_Users on m.CreatorUserId equals u.Id into ucr
				from uc in ucr.DefaultIfEmpty()
				select new {Message = m, User = uc};

			query = sortDescending
				? query.OrderByDescending(q => q.Message.CreatedDate)
				: query.OrderBy(q => q.Message.CreatedDate);

			return query.Select(x => ProjectMessage(x.Message, x.User)).GetPage(pagingInfo);

		}

		#region Private methods

		private static Message ProjectMessage(oxlet_Message message, oxlet_User user) {
			return new Message(
				message.Id,
				user == null
					? new UserDescription(0) {
						Email = message.CreatorEmail,
						Name = message.CreatorName,
						Url = message.CreatorUrl
					}
					: new UserDescription(user.Id) { Email = user.Email, EmailHash = user.EmailHash, Name = user.Name, Url = user.Url },
				message.CreatorIp,
				message.UserAgent,
				message.Body,
				message.CreatedDate
			);
		}

		#endregion

		
	}
}
