﻿using System.Web.Mvc;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Controllers {
	public class AboutController : Controller {
		//[OutputCache(Duration = 3600, VaryByParam = "none")]
		public OxletModel Index() {
			return new OxletModel {
				Container = new SiteContainer(0, "About", null)
			};
		}

		//[OutputCache(Duration = 3600, VaryByParam = "none")]
		public OxletModel About() {
			return new OxletModel {
				Container = new SiteContainer(0, "About", null)
			};
		}
	}
}
