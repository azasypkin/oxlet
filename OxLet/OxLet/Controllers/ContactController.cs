﻿using System.Text;
using System.Web.Mvc;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.ViewModels;
using OxLet.Models;
using OxLet.Models.Input;
using OxLet.Services;

namespace OxLet.Controllers {
	public class ContactController : Controller {

		private readonly SiteContext _siteContext;
		private readonly IMessageService _messageService;

		public ContactController(SiteContext siteContext, IMessageService messageService) {
			_siteContext = siteContext;
			_messageService = messageService;
		}

		public OxletModel Index() {
			return new OxletModelItem<MessageInput> {
				Item = new MessageInput {
					Name = _siteContext.User!= null ? _siteContext.User.Name : "",
					Email = _siteContext.User != null ? _siteContext.User.Email: "",
					EmailHash = _siteContext.User != null ? _siteContext.User.EmailHash : "",
					Url = _siteContext.User != null ? _siteContext.User.Url : "",
					IsRemember = Request.Form.IsTrue("MessageInput.Remember") || (_siteContext.User != null && !string.IsNullOrEmpty(_siteContext.User.Email)),
					Body = Request.Form["MessageInput.Body"] ?? ""
				}
			};
		}

		[HttpPost]
		public OxletModel Index(MessageInput input) {

			ModelResult<Message> addMessageResults = _messageService.AddMessage(input);

			if (!addMessageResults.IsValid) {
				ModelState.AddModelErrors(addMessageResults.ValidationResults);

				// check if it is not input validation error
				//if (!addMessageResults.ValidationResults.ContainsKey(typeof(MessageInput))) {
				//    input.MessageSendResult = false;
				//}
			} else {
				input.MessageSendResult = true;
			}

			return new OxletModelItem<MessageInput> {
				Item = input
			};
		}

		[HttpPost]
		public JsonResult SendMessage(MessageInput input) {

			ModelResult<Message> addMessageResults = _messageService.AddMessage(input);

			if (!addMessageResults.IsValid) {
				ModelState.AddModelErrors(addMessageResults.ValidationResults);

				// check if it is not input validation error
				//if(!addMessageResults.ValidationResults.ContainsKey(typeof(MessageInput))) {
				//    input.MessageSendResult = false;
				//}
			}
			else {
				input.MessageSendResult = true;
			}

			return new JsonResult() {
				ContentEncoding = Encoding.UTF8,
				Data = "{'message':'good'}"
			};
			//return new OxletModelItem<MessageInput> {
			//    Item = input
			//};
		}
	}
}
