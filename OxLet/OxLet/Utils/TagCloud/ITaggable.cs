﻿namespace OxLet.Utils.TagCloud {
	/// <summary>
	/// Represents item which may be used as an item in tag cloud
	/// </summary>
	public	interface ITaggable {
		string Tag { get; set; }
		int Weight { get; set; }
		string Controller { get; }
		string Action { get; }
	}
}