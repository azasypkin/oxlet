﻿namespace OxLet.Utils.TagCloud {

	public class ItemTaggable : ITaggable {
		public string Tag { get; set; }
		public int Weight { get; set; }

		public string Controller {get;set;}

		public string Action {get;set;}
	}
}