﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using OxLet.Utils.TagCloud;

namespace OxLet.Utils.Extensions {
	public static class HtmlHelperExtensions {

		private const string TagHtmlTemplate = "<span class=\"tag-{0}\">{1}</span>\r\n";

		public static string TagCloud<TModel>(this HtmlHelper<TModel> html, IEnumerable<ITaggable> taggables, int numberOfStyleVariations) {

			var sorted = taggables.Distinct().OrderBy(x => x.Tag);
			double min = sorted.Min(x => x.Weight);
			double max = sorted.Max(x => x.Weight);
			var distribution = (double)((max - min) / numberOfStyleVariations);

			var sb = new StringBuilder();
			sb.AppendFormat("<div class=\"tag-cloud\">");

			foreach (var x in sorted) {
				for (double i = min, j = 1; i < max; i += distribution, j++) {
					if (x.Weight >= i && x.Weight <= i + distribution) {
						string link = html.RouteLink(x.Tag, new { ID = x.Tag.Replace(" ", "-"), x.Controller, x.Action }, new { @class = "tag" }).ToHtmlString();
						sb.AppendFormat(TagHtmlTemplate, j, link);
						break;
					}
				}
			}

			sb.Append("</div>");
			return sb.ToString();
		}
	}
}