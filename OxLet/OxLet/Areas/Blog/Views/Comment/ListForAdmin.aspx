﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Comment>>" %>

<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="sections">
		<div class="primary">
			<h2 class="title"><%= OxLet.Resources.Views.Blog.Labels.Comment_RecentComments %></h2>
			<%= Html.PageState((IPagedList<Comment>)Model.List, OxLet.Resources.Views.Shared.Labels.PageStateTemplate) %>
			<% Html.RenderPartialFromSkin("CommentListMedium"); %>
			<%= Html.CommentListPager(
				(IPagedList<Comment>)Model.List,
				OxLet.Resources.Views.Shared.Labels.Pager_PreviousTemplate,
				OxLet.Resources.Views.Shared.Labels.Pager_NextTemplate
			) %>
		</div>
		<div class="secondary">
			<% 
				Html.RenderPartialFromSkin("SideBar"); %>
		</div>
	</div>
</asp:Content>
