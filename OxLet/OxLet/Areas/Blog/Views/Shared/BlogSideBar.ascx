﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Blog.ViewModels" %>
<%@ Import Namespace="OxLet.Core.Web.ViewModels" %>
<%
	Html.RenderPartial("Categories");
	Html.RenderPartial("Archives", new OxletModelPartial<ArchiveViewModel>(Model, Model.GetModelItem<ArchiveViewModel>()));
%>

