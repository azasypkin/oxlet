<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% ArchiveData archiveData = ((ArchiveContainer)Model.Container).ArchiveData; %>
<%= string.Format("{0} /", Html.Link(
	OxLet.Resources.Views.Blog.Labels.BreadCrumb_Posts,
	Url.Posts(),
	new {@class="crumblink"}
)) %>
<%= archiveData.Month > 0 ? Html.Link(archiveData.Year.ToString(), Url.Posts(archiveData.Year), new { @class = "crumblink" }) : archiveData.Year.ToString()%>
<%= archiveData.Month > 0
		? string.Format(" / {0}", archiveData.Day > 0 ? Html.Link(archiveData.ToDateTime().ToString("MMMM"), Url.Posts(archiveData.Year, archiveData.Month), new { @class = "crumblink" }) : archiveData.ToDateTime().ToString("MMMM")) 
	: "" %>
<%= archiveData.Day > 0 ? string.Format(" / {0}", archiveData.Day) : "" %>