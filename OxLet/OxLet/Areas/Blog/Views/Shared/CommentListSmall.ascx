<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Comment>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<%
	if (Model.List.Count > 0) { %><ul class="comments small">
	<%
	int counter = 0;
	foreach (Comment comment in Model.List) {
		var className = new StringBuilder("comment", 40);

		if (comment.Equals(Model.List.First())) { className.Append(" first"); }
		if (comment.Equals(Model.List.Last())) { className.Append(" last"); }

		if (Model.Context.User.HasAdminAccess()) { className.AppendFormat(" {0}", comment.State.ToString().ToLower()); }

		if (counter % 2 != 0) { className.Append(" odd"); }

		className.Append(comment.Creator == null
			? " anon"
			: string.Format(
				" {0} {1}",
				comment.Creator.Id == Model.Context.User.Id ? "author" : "user",
				comment.Creator.Name.CleanAttribute()
				)
			); 
	%>
	<li class="<%=className.ToString() %>">
		<div class="meta" id="<%=comment.GetSlug() %>">
			<%
			if (Model.Context.User.HasAdminAccess() && comment.State == EntityState.PendingApproval) { %>
				<span class="state" title="<%= OxLet.Resources.Views.Blog.Labels.Comment_PendingApproval %>">
					<%= OxLet.Resources.Views.Blog.Labels.Comment_PendingApproval%>
				</span>
			<% } %>
			<span class="name">
				<%=Html.LinkOrDefault(comment.Creator.Name.CleanText(), comment.Creator.Url.CleanHref()) %></span>
			<span class="when">-
				<%= 
					Html.Link(
						Html.ConvertToLocalTime(comment.Created.Value, Model).ToString("MMMM dd, yyyy - h:mm tt"), //todo: (nheskew) localize date format
						Model.Context.User.HasAdminAccess() && comment.State == EntityState.PendingApproval
							? Url.ManageComment(comment) //todo: (nheskew)need the route to work with a page of comments
							: Url.Comment(comment.Post, comment)
					) %>
			</span>
		</div>
		<div class="post"><%=comment.Post.Title.CleanText() %></div>
		<div class="text"><%=comment.Body.CleanText() %></div>
	</li>
	<% counter++; } %>
</ul>
<% } else { //todo: (nheskew) need an Html.Message html helper extension method that takes a message %>
<div class="message info">
	<%= OxLet.Resources.Views.Blog.Labels.NoneCommentFound %></div>
<% } %>