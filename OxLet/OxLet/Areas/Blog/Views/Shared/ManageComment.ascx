﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelPartial<OxLet.Blog.Models.Comment>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<% if (Model != null && Model.RootModel.Context.User.HasAdminAccess()) { %>
<div class="flags">
	<form class="flag remove" method="post" action="<%=Url.RemoveComment(Model.PartialModel.Post) %>">
	<fieldset>
		<input type="image" class="ibutton remove" src="<%=Url.ImagePath(Model.RootModel, "delete.png", false) %>" title="<%= OxLet.Resources.Views.Blog.Labels.Comment_Remove %>" />
		<input type="hidden" name="id" value="<%=Model.PartialModel.Id %>" />
		<input type="hidden" name="returnUri" value="<%=Request.Url.AbsoluteUri %>" />
		<%=Html.OxiletAntiForgeryToken(m => m.RootModel.AntiForgeryToken) %>
	</fieldset>
	</form>
	<% if (Model.PartialModel.State == EntityState.PendingApproval) { %>
		<form class="flag approve" method="post" action="<%=Url.ApproveComment(Model.PartialModel.Post) %>">
		<fieldset>
			<input type="image" class="ibutton approve" src="<%=Url.ImagePath(Model.RootModel, "accept.png", false) %>"
				title="<%= OxLet.Resources.Views.Blog.Labels.Comment_Approve %>" />
			<input type="hidden" name="id" value="<%=Model.PartialModel.Id %>" />
			<input type="hidden" name="returnUri" value="<%=Request.Url.AbsoluteUri %>" />
			<%=Html.OxiletAntiForgeryToken(m => m.RootModel.AntiForgeryToken) %>
		</fieldset>
		</form>
	<% } %>
</div>
<% } %>