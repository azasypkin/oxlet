<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%= string.Format("{0} / {1}", Html.Link(
	OxLet.Resources.Views.Blog.Labels.BreadCrumb_Posts,
	Url.Posts(),
	new { @class = "crumblink" }
), Model.Container.Name) %>
