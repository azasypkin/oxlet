﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelPartial<OxLet.Blog.Models.Comment>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%  
	if (Model.PartialModel != null) {
		Html.RenderPartial("ManageComment"); %>
	<div class="contents">
	<% if (Model.RootModel.Context.User.HasAdminAccess() && Model.PartialModel.State == EntityState.PendingApproval) { %>
		<span class="state" title="<%= OxLet.Resources.Views.Blog.Labels.Comment_PendingApproval %>">
			<%= OxLet.Resources.Views.Blog.Labels.Comment_PendingApproval %>
		</span>
	<% } %>
	<div class="name" id="<%=Model.PartialModel.GetSlug() %>">
		<div>
			<%=Html.LinkOrDefault(Html.Gravatar(Model.RootModel, Model.PartialModel.Creator, "48"), Model.PartialModel.Creator.Url.CleanHref(), new { @class = "avatar" })%>
		</div>
		<p class="comment">
			<strong><%=Html.LinkOrDefault(Model.PartialModel.Creator.Name.CleanText(), Model.PartialModel.Creator.Url.CleanHref())%></strong>
			<%= OxLet.Resources.Views.Blog.Labels.Comment_AuthorSaid %>
			<% Html.RenderPartial("CommentCreatorInfo"); %>
		</p>
	</div>
	<div class="text">
		<p>	<%=Model.PartialModel.Body.CleanCommentBody() %></p>
	</div>
</div>
<%
} %>