<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<Comment>>" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.ViewModels" %>
<%
	if (Model.List.Count > 0) { %><ul class="comments medium">
	<%
	int counter = 0;
	foreach (Comment postBaseAndComment in Model.List) {
		var className = new StringBuilder("comment", 40);

		if (postBaseAndComment.Equals(Model.List.First())) { className.Append(" first"); }
		if (postBaseAndComment.Equals(Model.List.Last())) { className.Append(" last"); }

		if (Model.Context.User.HasAdminAccess()) { className.AppendFormat(" {0}", postBaseAndComment.State.ToString().ToLower()); }

		if (counter % 2 != 0) { className.Append(" odd"); }

		className.Append(postBaseAndComment.Creator.Id == 0
			? " anon"
			: string.Format(
				" {0} {1}",
				postBaseAndComment.Creator.Id == postBaseAndComment.Post.Creator.Id ? "author" : "user",
				postBaseAndComment.Creator.Name.CleanAttribute()
				)
			); 
	%>
	<li class="<%=className.ToString() %>">
		<% Html.RenderPartial(
			"Comment",
			new OxletModelPartial<Comment> (Model, postBaseAndComment),
			ViewData
		); %>
	</li>
	<%
	 counter++;
	} %>
</ul>
<% 
} %>