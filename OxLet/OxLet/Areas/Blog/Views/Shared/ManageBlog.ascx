﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Resources.Views.Blog" %>

<% if(Model.Context.User.HasAdminAccess()) { %>
		<nav>
			<h3><%= Labels.ManageBlog_Title %></h3>
			<ul>
				<li><%=Html.Link(Labels.ManageBlog_DashBoard, Url.Admin())%></li>
				<li><%=Html.Link(Labels.ManageBlog_Setupblog, Url.Blog(Model.GetModelItem<Blog>()))%></li>
				<li><%=Html.Link(Labels.ManageBlog_NewPost, Url.AddPost(Model.GetModelItem<Blog>()))%></li>
			</ul>
		</nav>
<% } %>