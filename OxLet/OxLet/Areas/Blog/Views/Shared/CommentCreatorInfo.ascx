﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelPartial<OxLet.Blog.Models.Comment>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% if (Model.PartialModel.Created.HasValue) { %>
	<span class="date">
		<%= Html.Link(
			Html.ConvertToLocalTime(Model.PartialModel.Created.Value, Model.RootModel).ToString("MMMM dd, yyyy"),
			Url.Comment(Model.PartialModel.Post, Model.PartialModel)
 		) %>
	</span>
<% } %>