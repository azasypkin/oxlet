<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%= string.Format("{0} / {1}", Html.Link(
	OxLet.Resources.Views.Blog.Labels.BreadCrumb_Posts,
	Url.Posts(),
	new { @class = "crumblink" }
	), 
	Model.Item.Title.CleanText()
)%>
