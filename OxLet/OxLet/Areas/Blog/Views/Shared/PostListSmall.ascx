<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<% if (((IPagedList<Post>)Model.List).TotalItemCount > 0) { %>
	<ul class="posts small">
	<%
	int counter = 0;
	foreach (Post post in Model.List) {
		var className = new StringBuilder("post", 15);

		if (post.Equals(Model.List.First())) { className.Append(" first"); }
		if (post.Equals(Model.List.Last())) { className.Append(" last"); }

		if (counter % 2 != 0) { className.Append(" odd"); }
	%>
	<li class="<%=className.ToString() %>">
		<div class="meta">
			<span class="title">
				<%= Html.Link(post.Title.CleanText(), Url.Post(post)) %>
			</span> 
			<span class="comments">-
				<%= Html.Link(string.Format(
						"{0} ({1})",
						OxLet.Resources.Views.Blog.Labels.PostCommentsLower,
						post.Comments.Count
					),
					string.Format("{0}#comments", Url.Post(post))
				) %>
			</span>
		</div>
		<div class="info">
			<span class="posted"><%=post.Published.HasValue 
				? Html.ConvertToLocalTime(post.Published.Value).ToLongDateString()
				: OxLet.Resources.Views.Blog.Labels.Post_Status_Draft %>
			</span>
		</div>
	</li>
	<% counter++; } %>
</ul>
<% } else { //todo: (nheskew) need an Html.Message html helper extension method that takes a message %>
<div class="message info">
	<%= OxLet.Resources.Views.Blog.Labels.NonePostFound %></div>
<% } %>