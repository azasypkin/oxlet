﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Blog>>" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
	<%=Html.PageTitle(Model.Localize("Admin"), Model.Localize("Blog"), Model.Item.Id == 0 ? Model.Localize("Setup") : Model.Localize("Edit")) %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeading" runat="server">
	<%=(Model.Item.Id == 0 ? Model.Localize("Blog.Add", "Initial Blog Setup") : Model.Localize("Blog.Edit", "Edit Blog")) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<%=Html.ValidationSummary() %>
	<form action="" method="post" id="blogSettings">
	<%
		if (Model.Item.Id != 0) { %>
	<div>
		<%=Html.Hidden("blogId", Model.Item.Id) %></div>
	<%
	} %>
	<h3>
		<%=Model.Localize("Blog.Section.About", "What the blog is about") %></h3>
	<div>
		<%=Html.TextBox("blogDisplayName", m => m.Item.DisplayName, "Display Name", new { size = 60, @class = "text" })%></div>
	<div>
		<%=Html.TextArea("blogDescription", m => m.Item.Description, 4, 80, "Description")%></div>
	<h3>
		<%=Model.Localize("Blog.Section.Appearance", "How it should look") %></h3>
	<div>
		<%=Html.TextBox("skinDefault", m => m.Item.SkinDefault, "Skin Name", new { size = 20, @class = "text" })%></div>
	<h3>
		<%=Model.Localize("Blog.Section.Behavior", "How should it behave") %></h3>
	<div>
		<%=Html.TextBox("postEditTimeout", m => m.Item.PostEditTimeout, "Post Editable Timeperiod (in hours)", new { size = 6, @class = "text" })%>
		<span class="hint">Leave blank to always allow editing of post urls.</span></div>
	<div>
		<%=Html.CheckBox("commentingDisabled", m => m.Item.CommentingDisabled, "")%>
		<label for="commentingDisabled" class="checkbox">
			Commenting Disabled</label></div>
	<div>
		<label for="Item.CommentStateDefault">Post Comments Pending By Default</label>
		<%=Html.EnumDropDownListFor(m => m.Item.CommentStateDefault, new[]{EntityState.NotSet})%>		
	</div>
	<div>
		<%=Html.CheckBox("authorAutoSubscribe", m => m.Item.AuthorAutoSubscribe, "")%>
		<label for="authorAutoSubscribe" class="checkbox">
			Post Author Is Auto Subscribed</label></div>
	<div class="buttons">
		<input type="submit" name="submit" class="button submit" value="<%=Model.Item.Id == 0 ? Model.Localize("Blog.Create", "Create Blog") : Model.Localize("Blog.Edit", "Edit Blog") %>" />
		<%if (Model.Item.Id != 0) {%>
		<%=Html.Button(
                "cancel",
                Model.Localize("Cancel"),
                new { @class = "cancel", onclick = string.Format("if (window.confirm('{0}')){{window.document.location='{1}';}}return false;", Model.Localize("really?"), Url.ManageBlog()) }
                )%>
			<%=Html.OxiletAntiForgeryToken(m => m.AntiForgeryToken)%>
		<% } %>
	</div>
	</form>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopAside" runat="server">
	<% 	Html.RenderPartial("ManageBlog"); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="BottomAside" runat="server">
	<% Html.RenderPartial("BlogSideBar"); %>
</asp:Content>
