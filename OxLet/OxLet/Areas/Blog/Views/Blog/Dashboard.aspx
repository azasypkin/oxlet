<%@ Page Language="C#" MasterPageFile="/Views/Shared/OxLetSite.Master" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModel>" %>

<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Blog.ViewModels" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.ViewModels" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%=Html.PageTitle(OxLet.Resources.Views.Blog.Labels.Dashboard_Admin, OxLet.Resources.Views.Blog.Labels.Dashboard) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainHeading" runat="server">
	<%= OxLet.Resources.Views.Blog.Labels.Dashboard_Title %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<% var adminData = Model.GetModelItem<AdminDataViewModel>(); %>
	<div id="dashboard">
		<ul id="data">
			<li id="recentPosts">
				<h3><%= OxLet.Resources.Views.Blog.Labels.Dashboard_RecentPosts %></h3>
				<% Html.RenderPartial("PostListSmall", new OxletModelList<Post> { List = adminData.Posts, Context = Model.Context }); %>
				<% if (adminData.Posts.Count > 0) { %><div class="more">
					<%=Html.Link(OxLet.Resources.Views.Blog.Labels.Dashboard_AllPosts, Url.PostsWithDrafts())%></div>
				<% } %>
			</li>
			<li id="recentComments">
				<h3>
					<%= OxLet.Resources.Views.Blog.Labels.Dashboard_RecentComments %></h3>
				<% Html.RenderPartial("CommentListSmall", new OxletModelList<Comment> { List = adminData.Comments, Context = Model.Context }); %>
				<% if (adminData.Comments.Count > 0) { %><div class="more">
					<%=Html.Link(OxLet.Resources.Views.Blog.Labels.Dashboard_AllComments, Url.ManageComments())%></div>
				<% } %>
			</li>
		</ul>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="TopAside" runat="server">
	<% 	Html.RenderPartial("ManageBlog"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="BottomAside" runat="server">
	<% Html.RenderPartial("BlogSideBar"); %>
</asp:Content>
