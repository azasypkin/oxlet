﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Models.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%=Html.PageTitle(OxLet.Resources.Views.Blog.Labels.Posts_Search_Title) %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="HeadCustom" runat="server">
	<%
		Html.RenderFeedDiscoveryRss(string.Format("{0} (RSS)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
		Html.RenderFeedDiscoveryAtom(string.Format("{0} (ATOM)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
	//Html.RenderFeedDiscoveryRss("All Comments (RSS)", Url.Comments("RSS"));
	//Html.RenderFeedDiscoveryAtom("All Comments (ATOM)", Url.Comments("ATOM"));
    Html.RenderRsd();
    Html.RenderLiveWriterManifest(); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MetaKeywords" runat="server">
	<%= Html.PageKeywords(Model.List.SelectMany(x=>x.Tags.Select(y=>y.DisplayName)).Distinct().ToArray()) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MetaDescription" runat="server">
	<%= Html.PageDescription(
		string.Format(
			OxLet.Resources.Views.Blog.Labels.PostListBySearchMetaDescription,
			Model.Container.GetDisplayName()
		)
	) %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderPartial("JQueryScriptTag"); %>
	<% Html.RenderPartial("OxilLetScriptTag"); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeading" runat="server">
	<%= OxLet.Resources.Views.Blog.Labels.Posts_Search_Heading %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<form method="get" action="" class="search main">
		<div class="search-box">
			<fieldset>
				<label for="search_term"><%= OxLet.Resources.Views.Blog.Labels.Posts_Search_Hint %></label>
				<input id="search_term" name="Term" class="text" type="text" size="42" value="<%= Model.GetModelItem<SearchCriteria>().Term.CleanAttribute() %>" />
				<input class="button" type="submit" value="<%: OxLet.Resources.Views.Blog.Labels.Posts_Search_SubmitButton_Text %>" />
			</fieldset>
		</div>
	</form>
	<div class="main search">
		<%= Html.PageState((IPagedList<Post>)Model.List, OxLet.Resources.Views.Shared.Labels.PageStateTemplate) %>
		<% Html.RenderPartial("PostListMedium"); %>
		<%= Html.PostListBySearchPager(
			(IPagedList<Post>)Model.List,
			OxLet.Resources.Views.Shared.Labels.Pager_PreviousTemplate,
			OxLet.Resources.Views.Shared.Labels.Pager_NextTemplate
		) %>
	</div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TopAside" runat="server">
	<% 	Html.RenderPartial("ManageBlog"); %>
</asp:Content>

<%--<asp:Content ID="Content3" ContentPlaceHolderID="BottomAside" runat="server">
	<% Html.RenderPartial("BlogSideBar"); %>
</asp:Content>--%>
