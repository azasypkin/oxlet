<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<div class="admin buttons">
	<input type="submit" value="<%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Buttons_Save %>" class="button submit" tabindex="" />
	<input 
		type="button" 
		value="<%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Buttons_Cancel %>"
		class="button cancel" tabindex=""
		onclick="<%= string.Format("if (window.confirm('{0}')){{window.document.location='{1}';}}return false;",
				OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Buttons_CancelConfirmation,
				Model.Item != null ? Url.Post(Model.Item) : Url.BlogHome()
			) %>"/>
	<%--<%=Html.Button(
		"cancel",
		,
		new {
			@class = "cancel",
			onclick = 
		}
	)%>--%>
</div>
