﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Post>>" %>

<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<% ArchiveData archiveData = ((ArchiveContainer)Model.Container).ArchiveData; %>
	<%= Html.PageTitle(
		OxLet.Resources.Views.Blog.Labels.BreadCrumb_Posts,
		archiveData.Year.ToString(),
		archiveData.Month > 0 ? new DateTime(archiveData.Year, archiveData.Month, 1).ToString("MMMM") : null,
		archiveData.Day > 0 ? archiveData.Day.ToString() : null
	) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="HeadCustom" runat="server">
	<%
    Html.RenderFeedDiscoveryRss(string.Format("{0} (RSS)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
	Html.RenderFeedDiscoveryAtom(string.Format("{0} (ATOM)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
	//Html.RenderFeedDiscoveryRss("All Comments (RSS)", Url.Comments("RSS"));
	//Html.RenderFeedDiscoveryAtom("All Comments (ATOM)", Url.Comments("ATOM"));
    Html.RenderRsd();
    Html.RenderLiveWriterManifest(); %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderPartial("JQueryScriptTag"); %>
	<% Html.RenderPartial("OxilLetScriptTag"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaKeywords" runat="server">
	<%= Html.PageKeywords(Model.List.SelectMany(x=>x.Tags.Select(y=>y.DisplayName)).Distinct().ToArray()) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaDescription" runat="server">
	<% ArchiveData archiveData = ((ArchiveContainer)Model.Container).ArchiveData; %>
	<%= Html.PageDescription(
		string.Format(
			OxLet.Resources.Views.Blog.Labels.PostListByArchiveMetaDescription,
			string.Join(
				"/",
				(new[] {
					archiveData.Year.ToString(),
					archiveData.Month > 0 ? new DateTime(archiveData.Year, archiveData.Month, 1).ToString("MMMM") : null,
					archiveData.Day > 0 ? archiveData.Day.ToString() : null}
				).Where(x => x != null)
			)
		)
	) %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeading" runat="server">
	<% Html.RenderPartial("ArchiveBreadcrumb"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="blog">
		<%= Html.PageState((IPagedList<Post>)Model.List, OxLet.Resources.Views.Shared.Labels.PageStateTemplate) %>
		<% Html.RenderPartial("PostListMedium");%>
		<%= Html.PostArchiveListPager(
			(IPagedList<Post>)Model.List,
			OxLet.Resources.Views.Shared.Labels.Pager_PreviousTemplate,
			OxLet.Resources.Views.Shared.Labels.Pager_NextTemplate
		) %>
	</div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TopAside" runat="server">
	<% 	Html.RenderPartial("ManageBlog"); %>
</asp:Content>

<%--<asp:Content ID="Content3" ContentPlaceHolderID="BottomAside" runat="server">
	<% Html.RenderPartial("BlogSideBar"); %>
</asp:Content>
--%>