﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="/Views/Shared/OxLetSite.Master" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
	<%= Html.PageTitle(OxLet.Resources.Views.Blog.Labels.PostAdd_Title) %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeading" runat="server">
	<%= OxLet.Resources.Views.Blog.Labels.PostAdd_Title %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="post addPost" id="post"><% Html.RenderPartial("ItemEditForm"); %></div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadCssFiles">
	<% Html.RenderCssFile("jquery.css"); %>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ScriptVariablesPre">
	<script type="text/javascript">
		window.cssPath = "<%=Url.CssPath(Model) %>";
	</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Scripts">
	<% Html.RenderScriptTag("JQuery/jquery-ui-20081126-1.5.2.min.js"); %>
</asp:Content>
