﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<% if (Model.Context.User.HasAdminAccess()) { %>
<div class="admin manage buttons">
	<% if (Model.Item.State != EntityState.Removed) { %>
		<a href="<%=Url.EditPost(Model.Item) %>" title="<%= OxLet.Resources.Views.Blog.Labels.Post_Edit %>" class="ibutton edit">
			<%= Html.SkinImage("page_edit.png", OxLet.Resources.Views.Blog.Labels.Post_Edit, new { width = 16, height = 16 }, false)%>
		</a>
		<form class="remove post" method="post" action="<%=Url.RemovePost(Model.Item) %>">
			<fieldset>
				<input type="image" src="<%=Url.ImagePath(Model, "page_delete.png", false) %>" alt="<%=OxLet.Resources.Views.Blog.Labels.Post_Remove %>"
					title="<%= OxLet.Resources.Views.Blog.Labels.Post_Remove %>" class="ibutton image remove" />
				<%=Html.Hidden("returnUri", Url.Posts())%>
				<%=Html.OxiletAntiForgeryToken(m => m.AntiForgeryToken) %>
			</fieldset>
		</form>
	<% } %>
</div><% } %>