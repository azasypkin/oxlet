﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%=Html.PageTitle(OxLet.Resources.Views.Blog.Labels.Label)%>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaKeywords" runat="server">
	<%= Html.PageKeywords(Model.List.SelectMany(x=>x.Tags.Select(y=>y.DisplayName)).Distinct().ToArray()) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaDescription" runat="server">
	<%= Html.PageDescription(OxLet.Resources.Views.Blog.Labels.PostListMetaDescription) %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="HeadCustom" runat="server">
	<%
		Html.RenderFeedDiscoveryRss(string.Format("{0} (RSS)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
		Html.RenderFeedDiscoveryAtom(string.Format("{0} (ATOM)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
	//Html.RenderFeedDiscoveryRss("All Comments (RSS)", Url.Comments("RSS"));
	//Html.RenderFeedDiscoveryAtom("All Comments (ATOM)", Url.Comments("ATOM"));
    Html.RenderRsd();
    Html.RenderLiveWriterManifest(); %>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderPartial("JQueryScriptTag"); %>
	<% Html.RenderPartial("OxilLetScriptTag"); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainHeading" runat="server">
	<%= OxLet.Resources.Views.Blog.Labels.BreadCrumb_Posts %>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="blog main">
		<%= Html.PageState((IPagedList<Post>)Model.List, OxLet.Resources.Views.Shared.Labels.PageStateTemplate)%>
		<% Html.RenderPartial("PostListMedium"); %>
		<%= Html.PostListPager(
			(IPagedList<Post>)Model.List,
			OxLet.Resources.Views.Shared.Labels.Pager_NextTemplate,
			OxLet.Resources.Views.Shared.Labels.Pager_PreviousTemplate
		) %>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopAside" runat="server">
	<% 	Html.RenderPartial("ManageBlog"); %>
</asp:Content>

<%--<asp:Content ID="Content4" ContentPlaceHolderID="BottomAside" runat="server">
	<% Html.RenderPartial("BlogSideBar"); %>
</asp:Content>--%>

