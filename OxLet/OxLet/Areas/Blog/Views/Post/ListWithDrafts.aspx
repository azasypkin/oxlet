﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ID="Content6" ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderPartial("JQueryScriptTag"); %>
	<% Html.RenderPartial("OxilLetScriptTag"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="sections">
		<div class="primary">
			<%= Html.PageState((IPagedList<Post>)Model.List, OxLet.Resources.Views.Shared.Labels.PageStateTemplate) %>
			<% Html.RenderPartial("PostListMedium");%>
			<%= Html.PostListPager(
				(IPagedList<Post>)Model.List,
				OxLet.Resources.Views.Shared.Labels.Pager_NextTemplate,
				OxLet.Resources.Views.Shared.Labels.Pager_PreviousTemplate
			) %>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopAside" runat="server">
	<% 	Html.RenderPartial("ManageBlog"); %>
</asp:Content>

<%--<asp:Content ID="Content4" ContentPlaceHolderID="BottomAside" runat="server">
	<% Html.RenderPartial("BlogSideBar"); %>
</asp:Content>
--%>