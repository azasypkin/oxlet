﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%
	bool urlIsLocked = Model.Item != null
		&& Model.Item.State == EntityState.Normal
		&& Model.Item.Published.HasValue
		&& Model.Item.Published.Value.AddHours(24) < DateTime.UtcNow;
%>
<div class="admin metadata">
	<ul>
		<li class="input slug">
		<% if (ViewData.ModelState.ContainsKey("Post.Slug")) { %>
			<%= Html.ValidationMessage("Post.Slug", OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Slug_ValidationMessage)%>
		<% } else { %>
			<label for="post_slug"><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Slug%></label>
		<% } %>
			<%=Html.TextBox(
				"slug",
				Request["slug"] ?? (Model.Item != null ? Model.Item.Slug : ""),
				new { id = "post_slug", @class = "text", size = "72", title = OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Slug_Hint },
				!urlIsLocked
			) %>
		</li>
		<% if (!urlIsLocked) { %>
		<li class="input draft">
			<%= Html.RadioButton(
				"isPublished",
				false,
				!(Model.Item != null && Model.Item.Published.HasValue) || Request.Form.IsTrue("isPublished"),
				new { id = "post_stateDraft" }
			) %>
			<label for="post_stateDraft" class="radio"><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_PostDraftState %></label>
		</li>
		<% } %>
		<li class="input publish">
			<fieldset>
				<legend><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Publish_Legend %></legend>
				<%= Html.RadioButton(
					"isPublished",
					true,
					(Model.Item != null && Model.Item.Published.HasValue) || Request.Form.IsTrue("isPublished"),
					new { id = "post_statePublished" },
					!urlIsLocked
				)%>
				<label for="post_statePublished" class="radio">
					<%= Model.Item != null && Model.Item.Published.HasValue && Html.ConvertToLocalTime(Model.Item.Published.Value, Model) < DateTime.UtcNow 
						? OxLet.Resources.Views.Blog.Labels.PostEdit_Form_PostPublishedState 
						: OxLet.Resources.Views.Blog.Labels.PostEdit_Form_PostPrePublishedState 
					%>
				</label>
				<label for="post_published"><%=OxLet.Resources.Views.Blog.Labels.PostEdit_Form_PublishDate%></label>
				<%= Html.ValidationMessage("Post.Published", OxLet.Resources.Views.Blog.Labels.PostEdit_Form_PublishDate_ValidationMessage) %>
				<%= Html.TextBox(
					"published",
					Model.Item != null && Model.Item.Published.HasValue && Model.Item.Published.Value > default(DateTime) 
						? Model.Item.Published.Value.ToStringForEdit()
						: "",
					new { id = "post_published", @class = "text date", size = "22", title = OxLet.Resources.Views.Blog.Labels.PostEdit_Form_PublishDate_Hint },
					!urlIsLocked
				) %>
			</fieldset>
		</li>
		<li class="input allowComments">
			<%= Html.CheckBox("commentingDisabled", m => m.Item != null && m.Item.CommentingDisabled, null, new { @class = "checkbox" })%>
			<label for="commentingDisabled" class="checkbox"><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_CommentingDisabled %></label>
		</li>
	</ul>
	<% Html.RenderPartial("ItemEditButtons"); %>
</div>
