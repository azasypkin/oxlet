﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>


<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%=Html.PageTitle(OxLet.Resources.Views.Blog.Labels.Label, Model.Item.DisplayName)%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="HeadCustom" runat="server">
	<%
		Html.RenderFeedDiscoveryRss(string.Format("{0} (RSS)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
		Html.RenderFeedDiscoveryAtom(string.Format("{0} (ATOM)", Model.Context.Site.DisplayName), "http://feeds.feedburner.com/AlehZasypkin");
	%>
	<link rel="canonical" href="<%=Url.AbsolutePath(Url.Post(Model.Item)) %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaKeywords" runat="server">
	<%=Html.PageKeywords(Model.Item.Tags.Select(t => t.DisplayName).Union(new[]{Model.Item.Title}).ToArray()) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaDescription" runat="server">
	<%=Html.PageDescription(Model.Item.GetBodyShort()) %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderPartial("JQueryScriptTag"); %>
	<% Html.RenderPartial("JQueryValidateScriptTag"); %>
	<% Html.RenderPartial("OxilLetScriptTag"); %>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHeading" runat="server">
	<% Html.RenderPartial("PostBreadcrumb"); %><% Html.RenderPartial("ManagePost"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="blog">
		<article class="post">
			<header role="banner">
				<div class="metadata">
					<div class="posted">
						<%= Html.Published(
							Model.Item,
							OxLet.Resources.Views.Blog.Labels.Post_Status_Removed,
							OxLet.Resources.Views.Blog.Labels.Post_Status_Draft
						)%>
					</div>
					<% if (Model.Item.Tags.Count > 0) { %>
						<%= OxLet.Resources.Views.Blog.Labels.PostTags %>
						<%=Html.UnorderedList(Model.Item.Tags, (t, i) => Html.Link(t.Name.CleanText(), Url.Posts(t), new { rel = "tag" }), "tags") %>
					<% } %>
				</div>
			</header>
			<div style="clear: both;">
			</div>
			<%= Model.Item.Body %>
			<footer>
				<% Html.RenderScriptTag("http://feeds.feedburner.com/~s/AlehZasypkin?i=" + Url.AbsolutePath(Url.Post(Model.Item))); %>
			</footer>
		</article>
		
		<% if (!(Model.Item.CommentingDisabled && Model.Item.Comments.Count < 1)) {
				Html.RenderPartial("Comments");
			}

	 if (Model.Item.CommentingDisabled) { %>
				<div class="message"> <%= OxLet.Resources.Views.Blog.Labels.Comment_CommentingDisabled %> </div>
		<% } %>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="TopAside" runat="server">
	<% 	Html.RenderPartial("ManageBlog"); %>
</asp:Content>

<%--<asp:Content ID="Content2" ContentPlaceHolderID="BottomAside" runat="server">
	<% Html.RenderPartial("BlogSideBar"); %>
</asp:Content>--%>

<asp:Content ContentPlaceHolderID="ScriptVariablesPre" runat="server">
	<script type="text/javascript">
		<% Html.RenderScriptVariable("computeHashPath", Url.ComputeHash()); %>
	</script>
</asp:Content>
