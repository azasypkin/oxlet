<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.ViewModels" %>
<channel>
	<title><%=Html.PageTitle(Model.Container.Name) %></title>
	<description><%=Model.Context.Site.Description %></description>
	<link><%=Url.AbsolutePath(Url.Container(Model.Container)) %></link>
	<language><%=Model.Context.Site.DefaultLanguage %></language>
	<image>
		<url><%=Url.AbsolutePath(Url.AppPath(Model.Context.Site.FavIconUrl.Replace(".ico", ".png")))%></url>
		<title><%=Model.Context.Site.DisplayName%></title>
		<link><%=Url.AbsolutePath(Url.Container(Model.Container)) %></link>
		<width>32</width>
		<height>32</height>
	</image>
	<% foreach (Post post in Model.List) {
		Html.RenderPartialFromSkin("RssChannelItem", new OxletModelPartial<Post>(Model, post));
	} %>
</channel>
