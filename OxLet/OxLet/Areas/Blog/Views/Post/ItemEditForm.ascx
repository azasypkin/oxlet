﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<form method="post" action="">
<%--<div class="avatar"><%=Html.Gravatar("48") %></div>--%>
<h2 class="title">
	<label for="post_title"><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Title %></label>
	<%=Html.ValidationMessage("Post.Title", OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Title_ValidationMessage)%>
	<%=Html.TextBox(
		"title",
		Request["title"] ?? (Model.Item != null ? Model.Item.Title : ""),
		new { id = "post_title", @class = "text", size = "60", title = OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Title_Hint }
	) %>
	<%=Html.OxiletAntiForgeryToken(m => m.AntiForgeryToken) %>
</h2>
<% Html.RenderPartial("ItemEditPrimaryMetadata"); %>
<div class="content">
	<label for="post_bodyShort"><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Excerpt %></label>
	<%=Html.ValidationMessage("Post.BodyShort", OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Excerpt_ValidationMessage)%>
	<%=Html.TextArea(
		"bodyShort",
		Request["bodyShort"] ?? ( Model.Item != null ? Model.Item.BodyShort : ""),
		6 /*rows*/,
		120 /*cols*/,
		new { id = "post_bodyShort", title = OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Excerpt_Hint }
	) %>
	<label for="post_body"><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Body %></label>
	<%= Html.ValidationMessage("Post.Body", OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Body_ValidationMessage)%>
	<%= Html.TextArea(
		"body",
		Request["body"] ?? (Model.Item != null ? Model.Item.Body : ""),
		24 /*rows*/,
		120 /*cols*/,
		new { id = "post_body", title = OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Body_Hint, @class = "html" }
	) %>
</div>
<% Html.RenderPartial("ItemEditSecondaryMetadata"); %>
</form>
