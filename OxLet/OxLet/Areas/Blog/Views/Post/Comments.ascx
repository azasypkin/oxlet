﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Blog.Models.Input" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.ViewModels" %>
<section id="comments">
<% 
	string statusClass = "status";
	if (Model.Item.Comments.Count < 1) { statusClass = statusClass + " empty"; }
%>
	<div class="<%=statusClass %>">
		<h3><%= string.Format(
			"{0} ({1})",
			OxLet.Resources.Views.Blog.Labels.PostCommentsLower,
			Model.Item.Comments.Count
		)%></h3>
		<div><a href="#comment"><%= OxLet.Resources.Views.Blog.Labels.Post_LeaveYourOwn %></a></div>
	</div>
	<% Html.RenderPartial("CommentListMedium", new OxletModelList<Comment> { List = Model.Item.Comments, Container = Model.Container, Context = Model.Context, SkinName = Model.SkinName, AntiForgeryToken = Model.AntiForgeryToken }); %>
	<% if (!Model.Item.CommentingDisabled) {
		if (Model.Context.User != null && Model.Context.User.IsAuthenticated) {
			Html.RenderPartial("CommentFormAuthenticated",
				new OxletModelItem<CommentInput> {
					AntiForgeryToken = Model.AntiForgeryToken,
					Container = Model.Item,
					Context = Model.Context,
					Item = new CommentInput {
						Body = Request.Form["Body"] ?? ""
					},
					Navigation = Model.Navigation,
					SkinName = Model.SkinName,
					ValidationService = Model.ValidationService
				}
			);
		} else {
			Html.RenderPartial("CommentFormAnonymous", 
				new OxletModelItem<CommentInput> {
					AntiForgeryToken = Model.AntiForgeryToken,
					Container = Model.Item,
					Context = Model.Context,
					Item = new CommentInput {
						Name = Request.Form["Name"] ?? (Model.Context.User != null ? Model.Context.User.Name : ""),
						Email = Request.Form["Email"] ?? (Model.Context.User != null ? Model.Context.User.Email : ""),
						Url = (Request.Form["Url"] ?? (Model.Context.User != null ? Model.Context.User.Url : "")),
						IsRemember = Request.Form.IsTrue("IsRemember") || (Model.Context.User != null && !string.IsNullOrEmpty(Model.Context.User.Email)),
						Body = Request.Form["Body"] ?? "",
						EmailHash = Request.Form["EmailHash"] ?? (Model.Context.User != null ? Model.Context.User.EmailHash : "")
					},
					Navigation = Model.Navigation,
					SkinName = Model.SkinName,
					ValidationService = Model.ValidationService
				}
			);
		}
	} %>
</section>
