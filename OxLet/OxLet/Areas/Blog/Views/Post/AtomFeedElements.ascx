<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.ViewModels" %>
<title type="html"><%=Html.PageTitle(Model.Container.Name) %></title>
<icon><%=Url.AbsolutePath(Url.AppPath(Model.Context.Site.FavIconUrl)) %></icon>
<logo><%=Url.AbsolutePath(Url.AppPath(Model.Context.Site.FavIconUrl.Replace(".ico", ".png"))) %></logo>
<updated><%=XmlConvert.ToString(Model.List.First().Published.Value, XmlDateTimeSerializationMode.Utc) %></updated>
<% if (!string.IsNullOrEmpty(Model.Context.Site.Description)) { %>
	<subtitle type="html"><%=Html.Encode(Model.Context.Site.Description)%></subtitle>
<% } %>
<id><%=Context.Request.Url.ToString().ToLower() %></id>
<link rel="alternate" type="text/html" hreflang="<%=Model.Context.Site.DefaultLanguage %>" href="<%=Url.Container(Model.Container).ToLower() %>"/>
<link rel="self" type="application/atom+xml" href="<%=Context.Request.Url.ToString() %>"/>
<generator uri="<%=Url.OxiLet() %>" version="1.0">Oxite</generator>
<% foreach (Post post in Model.List) {
	Html.RenderPartialFromSkin("AtomFeedEntry", new OxletModelPartial<Post>(Model, post));
}%>