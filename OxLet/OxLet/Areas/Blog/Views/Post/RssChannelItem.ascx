<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelPartial<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%
	Post post = Model.PartialModel;
	string postUrl = Url.AbsolutePath(Url.Post(post)); 
%>
<item>
	<dc:creator><%=Html.Encode(post.Creator.Name)%></dc:creator>
	<title><%=Html.Encode(post.Title)%></title>
	<description><%=Html.Encode(post.BodyShort)%></description>
	<link><%=postUrl %></link>
	<guid isPermaLink="true"><%=postUrl %></guid>
	<pubDate><%=post.Published.Value.ToStringForFeed()%></pubDate><%
	foreach (Tag tag in post.Tags)
	{ %>
		<category><%=tag.Name %></category><%
	} %>
</item>