<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelPartial<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% Post post = Model.PartialModel;
	string postUrl = Url.AbsolutePath(Url.Post(post));
%>
<entry>
	<title type="html"><%=Html.Encode(post.Title)%></title>
	<link rel="alternate" type="text/html" href="<%=postUrl %>"/>
	<id><%=postUrl %></id>
	<updated><%=XmlConvert.ToString(post.Modified.Value, XmlDateTimeSerializationMode.Utc)%></updated>
	<published><%=XmlConvert.ToString(post.Published.Value, XmlDateTimeSerializationMode.Utc)%></published>
	<author>
		<name><%=Html.Encode(post.Creator.Name)%></name>
	</author>
	<% foreach (Tag tag in post.Tags) { %>
		<category term="<%=tag.Name %>" />
	<% } %>
	<content type="html" xml:lang="<%=Model.Context.Site.DefaultLanguage %>">
		<%=Html.Encode(post.BodyShort)%>
	</content>
</entry>