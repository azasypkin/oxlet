﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Post>>" %>
<div class="admin metadata">
	<ul class="single">
		<li class="input tags">
			<label for="post_tags"><%= OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Tags%></label>
			<%=Html.ValidationMessage("Post.Tags", OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Tags_ValidationMessage)%>
			<%= Html.TextBox(
				"tags", 
				Model.Item != null ? string.Join(", ", Model.Item.Tags.Select((tag, name) => tag.Name).ToArray()) : string.Empty,
				new { id = "post_tags", @class = "text", size = "60", title = OxLet.Resources.Views.Blog.Labels.PostEdit_Form_Tags_Hint }
			) %>
		</li>
	</ul>
	<% Html.RenderPartial("ItemEditButtons"); %>
</div>
