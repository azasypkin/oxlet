﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Input.CommentInput>>" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% using (Html.BeginRouteForm("AddCommentToPost", new {slug = ((Post)Model.Container).Slug}, FormMethod.Post, new {@id = "comment", @class="user"})) {%>
<div class="avatar"><%=Html.Gravatar("48") %></div>
	<fieldset class="comment">
		<legend><%=OxLet.Resources.Views.Blog.Labels.Comment_Form_Body_Legend%></legend>
		<div>
			<%= Html.LabelFor(x => x.Item.Body, OxLet.Resources.Views.Blog.Labels.Comment_Form_Body_Label)%>
			<%= Html.TextAreaFor(x => x.Item.Body, new {
				title = OxLet.Resources.Views.Blog.Labels.Comment_Form_Body_Hint,
				@class = "text mandatory authed",
				tabindex = "1"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Body)%>
		</div>
		<div class="submit">
			<input type="submit" value="<%= OxLet.Resources.Views.Blog.Labels.Comment_Form_SubmitButton_Text %>" id="comment_submit" class="submit button" />
			<%=Html.OxiletAntiForgeryToken(m => m.AntiForgeryToken) %>
		</div>
	</fieldset>
<% } %>

