﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Blog.Models.Input.CommentInput>>" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% using (Html.BeginRouteForm("AddCommentToPost", new {slug = ((Post)Model.Container).Slug}, FormMethod.Post, new {@id = "comment"})) {%>
	<% if (Request.QueryString.Get("pending") == bool.TrueString) { %>
		<div class="message info">
		<%= OxLet.Resources.Views.Blog.Labels.Comment_CommentWillWaitForApproval %></div>
	<% } %>
	<fieldset class="info">
		<legend><%= OxLet.Resources.Views.Blog.Labels.Comment_Form_Title %></legend>
		<p class="gravatarhelp"><%= Html.Link(OxLet.Resources.Views.Blog.Labels.Comment_Form_GravatarLabel, "http://en.gravatar.com/site/signup", new { rel = "noindex,nofollow"})%></p>
		<div id="comment_grav"><%= Html.Gravatar("48") %></div>
		<div class="name">
			<%= Html.LabelFor(x => x.Item.Name, OxLet.Resources.Views.Blog.Labels.Comment_Form_Name)%>
			<%= Html.TextBoxFor(x => x.Item.Name, new {
				title =  OxLet.Resources.Views.Blog.Labels.Comment_Form_Name_Hint,
				@class = "text mandatory",
				tabindex = "1"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Name)%>
		</div>
		<div class="email">
			<%= Html.LabelFor(x => x.Item.Email, OxLet.Resources.Views.Blog.Labels.Comment_Form_Email)%>
			<%= Html.TextBoxFor(x => x.Item.Email, new {
				title =  OxLet.Resources.Views.Blog.Labels.Comment_Form_Email_Hint,
				@class = "text",
				tabindex = "2"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Email)%>
		</div>
		<div class="url">
			<%= Html.LabelFor(x => x.Item.Url, OxLet.Resources.Views.Blog.Labels.Comment_Form_URL)%>
			<%= Html.TextBoxFor(x => x.Item.Url, new {
				title =  OxLet.Resources.Views.Blog.Labels.Comment_Form_URL_Hint,
				@class = "text",
				tabindex = "3"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Url)%>
		</div>
		<div class="remember">
			<%= Html.CheckBoxFor(
				x=>x.Item.IsRemember,
				new {id = "comment_remember", tabindex = "5"}
			)%>
			<%= Html.LabelFor(x => x.Item.IsRemember, OxLet.Resources.Views.Blog.Labels.Comment_Form_Remember)%>
		</div>
		<div class="submit">
			<input type="submit" value="<%= OxLet.Resources.Views.Blog.Labels.Comment_Form_SubmitButton_Text %>" id="comment_submit" class="submit button" tabindex="6" />
		</div>
	</fieldset>
	<fieldset class="comment">
		<legend><%=OxLet.Resources.Views.Blog.Labels.Comment_Form_Body_Legend%></legend>
		<div>
			<%= Html.LabelFor(x => x.Item.Body, OxLet.Resources.Views.Blog.Labels.Comment_Form_Body_Label)%>
			<%= Html.TextAreaFor(x => x.Item.Body, new {
				title = OxLet.Resources.Views.Blog.Labels.Comment_Form_Body_Hint,
				@class = "text mandatory",
				tabindex = "4"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Body)%>
		</div>
	</fieldset>
	<%=Html.OxiletAntiForgeryToken(m => m.AntiForgeryToken) %>
<% } %>
