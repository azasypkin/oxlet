﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Blog.Models.Post>>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<feed xmlns="http://www.w3.org/2005/Atom">
	<% Html.RenderPartialFromSkin("AtomFeedElements", Model); %>
</feed>
