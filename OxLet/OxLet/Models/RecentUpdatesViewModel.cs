﻿using System.Collections.Generic;
using OxLet.Blog.Models;
using OxLet.Core.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Models {
	public class RecentUpdatesViewModel {
		public RecentUpdatesViewModel(IPagedList<RecentUpdate> recentUpdates, IList<Post> posts) {
			RecentUpdates = recentUpdates;
			Posts = posts;
		}

		public IPagedList<RecentUpdate> RecentUpdates { get; private set; }

		public IList<Post> Posts { get; private set; }
	}
}
