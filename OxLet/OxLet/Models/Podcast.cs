﻿using System;
using System.Collections.Generic;
using OxLet.Core.Models;
using OxLet.Core.Models.Projections;

namespace OxLet.Models {
	public class Podcast : ContentItem {
		public Podcast(int id, int duration) : base(id) {
			Duration = duration;
		}

		//public Podcast(int id, string name, string displayName, string slug, ContentItemProjection parent, ContentItemResource value, ContentItemResource thumbnail, DateTime created, DateTime modified, DateTime? published, EntityState state, Language language, bool isFolder, UserDescription creator, IEnumerable<ContentItemCommentProjection> comments, IEnumerable<ContentItemProjection> children, int duration) 
		//    : base(id, name, displayName, slug, parent, value, thumbnail, created, modified, published, state, language, isFolder, creator, comments, children) {
		//    Duration = duration;
		//}

		public int Duration { get; private set; }

	}
}