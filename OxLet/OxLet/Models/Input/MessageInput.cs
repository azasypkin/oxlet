﻿using System;
using OxLet.Core.Models;

namespace OxLet.Models.Input {
	public class MessageInput{

		// service fields
		public bool? MessageSendResult { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string EmailHash { get; set; }
		public string Url { get; set; }
		public string Body { get; set; }
		public bool IsRemember { get; set; }
		public long CreatorIp { get; set; }
		public string CreatorUserAgent { get; set; }
		public Message ToMessage() {
			return new Message(
				0,
				new UserDescription(0) { Name = Name, Email = Email, Url = Url },
				CreatorIp,
				CreatorUserAgent,
				Body,
				DateTime.MinValue
			);
		}
	}
}