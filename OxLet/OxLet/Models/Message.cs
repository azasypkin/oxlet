﻿using System;
using OxLet.Core.Models;

namespace OxLet.Models {
	public class Message : EntityBase {
		public Message(int id, UserDescription creator, long creatorIp, string userAgent, string body, DateTime created) : base(id) {
			Creator = creator;
			CreatorIp = creatorIp;
			CreatorUserAgent = userAgent;
			Body = body;
			Created = created;
		}

		public UserDescription Creator { get; private set; }
		public long CreatorIp { get; private set; }
		public string CreatorUserAgent { get; private set; }
		public string Body { get; private set; }
		public DateTime Created { get; private set; }
	}
}
