﻿using System.Collections.Generic;
using System.Web.Mvc;
using OxLet.Core.Models;
using OxLet.Models;
using OxLet.Models.Input;

namespace OxLet.Services {
	public interface IMessageService {
		Message GetMessage(int id);

		IPagedList<Message> GetMessages(PagingInfo pagingInfo, bool sortDescending);

		IEnumerable<ModelValidationResult> ValidateMessageInput(MessageInput messageInput);

		ModelResult<Message> AddMessage(MessageInput messageInput);
	}
}
