﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Transactions;
using System.Web.Mvc;
using OxLet.Core.Email;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Validation;
using OxLet.Models;
using OxLet.Models.Input;
using OxLet.Repositories;

namespace OxLet.Services {
	public class MessageService : IMessageService {

		private readonly IMessageRepository _repository;
		private readonly IValidationService _validator;
		private readonly SiteContext _context;
		private readonly IEmailSender<EmailMessageInfo> _emailSender;

		private const string MessageEmailFormat = @"
			<table border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%;border-collapse:collapse;'>
			<tr><td width='100%' valign='top' style='width:100.0%;background:#4F81BD; padding:0in 5.4pt 0in 5.4pt;'>
				<span style='font-family:'Times New Roman','serif'; color:white; font-size:100px;'>Message from {site} site</span>
			</td></tr></table>
			<p></p>
			<table border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100%;border-collapse:collapse;'>
				<tr><td width='10%' valign='top' style='border:solid #78C0D4 1.0pt; border-right:dashed windowtext 1.0pt;background:#D2EAF1;padding:0in 5.4pt 0in 5.4pt;'>
				<span>Name</span></td>
				<td width='89%' valign='top' style='border:solid #78C0D4 1.0pt; border-left:none;background:#D2EAF1;padding:0in 5.4pt 0in 5.4pt;'>
				<span>{name}</span></td></tr>
				<tr><td width='10%' valign='top' style='border-top:none;border-left: solid #78C0D4 1.0pt;border-bottom:solid #78C0D4 1.0pt;border-right:dashed windowtext 1.0pt; padding:0in 5.4pt 0in 5.4pt;'>
				<span>Email</span></td>
				<td width='89%' valign='top' style='border-top:none;border-left: none;border-bottom:solid #78C0D4 1.0pt;border-right:solid #78C0D4 1.0pt; padding:0in 5.4pt 0in 5.4pt;'>
				<span><a href='mailto:{email}'>{email}</a></span></td></tr>
				<tr><td width='10%' valign='top' style='border:solid #78C0D4 1.0pt; border-right:dashed windowtext 1.0pt;background:#D2EAF1;padding:0in 5.4pt 0in 5.4pt;'>
				<span>Url</span></td>
				<td width='89%' valign='top' style='border:solid #78C0D4 1.0pt; border-left:none;background:#D2EAF1;padding:0in 5.4pt 0in 5.4pt;'>
				<span><a href='{url}'>{url}</a></span></td></tr>
				<tr><td width='10%' valign='top' style='border-top:none;border-left: solid #78C0D4 1.0pt;border-bottom:solid #78C0D4 1.0pt;border-right:dashed windowtext 1.0pt; padding:0in 5.4pt 0in 5.4pt;'>
				<span>Browser</span></td>
				<td width='89%' valign='top' style='border-top:none;border-left: none;border-bottom:solid #78C0D4 1.0pt;border-right:solid #78C0D4 1.0pt; padding:0in 5.4pt 0in 5.4pt;'>
				<span>{userAgent}</span></td></tr>
				<tr><td width='10%' valign='top' style='border:solid #78C0D4 1.0pt; border-right:dashed windowtext 1.0pt;background:#D2EAF1;padding:0in 5.4pt 0in 5.4pt;'>
				<span>IP</span></td>
				<td width='89%' valign='top' style='border:solid #78C0D4 1.0pt; border-left:none;background:#D2EAF1;padding:0in 5.4pt 0in 5.4pt;'>
				<span>{ip}</span></td></tr>
				<tr><td width='10%' valign='top' style='border-top:none;border-left: solid #78C0D4 1.0pt;border-bottom:solid #78C0D4 1.0pt;border-right:dashed windowtext 1.0pt; padding:0in 5.4pt 0in 5.4pt;'>
				<span>Message</span></td>
				<td width='89%' valign='top' style='border-top:none;border-left: none;border-bottom:solid #78C0D4 1.0pt;border-right:solid #78C0D4 1.0pt; padding:0in 5.4pt 0in 5.4pt;'>
				<span>{message}</span></td></tr></table>";

		public MessageService(SiteContext context, IMessageRepository repository, IValidationService validator, IEmailSender<EmailMessageInfo> emailSender) {
			_repository = repository;
			_validator = validator;
			_context = context;
			_emailSender = emailSender;
		}

		public Message GetMessage(int id) {
			return _repository.GetMessage(id);
		}

		public IPagedList<Message> GetMessages(PagingInfo pagingInfo, bool sortDescending) {
			return _repository.GetMessages(pagingInfo, sortDescending);
		}

		public IEnumerable<ModelValidationResult> ValidateMessageInput(MessageInput messageInput) {
			return  _validator.Validate(messageInput);
		}

		public ModelResult<Message> AddMessage(MessageInput messageInput) {
			var validationResults = ValidateMessageInput(messageInput);

			if (!validationResults.IsValid()) {
				return new ModelResult<Message>(validationResults);
			}

			Message message = null;
			try {
				using (var transaction = new TransactionScope()) {
					message = messageInput.ToMessage();
					message = _repository.Save(message);
					transaction.Complete();
				}
				SendMessageToEmail(message);
			}
			catch (Exception exception) {
				Trace.TraceError(exception.Message);
			}
			return new ModelResult<Message>(message, validationResults);
		}

		private bool SendMessageToEmail(Message message) {
			IDictionary<EmailMessageInfo, Exception> result = _emailSender.SendEmail(
				new[] {
					new EmailMessageInfo {
						BodyText = MessageEmailFormat.FormatWithObject(
							new {
								site = _context.Site.DisplayName,
								name = message.Creator.Name,
								email = message.Creator.Email,
								url = message.Creator.Url,
								userAgent = message.CreatorUserAgent,
								ip = IPAddress.Parse(message.CreatorIp.ToString()).ToString(),
								message = message.Body
							}
						),
						Encoding = Encoding.UTF8,
						Recipients = new[] {
							new EmailContact {
								Address = "oxycommail@gmail.com"
							}
						},
						ReplyTo = new[] {
							new EmailContact {
								Address = "oxycommail@gmail.com"
							}
						},
						Sender = new EmailContact {
							Address = "oxycommail@gmail.com",
							DisplayName = "OxiLet.com Contact form"
						},
						Subject = string.Format("Message from {0} site from {1}", _context.Site.DisplayName, message.Creator.Name)
					}
				}
			);

			return !result.Any(x => x.Value != null);
		}
	}
}
