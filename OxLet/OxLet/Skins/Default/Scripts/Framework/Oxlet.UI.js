﻿/* #region SectionExpander */

(function ($) {

	$.Oxlet("Oxlet.UI.SectionExpander", {

		apply: function () {
			if (this.option('isCollapsed')) {
				section = $('section.expandable');
				$('span.switch', section).addClass('collapsed').removeClass('expanded');
				$('div.expandable-content', section).slideToggle();
			} else {
				$('span.switch', 'section.expandable').addClass('expanded').removeClass('collapsed');
			}

			$('h2:first', 'section.expandable').click(function () {
				section = $(this).parent();
				$('div.expandable-content', section).slideToggle();
				$('span.switch', section).toggleClass('expanded').toggleClass('collapsed');
			});
		}
	});

})(jQuery);

$['Oxlet.UI']['SectionExpander'].defaults = {
	isCollapsed: false
};

/* #endregion */
