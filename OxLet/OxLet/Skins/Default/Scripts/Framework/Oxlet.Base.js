﻿jQuery.Oxlet = function(name, prototype) {
    // Get name parts
    var nameParts = name.split(".");
    // Take name
    name = nameParts[nameParts.length - 1];
	var namespace = nameParts.slice(0,-1).join(".");

	// create widget constructor
	$[namespace] = $[namespace] || {};
	
	// Set constructor
	$[namespace][name] = function(options) {
		var self = this;
		this.namespace = namespace;
		this.widgetName = name;
		
		// Set widgets options		
		this.options = $.extend({},
			$.Oxlet.defaults,
			$[namespace][name].defaults,
			options
		);
			
		// Initialization
		self._init();
		
		// return new instance
		return self;
	};
 
	// add widget prototype
	$[namespace][name].prototype = $.extend({}, $.Oxlet.prototype, prototype);
};

$.Oxlet.prototype = {
	// Need to ovverride in inherotors
	_init: function () { },

	// Operate with options
	option: function (key, value) {
		var options = key, self = this;

		if (typeof key == "string") {
			if (value === undefined) {
				return this._getData(key);
			}
			options = {};
			options[key] = value;
		}

		// if key it an dictionary of key\value pairs
		$.each(options, function (key, value) {
			self._setData(key, value);
		});
	},

	// Get data item
	_getData: function (key) {
		return this.options[key];
	},

	// Set data item
	_setData: function (key, value) {
		this.options[key] = value;
	},

	enable: function () {
		this._setData('disabled', false);
	},

	disable: function () {
		this._setData('disabled', true);
	},
};

$.Oxlet.defaults = {
	disabled: false
};
