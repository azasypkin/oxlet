﻿/// <reference path="../JQuery/jquery-1.4.4-vsdoc.js" />
/// <reference path="../JQuery/jquery.validate-vsdoc.js" />
/// <reference path="../JQuery/jquery.validate.unobtrusive.js" />

jQuery.validator.addMethod("regexallowempty", function (value, element, params) {
	var match;
	if (!value) {
		return true;
	}

	match = new RegExp(params).exec(value);
	return (match && (match.index === 0) && (match[0].length === value.length));
});

jQuery.validator.unobtrusive.adapters.addSingleVal("regexallowempty", "pattern");