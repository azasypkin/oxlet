﻿/// <reference path="../JQuery/jquery-1.4.4-vsdoc.js" />
window._emailRegex = /^[a-z0-9]+([-+\.]*[a-z0-9]+)*@[a-z0-9]+([-\.][a-z0-9]+)*$/i;

/** field hinting **/
$(document).ready(function () {
	$("form").each(function () { $(":text[title],:textarea[title]", this).filter(":enabled").each(function () { $(this).hintify() });});
});
$.extend(jQuery.expr[":"], {
	textarea: function (a) { return $.nodeName(a, 'textarea'); }
});

/*** gravatar fetch and alt change ***/
$(document).ready(function () {
	$('#Item_Email').blur(function () {
		var email = $(this).val();
		if (email.indexOf("@") > 0 && window._emailRegex.test(email)) {
			$.post(window.computeHashPath, { value: email }, function (emailHash) { if (emailHash && emailHash.length < 50) { $('#comment_grav').changeGravatarSrcTo(emailHash); } });
		} else {
			var emailHash = $('#comment_hashedEmail') ? $('#comment_hashedEmail').val() : "";
			$('#comment_grav').changeGravatarSrcTo(emailHash);
		}
	});
	$('#Item_Name').blur(function () {
		$('#comment_grav').changeGravatarAltTo($(this).val());
	});
});
$.fn.extend({
	changeGravatarSrcTo: function (emailHash) {
		var gravatar = $(this).find("img.gravatar");

		var gravatarUrlParts = gravatar.attr("src").split("?");
		var gravatarPathParts = gravatarUrlParts[0].split("/");

		gravatarPathParts[gravatarPathParts.length - 1] = emailHash;

		gravatar.attr("src", gravatarPathParts.join("/") + "?" + gravatarUrlParts[1]);
	},
	changeGravatarAltTo: function (name) {
		var gravatar = $(this).find("img.gravatar");
		if ($.trim(name) !== "") {
			gravatar.attr("title", name + " (gravatar)");
		} else {
			gravatar.attr("title", "(gravatar)");
		}
	}
});

/** username in the login form gets focus on load **/
$(document).ready(function () {
	$("#login_username").focus();
});

/** archives **/
$(document).ready(function () {
	$('.archives ul.yearList li.previous').each(function () {
		$(this).click(function (ev) {
			if (!ev || $(ev.target).not("a").size()) {
				$(this).toggleClass("open");
				$(this).find("h4>span").toggle();
				$(this).children("ul").toggle();
			}
		});

		$(this).hoverClassIfy();
	});
});

/** list item highlighting - just comma seperate additional selectors for now because we like to try to make the browser work **/
$(document).ready(function () {
	$("ul.small li.comment,ul.small li.post,ul.medium li.comment.pendingapproval,ul.medium li.comment.normal").each(function () {
		$(this).hoverClassIfy();
		$(this).clickClassIfy();
	});
});
$.fn.extend({
	hoverClassIfy: function () {
		$(this).mouseover(function () {
			$(this).addClass("hover");
		});

		$(this).mouseout(function () {
			$(this).removeClass("hover");
		});

		return this;
	},
	clickClassIfy: function () {
		$(this).click(function (ev) {
			if (!($(ev.target).is("a"))) {
				$(this).toggleClass("active");
			}
		});
	}
});

/** flags **/
$(document).ready(function () {
	/* removal */
	$("form.remove.post").submit(function () {
		return window.confirm('really?');
	});
	$("form.flag.remove").submit(function () {
		var form = $(this);
		var comment = $(this).offsetParent("li.comment");
		comment.fadeTo(350, .4);
		$.ajax({
			type: "POST",
			url: this.action,
			data: { id: this.id.value, __AntiForgeryTicks: this.__AntiForgeryTicks.value },
			success: function () {
				comment.animate({ height: 0, opacity: 0, marginTop: 0, marginBottom: 0, paddingTop: 0, paddingBottom: 0 }, 200); form = comment = 0;
			},
			error: function () { comment.fadeTo(350, 1); form = comment = 0; }
		});
		return false;
	});
	/* approval */
	$("form.flag.approve").submit(function () {
		var form = $(this);
		var markers = $(".approve,.state", $(this).offsetParent("li.comment"));
		markers.fadeTo(350, .4);
		$.ajax({
			type: "POST",
			url: this.action,
			data: { id: this.id.value, __AntiForgeryTicks: this.__AntiForgeryTicks.value },
			success: function () {
				markers.hide(200); markers = 0;
			},
			error: function () { markers.fadeTo(350, .1); markers = 0; }
		});
		return false;
	});
});

/** highlight anchored element **/
$(document).ready(function () {
	var hash = window.location.hash;
	if (hash) {
		$(hash).each(function () { $(this).highlight() });
	}
});

/* really, really simple implementation. some todos:
- listen to all hashed hrefs on the page so the highlight can change
- make some time to think of other todos :P */
$.fn.extend({
	highlight: function (highlightColor) {
		this.addClass("highlight");
	}
});

/** add/edit post **/
$(document).ready(function () {
	if ($("#post_published").is(":enabled")) {
		$("#post_published").change(function () {
			$("#post_statePublished").attr("checked", "checked");
		});
		$("#post_statePublished").focus(function () {
			$("#post_published").focus();
			if ($.trim($("#post_published").val()) === "") {
				var date = new Date();
				$("#post_published").val(date.toShortString());
			}
			$("#post_published").blur();
		});
		$("#post_published").datepicker({
			duration: "",
			dateFormat: "yy/mm/dd '8:00 AM'",
			showOn: "button",
			buttonImage: window.cssPath + "/images/calendar.png",
			buttonImageOnly: true,
			closeAtTop: false,
			isRTL: true
		});
	};

	$("input[name='postState']").change(function () {
		if ($("#post_statePublished").is(":checked")) {
			$("#post_published").addClass("active");
		} else {
			$("#post_published").removeClass("active");
		}
	});

	$("#post_title").change(function () {
		$("#post_slug").slugify($(this).val());
	});

	$.fn.extend({
		slugify: function (string) {
			if (!this.is(":enabled"))
				return;

			slug = $.trim(string);

			if (slug && slug !== "") {
				var cleanReg = new RegExp("[^A-Za-z0-9-]", "g");
				var spaceReg = new RegExp("\\s+", "g");
				var dashReg = new RegExp("-+", "g");

				slug = slug.replace(spaceReg, '-');
				slug = slug.replace(dashReg, "-");
				slug = slug.replace(cleanReg, "");

				if (slug.length * 2 < string.length) {
					return "";
				}

				if (slug.Length > 100) {
					slug = slug.substring(0, 100);
				}
			}

			this.val(slug);
		}
	});
});

Date.prototype.toShortString = function () {
	var y = this.getYear();
	var year = y % 100;
	year += (year < 38) ? 2000 : 1900;
	return (this.getMonth() + 1).toString() + "/" + this.getDate() + "/" + year + " " + this.toLocaleTimeString();
};

/** site settings icon picker **/
//$(document).ready(function () {
//	$("form#siteSettings span.hint.icons img").each(function () {
//		$(this).click(function () {
//			$("#favIconUrl").val($(this).attr("title"));
//			$(this).siblings(".selected").removeClass("selected");
//			$(this).addClass("selected");
//		});
//		$(this).hoverClassIfy();
//	});
//});


/*
* Description (jQuery Plugin) v0.1.0
*
* Copyright 2010, de Groot Software
* Date: 11 september 2010
*
* Last update: 11 november 2010
*/

//(function ($) {
//	$.fn.description = function (options) {
//		// build main options before element iteration
//		var settings = $.extend({}, $.fn.description.defaults, options);

//		// iterate and reformat each matched element
//		return this.each(function () {
//			var $this = $(this);

//			// store the description in the input
//			$this.data("description", settings.text);

//			// set the first parent div to relative to calculate the position from
//			$this.parents("div:first").css({ position: "relative" });

//			// calculate position
//			var marginTop = parseInt($this.css("margin-top"))
//			var marginLeft = parseInt($this.css("margin-left"));
//			var top = $this.position().top + ($this.outerHeight() - $this.height()) / 2 + (marginTop > 0 ? marginTop - 1 : 0);
//			var left = $this.position().left + ($this.outerWidth() - $this.width()) / 2 + (marginLeft > 0 ? marginLeft - 1 : 0);

//			// setup the div which is going to cover the input
//			$("<div />").attr("id", $this.attr("id") + "-description")
//                        .attr("class", settings.descriptionClass)
//                        .css({
//                        	position: "absolute",
//                        	top: top,
//                        	left: left,
//                        	paddingTop: $this.css("padding-top"),
//                        	paddingRight: $this.css("padding-right"),
//                        	paddingBottom: $this.css("padding-bottom"),
//                        	paddingLeft: $this.css("padding-left"),
//                        	backgroundColor: "transparent",
//                        	display: "none"
//                        })
//                        .text($this.data("description"))
//                        .insertAfter($this);


//			var $description = $("#" + $this.attr("id") + "-description");

//			// show the description if the input is empty
//			if ($this.val() == "")
//				$description.show();

//			$description.click(function () {
//				$description.hide();
//				$this.focus();
//			});

//			$this.focus(function (e) {
//				// in the case that the input "magically" got focus
//				$description.hide();
//			}).blur(function () {
//				// fill the input with its description if its empty on blur
//				if ($this.val() == "")
//					$description.show();
//			});
//		});
//	};

//	// defaults
//	$.fn.description.defaults = {
//		text: "No description entered ...",
//		descriptionClass: "description"
//	};
//})(jQuery);

(function ($) {
	$.fn.hintify = function (options) {
		// build main options before element iteration
		var settings = $.extend({}, $.fn.hintify.defaults, options);

		// iterate and reformat each matched element
		return this.each(function () {
			var $this = $(this);

			// store the description in the input
			$this.data("hint", $this.attr(settings.hintTextAttribute) || settings.text);

			// set the first parent div to relative to calculate the position from
			$this.parents("div:first").css({ position: "relative" });

			// calculate position
			var marginTop = parseInt($this.css("margin-top"))
			var marginLeft = parseInt($this.css("margin-left"));
			var top = $this.position().top + ($this.outerHeight() - $this.height()) / 2 + (marginTop > 0 ? marginTop - 1 : 0);
			var left = $this.position().left + ($this.outerWidth() - $this.width()) / 2 + (marginLeft > 0 ? marginLeft - 1 : 0);

			// setup the div which is going to cover the input
			$("<div />").attr("id", $this.attr("id") + "-hint")
				.attr("class", settings.hintClass)
				.css({
					position: "absolute",
					top: top,
					left: left,
					paddingTop: $this.css("padding-top"),
					paddingRight: $this.css("padding-right"),
					paddingBottom: $this.css("padding-bottom"),
					paddingLeft: $this.css("padding-left"),
					backgroundColor: "transparent",
					display: "none"
				})
				.text($this.data("hint"))
				.insertAfter($this);


			var $hint = $("#" + $this.attr("id").replace(/(:|\.)/g,'\\$1') + "-hint");

			// show the description if the input is empty
			if ($this.val() == "")
				$hint.show();

			$hint.click(function () {
				$hint.hide();
				$this.focus();
			});

			$this.focus(function (e) {
				// in the case that the input "magically" got focus
				$hint.hide();
			}).blur(function () {
				// fill the input with its description if its empty on blur
				if ($this.val() == "")
					$hint.show();
			});
		});
	};

	// defaults
	$.fn.hintify.defaults = {
		text: "No description entered ...",
		hintClass: "hinted",
		hintTextAttribute: "title"
	};
})(jQuery);

(function ($) {
	$.fn.showLoading = function (options) {
		// build main options before element iteration
		var settings = $.extend({}, $.fn.showLoading.defaults, options);

		// iterate and reformat each matched element
		return this.each(function () {
			var $this = $(this);

			// get id of the loading screen
			var id = $this.attr("id") + "-loading";

			// check if loading screen already created
			if($("#" + id.replace(/(:|\.)/g, '\\$1')).length) {
				$("#" + id.replace(/(:|\.)/g, '\\$1')).show();
			} else{
					// set the first parent div to relative to calculate the position from
				$this.parents("div:first").css({ position: "relative" });

				// calculate position
				var marginTop = parseInt($this.css("margin-top"))
				var marginLeft = parseInt($this.css("margin-left"));
				var top = $this.position().top + ($this.outerHeight() - $this.height()) / 2 + (marginTop > 0 ? marginTop - 1 : 0);
				var left = $this.position().left + ($this.outerWidth() - $this.width()) / 2 + (marginLeft > 0 ? marginLeft - 1 : 0);
				var width = $this.width();
				var height = $("fieldset", $this).height();
			
				// setup the div which is going to cover the input
				$("<div />")
					.attr("id", id)
					.attr("class", settings.loadingClass)
					.css({
						position: "absolute",
						top: top,
						left: left,
						paddingTop: $this.css("padding-top"),
						paddingRight: $this.css("padding-right"),
						paddingBottom: $this.css("padding-bottom"),
						paddingLeft: $this.css("padding-left"),
						display: "none",
						width:width,
						height:height,
						cursor:"wait"
					})
					.insertAfter($this)
					.show();
			}
		});
	};

	// defaults
	$.fn.showLoading.defaults = {
		loadingClass:"loading"
	};

	$.fn.hideLoading = function (options) {
		// build main options before element iteration
		var settings = $.extend({}, $.fn.hideLoading.defaults, options);

		// iterate and reformat each matched element
		return this.each(function () {
			var $this = $(this);

			// get id of the loading screen
			var id = $this.attr("id") + "-loading";

			// check if loading screen already created
			if($("#" + id.replace(/(:|\.)/g, '\\$1')).length) {
				$("#" + id.replace(/(:|\.)/g, '\\$1')).hide();
			}
		});
	};

	// defaults
	$.fn.hideLoading.defaults = {};

})(jQuery);

