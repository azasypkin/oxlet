﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;
using OxLet.Models.Input;

namespace OxLet.Validators {
	public class MessageInputValidator : ModelValidatorBase<MessageInput> {

		#region Fields

		private const string LocalizationStore = "MessagesValidationMessages";

		#endregion

		#region IValidator Members

		public MessageInputValidator(ILocalizationService localizationService, IRegularExpressionService regularExpressionService) : base(localizationService, regularExpressionService) {
		}

		public override IEnumerable<ModelValidationResult> Validate(MessageInput message) {
			if (message == null) {
				throw new ArgumentNullException("message");
			}

			if (string.IsNullOrEmpty(message.Body)) {
				yield return new ModelValidationResult {
					MemberName = "Item.Body",
					Message = LocalizationService.GetPhrase(LocalizationStore, "Form_Body_ValidationMessage").Value
				};
			}

			if (string.IsNullOrEmpty(message.Name)) {
				yield return new ModelValidationResult {
					MemberName = "Item.Name",
					Message = LocalizationService.GetPhrase(LocalizationStore, "Form_Name_ValidationMessage").Value
				};
			}

			if (string.IsNullOrEmpty(message.Email) || !RegularExpressionService.IsMatch("IsEmail", message.Email)) {
				yield return new ModelValidationResult {
					MemberName = "Item.Email",
					Message = LocalizationService.GetPhrase(LocalizationStore, "Form_Email_ValidationMessage").Value
				};
			}

			if (!string.IsNullOrEmpty(message.Url) && !RegularExpressionService.IsMatch("IsUrl", message.Url)) {
				yield return new ModelValidationResult {
					MemberName = "Item.Url",
					Message = LocalizationService.GetPhrase(LocalizationStore, "Form_URL_ValidationMessage").Value
				};
			}

		}

		public override IEnumerable<ModelClientValidationRule> GetClientValidationRules(string modelPropertyName) {
			if (modelPropertyName == "Name") {
				return new ModelClientValidationRule[] {
					new ModelClientValidationRequiredRule(
						LocalizationService.GetPhrase(LocalizationStore, "Form_Name_ValidationMessage").Value
					)
				};
			}
			if (modelPropertyName == "Email") {
				var message = LocalizationService.GetPhrase(LocalizationStore, "Form_Email_ValidationMessage").Value;
				return new ModelClientValidationRule[]{
					new ModelClientValidationRequiredRule(
						message
					),
					new ModelClientValidationRegexRule(
						message,
						RegularExpressionService.GetExpression("IsEmail").ToString()
					)
				};
			}
			if (modelPropertyName == "Url") {
				return new ModelClientValidationRule[]{
					new ModelClientValidationRegexRule(
						LocalizationService.GetPhrase(LocalizationStore, "Form_URL_ValidationMessage").Value,
						RegularExpressionService.GetExpression("IsUrl").ToString()
					)
				};
			}
			if (modelPropertyName == "Body") {
				return new ModelClientValidationRule[] {
					new ModelClientValidationRequiredRule(
						LocalizationService.GetPhrase(LocalizationStore, "Form_Body_ValidationMessage").Value
					)
				};
			}
			return Enumerable.Empty<ModelClientValidationRule>();
		}

		#endregion
	}
}
