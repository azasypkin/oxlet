﻿using System;
using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;
using OxLet.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Services;

namespace OxLet.Filters {
	public class RecentUpdatesActionFilter : IActionFilter {
		private readonly IRecentUpdateService _recentUpdateService;
		private readonly IPostService _postService;
		private const string PostsCategoryName = "Posts";

		public RecentUpdatesActionFilter(IRecentUpdateService recentUpdateService, IPostService postService) {
			_recentUpdateService = recentUpdateService;
			_postService = postService;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model == null) {
				return;
			}

			IPagedList<RecentUpdate> recentUpdates = _recentUpdateService.GetRecentUpdates(
				DateTime.UtcNow.Subtract(new TimeSpan(10, 0, 0, 0)),
				new PagingInfo(0, 10)
			);

			var recentUpdatedPosts = recentUpdates
				.Where(x => x.Category == PostsCategoryName && x.ItemId != null && x.ItemId != 0)
				.Select(x => _postService.GetPost(x.ItemId.Value))
				.ToList();

			model.AddModelItem(new RecentUpdatesViewModel(recentUpdates, recentUpdatedPosts));
			
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
