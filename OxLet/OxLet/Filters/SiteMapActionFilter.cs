﻿using System;
using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Extensions;
using OxLet.Blog.Services.Interfaces;
using OxLet.Core.Configuration;
using OxLet.Core.Models;
using OxLet.Core.Models.SEO;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Filters {
	public class SiteMapActionFilter : IActionFilter {
		private readonly IPostService _postService;
		private readonly IBlogService _blogService;
		private readonly OxLetSection _configuration;

		public SiteMapActionFilter(IPostService postService, IBlogService blogService, OxLetSection configuration) {
			_postService = postService;
			_blogService = blogService;
			_configuration = configuration;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModelItem<SiteMap>;

			if (model == null) {
				return;
			}

			var helper = new UrlHelper(filterContext.RequestContext);

			model.Item.SiteMapUrls.AddRange(
				new [] {
					new SiteMapUrl {
						Location = helper.AbsolutePath(helper.RouteOxiLetUrl("AboutPage")),
						ChangeFrequency = ChangeFrequency.Monthly,
						LastModified = "2011-05-01",
						Priority = (decimal)0.6
					},
					new SiteMapUrl {
						Location = helper.AbsolutePath(helper.RouteOxiLetUrl("ContactPage")),
						ChangeFrequency = ChangeFrequency.Monthly,
						LastModified = "2011-05-01",
						Priority = (decimal)0.6
					},
				}
			);

			var blogName = filterContext.RouteData.Values["blogName"] as string;

			var blog = string.IsNullOrEmpty(blogName)
				? _blogService.GetBlog(_configuration.GetValue<string>("BlogName"))
				: _blogService.GetBlog(blogName);

			// add post
			model.Item.SiteMapUrls.AddRange(
				_postService.GetPosts(blog, new PagingInfo(0, 10000)).Select(x => new SiteMapUrl {
					Location = helper.AbsolutePath(helper.Post(x)),
					ChangeFrequency = ChangeFrequency.Weekly,
					LastModified = x.Modified.HasValue ? x.Modified.Value.ToUniversalTime().ToString("yyy-MM-dd") : DateTime.UtcNow.ToString("yyyy-MM-dd"),
					Priority = (decimal)0.5
				})
			);
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
