﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%=Html.PageTitle(OxLet.Resources.Views.User.Labels.SignIn_Title)%>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="RobotsMetaTag" runat="server">
	<%= Html.RobotsMetaTags("noindex,nofollow") %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<div class="logincontainer">
	<form method="post" id="signIn" action="" class="login">
		<h2 class="title"><%= OxLet.Resources.Views.User.Labels.SignIn_Title %></h2>
		<div><%= OxLet.Resources.Views.User.Labels.SignIn_Help %></div>
		<%=Html.ValidationSummary() %>
		<div class="username">
			<label for="login_username"><%= OxLet.Resources.Views.User.Labels.SignIn_Username %></label>
			<%=Html.TextBox("username", Request["username"], new { id = "login_username", @class = "text" }) %>
		</div>
		<div class="password">
			<label for="login_password"><%= OxLet.Resources.Views.User.Labels.SignIn_Password %></label>
			<%=Html.Password("password", Request["password"], new { id = "login_password", @class = "text" })%>
		</div>
		<div class="remember">
			<%=Html.CheckBox("rememberMe", Request.Form.IsTrue("rememberMe"), new { id = "login_remember" })%>
			<label for="login_remember"><%= OxLet.Resources.Views.User.Labels.SignIn_RememberUser %></label>
		</div>
		<div class="submit">
			<input type="submit" value="<%= OxLet.Resources.Views.User.Labels.SignIn_Button %>" id="login_submit" class="submit button" />
			<%= Html.OxiletAntiForgeryToken(m => m.AntiForgeryToken) %>
		</div>
	</form>
	</div>
</asp:Content>