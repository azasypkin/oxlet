﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModel>" ContentType="text/xml" %>

<%@ Import Namespace="OxLet.Blog.Extensions" %>

<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
    <ShortName><%= Model.Context.Site.DisplayName %></ShortName>
    <Description><%= string.Format("Search the content on {0}", Model.Context.Site.DisplayName) %></Description>
    <Image height="16" width="16" type="image/x-icon"><%=Url.AbsolutePath(Url.AppPath(Model.Context.Site.FavIconUrl))%></Image>
    <Image height="64" width="64" type="image/png"><%=Url.AbsolutePath(Url.AppPath(Model.Context.Site.FavIconUrl.Replace(".ico", ".png")))%></Image>
    <Url type="text/html" template="<%=Server.UrlDecode(Url.AbsolutePath(Url.PostSearch("{searchTerms}"))) %>" />
    <Url type="application/rss+xml" template="<%=Server.UrlDecode(Url.AbsolutePath(Url.PostSearch("RSS", "{searchTerms}"))) %>" />
</OpenSearchDescription>
