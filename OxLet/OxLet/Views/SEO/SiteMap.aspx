﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Core.Models.SEO.SiteMap>>" ContentType="text/xml" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%= Model.Item.ToXml() %>