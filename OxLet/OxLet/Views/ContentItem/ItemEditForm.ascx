﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Core.Models.Input.ContentItemInput>>" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% using(Html.BeginForm()) { %>
	<fieldset class="info">
		<legend>Info</legend>
		<div class="parent">
			<%= Html.LabelFor(x => x.Item.Parent, "Parent")%>
			<input type="text" value="<%= Model.Container != null ? Model.Container.DisplayName : "" %>" disabled="disabled"/>
			<%= Html.HiddenFor(x=>x.Item.Parent) %>
		</div>
		<div class="name">
			<%= Html.LabelFor(x => x.Item.Name, "Name")%>
			<%= Html.TextBoxFor(x => x.Item.Name, new {
				title = "Name",
				@class = "text mandatory",
				tabindex = "1"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Name)%>
		</div>
		<div class="displayName">
			<%= Html.LabelFor(x => x.Item.DisplayName, "DisplayName")%>
			<%= Html.TextBoxFor(x => x.Item.DisplayName, new {
				title = "DisplayName",
				@class = "text mandatory",
				tabindex = "1"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.DisplayName)%>
		</div>
		<div class="slug">
			<%= Html.LabelFor(x => x.Item.Slug, "Slug")%>
			<%= Html.TextBoxFor(x => x.Item.Slug, new {
				title = "Slug",
				@class = "text mandatory",
				tabindex = "1"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Slug)%>
		</div>
		<div class="Language">
			<%= Html.DropDownListFor(x=>x.Item.Language, Model.GetModelItem<Language[]>().Select(x=> new SelectListItem{Text = x.DisplayName, Value = x.Id.ToString()})) %>
		</div>
		<div class="thumbnail">
			<%= Html.Label("ContentItemInput.Thumbnail", "Thumbnail")%>
			<select>
				<option> File </option>
				<option> Url </option>
			</select>
			<%= Html.OxiLetEditor(
				null,
				Model.Item.Thumbnail != null ? Model.Item.Thumbnail.Path ?? Model.Item.Thumbnail.File.Name  : "",
				new {
					id = "ContentItemInput.Thumbnail",
					name = "ContentItemInput.Slug",
					title = "Slug",
					@class = "text",
					tabindex = "3"
			},true)%>
		</div>
		<div class="remember">
			<%=Html.CheckBoxFor(
				x => x.Item.IsFolder,
				new { tabindex = "5" }
			)%>
			<%= Html.LabelFor(x => x.Item.IsFolder, "IsFolder")%>
		</div>
		<div class="submit">
			<input type="submit" value="<%=OxLet.Resources.Views.Contact.Labels.Form_SubmitButtonText%>" id="comment_submit" class="submit button" tabindex="6" />
		</div>
	</fieldset>
<% } %>
