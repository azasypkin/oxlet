﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"
	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Core.Models.ContentItem>>" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%=Html.PageTitle("Content Item list")%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainHeading" runat="server">
	<%= "Content Item list"%>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="blog main">
		<%= Html.PageState((IPagedList<ContentItem>)Model.List, OxLet.Resources.Views.Shared.Labels.PageStateTemplate)%>
		<% Html.RenderPartial("ContentItemList"); %>
		<%= Html.SimplePager(
			(IPagedList<ContentItem>)Model.List,
			"PageOfPosts",
			null,
			OxLet.Resources.Views.Shared.Labels.Pager_PreviousTemplate, 
			OxLet.Resources.Views.Shared.Labels.Pager_NextTemplate, true)
		%>
	</div>
</asp:Content>

