<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelList<OxLet.Core.Models.ContentItem>>" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="OxLet.Core.Models" %>
<%
	if (((IPagedList<ContentItem>)Model.List).TotalItemCount > 0) { %><ul class="posts medium">
	<%
	int counter = 0;
	foreach (var contentItem in Model.List) {
		var className = new StringBuilder("post", 15);

		if (contentItem.Equals(Model.List.First())) { className.Append(" first"); }
		if (contentItem.Equals(Model.List.Last())) { className.Append(" last"); }

		if (counter % 2 != 0) { className.Append(" odd"); }
	%>
	<li class="<%=className.ToString() %>">
		<div><strong>Display Name:</strong><%=contentItem.DisplayName.CleanText()%></div>
		<div><strong>Name:</strong><%= contentItem.Name %></div>
		<div><strong>Slug:</strong><%= contentItem.Slug %></div>
		<div class="posted">
			<%=contentItem.Published.HasValue ? Html.ConvertToLocalTime(contentItem.Published.Value).ToLongDateString() : OxLet.Resources.Views.Blog.Labels.Post_Status_Draft %></div>
	<%--	<div class="content">	<%=contentItem.GetBodyShort() %></div>--%>
	</li>
	<%
counter++;
	} %>
</ul>
<% } else { //todo: (nheskew) need an Html.Message html helper extension method that takes a message %>
<div class="message info">
	<%= OxLet.Resources.Views.Blog.Labels.NonePostFound %></div>
<% } %>