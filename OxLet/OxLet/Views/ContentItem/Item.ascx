﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Core.Models.ContentItem>>" %>
<%@ Import Namespace="OxLet.Core.Extensions" %>
<div>Display Name:<%=Model.Item.DisplayName.CleanText()%></div>
<div>Name:<%= Model.Item.Name%></div>