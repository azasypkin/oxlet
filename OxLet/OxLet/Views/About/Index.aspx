﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/OxLetSite.Master" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%= Html.PageTitle(OxLet.Resources.Views.About.Labels.Label) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaKeywords" runat="server">
	<%=Html.PageKeywords(OxLet.Resources.Views.About.Labels.MetaKeywords) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaDescription" runat="server">
	<%=Html.PageDescription(OxLet.Resources.Views.About.Labels.MetaDescription) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderPartial("JQueryScriptTag"); %>
	<% Html.RenderPartial("OxilLetScriptTag"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainHeading" runat="server">
	<%=OxLet.Resources.Views.About.Labels.Heading %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<article class="main">
		<p><%: OxLet.Resources.Views.About.Labels.Introduction %></p>
		<section class="expandable">
			<h2><span class="switch"></span><span><%: OxLet.Resources.Views.About.Labels.Skills %></span></h2>
			<div style="clear: both;"></div>
			<div class="expandable-content">
				<p><%: OxLet.Resources.Views.About.Labels.PrimarySkills %>&nbsp;<b><%: OxLet.Resources.Views.About.Labels.PrimarySkillsContent %></b></p>
				<p><%: OxLet.Resources.Views.About.Labels.SecondarySkills %>&nbsp;<b><%: OxLet.Resources.Views.About.Labels.SecondarySkillsContent %></b></p>
			</div>
		</section>
		<section class="expandable">
			<h2><span class="switch"></span><span><%: OxLet.Resources.Views.About.Labels.Experience%></span></h2>
			<div style="clear: both;"></div>
			<div class="expandable-content">
				<p><%: OxLet.Resources.Views.About.Labels.ExperienceFreelance%></p>
				<p><%= OxLet.Resources.Views.About.Labels.ExperienceEPAM %></p>
			</div>
		</section>
		<section class="expandable">
			<h2><span class="switch"></span><span><%: OxLet.Resources.Views.About.Labels.Education %></span></h2>
			<div style="clear: both;"></div>
			<div class="expandable-content">
				<p><%: OxLet.Resources.Views.About.Labels.EducationBNTU %></p>
				<p><%: OxLet.Resources.Views.About.Labels.EducationMCAD %></p>
				<p><%: OxLet.Resources.Views.About.Labels.EducationMCP %></p>
			</div>
		</section>
	</article>
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptVariablesPre" runat="server">
	<script type="text/javascript">
		(new $['Oxlet.UI']['SectionExpander']({ isCollapsed: false })).apply();
	</script>
</asp:Content>



