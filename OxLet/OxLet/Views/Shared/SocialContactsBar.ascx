﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="socialContacts">
	<a title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_Twitter %>" href="http://twitter.com/oxilet">
		<span title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_Twitter %>" class="twitter">&nbsp;</span>
	</a>
	<a title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_Skype %>" href="skype:aleh_zasypkin?call">
		<span title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_Skype %>" class="skype">&nbsp;</span>
	</a>
	<a title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_Gmail %>" href="mailto:oxycommail@gmail.com?Subject=Message%20from%20oxilet.com">
		<span title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_Gmail %>" class="gmail">&nbsp;</span>
	</a>
	<a title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_LinkedIn %>" href="http://www.linkedin.com/pub/aleh-zasypkin/36/b39/7">
		<span title="<%= OxLet.Resources.Views.Shared.Labels.SocialContacts_LinkedIn %>" class="linkedin">&nbsp;</span>
	</a>
</div>
