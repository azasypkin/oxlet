﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<div class="sub search">
	<form id="search" method="get" action="<%=Url.PostSearch() %>">
	<fieldset>
		<label for="search_term"><%= OxLet.Resources.Views.Shared.Labels.Search_Label %></label>
		<%= Html.TextBox("term", "", new { id = "search_term", @class = "text", title = OxLet.Resources.Views.Shared.Labels.Search_Hint })%>
		<input type="submit" value="<%= OxLet.Resources.Views.Shared.Labels.Search_ButtonText %>" class="button" />
	</fieldset>
	</form>
</div>
