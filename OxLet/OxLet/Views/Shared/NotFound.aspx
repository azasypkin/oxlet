﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/OxLetSite.Master"	Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%= Html.PageTitle(OxLet.Resources.Views.Shared.Labels.ServicePage_NotFound_Title) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<p class="notFound"> <%= OxLet.Resources.Views.Shared.Labels.ServicePage_NotFound_Text %></p>
	<% if (!string.IsNullOrEmpty((string)ViewData["Description"])) { %>
		<p>	<%= ViewData["Description"] %></p>
	<% } %>
</asp:Content>

