﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%@ Import Namespace="System.Threading" %>
<div id="logindisplay" style="float:right;margin-right:10px;">
	<% if (Model.Context.User != null && Model.Context.User.IsAuthenticated) { %>
		<%= string.Format("<span class=\"username\">{0}</span>", Html.Encode(Model.Context.User.DisplayName)) %>
		<span class="logout">&laquo;<%= Html.Link(
			OxLet.Resources.Views.User.Labels.Logout,
			Url.SignOut(Request.Url.PathAndQuery)
		)%>&raquo;</span>
	<% } else { %>
		<span class="login">&laquo;<%=Html.Link(
			OxLet.Resources.Views.User.Labels.Login,
			Url.SignIn(Request.Url.PathAndQuery),
			new { rel = "noindex,nofollow" }
		)%>&raquo;</span>
	<% } %>
</div>