﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<ul class="languageBar">
	<%--<li class="<%= Thread.CurrentThread.CurrentUICulture.Name == "be-BY" ?"selected":"" %>" title="<%= OxLet.Resources.Views.Shared.Labels.LanguageBarTitle_Belarus %>">
		<%= Html.Link("<span class=\"language by\">&nbsp;</span>", Url.ChangeLanguage("be-BY", Request.Url.PathAndQuery))%>
	</li>--%>
	<li class="<%= Thread.CurrentThread.CurrentUICulture.Name == "ru-RU" ?"selected":"" %>" title="<%= OxLet.Resources.Views.Shared.Labels.LanguageBarTitle_Russian %>">
		<%= Html.Link(
			"<span class=\"language ru\">&nbsp;</span>",
			Thread.CurrentThread.CurrentUICulture.Name != "ru-RU" ? Url.LocalizedUrl("ru-ru", Request.Url.PathAndQuery) : "javascript:void(0)"
		)%>
	</li>
	<li class="<%= Thread.CurrentThread.CurrentUICulture.Name.StartsWith("en") ?"selected":"" %>" title="<%= OxLet.Resources.Views.Shared.Labels.LanguageBarTitle_English %>">
		<%= Html.Link(
			"<span class=\"language en\">&nbsp;</span>",
									Thread.CurrentThread.CurrentUICulture.Name != "en-US" ? Url.LocalizedUrl("en-us", Request.Url.PathAndQuery) : "javascript:void(0)"
		)%>
	</li>
</ul>
