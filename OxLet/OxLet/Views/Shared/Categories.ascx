<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Blog.Models" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<% var categories = Model.GetModelItem<IList<KeyValuePair<Tag, int>>>(); %>

<% if (categories != null) {%>
<nav>
	<h3><%= OxLet.Resources.Views.Blog.Labels.SideBar_Categories %></h3>
		<% if (categories.Count > 0) {
			Response.Write(
				Html.UnorderedList(
					categories,
					t => Html.Link(string.Format("{0} ({1})", t.Key.Name, t.Value), Url.Posts(t.Key), "tagList")
				)
			);
		} else {
			Html.RenderPartial("SideBarEmptyResult"); 
	 } %>
</nav>
<% } %>

