﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Configuration" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<%
	Html.RenderCssFile("reset.css", "reset.min.css");
	Html.RenderCssFile("default.css", "default.min.css");
%>
<% if(Model.Context.Configuration.Mode == SiteMode.Production) { %>
	<% Html.RenderScriptTag("Externals/ga.js", "Externals/ga.min.js"); %>
<% } %>
<!--[if lte IE 9]>
<% Html.RenderScriptTag("Externals/html5.js", "http://html5shim.googlecode.com/svn/trunk/html5.js"); %>
<![endif]-->
<!-- <% Html.RenderScriptTag("https://apis.google.com/js/plusone.js"); %> -->