﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelPartial<OxLet.Blog.ViewModels.ArchiveViewModel>>" %>
<%@ Import Namespace="OxLet.Blog.Extensions" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% if (Model.PartialModel != null) {
	var months = Model.PartialModel.Archives; %>
	<nav class="sub archives">
	<h3><%= OxLet.Resources.Views.Blog.Labels.SideBar_Archives %></h3>
	<% if (Model.PartialModel.Archives != null && Model.PartialModel.Archives.Count > 0) {
			if (months.Count > 20) { %>
				<% Html.RenderPartial("ArchivesByYear"); %><%
		} else {
			Response.Write(
				Html.UnorderedList(
				months,
				t => Html.Link(
					string.Format("{0:MMMM yyyy} ({1})", t.Key.ToDateTime(), t.Value),
					Url.Posts(t.Key.Year, t.Key.Month)),
				"archiveMonthList"
				)
			);
		}
	} else { 
		Html.RenderPartial("SideBarEmptyResult"); 
	} %>
</nav>
<% } %>
