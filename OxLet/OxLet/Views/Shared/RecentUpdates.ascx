<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Extensions" %>
<%@ Import Namespace="OxLet.Models" %>
<% var model = Model.GetModelItem<RecentUpdatesViewModel>(); 
	if(model != null) { %>
		<nav>
			<h3><%= OxLet.Resources.Views.Shared.Labels.RecentUpdates %></h3>
			<% if (model.RecentUpdates != null && model.RecentUpdates.Count > 0) { %>
			<ul class="recent">
				<% foreach (var update in model.RecentUpdates) { %>
					<li><%= Html.RecentUpdateItem(Url, update, model)%></li>
				<% } %>
			</ul>
			<% } else {
				Html.RenderPartial("SideBarEmptyResult"); 
			} %>
		</nav>
<% } %>
