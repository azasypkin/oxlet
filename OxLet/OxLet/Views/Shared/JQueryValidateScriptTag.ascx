﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>
<% Html.RenderScriptTag("JQuery/jquery.validate.min.js", "http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"); %>
<% Html.RenderScriptTag("JQuery/jquery.validate.unobtrusive.min.js", "http://ajax.aspnetcdn.com/ajax/mvc/3.0/jquery.validate.unobtrusive.min.js"); %>