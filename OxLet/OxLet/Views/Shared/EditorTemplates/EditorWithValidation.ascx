﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%= Html.LabelFor(x=>x, ViewData["Label"].ToString())%>
<% if (ViewData["Multiline"] != null && (bool)ViewData["Multiline"]) { %>
	<%= Html.TextAreaFor(x => x, new { @class = ViewData["Class"], @title = ViewData["Hint"], @tabIndex = ViewData["TabIndex"] })%>	
<% } else {%>
	<%= Html.TextBoxFor(x => x, new { @class = ViewData["Class"], @title = ViewData["Hint"], @tabIndex = ViewData["TabIndex"] })%>
<% } %>
<%= Html.ValidationMessageFor(x=>x)%>

