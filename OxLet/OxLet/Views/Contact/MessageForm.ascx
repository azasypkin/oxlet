﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewUserControl<OxLet.Core.Web.ViewModels.OxletModelItem<OxLet.Models.Input.MessageInput>>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<% if (Model.Item.MessageSendResult.HasValue) {%>
<div class="contactStatus">
	<p class="<%= Model.Item.MessageSendResult.Value ? "successfull" : "failed" %>">
		<%= Model.Item.MessageSendResult.Value 
			? OxLet.Resources.Views.Contact.Labels.Form_SendResult_Successfull
			: OxLet.Resources.Views.Contact.Labels.Form_SendResult_Failed
		%>
	</p>
</div>
<% } %>

<% using (Html.BeginForm("SendMessage", "Contact", FormMethod.Post, new {@id = "contactForm"})) {%>
	<fieldset class="info">
		<legend><%=OxLet.Resources.Views.Contact.Labels.Form_Title%></legend>
		<%--<p class="gravatarhelp">
			<%=Html.Link(OxLet.Resources.Views.Contact.Labels.Form_GravatarLabel, "http://en.gravatar.com/site/signup", new { rel = "noindex,nofollow" })%>
		</p>
		<div id="comment_grav"><%=Html.Gravatar("48")%></div>--%>
		<div class="name">
			<%= Html.LabelFor(x => x.Item.Name, OxLet.Resources.Views.Contact.Labels.Form_Name)%>
			<%= Html.TextBoxFor(x => x.Item.Name, new {
				title =  OxLet.Resources.Views.Contact.Labels.Form_Name_Hint,
				@class = "text mandatory",
				tabindex = "1"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Name)%>
		</div>
		<div class="email">
			<%= Html.LabelFor(x => x.Item.Email, OxLet.Resources.Views.Contact.Labels.Form_Email)%>
			<%= Html.TextBoxFor(x => x.Item.Email, new {
				title =  OxLet.Resources.Views.Contact.Labels.Form_Email_Hint,
				@class = "text mandatory",
				tabindex = "2"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Email)%>
		</div>
		<div class="url">
			<%= Html.LabelFor(x => x.Item.Url, OxLet.Resources.Views.Contact.Labels.Form_URL)%>
			<%= Html.TextBoxFor(x => x.Item.Url, new {
				title =  OxLet.Resources.Views.Contact.Labels.Form_URL_Hint,
				@class = "text",
				tabindex = "3"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Url)%>
		</div>
		<div class="remember">
			<%= Html.CheckBoxFor(
				x=>x.Item.IsRemember,
				new {id = "comment_remember", tabindex = "5"}
			)%>
			<%= Html.LabelFor(x => x.Item.IsRemember, OxLet.Resources.Views.Contact.Labels.Form_Remember)%>
		</div>
		<div class="submit">
			<input type="submit" value="<%=OxLet.Resources.Views.Contact.Labels.Form_SubmitButtonText%>" id="comment_submit" class="submit button" tabindex="6" />
		</div>
	</fieldset>
	<fieldset class="message">
	<legend><%=OxLet.Resources.Views.Contact.Labels.Form_Body_Legend%></legend>
		<div>
			<%= Html.LabelFor(x => x.Item.Body, OxLet.Resources.Views.Contact.Labels.Form_Body_Label)%>
			<%= Html.TextAreaFor(x => x.Item.Body, new {
				title = OxLet.Resources.Views.Contact.Labels.Form_Body_Hint,
				@class = "text mandatory",
				tabindex = "4"
			})%>
			<%= Html.ValidationMessageFor(x => x.Item.Body)%>
		</div>
	</fieldset>
	<%=Html.OxiletAntiForgeryToken(m => m.AntiForgeryToken) %>
<% } %>
<%--<script type="text/javascript">
	$(document).ready(function () {
//		$('#contactForm').ajaxForm({
//			beforeSubmit: function () {
//				$("#contactForm").showLoading();
//			},
//			success: function () {
//				$("#contactForm").hideLoading();
//			}
//		});
		<% Html.RenderScriptVariable("computeHashPath", Url.ComputeHash()); %>
	}); 
</script>--%>
