﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/OxLetSite.Master" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
	<%= Html.PageTitle(OxLet.Resources.Views.Contact.Labels.Label) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaKeywords" runat="server">
	<%=Html.PageKeywords(OxLet.Resources.Views.Contact.Labels.MetaKeywords) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MetaDescription" runat="server">
	<%=Html.PageDescription(OxLet.Resources.Views.Contact.Labels.MetaDescription) %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderPartial("JQueryScriptTag"); %>
	<% Html.RenderPartial("JQueryValidateScriptTag"); %>
	<% Html.RenderPartial("OxilLetScriptTag"); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainHeading" runat="server">
	<%:OxLet.Resources.Views.Contact.Labels.Label%>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="contact">
		<section class="main">
			<p><%= OxLet.Resources.Views.Contact.Labels.SocialContactsWelcomeMessage %></p>
			<br />
			<% Html.RenderPartial("SocialContactsBar"); %>
			<div style="clear:both;"></div>
				<br />
			<p> <%= OxLet.Resources.Views.Contact.Labels.LeaveDirectMessage %></p>
			<br />
			<% Html.RenderPartial("MessageForm"); %>
		</section>
	</div>
</asp:Content>

