﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/OxLetSite.Master" Inherits="System.Web.Mvc.ViewPage<OxLet.Core.Web.ViewModels.OxletModel>" %>
<%@ Import Namespace="OxLet.Core.Web.Extensions" %>

<asp:Content ContentPlaceHolderID="Title" runat="server">
	<%= Html.PageTitle(OxLet.Resources.Views.Portfolio.Labels.Label) %>
</asp:Content>

<asp:Content ContentPlaceHolderID="HeadScripts" runat="server">
	<% Html.RenderScriptTag("Framework/Oxlet.UI.js"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainHeading" runat="server">
	<%: OxLet.Resources.Views.Portfolio.Labels.Heading %>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<article class="portfolio">
		<p>
			Much of feature set involves JavaScript APIs that make it easier to develop interactive
			web pages but there are a slew of new elements that allow you extra semantics in
			your conventional Web 1.0 pages. In order to investigate these, let&#8217;s look
			at marking up a blog.
		</p>
		<section class="expandable">
			<h2>
				<span class="switch"></span><span>www.gmail.com</span></h2>
			<div style="clear: both;">
			</div>
			<div class="content">
				<p>
					Areas: CMS, javascript, mark up and deploying.</p>
				<div>
					<a href="#">
						<img src="/Images/Portfolio/www_gmail_com.jpg" alt="www.gmail.com" /></a>
				</div>
			</div>
		</section>
		<section class="expandable">
			<h2>
				<span class="switch"></span><span>www.tut.by</span></h2>
			<div style="clear: both;">
			</div>
			<div class="content">
				<p>
					Areas: CMS, javascript, mark up and deploying.</p>
				<div>
					<a href="#">
						<img src="/Images/Portfolio/www_tut_by.jpg" alt="www.tut.by" /></a>
				</div>
			</div>
		</section>
		<section class="expandable">
			<h2>
				<span class="switch expanded"></span><span>www.google.com</span></h2>
			<div style="clear: both;">
			</div>
			<div class="content">
				<p>
					Areas: CMS, javascript, mark up and deploying.</p>
				<div>
					<a href="#">
						<img src="/Images/Portfolio/www_google_com.jpg" alt="www.google.com" /></a>
				</div>
			</div>
		</section>
	</article>
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptVariablesPre" runat="server">
	<script type="text/javascript">
		(new $['Oxlet.UI']['SectionExpander']({ isCollapsed: false })).apply();
	</script>
</asp:Content>
