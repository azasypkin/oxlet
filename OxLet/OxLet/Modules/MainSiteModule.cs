﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Blog.Controllers;
using OxLet.Blog.Filters;
using OxLet.Blog.Models;
using OxLet.Controllers;
using OxLet.Core.Configuration;
using OxLet.Core.Email;
using OxLet.Core.Infrastructure;
using OxLet.Core.Localization;
using OxLet.Core.Models;
using OxLet.Core.Modules;
using OxLet.Core.Web.Controllers;
using OxLet.Core.Web.Filters;
using OxLet.Core.Web.Filters.Interfaces;
using Microsoft.Practices.Unity;
using OxLet.Filters;
using OxLet.Models.Input;
using OxLet.Plugins.Data.RecentUpdatesPlugin.ModelBinders;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Modules {
	public class MainSiteModule : SiteModuleBase {
		public MainSiteModule(IUnityContainer container, OxLetSection section)
			: base(container, section) {
		}

		public override void RegisterRoutes(RouteCollection routes) {

			MapRoute(
				routes,
				"AboutPage",
				"about",
				new { controller = "About", action = "Index"},
				true
			);

			MapRoute(
				routes,
				"ContactPage",
				"contact",
				new { controller = "Contact", action = "Index", validateAntiForgeryToken = true },
				true
			);
		}

		public override void RegisterActionFilters(IActionFilterRegistry registry) {
			var listActionsCriteria = new ControllerActionCriteria();
			listActionsCriteria.AddMethod<BlogController>(p => p.Dashboard());
			listActionsCriteria.AddMethod<BlogController>(p => p.ItemEdit(null));
			listActionsCriteria.AddMethod<PostController>(p => p.List(null, null, 0, null));
			listActionsCriteria.AddMethod<PostController>(p => p.Item(null, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListByArchive(null, 0, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListBySearch(null, null, 0, null, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListByTag(null, null, 0, null, null));
			listActionsCriteria.AddMethod<PostController>(p => p.ListWithDrafts(null, null, 0));
			listActionsCriteria.AddMethod<AboutController>(p => p.Index());
			listActionsCriteria.AddMethod<ContactController>(p => p.Index());
			listActionsCriteria.AddMethod<PortfolioController>(p => p.Index());
			listActionsCriteria.AddMethod<UserController>(p => p.SignIn());
			registry.Add(new[] { listActionsCriteria }, typeof(RecentUpdatesActionFilter));

			var seoActionsCriteria = new ControllerActionCriteria();
			seoActionsCriteria.AddMethod<SEOController>(p => p.SiteMap());
			registry.Add(new[] { seoActionsCriteria }, typeof(SiteMapActionFilter));

			registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(GzipCompressFilter));

			var tagCloudActionCriteria = new ControllerActionCriteria();
			tagCloudActionCriteria.AddMethod<AboutController>(p => p.Index());
			tagCloudActionCriteria.AddMethod<ContactController>(p => p.Index());
			tagCloudActionCriteria.AddMethod<UserController>(p => p.SignIn());
			registry.Add(new[] { tagCloudActionCriteria }, typeof(ArchiveListActionFilter));

			var tagStatisticActionCriteria = new ControllerActionCriteria();
			tagStatisticActionCriteria.AddMethod<AboutController>(p => p.Index());
			tagStatisticActionCriteria.AddMethod<ContactController>(p => p.Index());
			tagStatisticActionCriteria.AddMethod<UserController>(p => p.SignIn());
			registry.Add(new[] { tagStatisticActionCriteria }, typeof(TagStatisticActionFilter));

			//var adminActionsCriteria = new ControllerActionCriteria();
			//adminActionsCriteria.AddMethod<PortfolioController>(a => a.Index());
			//registry.Add(new[] { adminActionsCriteria }, typeof(AuthorizationFilter));
		}

		public override void RegisterModelBinders(ModelBinderDictionary binders) {
			binders[typeof(MessageInput)] = Container.Resolve<IModelBinder>("MessageInputModelBinder");
		}

		public override string CongfigurationName {
			get { return "MainSiteModule"; }
		}

		public override void RegisterInContainer(IUnityContainer container) {

			// register recent updates plugin staff
			var binderRegistry = new RecentUpdateModelBinderRegistry();

			Func<object, EntityState, RecentUpdate> extractor = (x,s) => {
				var context = (SiteContext) ((object[]) x)[0];
				var post = (Post) ((object[]) x)[1];
				return new RecentUpdate(
					0,
					post.Modified.HasValue ? post.Modified.Value : DateTime.Now,
					"Posts",
					post.Id,
					null,
					s,
					context.User.GetDescription(),
					post.Published
				);
			};

			binderRegistry.Add("Post:AfterAdd", x=> extractor(x, EntityState.Normal));
			binderRegistry.Add("Post:AfterRemove", x => extractor(x, EntityState.Removed));
			binderRegistry.Add("Post:AfterEdit", x => extractor(x, EntityState.Normal));

			container.RegisterInstance<IRecentUpdateModelBinderRegistry>(binderRegistry);

			container.RegisterType<IEmailSender<EmailMessageInfo>, EmailSenderBase<EmailMessageInfo>>();

			RegisterLocalizedResources(container.Resolve<IResourceMapper>());
		}

		private static void RegisterLocalizedResources(IResourceMapper resourceMapper) {
			resourceMapper
				.AddResourceMapping("MessagesValidationMessages", typeof (Resources.Views.Contact.Labels))
				.AddResourceMapping("CommentValidationMessages", typeof(Resources.Views.Blog.Labels))
				.AddResourceMapping("FileResourceValidationMessages", typeof(Resources.Views.Shared.Labels));
		}
	}
}
