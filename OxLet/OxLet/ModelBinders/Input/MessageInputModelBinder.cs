﻿using System.Web.Mvc;
using OxLet.Core.Extensions;
using OxLet.Core.Web.Extensions;
using OxLet.Models.Input;

namespace OxLet.ModelBinders.Input {
	public class MessageInputModelBinder : IModelBinder {

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var valueProvider = bindingContext.ValueProvider;

			var message = new MessageInput();

			ValueProviderResult providerResult = valueProvider.GetValue("Item.Body");
			if (providerResult != null) {
				message.Body = providerResult.GetValue<string>();
			}

			//bool isAuthenticated = controllerContext.HttpContext.User.Identity.IsAuthenticated;

			// if message is posted by anonymous user let's extract it's info
			//if (!isAuthenticated) {
			string creatorName = null;
			providerResult = valueProvider.GetValue("Item.Name");
			if (providerResult != null) {
				creatorName = providerResult.GetValue<string>();
			}

			string creatorEmailHash = null;
			string creatorEmail = null;
			providerResult = valueProvider.GetValue("Item.Email");
			if (providerResult != null) {
				creatorEmail = providerResult.GetValue<string>();

				providerResult = valueProvider.GetValue("Item.EmailHash");

				if (providerResult != null) {
					creatorEmailHash = providerResult.GetValue<string>();
				}

				if (!string.IsNullOrEmpty(creatorEmail) && string.IsNullOrEmpty(creatorEmailHash)) {
					creatorEmailHash = creatorEmail.ComputeHash();
				}
			}

			string creatorUrl = null;
			providerResult = valueProvider.GetValue("Item.Url");
			if (providerResult != null) {
				creatorUrl = providerResult.GetValue<string>();
			}

			providerResult = valueProvider.GetValue("Item.IsRemember");
			if (providerResult != null) {
				message.IsRemember = providerResult.GetValue<string>().Contains("true");
			}
			message.Email = creatorEmail;
			message.EmailHash = creatorEmailHash;
			message.Name = creatorName;
			message.Url = creatorUrl;
			//}

			message.CreatorIp = controllerContext.HttpContext.Request.GetUserIpAddress().ToLong();

			message.CreatorUserAgent = controllerContext.HttpContext.Request.UserAgent;

			return message;
		}
	}
}
