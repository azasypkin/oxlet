﻿using System.Linq;
using System.Web.Mvc;
using OxLet.Blog.Extensions;
using OxLet.Core.Extensions;
using OxLet.Core.Web.Extensions;
using OxLet.Models;
using OxLet.Plugins.Data.RecentUpdatesPlugin.Models;

namespace OxLet.Extensions {
	public static class HtmlHelperExtensions {

		#region Pager

		public static string RecentUpdateItem(this HtmlHelper htmlHelper, UrlHelper url, RecentUpdate recentUpdate, RecentUpdatesViewModel viewModel) {

			switch (recentUpdate.Category) {
				case "Posts":
					var post = viewModel.Posts.FirstOrDefault(x => x.Id == recentUpdate.ItemId);
					return post != null
						? htmlHelper.Link(
							string.Format("<span class=\"postIcon\">&nbsp;</span>{0}", post.Title.CleanText()), url.Post(post), new { title = Resources.Views.Shared.Labels.RecentUpdateCategory_Posts})
						: string.Empty;
				case "Portfolio":
					return htmlHelper.Link(
							string.Format("<span class=\"portfolioIcon\">&nbsp;</span>{0}", recentUpdate.ItemValue), "#", new { title = Resources.Views.Shared.Labels.RecentUpdateCategory_Portfolio });
					default:
					return "";
			}
		}

		#endregion
	}
}
