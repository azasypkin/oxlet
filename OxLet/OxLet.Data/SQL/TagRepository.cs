﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using OxLet.Blog.Models;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Core.Models;

namespace OxLet.Data.SQL {
	public class TagRepository : ITagRepository {
		private readonly OxletDataContext _context;

		public TagRepository(OxletDataContext context) {
			_context = context;
		}

		#region ITagRepository Members

		public IQueryable<Tag> GetTags(bool includeServiceTags) {
			return ProjectTags(includeServiceTags ? GetTagsQuery() : ExcludeServiceTags(GetTagsQuery()));
		}

		public IQueryable<KeyValuePair<Tag, int>> GetTagsWithPostCount(bool includeServiceTags) {

			var mainQuery =
				from t in _context.oxlet_Tags
				join ptr in _context.oxlet_PostTagRelationships on t.Id equals ptr.TagId
				join p in _context.oxlet_Posts on ptr.PostId equals p.Id
				where p.State == (byte)EntityState.Normal && p.PublishedDate <= DateTime.UtcNow
				select new { Tag = t, Post = p };

			if(!includeServiceTags) {
				mainQuery = mainQuery.Where(x => !x.Tag.IsService);
			}

			return 
				from tt in mainQuery
				group tt by tt.Tag
				into results
				//where results.Key.Id == results.Key.ParentId
				orderby results.Key.Name
				select new KeyValuePair<Tag, int>(
					new Tag(results.Key.Id, results.Key.Name, results.Key.Name, results.Key.CreatedDate, results.Key.IsService),
					results.Count()
				);
		}

		public Tag GetTag(string tagName) {
			return ProjectTags(GetTagsQuery(tagName)).FirstOrDefault();
		}

		public Tag GetTag(int id) {
			return ProjectTags(GetTagsQuery(id)).FirstOrDefault();
		}

		public Tag SaveTag(Tag tag) {

			oxlet_Tag tagToSave = null;
			using (var transaction = new TransactionScope()) {
				if (tag.Id != 0) {
					tagToSave = _context.oxlet_Tags.FirstOrDefault(t => t.Id == tag.Id);
				}

				if(tagToSave == null) {
					tagToSave = _context.oxlet_Tags.FirstOrDefault(t => t.Name == tag.Name);
				}

				if (tagToSave == null) {
					tagToSave = new oxlet_Tag {CreatedDate = DateTime.UtcNow};
					_context.oxlet_Tags.InsertOnSubmit(tagToSave);
				}

				tagToSave.Name = tag.Name;
				tagToSave.DisplayName = tag.DisplayName ?? tag.Name;
				tagToSave.IsService = tag.IsService;
				
				_context.SubmitChanges();
				transaction.Complete();
			}

			return GetTag(tagToSave.Id);
		}

		private IQueryable<oxlet_Tag> GetTagsQuery() {
			return
				from t in _context.oxlet_Tags
				select t;
		}

		private IQueryable<oxlet_Tag> GetTagsQuery(int id) {
			return
				from t in _context.oxlet_Tags
				where t.Id == id
				select t;
		}

		private IQueryable<oxlet_Tag> GetTagsQuery(string name) {
			return
				from t in _context.oxlet_Tags
				where t.Name == name
				select t;
		}

		private static IQueryable<oxlet_Tag> ExcludeServiceTags(IQueryable<oxlet_Tag> query) {
			return query.Where(t=>!t.IsService);
		}

		private static IQueryable<Tag> ProjectTags(IQueryable<oxlet_Tag> tags) {
			return
				from t in tags
				select ProjectTag(t);
		}

		private static Tag ProjectTag(oxlet_Tag tag) {

			return new Tag(
				tag.Id,
				tag.Name,
				tag.DisplayName,
				tag.CreatedDate,
				tag.IsService
			);
		}


		#endregion
	}
}
