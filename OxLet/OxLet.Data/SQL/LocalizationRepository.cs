﻿using System;
using System.Linq;
using OxLet.Core.Localization.Repositories;
using OxLet.Core.Models;

namespace OxLet.Data.SQL {
	public class LocalizationRepository : ILocalizationRepository {
		private readonly OxletDataContext _context;

		public LocalizationRepository(OxletDataContext context) {
			_context = context;
		}

		#region ILocalizationRepository Members

		public IQueryable<Phrase> GetPhrases() {
			return from r in _context.oxlet_StringResources
				   select new Phrase {
					   Key = r.StringResourceKey,
					   Value = r.StringResourceValue,
					   Language = r.Language
			};
		}

		#endregion

		public void AddPhrase(Phrase phrase) {
			throw new NotImplementedException();
		}

		public Phrase GetPhrase(string key) {
			throw new NotImplementedException();
		}

		public Phrase GetPhrase(string localizationStore, string key) {
			throw new NotImplementedException();
		}
	}
}
