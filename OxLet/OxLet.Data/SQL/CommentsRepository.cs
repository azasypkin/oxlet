﻿using System;
using System.Linq;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Projections;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Models;

namespace OxLet.Data.SQL {
	public class CommentRepository : ICommentRepository {
		private readonly OxletDataContext _context;
		private readonly int _blogId;

		public CommentRepository(OxletDataContext context,  Blog.Models.Blog blog) {
			_context = context;
			_blogId = blog.Id;
		}

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, Post post, bool includeUnapproved) {
			var query =
				from c in _context.oxlet_Comments
				join p in _context.oxlet_Posts on c.PostId equals p.Id
				join b in _context.oxlet_Blogs on p.BlogId equals b.Id
				join u in _context.oxlet_Users on c.CreatorUserId equals u.Id into ucr
				join anu in _context.oxlet_CommentAnonymous on c.Id equals anu.CommentId into aucr
				from uc in ucr.DefaultIfEmpty()
				from auc in aucr.DefaultIfEmpty()
				where p.Id == post.Id && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal
				orderby c.CreatedDate descending
				select new {Comment = c, Post = p, User = uc, AnonymousUser = auc, Blog = b};

			query = includeUnapproved
				? query.Where(q => q.Comment.State == (byte)EntityState.Normal || q.Comment.State == (byte)EntityState.PendingApproval)
				: query.Where(q => q.Comment.State == (byte)EntityState.Normal);

			return query.Select(x => ProjectComment(x.Comment, x.User, x.AnonymousUser, x.Post, x.Blog)).GetPage(pagingInfo);
		}

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, bool includePending, bool sortDescending) {
			var query =
				from b in _context.oxlet_Blogs
				join p in _context.oxlet_Posts on b.Id equals p.BlogId
				join c in _context.oxlet_Comments on p.Id equals c.PostId
				join u in _context.oxlet_Users on c.CreatorUserId equals u.Id into ucr
				join anu in _context.oxlet_CommentAnonymous on c.Id equals anu.CommentId into aucr
				from uc in ucr.DefaultIfEmpty()
				from auc in aucr.DefaultIfEmpty()
				where b.Id == _blogId && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal
				select new {Comment = c, Post = p, User = uc, AnonymousUser = auc, Blog = b};

			query = includePending
				? query.Where(q => q.Comment.State == (byte)EntityState.Normal || q.Comment.State == (byte)EntityState.PendingApproval)
				: query.Where(q => q.Comment.State == (byte)EntityState.Normal);

			query = sortDescending
				? query.OrderByDescending(q => q.Comment.CreatedDate)
				: query.OrderBy(q => q.Comment.CreatedDate);

			return query.Select(x => ProjectComment(x.Comment, x.User, x.AnonymousUser, x.Post, x.Blog)).GetPage(pagingInfo);

		}

		//private Comment ConvertToComment(oxlet_Comment comment) {
		//    return new Comment(
		//        comment.Id,
		//        new UserDescription { Id = comment.CreatorUserId },
		//        comment.CreatorIp,
		//        comment.UserAgent, 
		//        comment.Body,
		//        EntityState.NotSet,
		//        comment.CreatedDate,
		//        comment.ModifiedDate,
		//        new Post { Id = comment.PostId },
		//        new Language(comment.LanguageId, null, null)
		//    );
		//}

		//public Comment GetComment(string blogName, string postSlug, string commentSlug) {
		//    return (
		//        from c in _context.oxlet_Comments
		//        join p in _context.oxlet_Posts on c.PostId equals p.Id
		//        join b in _context.oxlet_Blogs on p.BlogId equals b.Id
		//        join u in _context.oxlet_Users on c.CreatorUserId equals u.Id
		//        where b.SiteId == _siteId && string.Compare(b.BlogName, blogName, true) == 0 && p.Slug == postSlug && c.Slug == commentSlug
		//        select new Comment(new PostSmall(p.PostId, b.BlogName, p.Slug, p.Title), pcr.CommentID, pcr.Slug)
		//        ).FirstOrDefault();
		//}

		//private Comment ConvertToComment(oxlet_Comment comment, oxlet_User user) {
		//    return new Comment {
		//            Body = comment.Body,
		//            Created = comment.CreatedDate,
		//            Creator = new User(u.UserId, u.DisplayName, u.Email),
		//            CreatorIp = c.CreatorIp,
		//            CreatorUserAgent = c.UserAgent,
		//            Id = c.Id,
		//            Modified = c.ModifiedDate,
		//            Post = new Post { Id = p.BlogId, Slug = p.Slug, Title = p.Title },
		//        };
		//}

		//private User ConvertToUser()

		//public IPageOfItems<Comment> GetComments(PagingInfo pagingInfo, bool includePending, bool sortDescending) {
		//    var query =
		//        from b in _context.oxlet_Blogs
		//        join p in _context.oxlet_Posts on b.Id equals p.BlogID
		//        join pcr in _context.oxite_Blogs_PostCommentRelationships on p.PostId equals pcr.PostId
		//        join c in _context.oxite_Comments on pcr.CommentID equals c.CommentID
		//        where b.Id == _blogId && p.PublishedDate != null && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal
		//        select new { Blog = b, Post = p, PostComment = pcr, Comment = c };

		//    query = includePending
		//        ? query.Where(q => q.Comment.State == (byte)EntityState.Normal || q.Comment.State == (byte)EntityState.PendingApproval)
		//        : query.Where(q => q.Comment.State == (byte)EntityState.Normal);

		//    query = sortDescending
		//        ? query.OrderByDescending(q => q.Comment.CreatedDate)
		//        : query.OrderBy(q => q.Comment.CreatedDate);

		//    return query.Select(q => new Comment(new PostSmall(q.Post.PostId, q.Blog.BlogName, q.Post.Slug, q.Post.Title), q.PostComment.CommentID, q.PostComment.Slug)).GetPage(pagingInfo);
		//}

		//public IPageOfItems<Comment> GetComments(PagingInfo pagingInfo, Blog blog) {
		//    var query =
		//        from c in _context.oxite_Comments
		//        join pcr in _context.oxite_Blogs_PostCommentRelationships on c.CommentID equals pcr.CommentID
		//        join p in _context.oxlet_Posts on pcr.PostId equals p.PostId
		//        join b in _context.oxlet_Blogs on p.BlogID equals b.Id
		//        where p.BlogID == blog.ID && c.State == (byte)EntityState.Normal && p.PublishedDate != null && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal
		//        orderby c.CreatedDate descending
		//        select new { Blog = b, Post = p, PostComment = pcr, Comment = c };

		//    return query.Select(q => new Comment(new PostSmall(q.Post.PostId, q.Blog.BlogName, q.Post.Slug, q.Post.Title), q.PostComment.CommentID, q.PostComment.Slug)).GetPage(pagingInfo);
		//}

		//public IPageOfItems<Comment> GetComments(PagingInfo pagingInfo, Tag tag) {
		//    return (
		//        from b in _context.oxlet_Blogs
		//        join p in _context.oxlet_Posts on b.Id equals p.BlogID
		//        join ptr in _context.oxite_Blogs_PostTagRelationships on p.PostId equals ptr.PostId
		//        join pcr in _context.oxite_Blogs_PostCommentRelationships on p.PostId equals pcr.PostId
		//        where b.Id == _blogId && ptr.TagID == tag.ID && p.PublishedDate != null && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal
		//        select new Comment(new PostSmall(p.PostId, b.BlogName, p.Slug, p.Title), pcr.CommentID, pcr.Slug)
		//        ).GetPage(pagingInfo);
		//}

		//public IPageOfItems<Comment> GetComments(PagingInfo pagingInfo, Post post, bool includeUnapproved) {
		//    var query =
		//        from c in _context.oxite_Comments
		//        join pcr in _context.oxite_Blogs_PostCommentRelationships on c.CommentID equals pcr.CommentID
		//        join p in _context.oxlet_Posts on pcr.PostId equals p.PostId
		//        join b in _context.oxlet_Blogs on p.BlogID equals b.Id
		//        where p.PostId == post.ID && c.State == (byte)EntityState.Normal && p.PublishedDate != null && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal
		//        orderby c.CreatedDate descending
		//        select new { Blog = b, Post = p, PostComment = pcr, Comment = c };

		//    query = includeUnapproved
		//        ? query.Where(q => q.Comment.State == (byte)EntityState.Normal || q.Comment.State == (byte)EntityState.PendingApproval)
		//        : query.Where(q => q.Comment.State == (byte)EntityState.Normal);

		//    return query.Select(q => new Comment(new PostSmall(q.Post.PostId, q.Blog.BlogName, q.Post.Slug, q.Post.Title), q.PostComment.CommentID, q.PostComment.Slug)).GetPage(pagingInfo);
		//}

		//public PostComment Save(PostComment comment, string blogName, string postSlug) {
		//    oxite_Comment commentToSave = null;

		//    if (comment.ID != 0)
		//        commentToSave = _context.oxite_Comments.FirstOrDefault(c => c.CommentID == comment.ID);

		//    if (commentToSave == null) {
		//        commentToSave = new oxite_Comment();

		//        commentToSave.CommentID = comment.ID != 0 ? comment.ID : int.Newint();
		//        commentToSave.CreatedDate = commentToSave.ModifiedDate = DateTime.UtcNow;

		//        _context.oxite_Comments.InsertOnSubmit(commentToSave);
		//    } else
		//        commentToSave.ModifiedDate = DateTime.UtcNow;

		//    oxite_Blogs_Post post = (from p in _context.oxlet_Posts join b in _context.oxlet_Blogs on p.BlogID equals b.Id where b.Id == _blogId && string.Compare(b.BlogName, blogName, true) == 0 && string.Compare(p.Slug, postSlug, true) == 0 select p).FirstOrDefault();
		//    if (post == null) throw new InvalidOperationException(string.Format("Post in blog {0} at slug {1} could not be found to add the comment to", blogName, postSlug));
		//    _context.oxite_Blogs_PostCommentRelationships.InsertOnSubmit(
		//        new oxite_Blogs_PostCommentRelationship {
		//            CommentID = commentToSave.CommentID,
		//            PostId = post.PostId,
		//            Slug = comment.Slug
		//        }
		//        );

		//    commentToSave.ParentCommentID = comment.Parent != null && comment.Parent.ID != 0 ? comment.Parent.ID : commentToSave.CommentID;
		//    commentToSave.Body = comment.Body;
		//    commentToSave.CreatorIP = comment.CreatorIP;
		//    commentToSave.State = (byte)comment.State;
		//    commentToSave.UserAgent = comment.CreatorUserAgent;
		//    commentToSave.oxite_Language = _context.oxite_Languages.Where(l => l.LanguageName == comment.Language.Name).FirstOrDefault();

		//    if (comment.CreatorUserID != 0)
		//        commentToSave.CreatorUserID = comment.CreatorUserID;
		//    else {
		//        oxite_User anonymousUser = _context.oxite_Users.FirstOrDefault(u => u.Username == "Anonymous");
		//        if (anonymousUser == null) throw new InvalidOperationException("Could not find anonymous user");
		//        commentToSave.CreatorUserID = anonymousUser.UserID;

		//        commentToSave.CreatorName = comment.CreatorName;
		//        commentToSave.CreatorEmail = comment.CreatorEmail;
		//        commentToSave.CreatorHashedEmail = comment.CreatorEmailHash;
		//        commentToSave.CreatorUrl = comment.CreatorUrl;
		//    }

		//    _context.SubmitChanges();

		//    return (
		//        from c in _context.oxite_Comments
		//        join pcr in _context.oxite_Blogs_PostCommentRelationships on c.CommentID equals pcr.CommentID
		//        join p in _context.oxlet_Posts on pcr.PostId equals p.PostId
		//        join b in _context.oxlet_Blogs on p.BlogID equals b.Id
		//        join u in _context.oxite_Users on c.CreatorUserID equals u.UserID
		//        where b.Id == _blogId && string.Compare(b.BlogName, blogName, true) == 0 && string.Compare(p.Slug, postSlug, true) == 0 && string.Compare(pcr.Slug, comment.Slug, true) == 0
		//        select projectComment(c, pcr, p, b, u)
		//        ).FirstOrDefault();
		//}

		//private PostComment projectComment(oxite_Comment comment, oxite_Blogs_PostCommentRelationship pcr, oxite_Blogs_Post p, oxite_Blogs_Blog b, oxite_User user) {
		//    PostCommentSmall parent = comment.ParentCommentID != comment.CommentID ? getParentComment(comment.ParentCommentID) : null;
		//    Language language = new Language(comment.oxite_Language.LanguageID) {
		//        DisplayName = comment.oxite_Language.LanguageDisplayName,
		//        Name = comment.oxite_Language.LanguageName
		//    };

		//    if (user.Username != "Anonymous")
		//        return new PostComment(comment.Body, comment.CreatedDate, getUserAuthenticated(comment, user), comment.CreatorIP, comment.UserAgent, comment.CommentID, language, comment.ModifiedDate, parent, new PostSmall(p.PostId, b.BlogName, p.Slug, p.Title), pcr.Slug, (EntityState)comment.State);
		//    else
		//        return new PostComment(comment.Body, comment.CreatedDate, getUserAnonymous(comment, user), comment.CreatorIP, comment.UserAgent, comment.CommentID, language, comment.ModifiedDate, parent, new PostSmall(p.PostId, b.BlogName, p.Slug, p.Title), pcr.Slug, (EntityState)comment.State);
		//}

		//private PostCommentSmall getParentComment(int commentID) {
		//    return (
		//        from c in _context.oxite_Comments
		//        join pcr in _context.oxite_Blogs_PostCommentRelationships on c.CommentID equals pcr.CommentID
		//        join u in _context.oxite_Users on c.CreatorUserID equals u.UserID
		//        where c.State != (byte)EntityState.Removed && c.CommentID == commentID
		//        select projectPostCommentSmall(c, pcr, u)
		//        ).FirstOrDefault();
		//}

		//private static User getUserAuthenticated(oxite_Comment comment, oxite_User user) {
		//    return new User(user.UserID, user.Username, user.DisplayName, user.Email, user.HashedEmail, (EntityState)user.Status);
		//}

		//private static UserAnonymous getUserAnonymous(oxite_Comment comment, oxite_User user) {
		//    return new UserAnonymous(comment.CreatorName, comment.CreatorEmail, comment.CreatorHashedEmail, comment.CreatorUrl);
		//}

		//private static PostCommentSmall projectPostCommentSmall(oxite_Comment comment, oxite_Blogs_PostCommentRelationship pcr, oxite_User user) {
		//    if (user.Username != "Anonymous")
		//        return new PostCommentSmall(new CommentSmall(comment.CommentID, comment.CreatedDate, getUserAuthenticated(comment, user)), pcr.Slug);
		//    else
		//        return new PostCommentSmall(new CommentSmall(comment.CommentID, comment.CreatedDate, getUserAnonymous(comment, user)), pcr.Slug);
		//}

		public Comment GetComment(int id) {
			return (
				from c in _context.oxlet_Comments
				join p in _context.oxlet_Posts on c.PostId equals p.Id
				join b in _context.oxlet_Blogs on p.BlogId equals b.Id
				join u in _context.oxlet_Users on c.CreatorUserId equals u.Id into ucr
				join anu in _context.oxlet_CommentAnonymous on c.Id equals anu.CommentId into aucr
				from uc in ucr.DefaultIfEmpty()
				from auc in aucr.DefaultIfEmpty()
				where c.Id == id
				select ProjectComment(c, uc, auc, p, b)
			 ).FirstOrDefault();
		}

		public Comment GetComment(string blogName, string postSlug, string commentSlug) {
			return (
				from c in _context.oxlet_Comments
				join p in _context.oxlet_Posts on c.PostId equals p.Id
				join b in _context.oxlet_Blogs on p.BlogId equals b.Id
				join u in _context.oxlet_Users on c.CreatorUserId equals u.Id into ucr
				join anu in _context.oxlet_CommentAnonymous on c.Id equals anu.CommentId into aucr
				from uc in ucr.DefaultIfEmpty()
				from auc in aucr.DefaultIfEmpty()
				where string.Compare(b.BlogName, blogName, true) == 0 && p.Slug == postSlug && c.Slug == commentSlug
				select ProjectComment(c, uc, auc, p, b)
			 ).FirstOrDefault();
		}

		public void ChangeState(int commentId, EntityState state) {
			oxlet_Comment comment = _context.oxlet_Comments.FirstOrDefault(c => c.Id == commentId);

			if (comment != null){
				comment.State = (byte)state;
			}

			_context.SubmitChanges();
		}

		

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, Blog.Models.Blog blog) {
			throw new NotImplementedException();
		}

		public IPagedList<Comment> GetComments(PagingInfo pagingInfo, Tag tag) {
			throw new NotImplementedException();
		}

		public Comment Save(Comment comment, string blogName, string postSlug) {
			oxlet_Comment commentToSave = null;

			if (comment.Id != 0) {
				commentToSave = _context.oxlet_Comments.FirstOrDefault(c => c.Id == comment.Id);
			}

			if (commentToSave == null) {
				commentToSave = new oxlet_Comment {
					Id = comment.Id != 0 ? comment.Id : 0
				};

				commentToSave.CreatedDate = commentToSave.ModifiedDate = DateTime.UtcNow;

				_context.oxlet_Comments.InsertOnSubmit(commentToSave);
			} else {
				commentToSave.ModifiedDate = DateTime.UtcNow;
			}
				

			oxlet_Post post = (
				from p in _context.oxlet_Posts 
				join b in _context.oxlet_Blogs on p.BlogId equals b.Id
				where string.Compare(b.BlogName, blogName, true) == 0 && string.Compare(p.Slug, postSlug, true) == 0 select p
			).FirstOrDefault();

			if (post == null) {
				throw new InvalidOperationException(string.Format("Post in blog {0} at slug {1} could not be found to add the comment to", blogName, postSlug));
			}

			commentToSave.PostId = comment.Post.Id;
			commentToSave.Body = comment.Body;
			commentToSave.CreatorIp = comment.CreatorIp;
			commentToSave.State = (byte)comment.State;
			commentToSave.UserAgent = comment.CreatorUserAgent;
			commentToSave.Slug = comment.Slug;
			commentToSave.LanguageId = comment.Language.Id;

			if (comment.Creator != null && comment.Creator.Id != 0) {
				commentToSave.CreatorUserId = comment.Creator.Id;
			}
			_context.SubmitChanges();
			
			if(comment.Creator != null && comment.Creator.Id == 0) {
				_context.oxlet_CommentAnonymous.InsertOnSubmit(
					new oxlet_CommentAnonymous {
						Email = comment.Creator.Email,
						CommentId = commentToSave.Id,
						EmailHash = comment.Creator.EmailHash,
						Name = comment.Creator.Name,
						Url = comment.Creator.Url
					}
				);
				_context.SubmitChanges();
			}
			return GetComment(commentToSave.Id);
		}

		#region Private methods

		private static Comment ProjectComment(oxlet_Comment comment, oxlet_User user, oxlet_CommentAnonymous anonymousUser, oxlet_Post post, oxlet_Blog blog) {
			return new Comment(
				comment.Id,
				user == null
					? new UserDescription(0) {
						Email = anonymousUser.Email,
						EmailHash = anonymousUser.EmailHash,
						Name = anonymousUser.Name,
						Url = anonymousUser.Url
					}
					: new UserDescription(user.Id) { Email = user.Email, EmailHash = user.EmailHash, Name = user.Name, Url = user.Url },
				comment.CreatorIp,
				comment.UserAgent,
				comment.Body,
				(EntityState)comment.State,
				comment.CreatedDate,
				comment.ModifiedDate,
				new Post(
					post.Id,
					new UserDescription(post.CreatorUserId),
					null,
					null,
					null,
					EntityState.NotSet,
					post.Slug,
					null,
					null,
					null,
					null,
					false,
					new BlogProjection(blog.Id, blog.CommentingDisabled, blog.BlogName),
					null,
					null),
				new Language(0, null, null),
				comment.Slug
			);
		}

		#endregion

	}
}
