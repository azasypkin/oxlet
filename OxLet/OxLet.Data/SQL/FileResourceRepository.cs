﻿using System;
using System.Linq;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Data.SQL {
	public class FileResourceRepository: IFileResourceRepository {

		private readonly OxletDataContext _context;
		private readonly SiteContext _siteContext;

		public FileResourceRepository(OxletDataContext context, SiteContext siteContext) {
			_context = context;
			_siteContext = siteContext;
		}

		public FileResource GetFile(int id) {
			var query =
				from fr in _context.oxlet_FileResources
				where fr.Id == id
				select	fr;
			return ProjectFileResource(query);
		}

		public FileResource SaveFile(FileResource file) {
			oxlet_FileResource dbFileResource = null;

			if (file.Id != 0) {
				dbFileResource = (from fr in _context.oxlet_FileResources where fr.Id == file.Id select fr).FirstOrDefault();
			}

			if (dbFileResource == null) {
				dbFileResource = new oxlet_FileResource {
					SiteId = _siteContext.Site.Id,
				};

				_context.oxlet_FileResources.InsertOnSubmit(dbFileResource);

				dbFileResource.CreatedDate = dbFileResource.ModifiedDate = DateTime.UtcNow;
				MakeFieldsMatch(dbFileResource, file);
			} else {
				MakeFieldsMatch(dbFileResource, file);
				dbFileResource.ModifiedDate = DateTime.UtcNow;
			}

			_context.SubmitChanges();

			return GetFile(dbFileResource.Id);
		}

		private static FileResource ProjectFileResource(IQueryable<oxlet_FileResource> query) {
			var fileResource = query.FirstOrDefault();
			if (fileResource != null) {
				var data = fileResource.Data.ToArray();
				return new FileResource(
					fileResource.Id,
					fileResource.Name,
					fileResource.ContentType,
					data,
					new UserDescription(fileResource.CreatorUserId),
					fileResource.CreatedDate,
					fileResource.ModifiedDate
				);
			}
			return null;
		}

		private static void MakeFieldsMatch(oxlet_FileResource dbFileResource, FileResource file) {
			dbFileResource.ContentType = file.MimeType;
			dbFileResource.CreatorUserId = file.Creator.Id;
			dbFileResource.Data = file.Data;
			dbFileResource.Name = file.Name;
			dbFileResource.State = (byte) EntityState.Normal;
		}

	}
}
