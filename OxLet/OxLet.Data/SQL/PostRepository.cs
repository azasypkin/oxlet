﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using OxLet.Blog.Models;
using OxLet.Blog.Models.Projections;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;

namespace OxLet.Data.SQL {
	public class PostRepository : IPostRepository {
		private readonly OxletDataContext _context;
		private readonly SiteContext _siteContext;

		public PostRepository(OxletDataContext context, SiteContext siteContext) {
			_context = context;
			_siteContext = siteContext;
		}

		public IPagedList<Post> GetPostsWithDrafts(Blog.Models.Blog blog, PagingInfo pagingInfo) {
			return ProjectPosts(GetPostsQuery(blog)).GetPage(pagingInfo);
		}

		public IPagedList<Post> GetPosts(Blog.Models.Blog blog, Language language, PagingInfo pagingInfo) {
			return ProjectPosts(ExcludeNotYetPublished(GetPostsQuery(blog, language))).GetPage(pagingInfo);
		}

		public IPagedList<Post> GetPosts(Blog.Models.Blog blog, PagingInfo pagingInfo) {
			return ProjectPosts(ExcludeNotYetPublished(GetPostsQuery(blog))).GetPage(pagingInfo);
		}

		private IQueryable<oxlet_Post> GetPostsQuery(Blog.Models.Blog blog) {
			return
				from p in _context.oxlet_Posts
				where p.BlogId == blog.Id && p.State != (byte)EntityState.Removed
				select p;
		}

		private IQueryable<oxlet_Post> GetPostsQuery(Blog.Models.Blog blog, Tag tag) {
			return
				from p in _context.oxlet_Posts
				join ptr in _context.oxlet_PostTagRelationships on p.Id equals ptr.PostId
				where ptr.TagId == tag.Id && p.State != (byte)EntityState.Removed && p.BlogId == blog.Id
				select p;
		}

		private IQueryable<oxlet_Post> GetPostsQuery(Blog.Models.Blog blog, Language language) {
			return
				from p in _context.oxlet_Posts
				where p.LanguageId == language.Id && p.State != (byte)EntityState.Removed && p.BlogId == blog.Id
				select p;
		}

		private IQueryable<oxlet_Post> GetPostsQuery(Blog.Models.Blog blog, SearchCriteria searchCriteria) {
			return
				from p in _context.oxlet_Posts
				where p.SearchBody.Contains(searchCriteria.Term) && p.State != (byte)EntityState.Removed && p.BlogId == blog.Id
				select p;
		}

		private IQueryable<Post> ProjectPosts(IQueryable<oxlet_Post> posts) {
			return
				from p in posts
				join b in _context.oxlet_Blogs on p.BlogId equals b.Id
				let c = GetCommentsQuery(p.Id)
				let t = GetTagsQuery(p.Id)
				let l = GetLanguageQuery(p.LanguageId)
				where p.State != (byte)EntityState.Removed
				orderby p.PublishedDate descending
				select ProjectPost(b, p, c, t, l);
		}

		private static IQueryable<oxlet_Post> ExcludeNotYetPublished(IQueryable<oxlet_Post> query) {
			return query.Where(p => p.PublishedDate.HasValue && p.PublishedDate.Value <= DateTime.UtcNow);
		}

		private IQueryable<Tag> GetTagsQuery(int postId) {
			return
				from ptr in _context.oxlet_PostTagRelationships
				join t in _context.oxlet_Tags on ptr.TagId equals t.Id
				where ptr.PostId == postId
				orderby t.DisplayName
				select new Tag(t.Id, null, null, null, t.IsService);
		}

		private IQueryable<Comment> GetCommentsQuery(int postId) {
			return
				from c in _context.oxlet_Comments
				join p in _context.oxlet_Posts on c.PostId equals p.Id
				join b in _context.oxlet_Blogs on p.BlogId equals b.Id
				where c.PostId == postId
				orderby c.CreatedDate
				select new Comment (c.Id);
		}

		private IQueryable<Language> GetLanguageQuery(int languageId) {
			return
				from l in _context.oxlet_Languages
				where l.Id == languageId
				select new Language(l.Id, l.Name, l.DisplayName);
		}

		private static Post ProjectPost(oxlet_Blog blog, oxlet_Post post, IEnumerable<Comment> comments, IEnumerable<Tag> tags, IQueryable<Language> languages) {

			return new Post(
				post.Id,
				new UserDescription(post.CreatorUserId),
				post.Title,
				post.Body,
				post.BodyShort,
				(EntityState) post.State,
				post.Slug,
				post.CreatedDate,
				post.ModifiedDate,
				post.PublishedDate,
				comments.ToList(),
				post.CommentingDisabled,
				new BlogProjection(post.BlogId, blog.CommentingDisabled, blog.BlogName),
				tags.ToList(),
				languages.First()
			);
		}

		public IPagedList<Post> GetPosts(Blog.Models.Blog blog, Tag tag, PagingInfo pagingInfo) {
			return ProjectPosts(ExcludeNotYetPublished(GetPostsQuery(blog, tag))).GetPage(pagingInfo);
		}

		public IPagedList<Post> GetPostsWithDrafts(Blog.Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo) {
			return ProjectPosts(GetPostsQuery(blog, searchCriteria)).GetPage(pagingInfo);
		}

		public IPagedList<Post> GetPosts(Blog.Models.Blog blog, SearchCriteria searchCriteria, PagingInfo pagingInfo) {
			return ProjectPosts(ExcludeNotYetPublished(GetPostsQuery(blog, searchCriteria))).GetPage(pagingInfo);
		}

		public IPagedList<Post> GetPostsByArchive(Blog.Models.Blog blog, int year, int month, int day, PagingInfo pagingInfo) {
			var query =
				from b in _context.oxlet_Blogs
				join p in _context.oxlet_Posts on b.Id equals p.BlogId
				where p.PublishedDate != null && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal && p.PublishedDate.Value.Year == year && b.Id == blog.Id
				select p;

			if (month > 0){
				query = query.Where(p => p.PublishedDate.HasValue && p.PublishedDate.Value.Month == month);
			}

			if (day > 0){
				query = query.Where(p => p.PublishedDate.HasValue && p.PublishedDate.Value.Day == day);
			}

			return ProjectPosts(query).GetPage(pagingInfo);
		}

		public IEnumerable<DateTime> GetPostDateGroups(Blog.Models.Blog blog) {
			throw new NotImplementedException();
		}

		public Post GetPost(Blog.Models.Blog blog, string slug) {
			var post =
				from p in _context.oxlet_Posts
				where p.BlogId == blog.Id && string.Compare(p.Slug, slug, true) == 0
				select p;
			var result = ProjectPosts(post).FirstOrDefault();
			return result;
		}

		public Post GetPost(int id) {
			var post =
				from p in _context.oxlet_Posts
				where p.Id == id
				select p;
			return ProjectPosts(post).FirstOrDefault();
		}

		public Post GetRandomPost(Blog.Models.Blog blog) {
			throw new NotImplementedException();
		}

		public Post Save(Post post) {

			oxlet_Post postToSave = null;
			using (var transaction = new TransactionScope()) {
				if (post.Id != 0) {
					postToSave = _context.oxlet_Posts.FirstOrDefault(p => p.Id == post.Id);
				}

				if (postToSave == null) {
					postToSave = new oxlet_Post {Id = post.Id != 0 ? post.Id : 0};
					postToSave.CreatedDate = postToSave.ModifiedDate = DateTime.UtcNow;
					_context.oxlet_Posts.InsertOnSubmit(postToSave);
				}
				else {
					postToSave.ModifiedDate = DateTime.UtcNow;
				}

				postToSave.Body = post.Body;
				postToSave.BodyShort = post.BodyShort;
				postToSave.PublishedDate = post.Published;
				postToSave.Slug = post.Slug;
				postToSave.State = (byte) post.State;
				postToSave.Title = post.Title;
				postToSave.CommentingDisabled = post.CommentingDisabled;
				postToSave.BlogId = post.Blog.Id;
				postToSave.CreatorUserId = post.Creator.Id;
				postToSave.LanguageId = post.Language.Id;
				//TODO: (erikpo) Add an item to the search index
				postToSave.SearchBody = postToSave.Title + string.Join(
					"",
					post.Tags.Select(t => t.Name + t.DisplayName).ToArray()) + postToSave.Body + post.Creator.DisplayName + post.Creator.Name;

				_context.SubmitChanges();

				foreach (Tag tag in post.Tags) {

					if (!_context.oxlet_PostTagRelationships.Any(pt => pt.PostId == postToSave.Id && pt.TagId == tag.Id)) {
						_context.oxlet_PostTagRelationships.InsertOnSubmit(new oxlet_PostTagRelationship {
							PostId = postToSave.Id,
							TagId = tag.Id
						});
					}
				}

				var tagsRemoved =
					from pt in _context.oxlet_PostTagRelationships
					where pt.PostId == postToSave.Id && !post.Tags.Select(x => x.Id).Contains(pt.TagId)
					select pt;

				_context.oxlet_PostTagRelationships.DeleteAllOnSubmit(tagsRemoved);

				_context.SubmitChanges();
				transaction.Complete();
			}

			return 	GetPost(postToSave.Id);
		}

		public bool RemovePost(Post post) {
			oxlet_Post foundPost = _context.oxlet_Posts.FirstOrDefault(p => p.Id == post.Id);
			bool removedPost = false;

			if (foundPost != null) {
				foundPost.State = (byte)EntityState.Removed;
				_context.SubmitChanges();
				removedPost = true;
			}

			return removedPost;
		}

		public void RemoveAllPosts(Blog.Models.Blog blog) {
			throw new NotImplementedException();
		}

		public IEnumerable<KeyValuePair<ArchiveData, int>> GetArchives(Blog.Models.Blog blog) {
			return
				(from b in _context.oxlet_Blogs
				join p in _context.oxlet_Posts on b.Id equals p.BlogId
				where p.PublishedDate != null && p.PublishedDate <= DateTime.UtcNow && p.State == (byte)EntityState.Normal && b.SiteId == _siteContext.Site.Id
				let month = new DateTime(p.PublishedDate.Value.Year, p.PublishedDate.Value.Month, 1)
				group p by month into months
				orderby months.Key descending
				select new KeyValuePair<ArchiveData, int>(new ArchiveData(months.Key.Year + "/" + months.Key.Month), months.Count())).ToList();
		}

		public Tag Save(Post post, Tag tag) {
			throw new NotImplementedException();
		}
	}
}
