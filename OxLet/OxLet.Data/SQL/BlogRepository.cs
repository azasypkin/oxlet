﻿using System.Linq;
using OxLet.Blog.Repositories.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Models.Projections;

namespace OxLet.Data.SQL {
	public class BlogRepository : IBlogRepository {
		private readonly OxletDataContext _context;
		private readonly int _siteId;

		public BlogRepository(OxletDataContext context, Site site) {
			_context = context;
			_siteId = site.Id;
		}

		#region IBlogRepository Members

		public Blog.Models.Blog GetBlog(string name) {
			var query =
				from b in _context.oxlet_Blogs
				where b.BlogName == name && b.SiteId == _siteId
				select b;

			return ProjectBlogs(query).FirstOrDefault();
		}

		public Blog.Models.Blog GetBlog(int blogId) {
			var query =
				from s in _context.oxlet_Blogs
				where s.Id == blogId
				select s;

			return ProjectBlogs(query).FirstOrDefault();
		}

		public Blog.Models.Blog Save(Blog.Models.Blog blog) {
			oxlet_Blog dbBlog = null;

			if (blog.Id != 0) {
				dbBlog =
					(from s in _context.oxlet_Blogs where s.Id == blog.Id || s.BlogName == blog.Name select s).FirstOrDefault();
			}

			if (dbBlog == null) {

				dbBlog = new oxlet_Blog {
					SiteId = blog.Site.Id != 0 ? blog.Site.Id : _siteId,
				};

				_context.oxlet_Blogs.InsertOnSubmit(dbBlog);

				dbBlog.Id = blog.Id == 0 ? 0 : blog.Id;

				MakeFieldsMatch(dbBlog, blog);
			}
			else {
				MakeFieldsMatch(dbBlog, blog);
			}

			_context.SubmitChanges();

			return GetBlog(dbBlog.BlogName);
		}

		#endregion

		private static IQueryable<Blog.Models.Blog> ProjectBlogs(IQueryable<oxlet_Blog> query) {
			return
				from s in query
				select new Blog.Models.Blog(
					s.Id,
					new SiteProjection(s.SiteId),
					s.BlogName,
					s.BlogDisplayName,
					s.BlogDescription,
					(EntityState)s.CommentStateDefault,
					s.AuthorAutoSubscribe,
					s.PostEditTimeout,
					s.SkinDefault,
					s.CommentingDisabled,
					s.CreatedDate,
					s.ModifiedDate
				);
		}

		private static void MakeFieldsMatch(oxlet_Blog dbBlog, Blog.Models.Blog blog) {
			dbBlog.BlogName = blog.Name;
			dbBlog.BlogDisplayName = blog.DisplayName;
			dbBlog.BlogDescription = blog.Description;
			dbBlog.SkinDefault = blog.SkinDefault;
			dbBlog.AuthorAutoSubscribe = blog.AuthorAutoSubscribe;
			dbBlog.CommentingDisabled = blog.CommentingDisabled;
			dbBlog.PostEditTimeout = blog.PostEditTimeout;
			dbBlog.SkinDefault = blog.SkinDefault;
			if(blog.Created.HasValue) {
				dbBlog.CreatedDate = blog.Created.Value;
			}
			if (blog.Modified.HasValue) {
				dbBlog.ModifiedDate = blog.Modified.Value;
			}
			dbBlog.CommentStateDefault = (byte)blog.CommentStateDefault;
		}


		public IPagedList<Blog.Models.Blog> GetBlogs(PagingInfo pagingInfo) {
			var blogs = from b in _context.oxlet_Blogs where b.SiteId == _siteId select b;
			return ProjectBlogs(blogs).GetPage(pagingInfo);
		}
	}
}
