﻿using System;
using System.Linq;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Data.SQL {
	public class LanguageRepository : ILanguageRepository {
		private readonly OxletDataContext _context;

		public LanguageRepository(OxletDataContext context) {
			_context = context;
		}

		#region ILanguageRepository Members

		public Language GetLanguage(string name) {
			return (
				from l in _context.oxlet_Languages
				where l.Name == name
				select new Language(l.Id, l.Name, l.DisplayName) 
			).FirstOrDefault();
		}

		public void Save(Language language) {
			oxlet_Language dbLanguage = null;
			int languageId = language.Id;

			if (languageId != 0) {
				dbLanguage = (from l in _context.oxlet_Languages where l.Id == languageId select l).FirstOrDefault();
			}

			if (dbLanguage == null) {
				dbLanguage = new oxlet_Language();

				_context.oxlet_Languages.InsertOnSubmit(dbLanguage);
			}

			dbLanguage.Name = language.Name;
			dbLanguage.DisplayName = language.DisplayName;

			_context.SubmitChanges();

			//language.Id = languageId;
		}

		public IPagedList<Language> GetLanguages(PagingInfo pagingInfo) {
			return ProjectLanguages(GetLanguagesQuery()).GetPage(pagingInfo);
		}

		private static IQueryable<Language> ProjectLanguages(IQueryable<oxlet_Language> languages) {
			return
				from l in languages
				orderby l.DisplayName
				select ProjectLanguage(l);
		}

		private static Language ProjectLanguage(oxlet_Language language) {
			return new Language(language.Id, language.Name, language.DisplayName);
		}

		private IQueryable<oxlet_Language> GetLanguagesQuery() {
			return
			from l in _context.oxlet_Languages
			select l;
		}


		#endregion
	}
}
