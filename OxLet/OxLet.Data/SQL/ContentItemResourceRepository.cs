﻿using System;
using System.Linq;
using System.Transactions;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Data.SQL {
	public class ContentItemResourceRepository : IContentItemResourceRepository {
		private readonly OxletDataContext _context;
		private readonly IFileResourceRepository _fileResourceRepository;

		public ContentItemResourceRepository(OxletDataContext context, IFileResourceRepository fileResourceRepository) {
			_context = context;
			_fileResourceRepository = fileResourceRepository;
		}

		private static IQueryable<ContentItemResource> ProjectContentItemResources(IQueryable<oxlet_ContentItemResource> contentItemResources) {
			return
				from cir in contentItemResources
				where cir.State != (byte)EntityState.Removed
				select ProjectContentItemResource(cir);
		}

		private static ContentItemResource ProjectContentItemResource(oxlet_ContentItemResource contentItemResource) {
				return new ContentItemResource(
					contentItemResource.Id,
					contentItemResource.FileResourceId.HasValue 
						? new FileResource(contentItemResource.FileResourceId.Value)
						: null,
 					contentItemResource.Path,
					(EntityState)contentItemResource.State,
					contentItemResource.Created,
					contentItemResource.Modified
				);
		}


		public ContentItemResource GetContentItemResource(int id) {
			var contentItemResources =
				from cir in _context.oxlet_ContentItemResources
				where cir.Id == id
				select cir;
			return ProjectContentItemResources(contentItemResources).FirstOrDefault();
		}

		public ContentItemResource SaveContentItemResource(ContentItemResource contentItemResource) {
			oxlet_ContentItemResource contentItemResourceToSave = null;


			if (contentItemResource.Id != 0) {
				contentItemResourceToSave = _context.oxlet_ContentItemResources.FirstOrDefault(p => p.Id == contentItemResource.Id);
			}

			if (contentItemResourceToSave == null) {
				contentItemResourceToSave = new oxlet_ContentItemResource();
				contentItemResourceToSave.Created = contentItemResourceToSave.Modified = DateTime.UtcNow;
				_context.oxlet_ContentItemResources.InsertOnSubmit(contentItemResourceToSave);
			}
			else {
				contentItemResourceToSave.Modified = DateTime.UtcNow;
			}

			contentItemResourceToSave.Path = contentItemResource.Path;
			FileResource value = SaveFileResource(contentItemResource.File);
			if (value != null) {
				contentItemResourceToSave.FileResourceId = value.Id;
			}
			contentItemResourceToSave.State = (byte) contentItemResource.State;
			using (var transaction = new TransactionScope()) {
				_context.SubmitChanges();
				transaction.Complete();
			}

			return GetContentItemResource(contentItemResourceToSave.Id);
		}

		private FileResource SaveFileResource(FileResource file) {
			return file != null ? _fileResourceRepository.SaveFile(file) : null;
		}
	}
}
