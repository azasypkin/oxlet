﻿using System;
using System.Linq;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Data.SQL {
	public class UserDescriptionRepository : IUserDescriptionRepository {
		private readonly OxletDataContext _context;
		private readonly Site _site;

		public UserDescriptionRepository(OxletDataContext context, Site site) {
			_context = context;
			_site = site;
		}

		#region IUserDescriptionRepository Members

		public UserDescription GetUserDescription(string name) {

			var query =
				from u in _context.oxlet_Users
				where string.Compare(u.Name, name, true) == 0
 				select u;

			return ProjectUserDescription(query);
		}

		public UserDescription GetUserDescription(int id) {
			var query =
				from u in _context.oxlet_Users
				where u.Id == id
				select u;
			return ProjectUserDescription(query);
		}

		public UserDescription Save(UserDescription user) {
			oxlet_User dbUser = null;
			int userId = user.Id;

			if (userId != 0) {
				dbUser = (from u in _context.oxlet_Users where u.Id == userId select u).FirstOrDefault();
			}

			if (dbUser == null) {
				dbUser = new oxlet_User();

				_context.oxlet_Users.InsertOnSubmit(dbUser);
			}

			dbUser.DefaultLanguageId = _site.DefaultLanguage.Id;
			dbUser.Name = user.Name;
			dbUser.DisplayName = user.DisplayName;
			dbUser.Email = user.Email;
			dbUser.EmailHash = user.EmailHash;

			_context.SubmitChanges();

			return GetUserDescription(userId);
		}

		private static UserDescription ProjectUserDescription(IQueryable<oxlet_User> userQuery) {
			var user = userQuery.FirstOrDefault();
			if(user != null) {
				return new UserDescription(user.Id) {
					DisplayName = user.DisplayName,
					Email = user.Email,
					EmailHash = user.EmailHash,
					Name = user.Name
				};
			}
			return null;
		}

		#endregion
	}
}
