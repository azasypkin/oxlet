﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Models.Projections;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Data.SQL {
	public class ContentItemRepository : IContentItemRepository {
		private readonly OxletDataContext _context;
		private readonly SiteContext _siteContext;
		private readonly IContentItemResourceRepository _contentItemResourceRepository;

		public ContentItemRepository(OxletDataContext context, SiteContext siteContext, IContentItemResourceRepository contentItemResourceRepository) {
			_context = context;
			_siteContext = siteContext;
			_contentItemResourceRepository = contentItemResourceRepository;
		}

		private IQueryable<ContentItem> ProjectContentItems(IQueryable<oxlet_ContentItem> contentItems) {
			return
				from ci in contentItems
				join l in _context.oxlet_Languages on ci.LanguageId equals l.Id
				join v in _context.oxlet_ContentItemResources on ci.ValueResourceId equals v.Id into vrr
				join t in _context.oxlet_ContentItemResources on ci.ThumbnailResourceId equals t.Id into trr
				join p in _context.oxlet_ContentItems on ci.ParentId equals p.Id into prr
				from vr in vrr.DefaultIfEmpty()
				from tr in trr.DefaultIfEmpty()
				from pr in prr.DefaultIfEmpty()
				let c = GetCommentsQuery(ci.Id)
				let ch = GetChildrenQuery(ci.Id)
				where ci.State != (byte)EntityState.Removed
				orderby ci.Published descending
				select ProjectContentItem(ci, pr, l, vr, tr, ch, c);
		}

		private static IQueryable<oxlet_ContentItem> ExcludeNotYetPublished(IQueryable<oxlet_ContentItem> query) {
			return query.Where(p => p.Published.HasValue && p.Published.Value <= DateTime.UtcNow);
		}

		private IQueryable<oxlet_ContentItemComment> GetCommentsQuery(int contentItemId) {

			return
				from c in _context.oxlet_ContentItemComments
				where c.ContentItemId == contentItemId && c.State == (byte) EntityState.Normal
				orderby c.CreatedDate descending
				select c;

		}

		private IQueryable<oxlet_ContentItem> GetChildrenQuery(int contentItemId) {

			return
				from ci in _context.oxlet_ContentItems
				where ci.ParentId== contentItemId && ci.State == (byte) EntityState.Normal
				orderby ci.Created descending
				select ci;

		}

		private static ContentItem ProjectContentItem(
			oxlet_ContentItem contentItem,
			oxlet_ContentItem parent,
			oxlet_Language language,
			oxlet_ContentItemResource valueResource,
			oxlet_ContentItemResource thumbnailResource,
			IQueryable<oxlet_ContentItem> children,
			IEnumerable<oxlet_ContentItemComment> comments) {

				return new ContentItem(
				contentItem.Id,
				contentItem.Name,
				contentItem.DispalyName,
				contentItem.Slug,
				contentItem.ParentSlug,
				parent != null ? new ContentItemProjection(parent.Id, parent.DispalyName, parent.Slug, parent.ParentSlug, parent.IsFolder) : null,
 				valueResource != null ? new ContentItemResource(
					valueResource.Id,
					valueResource.FileResourceId.HasValue 
						? new FileResource(valueResource.FileResourceId.Value)
						: null,
					valueResource.Path,
					(EntityState)valueResource.State,
					valueResource.Created,
					valueResource.Modified
				) : null,
				thumbnailResource != null ? new ContentItemResource(
					thumbnailResource.Id,
					thumbnailResource.FileResourceId.HasValue 
						? new FileResource(thumbnailResource.FileResourceId.Value)
						: null,
					thumbnailResource.Path,
					(EntityState)thumbnailResource.State,
					thumbnailResource.Created,
					thumbnailResource.Modified
				) : null,
				contentItem.Created,
				contentItem.Modified,
				contentItem.Published,
				(EntityState)contentItem.State,
				new Language(language.Id, language.Name, language.DisplayName),
				contentItem.IsFolder,
				new UserDescription(contentItem.CreatorId),
				comments.Select(x=>new ContentItemCommentProjection(x.Id, x.Slug, x.ParentId)),
				children.Select(x=>new ContentItemProjection(x.Id, x.DispalyName, x.Slug, x.ParentSlug, x.IsFolder))
			);
		}

		public bool RemoveContentItem(ContentItem contentItem) {
			oxlet_ContentItem foundContentItem = _context.oxlet_ContentItems.FirstOrDefault(p => p.Id == contentItem.Id);
			bool removedContentItem = false;

			if (foundContentItem != null) {
				foundContentItem.State = (byte)EntityState.Removed;
				_context.SubmitChanges();
				removedContentItem = true;
			}

			return removedContentItem;
		}

		public ContentItem GetContentItem(int id) {
			var contentItems =
				from ci in _context.oxlet_ContentItems
				where ci.Id == id
				select ci;
			return ProjectContentItems(contentItems).FirstOrDefault();
		}

		public ContentItem GetContentItem(ContentItemPath path, bool includeDrafts) {
			var pathString = path.ToString();
			var index = pathString.LastIndexOf('/');
			var contentItems =
				from ci in _context.oxlet_ContentItems
				where ci.Slug == pathString.Substring(index + 1).TrimEnd('/') && ci.ParentSlug == pathString.Substring(0, index + 1)
				select ci;
			if(!includeDrafts) {
				contentItems = ExcludeNotYetPublished(contentItems);
			}
			return ProjectContentItems(contentItems).FirstOrDefault();
		}

		public IPagedList<ContentItem> GetContentItems(ContentItem parent, PagingInfo pagingInfo) {
			return ProjectContentItems(ExcludeNotYetPublished(GetContentItemByParentQuery(parent))).GetPage(pagingInfo);
		}

		public IPagedList<ContentItem> GetContentItemsWithDrafts(ContentItem parent, PagingInfo pagingInfo) {
			return ProjectContentItems(GetContentItemByParentQuery(parent)).GetPage(pagingInfo);
		}

		private IQueryable<oxlet_ContentItem> GetContentItemByParentQuery(ContentItem parent) {
			return parent != null
				? from ci in _context.oxlet_ContentItems
					where ci.ParentId.HasValue && ci.ParentId == parent.Id
					select ci
				: from ci in _context.oxlet_ContentItems
					where !ci.ParentId.HasValue
					select ci;
		}

		private IQueryable<oxlet_ContentItem> GetContentItemQuery(ContentItem parent, string slug) {
			return parent != null
				? from ci in _context.oxlet_ContentItems
				  where ci.ParentId.HasValue && ci.ParentId == parent.Id && string.Compare(ci.Slug, slug, true) == 0
				  select ci
				: from ci in _context.oxlet_ContentItems
				  where ci.ParentId == null && string.Compare(ci.Slug, slug, true) == 0
				  select ci;
		}

		public ContentItem GetContentItem(ContentItem parent, string slug, bool includeDrafts) {
			return ProjectContentItems(includeDrafts ? GetContentItemQuery(parent, slug) : ExcludeNotYetPublished(GetContentItemQuery(parent, slug))).FirstOrDefault();
		}

		public ContentItem SaveContentItem(ContentItem contentItem) {
			oxlet_ContentItem contentItemToSave = null;

			if (contentItem.Id != 0) {
				contentItemToSave = _context.oxlet_ContentItems.FirstOrDefault(p => p.Id == contentItem.Id);
			}

			if (contentItemToSave == null) {
				contentItemToSave = new oxlet_ContentItem();
				contentItemToSave.Created = contentItemToSave.Modified = DateTime.UtcNow;
				contentItemToSave.ParentSlug = _context.oxlet_ContentItems
					.Where(x => x.Id == contentItem.Parent.Id).Select(x => string.Format("{0}{1}/", x.ParentSlug, x.Slug))
					.Single();
				_context.oxlet_ContentItems.InsertOnSubmit(contentItemToSave);
			}
			else {
				contentItemToSave.Modified = DateTime.UtcNow;
			}

			contentItemToSave.Name = contentItem.Name;
			contentItemToSave.DispalyName = contentItem.DisplayName;
			contentItemToSave.Published = contentItem.Published;
			contentItemToSave.Slug = contentItem.Slug;
			contentItemToSave.State = (byte) contentItem.State;
			contentItemToSave.CreatorId = contentItem.Creator.Id;
			contentItemToSave.IsFolder = contentItem.IsFolder;
			contentItemToSave.LanguageId = contentItem.Language.Id;
			if (contentItem.Parent != null && contentItem.Parent.Id > 0) {
				contentItemToSave.ParentId = contentItem.Parent.Id;
			}
			contentItemToSave.SiteId = _siteContext.Site.Id;

			// check if thumbnail was set earlier
			if (contentItem.Thumbnail != null) {
				var thumbnail = _contentItemResourceRepository.SaveContentItemResource(contentItem.Thumbnail);
				contentItemToSave.ThumbnailResourceId = thumbnail.Id;
			}

			// check if value was set
			if (contentItem.Value != null) {
				var value = _contentItemResourceRepository.SaveContentItemResource(contentItem.Value);
				contentItemToSave.ValueResourceId = value.Id;
			}

			using (var transaction = new TransactionScope()) {
				_context.SubmitChanges();
				transaction.Complete();
			}

			return GetContentItem(contentItemToSave.Id);
		}
	}
}
