﻿using System;
using System.Collections.Generic;
using System.Linq;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Data.SQL {
	public class SiteRepository : ISiteRepository {
		private readonly OxletDataContext _context;

		public SiteRepository(OxletDataContext context) {
			_context = context;
		}

		#region ISiteRepository Members

		public Site GetSite(string name) {
			var query =
				from s in _context.oxlet_Sites
				where s.SiteName == name
				select s;

			return ProjectSites(query).FirstOrDefault();
		}

		public Site Save(Site site) {
			oxlet_Site dbSite = null;

			if (site.Id != 0) {
				dbSite =
					(from s in _context.oxlet_Sites where s.Id == site.Id || s.SiteName == site.Name select s).FirstOrDefault();
			}

			if (dbSite == null) {
				dbSite = new oxlet_Site();

				_context.oxlet_Sites.InsertOnSubmit(dbSite);

				MakeFieldsMatch(dbSite, site);
			}
			else {
				MakeFieldsMatch(dbSite, site);

				var siteRedirectsQuery =
					from sr in _context.oxlet_SiteRedirects
					where sr.SiteId == dbSite.Id
					select sr;

				if (siteRedirectsQuery.Count() > 0) {
					_context.oxlet_SiteRedirects.DeleteAllOnSubmit(siteRedirectsQuery);
				}
			}

			if (site.HostRedirects != null && site.HostRedirects.Count > 0) {
				_context.oxlet_SiteRedirects.InsertAllOnSubmit(
					site.HostRedirects.Select(
						hr =>
						new oxlet_SiteRedirect {
						                       	SiteId = dbSite.Id,
						                       	SiteRedirect = hr.ToString()
						                       }
						)
					);
			}

			_context.SubmitChanges();
			return GetSite(dbSite.SiteName);
		}

		#endregion

		private IQueryable<Site> ProjectSites(IQueryable<oxlet_Site> query) {
			return
				from s in query
				let redirects = GetRedirectsQuery(s)
				let language = GetLanguageQuery(s)
				select new Site(
					s.Id,
					ProjectSiteRedirects(redirects),
					new Uri(s.SiteHost),
					s.SiteName,
					s.SiteDisplayName,
					s.SiteDescription,
					ProjectLanguage(language),
					s.TimeZoneOffset,
					s.PageTitleSeparator,
					s.FavIconUrl,
					s.ScriptsPath,
					s.CssPath,
					s.ImagesPath,
					s.IncludeOpenSearch,
					s.GravatarDefault,
					s.SkinDefault,
					s.RouteUrlPrefix
				);
		}

		private IQueryable<oxlet_SiteRedirect> GetRedirectsQuery(oxlet_Site site) {
			return
				from sr in _context.oxlet_SiteRedirects
				where sr.SiteId == site.Id
				select sr;
		}

		private IQueryable<oxlet_Language> GetLanguageQuery(oxlet_Site site) {
			return
				from l in _context.oxlet_Languages
				where l.Id == site.DefaultLanguageId
				select l;
		}

		private static IList<Uri> ProjectSiteRedirects(IQueryable<oxlet_SiteRedirect> siteRedirects) {
			var query =
				from sr in siteRedirects
				select new Uri(sr.SiteRedirect);

			return query.ToList();
		}

		private Language ProjectLanguage(IQueryable<oxlet_Language> languages) {
			var query =
				from l in languages
				select new Language(l.Id, l.Name, l.DisplayName);

			return query.FirstOrDefault();
		}

		private static void MakeFieldsMatch(oxlet_Site dbSite, Site site) {
			dbSite.SiteHost = site.Host.ToString();
			dbSite.SiteName = site.Name;
			dbSite.SiteDisplayName = site.DisplayName;
			dbSite.SiteDescription = site.Description;
			dbSite.DefaultLanguageId = site.DefaultLanguage.Id;
			dbSite.TimeZoneOffset = site.TimeZoneOffset;
			dbSite.PageTitleSeparator = site.PageTitleSeparator;
			dbSite.FavIconUrl = site.FavIconUrl;
			dbSite.ScriptsPath = site.ScriptsPath;
			dbSite.CssPath = site.CssPath;
			dbSite.ImagesPath = site.ImagesPath;
			dbSite.IncludeOpenSearch = site.IncludeOpenSearch;
			dbSite.GravatarDefault = site.GravatarDefault;
			dbSite.SkinDefault = site.SkinDefault;
			dbSite.RouteUrlPrefix = site.RouteUrlPrefix;
		}
	}
}
