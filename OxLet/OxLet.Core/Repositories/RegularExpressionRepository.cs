﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;

namespace OxLet.Core.Repositories {
	class RegularExpressionRepository:IRegularExpressionRepository {
		public IEnumerable<RegularExpressionDescription> GetRegularExpressions() {
			return new[] {
				new RegularExpressionDescription {
					Name = "IsEmail",
					Expression = @"^[a-z0-9]+([-+\.]*[a-z0-9]+)*@[a-z0-9]+([-\.][a-z0-9]+)*$",
					Options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline
				},
				new RegularExpressionDescription {
					Name = "IsUrl",
					Expression = "^https?://(?:[^./\\s'\"<)\\]]+\\.)+[^./\\s'\"<\")\\]]+(?:/[^'\"<]*)*$",
					Options = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline
				},
				new RegularExpressionDescription {
					Name = "Username",
					Expression = "[^a-z0-9]",
					Options = RegexOptions.Compiled
				}
			};
		}

		public void SaveRegularExpression(RegularExpressionDescription regularExpressionDescription) {
			throw new NotImplementedException();
		}
	}
}
