﻿using System.Collections.Generic;
using OxLet.Core.Models;

namespace OxLet.Core.Repositories.Interfaces {
	public interface IRegularExpressionRepository {
		IEnumerable<RegularExpressionDescription> GetRegularExpressions();
		void SaveRegularExpression(RegularExpressionDescription regularExpressionDescription);
	}
}
