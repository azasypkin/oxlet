﻿using OxLet.Core.Models;

namespace OxLet.Core.Repositories.Interfaces {
	public interface ILanguageRepository {
		Language GetLanguage(string name);
		IPagedList<Language> GetLanguages(PagingInfo pagingInfo);
		void Save(Language language);
	}
}
