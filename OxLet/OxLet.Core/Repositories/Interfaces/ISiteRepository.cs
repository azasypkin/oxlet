﻿using OxLet.Core.Models;

namespace OxLet.Core.Repositories.Interfaces {
	public interface ISiteRepository {
		Site GetSite(string name);
		Site Save(Site site);
	}
}
