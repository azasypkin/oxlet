﻿using OxLet.Core.Models;

namespace OxLet.Core.Repositories.Interfaces {
	public interface IUserDescriptionRepository {
		UserDescription GetUserDescription(string name);
		UserDescription GetUserDescription(int id);
		UserDescription Save(UserDescription user);
	}
}
