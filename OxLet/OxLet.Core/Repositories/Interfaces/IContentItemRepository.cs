﻿using OxLet.Core.Models;

namespace OxLet.Core.Repositories.Interfaces {
	public interface IContentItemRepository {
		ContentItem GetContentItem(int id);
		IPagedList<ContentItem> GetContentItems(ContentItem parent, PagingInfo pagingInfo);
		IPagedList<ContentItem> GetContentItemsWithDrafts(ContentItem parent, PagingInfo pagingInfo);
		ContentItem GetContentItem(ContentItem parent, string slug, bool includeDrafts);
		ContentItem GetContentItem(ContentItemPath path, bool includeDrafts);
		ContentItem SaveContentItem(ContentItem item);
		bool RemoveContentItem(ContentItem item);
	}
}
