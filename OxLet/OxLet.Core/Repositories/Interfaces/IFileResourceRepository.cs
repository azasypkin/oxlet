﻿using OxLet.Core.Models;

namespace OxLet.Core.Repositories.Interfaces { 
	public interface IFileResourceRepository {
		FileResource GetFile(int id);
		FileResource SaveFile(FileResource file);
	}
}
