﻿using OxLet.Core.Models;

namespace OxLet.Core.Repositories.Interfaces {
	public interface IContentItemResourceRepository {
		ContentItemResource GetContentItemResource(int id);
		ContentItemResource SaveContentItemResource(ContentItemResource item);
	}
}
