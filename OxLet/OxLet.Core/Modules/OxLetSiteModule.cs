﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Core.Configuration;
using OxLet.Core.Infrastructure;
using OxLet.Core.Localization.Models.Input;
using OxLet.Core.Models;
using OxLet.Core.Models.Input;
using OxLet.Core.Routing;
using OxLet.Core.Web.Controllers;
using OxLet.Core.Web.Filters;
using Microsoft.Practices.Unity;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Modules {
	public class OxLetSiteModule : SiteModuleBase {
		public OxLetSiteModule(IUnityContainer container, OxLetSection section) : base(container, section) {
		}

		public override void RegisterRoutes(RouteCollection routes) {


			// register SEO related roots

			MapRoute(
				routes,
				"SiteMap",
				"sitemap.xml",
				new { controller = "SEO", action = "SiteMap" }
			);

			MapRoute(
				routes,
				"Robots",
				"robots.txt",
				new { controller = "SEO", action = "RobotsTxt" }
			);

			MapRoute(
				routes,
				"OpenSearch",
				"opensearch.xml",
				new { controller = "SEO", action = "OpenSearch" },
				null
			);

			// register authentification related roots

			MapRoute(
				routes,
				"SignOut",
				"signout",
				new { controller = "User", action = "SignOut" }
			);

			MapRoute(
				routes,
				"UserChangePassword",
				"changepassword",
				new { controller = "User", action = "ChangePassword", validateAntiForgeryToken = true }
			);

			MapRoute(
				routes,
				"SignIn",
				"signin",
				new { controller = "User", action = "SignIn" },
				true
			);

			MapRoute(
				routes,
				"ComputeHash",
				"computehash",
				new { controller = "Utility", action = "ComputeHash" }
			);

			MapRoute(
				routes,
				"FileLoad",
				"file/{fileId}",
				new { controller = "File", action = "Load" },
				new { fileId = new IsIntegerConstraint(0, int.MaxValue) }
			);

			MapRoute(
				routes,
				"ChangeLanguage",
				"language/{langCode}",
				new { controller = "Localization", action = "ChangeLanguage"}
			);

			//MapRoute(
			//    routes,
			//    "ContentItem",
			//    "{contentItemPath}",
			//    new { controller = "ContentItem", action = "List" },
			//    new { contentItemPath = new IsContentItemFolderConstraint(Container) },
			//    true
			//);

			MapRoute(
				routes,
				"AdminContentItemAdd",
				"admin/contentitems/{*contentItemPath}",
				new { controller = "ContentItem", action = "ItemAdd", contentItemPath = new IsContentItemPathConstraint(Container, ContentItemMode.Add, true) },
				null,
				true
			);

			MapRoute(
				routes,
				"AdminContentItemFolder",
				"admin/contentitems/{*contentItemPath}",
				new { controller = "ContentItem", action = "List", contentItemPath = UrlParameter.Optional },
				new { contentItemPath = new IsContentItemPathConstraint(Container, true) },
				true
			);

			MapRoute(
				routes,
				"AdminContentItem",
				"admin/contentitems/{*contentItemPath}",
				new { controller = "ContentItem", action = "Item" },
				new { contentItemPath = new IsContentItemPathConstraint(Container, false) },
				true
			);

			//MapRoute(
			//    routes,
			//    "NotFound",
			//    "{*url}",
			//    new { controller = "ErrorController", action = "NotFound" },
			//    true
			//);
		}

		public override void RegisterActionFilters(IActionFilterRegistry registry) {
			registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(AntiForgeryActionFilter));
			registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(SiteContextActionFilter));
			registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(NavigationActionFilter));
			//registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(LocalizationActionFilter));
			registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(AntiForgeryAuthorizationFilter));
			// register validation service filter
			registry.Add(Enumerable.Empty<IActionFilterCriteria>(), typeof(ValidationServiceActionFilter));

			var adminActionsCriteria = new ControllerActionCriteria();
			//adminActionsCriteria.AddMethod<PluginController>(p => p.List());
			//adminActionsCriteria.AddMethod<PluginController>(p => p.Item(0));

			//adminActionsCriteria.AddMethod<SiteController>(s => s.Dashboard());
			//if (Container.Resolve<Site>().Id != 0) {
			//    adminActionsCriteria.AddMethod<SiteController>(s => s.ItemEdit(null));
			//}
			//TODO: (erikpo) Once we have roles other than "authenticated" this should move to not be part of the admin, but just part of authed users
			
			adminActionsCriteria.AddMethod<UserController>(u => u.ChangePassword(null));
			adminActionsCriteria.AddMethod<ContentItemController>(u => u.List(null, 0, null));

			registry.Add(new[] { adminActionsCriteria }, typeof(AuthorizationFilter));

			var listActionsCriteria = new ControllerActionCriteria();

			listActionsCriteria.AddMethod<ContentItemController>(u => u.List(null, 0, null));
			registry.Add(new[] { listActionsCriteria }, typeof(PageSizeActionFilter));
		}

		public override void RegisterModelBinders(ModelBinderDictionary binders) {
			binders[typeof(SiteContext)] = Container.Resolve<IModelBinder>("SiteContextModelBinder");
			binders[typeof (Language)] = Container.Resolve<IModelBinder>("LanguageModelBinder");
			binders[typeof(ContentItemPath)] = Container.Resolve<IModelBinder>("ContentItemPathModelBinder");
			binders[typeof(ContentItemInput)] = Container.Resolve<IModelBinder>("ContentItemInputModelBinder");
			binders[typeof(SearchCriteria)] = Container.Resolve<IModelBinder>("SearchCriteriaModelBinder");
			binders[typeof(PagingInfo)] = Container.Resolve<IModelBinder>("PagingInfoModelBinder");
			binders[typeof(LanguageInput)] = Container.Resolve<IModelBinder>("LanguageInputModelBinder");
		}

		public override void RegisterInContainer(IUnityContainer container) {
		}

		public override string CongfigurationName {
			get { return "OxLetSiteModule"; }
		}

		
	}
}
