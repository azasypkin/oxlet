﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Core.Configuration;
using OxLet.Core.Modules.Interfaces;
using OxLet.Core.Routing;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Modules {
	public abstract class SiteModuleBase:ISiteModule {

		#region Fields

		private readonly SiteModuleElement _section;
		private readonly OxLetSection _configuration;
		private readonly string[] _controllerNamespaces;
		private readonly string _areaName;
		private readonly IUnityContainer _container;

		#endregion

		#region Properties

		public abstract string CongfigurationName { get; }

		protected SiteModuleElement Section {
			get { return _section; }
		}

		protected OxLetSection Configuration {
			get { return _configuration; }
		}

		protected string[] ControllerNamespaces {
			get { return _controllerNamespaces; }
		}

		protected string AreaName {
			get { return _areaName; }
		}


		protected IUnityContainer Container {
			get { return _container; }
		}

		#endregion

		#region Methods

		protected SiteModuleBase(IUnityContainer container, OxLetSection section) {
			_configuration = section;
			_section = section.SiteModules.FirstOrDefault(item => item.Name == CongfigurationName);
			if (_section != null) {
				_controllerNamespaces = _section.ControllerNamespaces.Select(item => item.Value).ToArray();
				_areaName = _section.AreaName;
			}
			_container = container;
		}

		public abstract void RegisterRoutes(RouteCollection routes);

		public abstract void RegisterActionFilters(IActionFilterRegistry registry);

		public abstract void RegisterModelBinders(ModelBinderDictionary binders);

		public abstract void RegisterInContainer(IUnityContainer container);

		#endregion

		#region Helper methods

		protected Route MapRoute(RouteCollection routes, string name, string url, object defaults) {
			return MapRoute(routes, name, url, defaults, false);
		}

		protected Route MapRoute(RouteCollection routes, string name, string url, object defaults, bool useLocalization) {
			return MapRoute(routes, name, url, defaults, null, useLocalization);
		}

		protected Route MapRoute(RouteCollection routes, string name, string url, object defaults, object constraints) {
			return MapRoute(routes, name, url, defaults, constraints, false);
		}

		protected Route MapRoute(RouteCollection routes, string name, string url, object defaults, object constraints, bool useLocalization) {
			return MapRoute(routes, name, url, defaults, constraints, null /* namespaces */, useLocalization);
		}

		protected Route MapRoute(RouteCollection routes, string name, string url, string[] namespaces) {
			return MapRoute(routes, name, url, null /* defaults */, namespaces, false);
		}

		protected Route MapRoute(RouteCollection routes, string name, string url, object defaults, string[] namespaces, bool useLocalization) {
			return MapRoute(routes, name, url, defaults, null /* constraints */, namespaces, useLocalization);
		}

		protected Route MapRoute(RouteCollection routes, string name, string url, object defaults, object constraints, string[] namespaces, bool useLocalization) {
			if (namespaces == null && ControllerNamespaces != null) {
				namespaces = ControllerNamespaces;
			}
			
			Route route = routes.MapRoute(name, url, defaults, constraints, namespaces);

			if (!string.IsNullOrEmpty(AreaName)) {
				route.DataTokens["area"] = AreaName;

				// disabling the namespace lookup fallback mechanism keeps this areas from accidentally picking up
				// controllers belonging to other areas
				bool useNamespaceFallback = (namespaces == null || namespaces.Length == 0);
				route.DataTokens["UseNamespaceFallback"] = useNamespaceFallback;
			}

			if (useLocalization) {
				Route localizedRoute = routes.MapRoute(name + "Localized", url, defaults, constraints, namespaces);
				// first modify url to accept language parameter
				localizedRoute.Url = string.Format("{{lang}}/{0}", route.Url);
				// check if defaults are set and register language route value
				var defaultsDictionary = new RouteValueDictionary {{"lang", UrlParameter.Optional}};
				foreach (var routeDefaultValue in localizedRoute.Defaults) {
					defaultsDictionary.Add(routeDefaultValue.Key, routeDefaultValue.Value);
				}
				//localizedRoute.Defaults.Add("lang", UrlParameter.Optional);
				localizedRoute.Defaults = defaultsDictionary;

				var constraintsDictionary = new RouteValueDictionary { { "lang", new LanguageConstraint(Container) } };
				foreach (var routeConstraintValue in localizedRoute.Constraints) {
					constraintsDictionary.Add(routeConstraintValue.Key, routeConstraintValue.Value);
				}
				localizedRoute.Constraints = constraintsDictionary;

				// check if constraints are set and register language route constraint
				//localizedRoute.Constraints.Add("lang", new LanguageConstraint(Container));
				if (!string.IsNullOrEmpty(AreaName)) {
					localizedRoute.DataTokens["area"] = AreaName;

					// disabling the namespace lookup fallback mechanism keeps this areas from accidentally picking up
					// controllers belonging to other areas
					bool useNamespaceFallback = (namespaces == null || namespaces.Length == 0);
					localizedRoute.DataTokens["UseNamespaceFallback"] = useNamespaceFallback;
				}
			}

			return route;
		}

		#endregion
	}
}
