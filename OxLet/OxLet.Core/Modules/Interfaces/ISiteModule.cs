﻿using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Modules.Interfaces {
	public interface ISiteModule {
		void RegisterRoutes(RouteCollection routes);
		void RegisterActionFilters(IActionFilterRegistry registry);
		void RegisterModelBinders(ModelBinderDictionary binders);
		void RegisterInContainer(IUnityContainer container);
		string CongfigurationName { get; }
	}
}
