﻿using System;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Services {
	public class UserDescriptionService : IUserDescriptionService {

		private readonly IUserDescriptionRepository _repository;
		private readonly ICacheService _cacheService;

		public UserDescriptionService(IUserDescriptionRepository repository, ICacheService cacheService) {
			_repository = repository;
			_cacheService = cacheService;
			//[TODO]:Add methods for modifiing users + cache dependencies 
		}

		public UserDescription GetUserDescription(string name) {
			return _cacheService.GetItem(
				string.Format("UserByName:{0}", name),
				()=>_repository.GetUserDescription(name),
				x => x != null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
			);
		}

		public UserDescription GetUserDescription(int id) {
			return _cacheService.GetItem(
				string.Format("UserById:{0}", id),
				() => _repository.GetUserDescription(id),
				x => x != null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
			);
		}

		public ModelResult<UserDescription> Add(UserDescription user) {
			UserDescription userDescription;
			using (var transaction = new TransactionScope()) {
				userDescription = _repository.Save(user);
				transaction.Complete();
			}

			_cacheService.InvalidateItem(user);

			return new ModelResult<UserDescription>(userDescription, Enumerable.Empty<ModelValidationResult>());
			
		}

		public ModelResult<UserDescription> Edit(UserDescription user) {
			throw new NotImplementedException();
		}
	}
}
