﻿using System;
using System.Linq;
using System.Transactions;
using OxLet.Core.Cache;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Models.Input;
using OxLet.Core.Plugins.Interfaces;
using OxLet.Core.Repositories.Interfaces;
using OxLet.Core.Secuity.Interfaces;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;
using OxLet.Core.Web.Extensions;

namespace OxLet.Core.Services {
	public class ContentItemService : IContentItemService {
		private readonly IContentItemRepository _repository;
		private readonly IValidationService _validator;
		private readonly IUserService _userService;
		private readonly IFileResourceService _fileResourceService;
		private readonly ICacheService _cacheService;
		private readonly SiteContext _context;
		private readonly IPluginEngine _pluginEngine;

		public ContentItemService(
			IContentItemRepository repository,
			IValidationService validationService,
			ICacheService cacheService,
			IUserService userService,
			IFileResourceService fileResourceService,
			SiteContext siteContext,
			IPluginEngine pluginEngine) {

			_repository = repository;
			_validator = validationService;
			_userService = userService;
			_fileResourceService = fileResourceService;
			_cacheService = cacheService;
			_context = siteContext;
			_pluginEngine = pluginEngine;
		}

		public ContentItem GetItem(int id) {
			return _cacheService.GetItem(
					string.Format("GetContentItem:{0}", id),
					() => _repository.GetContentItem(id),
					p => p != null ? p.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
				)
				.FillCreator(_userService)
				.FillResources(_fileResourceService);
		}

		public ContentItem GetItem(ContentItemPath path) {
			return GetItem(path, _context.User.IsAuthenticated && _context.User.IsInRole("Administrator"));
		}

		public ContentItem GetItem(ContentItemPath path, bool includeDrafts) {
			return _cacheService.GetItem(
					string.Format("GetContentItem:{0}-IncludeDrafts:{1}", path, includeDrafts),
					() => _repository.GetContentItem(path, includeDrafts),
					p => p != null ? p.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
				)
				.FillCreator(_userService)
				.FillResources(_fileResourceService);
		}

		public bool IsItemExists(ContentItemPath path) {
			bool includeDrafts = _context.User.IsAuthenticated && _context.User.IsInRole("Administrator");
			return _cacheService.GetItem<bool?>(
				string.Format("GetContentItemExists-Name:{0}", path),
				() => GetItem(path) != null,
				null
			).GetValueOrDefault(false);
		}

		public IPagedList<ContentItem> GetItems(ContentItemPath path, PagingInfo pagingInfo) {
			bool includeDrafts = _context.User.IsAuthenticated && _context.User.IsInRole("Administrator");
			var parent = GetItem(path, includeDrafts);
			IPagedList<ContentItem> contentItems = _cacheService.GetItems<IPagedList<ContentItem>, ContentItem>(
				string.Format("ParentSlug:{0}-GetContentItems-IncludeDrafts:{1}", parent != null ? parent.Slug : "none", includeDrafts),
				() => includeDrafts ? _repository.GetContentItemsWithDrafts(parent, pagingInfo) : _repository.GetContentItems(parent, pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : parent != null ? new []{parent} : Enumerable.Empty<ICacheable>(),
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);

			if (!includeDrafts) {
				contentItems = contentItems.Since(p => p.Published.Value, _context.HttpContext.Request.IfModifiedSince());
			}

			return contentItems.FillCreator(_userService).FillResources(_fileResourceService);
		}

		public ModelResult<ContentItem> Add(ContentItemInput contentItemInput, EntityState state) {
			return AddContentItem(
				contentItemInput,
				() => contentItemInput.ToContentItem(_context.User.GetDescription(), state)
			);
		}

		public void Remove(ContentItem contentItem) {
			if (contentItem == null) {
				throw new ArgumentNullException("contentItem");
			}

			using (var transaction = new TransactionScope()) {
				if (_repository.RemoveContentItem(contentItem)) {
					InvalidateCachedContentItemForRemove(contentItem);
				}
				transaction.Complete();
			}

			// signal that contentItem has been added
			_pluginEngine.Fire("ContentItem:AfterRemove", new object[] { _context, contentItem });
		}

		private ModelResult<ContentItem> AddContentItem<T>(T contentItemInput, Func<ContentItem> generateContentItem) {
			var validationResults = _validator.Validate(contentItemInput);

			if (!validationResults.IsValid()) {
				return new ModelResult<ContentItem>(validationResults);
			}

			ContentItem contentItem = generateContentItem();

			//ValidateContentItem(contentItem, validationState);

			//if (!validationState.IsValid) {
			//    return new ModelResult<ContentItem>(validationState);
			//}

			//for (int i = 0; i < contentItem.Tags.Count; i++) {
			//    contentItem.Tags[i] = _tagService.AddTag(contentItem.Tags[i]).Item;
			//}

			contentItem = _repository.SaveContentItem(contentItem);

			InvalidateCachedContentItemDependencies(contentItem);

			// signal that contentItem has been added
			_pluginEngine.Fire("ContentItem:AfterAdd", new object[] { _context, contentItem });

			return new ModelResult<ContentItem>(contentItem, validationResults);
		}

		private void InvalidateCachedContentItemDependencies(ContentItem contentItem) {
			_cacheService.InvalidateItem(contentItem);
			if (contentItem.Parent != null && contentItem.Parent.Id > 0) {
				_cacheService.InvalidateItem(contentItem.Parent);
			}

			//post.Tags.ToList().ForEach(t => _cacheService.InvalidateItem(t));
		}

		//private void InvalidateCachedContentItemForEdit(ContentItem newContentItem, ContentItem originalContentItem) {

		//    // invalidate any tags that have changed since the edit
		//    //originalPost.Tags.Except(newPost.Tags).Union(newPost.Tags.Except(originalPost.Tags)).ToList().ForEach(t => _cacheService.InvalidateItem(t));
		//    _cacheService.InvalidateItem(originalContentItem);
		//    _cacheService.InvalidateItem(newContentItem);
		//}

		private void InvalidateCachedContentItemForRemove(ContentItem contentItemToRemove) {

			//contentItemToRemove.Tags.Except(contentItemToRemove.Tags).Union(contentItemToRemove.Tags.Except(contentItemToRemove.Tags)).ToList().ForEach(t => _cacheService.InvalidateItem(t));

			_cacheService.InvalidateItem(contentItemToRemove);
		}
	}
}
