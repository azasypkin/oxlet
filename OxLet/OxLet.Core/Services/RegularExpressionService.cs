﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Practices.Unity;
using OxLet.Core.Configuration;
using OxLet.Core.Models;
using OxLet.Core.Repositories;
using OxLet.Core.Repositories.Interfaces;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Services {
	public class RegularExpressionService:IRegularExpressionService {
		private readonly IEnumerable<IRegularExpressionRepository> _repositories;
		private readonly AppSettingsHelper _appSettings;
		private readonly Dictionary<string, Regex> _expressions;

		public RegularExpressionService(IUnityContainer container, AppSettingsHelper appSettings) {
			_appSettings = appSettings;
			_repositories = container.ResolveAll<IRegularExpressionRepository>();
			_expressions = _repositories.SelectMany(repository => repository.GetRegularExpressions()).ToDictionary(
				expression => expression.Name,
				expression => new Regex(_appSettings.GetValue(expression.Name, expression.Expression), expression.Options)
			);
		}

		public IEnumerable<RegularExpressionDescription> GetRegularExpressionDescriptions() {
			return _repositories.SelectMany(repository => repository.GetRegularExpressions());
		}

		public void SaveRegularExpressionDescription(RegularExpressionDescription regularExpressionDescription) {
			throw new NotImplementedException();
		}

		public Regex GetExpression(string expressionName) {
			return _expressions[expressionName];
		}

		public bool IsMatch(string expressionName, string input) {
			Regex expression = _expressions[expressionName];
			return expression.IsMatch(input);
		}

		public string Clean(string expressionName, string input) {
			Regex expression = _expressions[expressionName];
			return expression.Replace(input, "");
		}
	}
}
