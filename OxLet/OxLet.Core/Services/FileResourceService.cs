﻿using System;
using System.Transactions;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Models.Input;
using OxLet.Core.Repositories.Interfaces;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;

namespace OxLet.Core.Services {
	public class FileResourceService : IFileResourceService {

		private readonly IFileResourceRepository _repository;
		private readonly IValidationService _validator;
		private readonly SiteContext _context;

		public FileResourceService(SiteContext context, IFileResourceRepository repository, IValidationService validator) {
			_repository = repository;
			_validator = validator;
			_context = context;
		}

		public FileResource GetFileResource(int id) {
			var fileResource = _repository.GetFile(id);
			
			// otherwise we must cache content into the file
			if(fileResource.Data != null && fileResource.Data.Length > 0) {
				fileResource.SaveToDisk(_context);
				// clean data to free up memory and return
				return fileResource.FreeUpBytes();
			}

			return null;
		}

		public ModelResult<FileResource> AddFileResource(FileResourceInput fileResourceInput) {
			var validationState = _validator.Validate(fileResourceInput);

			if (!validationState.IsValid()) {
				return new ModelResult<FileResource>(validationState);
			}

			FileResource fileResource;

			using (var transaction = new TransactionScope()) {
				fileResource = _repository.SaveFile(
					fileResourceInput.ToFileResource(_context.User.GetDescription())
				);

				transaction.Complete();
			}


			return new ModelResult<FileResource>(fileResource, validationState);
		}
	}
}
