﻿using OxLet.Core.Cache;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Repositories.Interfaces;
using OxLet.Core.Services.Interfaces;
using System.Linq;

namespace OxLet.Core.Services {
	public class LanguageService : ILanguageService {
		private readonly ILanguageRepository _repository;
		private readonly ICacheService _cacheService;

		public LanguageService(ILanguageRepository repository, ICacheService cacheService) {
			_repository = repository;
			_cacheService = cacheService;
		}

		public Language GetLanguage(string name) {
			return _cacheService.GetItem(
				string.Format("GetLanguage:{0}", name),
				() => _repository.GetLanguage(name),
				p => p != null ? p.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>()
			);
		}

		public void AddLanguage(Language language) {
			_repository.Save(language);
		}

		public IPagedList<Language> GetLanguage(PagingInfo pagingInfo) {
			return _cacheService.GetItems<IPagedList<Language>, Language>(
				"GetLanguages",
				() => _repository.GetLanguages(pagingInfo),
				p => p != null ? p.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>(),
				new CachePage(pagingInfo.Index, pagingInfo.Size)
			);
		}
	}
}
