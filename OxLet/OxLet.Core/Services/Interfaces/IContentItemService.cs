﻿using OxLet.Core.Models;
using OxLet.Core.Models.Input;

namespace OxLet.Core.Services.Interfaces {
	public interface IContentItemService {
		ContentItem GetItem(int id);
		ContentItem GetItem(ContentItemPath path);
		ContentItem GetItem(ContentItemPath path, bool includeDrafts);

		IPagedList<ContentItem> GetItems(ContentItemPath path, PagingInfo pagingInfo);

		ModelResult<ContentItem> Add(ContentItemInput contentItemInput, EntityState state);
		//ModelResult<ContentItem> Edit(ContentItem contentItem, ContentItemInput contentItemInput, EntityState state);
		bool IsItemExists(ContentItemPath path);
		void Remove(ContentItem contentItem);
	}
}
