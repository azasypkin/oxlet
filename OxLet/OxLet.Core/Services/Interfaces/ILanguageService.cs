﻿using OxLet.Core.Models;

namespace OxLet.Core.Services.Interfaces {
	public interface ILanguageService {
		Language GetLanguage(string name);
		void AddLanguage(Language language);
		IPagedList<Language> GetLanguage(PagingInfo pagingInfo);
	}
}
