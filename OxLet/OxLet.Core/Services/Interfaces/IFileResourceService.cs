﻿using System;
using OxLet.Core.Models;
using OxLet.Core.Models.Input;

namespace OxLet.Core.Services.Interfaces {
	public interface IFileResourceService {
		FileResource GetFileResource(int id);
		ModelResult<FileResource> AddFileResource(FileResourceInput fileResource);
	}
}
