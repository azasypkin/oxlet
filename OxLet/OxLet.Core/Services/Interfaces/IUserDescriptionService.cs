﻿using System;
using OxLet.Core.Models;

namespace OxLet.Core.Services.Interfaces {
	public interface IUserDescriptionService {
		UserDescription GetUserDescription(string name);
		UserDescription GetUserDescription(int id);
		ModelResult<UserDescription> Add(UserDescription user);
		ModelResult<UserDescription> Edit(UserDescription user);
	}
}
