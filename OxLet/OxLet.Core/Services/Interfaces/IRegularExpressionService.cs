﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using OxLet.Core.Models;

namespace OxLet.Core.Services.Interfaces {
	public interface IRegularExpressionService {
		IEnumerable<RegularExpressionDescription> GetRegularExpressionDescriptions();
		void SaveRegularExpressionDescription(RegularExpressionDescription regularExpressionDescription);

		Regex GetExpression(string expressionName);
		bool IsMatch(string expressionName, string input);
		string Clean(string expressionName, string input);
	}
}
