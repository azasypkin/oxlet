﻿using OxLet.Core.Models;
using OxLet.Core.Models.Input;

namespace OxLet.Core.Services.Interfaces {
	public interface ISiteService {
		Site GetSite();
		//ModelResult<Site> AddSite(SiteInput site);
		//ModelResult<Site> EditSite(Site site, SiteInput siteInput);
	}
}
