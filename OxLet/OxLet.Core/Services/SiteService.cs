﻿using System.Transactions;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Configuration;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Models.Input;
using OxLet.Core.Repositories.Interfaces;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;

namespace OxLet.Core.Services {
	public class SiteService : ISiteService {
		private readonly ISiteRepository _repository;
		private readonly IValidationService _validator;
		private readonly ICacheService _cacheService;
		private readonly OxLetSection _configuration;

		public SiteService(ISiteRepository repository, IValidationService validator, ICacheService cacheService, OxLetSection configuration) {
			_repository = repository;
			_validator = validator;
			_configuration = configuration;
			_cacheService = cacheService;
		}

		#region ISiteService Members

		public Site GetSite() {
			return _cacheService.GetItem(
				"GetSite", 
				()=>_repository.GetSite(_configuration.SiteName),
				null
			);
		}

		//public ModelResult<Site> AddSite(SiteInput siteInput) {

		//    var validationState = new ValidationStateDictionary { { typeof(SiteInput), _validator.Validate(siteInput) } };

		//    if (!validationState.IsValid) {
		//        return new ModelResult<Site>(validationState);
		//    }

		//    Site site;

		//    using (var transaction = new TransactionScope()) {
		//        site = siteInput.ToSite();

		//        ValidateSite(site, validationState);

		//        if (!validationState.IsValid) {
		//            return new ModelResult<Site>(validationState);
		//        }

		//        site = _repository.Save(site);

		//        transaction.Complete();
		//    }

		//    return new ModelResult<Site>(site, validationState);

		//}

		//public ModelResult<Site> EditSite(Site site, SiteInput siteInput) {
		//    var validationState = new ValidationStateDictionary { { typeof(SiteInput), _validator.Validate(siteInput) } };

		//    if (!validationState.IsValid) {
		//        return new ModelResult<Site>(validationState);
		//    }

		//    Site originalSite = site;
		//    Site newSite;

		//    using (var transaction = new TransactionScope()) {
		//        newSite = originalSite.Apply(siteInput);

		//        ValidateSite(newSite, originalSite, validationState);

		//        if (!validationState.IsValid) return new ModelResult<Site>(validationState);

		//        newSite = _repository.Save(newSite);

		//        transaction.Complete();
		//    }

		//    return new ModelResult<Site>(newSite, validationState);
		//}

		//private void ValidateSite(Site newSite, ValidationStateDictionary validationState) {
		//    ValidateSite(newSite, newSite, validationState);
		//}

		//private void ValidateSite(Site newSite, Site originalSite, ValidationStateDictionary validationState) {
		//    var state = new ValidationState();

		//    validationState.Add(typeof(Site), state);

		//    Site foundSite = _repository.GetSite(newSite.Name);

		//    if (foundSite != null && newSite.Name != originalSite.Name) {
		//        state.Errors.Add(new ValidationError("Sites.NameNotUnique", newSite.Name, "A site already exists with the supplied name"));
		//    }
		//}

		#endregion
	}
}
