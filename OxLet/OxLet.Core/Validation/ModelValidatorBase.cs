﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Validation {
	public abstract class ModelValidatorBase<TModel> : IModelValidator<TModel> where TModel : class {

		#region Properties

		protected ILocalizationService LocalizationService { get; private set; }
		protected IRegularExpressionService RegularExpressionService { get; private set; }

		#endregion

		protected ModelValidatorBase(ILocalizationService localizationService, IRegularExpressionService regularExpressionService) {
			LocalizationService = localizationService;
			RegularExpressionService = regularExpressionService;
		}

		public abstract IEnumerable<ModelValidationResult> Validate(TModel model);

		public virtual IEnumerable<ModelClientValidationRule> GetClientValidationRules(string propertyName) {
			return Enumerable.Empty<ModelClientValidationRule>();
		}

		public IEnumerable<ModelValidationResult> Validate(object model) {
			return Validate(model as TModel);
		}
	}
}
