﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace OxLet.Core.Validation {
	public interface IValidationService {
		IEnumerable<ModelValidationResult> Validate<T>(T model);
		IModelValidator<T> GetValidatorFor<T>();
	}
}
