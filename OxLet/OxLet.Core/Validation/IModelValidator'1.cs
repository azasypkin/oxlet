﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace OxLet.Core.Validation {
	public interface IModelValidator<TModel> : IModelValidator {
		IEnumerable<ModelValidationResult> Validate(TModel model);
	}
}
