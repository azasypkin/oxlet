﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace OxLet.Core.Validation {
	public interface IModelValidator{
		IEnumerable<ModelClientValidationRule> GetClientValidationRules(string propertyName);
		IEnumerable<ModelValidationResult> Validate(object model);
	}
}
