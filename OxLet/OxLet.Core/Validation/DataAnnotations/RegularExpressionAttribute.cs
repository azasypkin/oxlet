﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace OxLet.Core.Validation.DataAnnotations {
	[AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
	public class RegularExpressionAttribute : System.ComponentModel.DataAnnotations.RegularExpressionAttribute, IClientValidatable {
		public RegularExpressionAttribute(string pattern) : base(pattern) {
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context) {
			var rule = new ModelClientValidationRegexRule(FormatErrorMessage(metadata.GetDisplayName()), Pattern) {
				ValidationType = "regexallowempty"
			};
			yield return rule;
		}
	}
}
