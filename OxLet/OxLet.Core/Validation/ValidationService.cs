﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.Practices.Unity;

namespace OxLet.Core.Validation {
	public class ValidationService : IValidationService {
		private readonly IUnityContainer _container;

		public ValidationService(IUnityContainer container) {
			_container = container;
		}

		public IModelValidator<T> GetValidatorFor<T>() {
			return _container.Resolve<IModelValidator<T>>();
		}

		public IEnumerable<ModelValidationResult> Validate<T>(T entity) {
			IModelValidator<T> validator = GetValidatorFor<T>();

			if (validator == null) {
				throw new Exception(string.Format("No validator found for type ({0})", entity.GetType()));
			}

			return validator.Validate(entity);
		}
	}
}
