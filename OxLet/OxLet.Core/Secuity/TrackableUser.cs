﻿using System;
using System.Security.Principal;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity {
	public class TrackableUser : IUser {
		private readonly Func<IUser> _getUser;
		private IUser _user;

		public TrackableUser(Func<IUser> getUser) {
			_getUser = getUser;
		}

		private IUser InternalUser {
			get { return _user ?? (_user = _getUser()); }
		}

		#region IUser Members

		public bool IsAuthenticated {
			get { return InternalUser.IsAuthenticated; }
		}

		public string Name {
			get { return InternalUser.Name; }
		}

		#endregion

		#region IPrincipal Members

		public IIdentity Identity {
			get { return InternalUser.Identity; }
		}

		public bool IsInRole(string role) {
			return InternalUser.IsInRole(role);
		}

		#endregion
	}
}
