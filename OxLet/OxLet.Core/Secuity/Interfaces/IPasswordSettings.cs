﻿namespace OxLet.Core.Secuity.Interfaces {
	public interface IPasswordSettings {
		IPasswordResetRetrievalSettings ResetOrRetrieval { get; }
		int MinimumLength { get; }
		int MinimumNonAlphanumericCharacters { get; }
		string RegularExpressionToMatch { get; }
		PasswordFormat StorageFormat { get; }
	}
}