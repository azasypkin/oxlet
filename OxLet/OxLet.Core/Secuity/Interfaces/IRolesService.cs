﻿using System.Collections.Generic;

namespace OxLet.Core.Secuity.Interfaces {
	public interface IRolesService {
		IEnumerable<string> FindAll();
		IEnumerable<string> FindByUser(User user);
		IEnumerable<string> FindByUserName(string userName);
		IEnumerable<string> FindUserNamesByRole(string roleName);
		void AddToRole(User user, string roleName);
		void RemoveFromRole(User user, string roleName);
		bool IsInRole(User user, string roleName);
		void Create(string roleName);
		void Delete(string roleName);
	}
}