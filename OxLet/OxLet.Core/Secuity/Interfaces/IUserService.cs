﻿using OxLet.Core.Models;

namespace OxLet.Core.Secuity.Interfaces {
	public interface IUserService {
		//todo: add create user method
		int TotalUsers { get; }
		int UsersOnline { get; }
		IPagedList<User> FindAll(int pageIndex, int pageSize);
		IPagedList<User> FindByEmail(string emailAddressToMatch, int pageIndex, int pageSize);
		IPagedList<User> FindByUserName(string userNameToMatch, int pageIndex, int pageSize);

		User Get(string userName);
		User Get(string userName, bool isAuthenticated);

		User Get(object providerUserKey);
		
		User Get(int id);
		User Get(int id, bool isAuthenticated);

		void Update(User user);
		void Delete(User user);
		void Delete(User user, bool deleteAllRelatedData);
		User Touch(User user);
		User Touch(string userName);
		User Touch(object providerUserKey);
		bool ValidateUser(string name, string password);
	}
}