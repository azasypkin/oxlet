﻿namespace OxLet.Core.Secuity.Interfaces {
	public interface IFormsAuthentication {
		void SetAuthCookie(string userName, bool createPersistentCookie);
		void SignOut();
	}
}
