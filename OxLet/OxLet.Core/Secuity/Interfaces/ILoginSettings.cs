﻿namespace OxLet.Core.Secuity.Interfaces {
	public interface ILoginSettings {
		int MaximumInvalidPasswordAttempts { get; }
		int PasswordAttemptWindowInMinutes { get; }
	}
}