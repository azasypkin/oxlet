﻿namespace OxLet.Core.Secuity.Interfaces {
	public interface IRegistrationSettings {
		bool RequiresUniqueEmailAddress { get; }
	}
}