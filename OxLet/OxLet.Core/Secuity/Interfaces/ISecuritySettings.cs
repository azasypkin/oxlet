﻿namespace OxLet.Core.Secuity.Interfaces {
	public interface ISecuritySettings {
		IRegistrationSettings Registration { get; }
		IPasswordSettings Password { get; }
		ILoginSettings Login { get; }
	}
}