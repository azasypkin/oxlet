﻿namespace OxLet.Core.Secuity.Interfaces {
	public interface IPasswordService {
		void Unlock(User user);
		string ResetPassword(User user);
		string ResetPassword(User user, string passwordAnswer);
		void ChangePassword(User user, string newPassword);
		void ChangePassword(User user, string oldPassword, string newPassword);
	}
}