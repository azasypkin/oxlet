﻿using System.Web.Routing;

namespace OxLet.Core.Secuity.Interfaces {
	public interface IAuthenticationModule {
		IUser Authentificate(RequestContext context);
	}
}
