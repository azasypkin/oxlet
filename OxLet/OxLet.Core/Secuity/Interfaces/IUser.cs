﻿using System.Security.Principal;

namespace OxLet.Core.Secuity.Interfaces {
	public interface IUser : IPrincipal {
		bool IsAuthenticated { get; }
		string Name { get; }
	}
}
