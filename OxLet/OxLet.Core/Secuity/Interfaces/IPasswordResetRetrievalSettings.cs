﻿namespace OxLet.Core.Secuity.Interfaces {
	public interface IPasswordResetRetrievalSettings {
		bool CanReset { get; }
		bool CanRetrieve { get; }
		bool RequiresQuestionAndAnswer { get; }
	}
}