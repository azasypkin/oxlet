﻿using System.Web.Routing;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity {
	class FormsAuthenticationModule : IAuthenticationModule {

		private readonly IUserService _userSevice;

		public FormsAuthenticationModule(IUserService userService) {
			_userSevice = userService;
		}

		public IUser Authentificate(RequestContext context) {
			var identity = context.HttpContext.User.Identity;
			if(identity.IsAuthenticated) {
				return _userSevice.Get(identity.Name, true);
			}
			return null;
		}
	}
}
