﻿using System.Runtime.Serialization;
using OxLet.Core.Models;

namespace OxLet.Core.Secuity {
	[DataContract]
	public class UserCookieProxy {
		public UserCookieProxy() {
		}

		public UserCookieProxy(UserAnonymous user) {
			Name = user.Name;
			Email = user.Email;
			EmailHash = user.EmailHash;
			Url = user.Url;
		}

		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public string EmailHash { get; set; }
		[DataMember]
		public string Url { get; set; }

		public UserAnonymous ToUserAnonymous() {
			return new UserAnonymous(new UserDescription(0) {Email = Email, EmailHash = EmailHash, Name = Name, Url = Url});
		}
	}
}