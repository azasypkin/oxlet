﻿using System.Collections.Generic;

namespace OxLet.Core.Secuity {
	public class Role {
		public int Id { get; set; }
		public Role Parent { get; set; }
		public string Name { get; set; }
		public IEnumerable<Role> Roles { get; set; }
	}
}
