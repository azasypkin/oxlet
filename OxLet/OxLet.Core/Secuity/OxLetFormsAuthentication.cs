﻿using System.Web.Security;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity {
	public class OxLetFormsAuthentication : IFormsAuthentication {
		public void SetAuthCookie(string userName, bool createPersistentCookie) {
			FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
		}

		public void SignOut() {
			FormsAuthentication.SignOut();
		}
	}
}
