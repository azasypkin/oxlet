﻿using System.Web.Routing;
using OxLet.Core.Extensions;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity {
	class AnonymousCookieAuthenticationModule : IAuthenticationModule {
		public IUser Authentificate(RequestContext context) {
			return context.HttpContext.Request.Cookies.GetAnonymousUser();
		}
	}
}
