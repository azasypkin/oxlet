﻿using OxLet.Core.Models;

namespace OxLet.Core.Secuity {
	public class UserAnonymous : User {
		public UserAnonymous():base(new UserDescription(0){DisplayName = "Anonymous", Name = "Anonymous"}, false) {
			Identity = new UserIdentity(null, false, "Anonymous");
		}

		public UserAnonymous(UserDescription userDescription)
			: base(userDescription, false) {
		}
	}
}