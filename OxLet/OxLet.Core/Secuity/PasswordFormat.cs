﻿namespace OxLet.Core.Secuity {
	public enum PasswordFormat {
		Clear,
		Hashed,
		Encrypted,
	}
}
