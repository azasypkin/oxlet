﻿using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity {
	public class LoginSettings : ILoginSettings {
		public LoginSettings(int maximumInvalidPasswordAttempts, int passwordAttemptWindowInMinutes) {
			MaximumInvalidPasswordAttempts = maximumInvalidPasswordAttempts;
			PasswordAttemptWindowInMinutes = passwordAttemptWindowInMinutes;
		}

		#region ILoginSettings Members

		public int MaximumInvalidPasswordAttempts { get; private set; }
		public int PasswordAttemptWindowInMinutes { get; private set; }

		#endregion
	}
}