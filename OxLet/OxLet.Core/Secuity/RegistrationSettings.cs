﻿using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity {
	public class RegistrationSettings : IRegistrationSettings {
		public RegistrationSettings(bool requiresUniqueEmailAddress) {
			RequiresUniqueEmailAddress = requiresUniqueEmailAddress;
		}

		#region IRegistrationSettings Members

		public bool RequiresUniqueEmailAddress { get; private set; }

		#endregion
	}
}