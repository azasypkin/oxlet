﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity {

	public class User : EntityBase, IUser {

		private readonly UserDescription _userDescription;

		public User(UserDescription userDescription, bool isAuthenticated) : base(userDescription.Id) {
			_userDescription = userDescription;
			Identity = new UserIdentity(null, isAuthenticated, _userDescription.Name);
		}

		public string DisplayName { get { return _userDescription.DisplayName; } }

		public string Email { get { return _userDescription.Email; } }

		public string EmailHash { get { return _userDescription.EmailHash; } }

		public string Url { get { return _userDescription.Url; } }

		public string[] Roles { get { return _userDescription.Roles; } }

		#region IUser Members

		public bool IsAuthenticated {
			get { return Identity.IsAuthenticated; }
		}

		public string Name {
			get { return Identity.Name; }
		}

		#endregion

		#region IPrincipal Members

		public bool IsInRole(string role) {
			return !string.IsNullOrEmpty(Array.Find(Roles, x => x == role));
		}

		public IIdentity Identity { get; protected set; }

		#endregion

		public UserDescription GetDescription() {
			return _userDescription;
		}

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			return new[] {this};
		}

	}
}