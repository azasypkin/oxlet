﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using OxLet.Core.Cache;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Secuity.Interfaces;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Secuity.Membership {
	public class OxLetMembershipProvider : IUserService, IPasswordService {
		private readonly MembershipProvider _membershipProvider;
		private readonly IUserDescriptionService _userDescriptionService;
		private readonly IRolesService _roleService;
		private readonly ICacheService _cacheService;

		public OxLetMembershipProvider(MembershipProvider membershipProvider, IRolesService roleService, IUserDescriptionService userDescriptionService, ICacheService cacheService) {
			_membershipProvider = membershipProvider;
			_userDescriptionService = userDescriptionService;
			_roleService = roleService;
			_cacheService = cacheService;
		}

		#region IPasswordService Members

		public void Unlock(User user) {
			GetMembershipUser(user).UnlockUser();
		}

		public string ResetPassword(User user) {
			return GetMembershipUser(user).ResetPassword();
		}

		public string ResetPassword(User user, string passwordAnswer) {
			return GetMembershipUser(user).ResetPassword(passwordAnswer);
		}

		public void ChangePassword(User user, string newPassword) {
			var membershipUser = GetMembershipUser(user);
			var resetPassword = membershipUser.ResetPassword();
			if (!membershipUser.ChangePassword(resetPassword, newPassword))
				throw new MembershipPasswordException("Could not change password.");
		}

		public void ChangePassword(User user, string oldPassword, string newPassword) {
			if (!GetMembershipUser(user).ChangePassword(oldPassword, newPassword)){
				throw new MembershipPasswordException("Could not change password.");
			}
		}

		#endregion

		#region IUserService Members

		public IPagedList<User> FindAll(int pageIndex, int pageSize) {
			return _cacheService.GetItems<IPagedList<User>, User>(
				"FindAllUsers",
				()=> {
					int totalUserCount;
					var usersCollection = _membershipProvider.GetAllUsers(pageIndex, pageSize, out totalUserCount);
					return new PagedList<User>(GetUsers(usersCollection), pageIndex, pageSize, totalUserCount);
				},
				x=> x!= null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>(),
				new CachePage(pageIndex, pageSize)
			);
		}

		public IPagedList<User> FindByEmail(string emailAddressToMatch, int pageIndex, int pageSize) {

			return _cacheService.GetItems<IPagedList<User>, User>(
				string.Format("FindUsersByEmail:{0}", emailAddressToMatch),
				() => {
					int totalUserCount;
					var usersCollection = _membershipProvider.FindUsersByEmail(
						emailAddressToMatch,
						pageIndex,
						pageSize,
						out totalUserCount
					);
					return new PagedList<User>(GetUsers(usersCollection), pageIndex, pageSize, totalUserCount);
				},
				x => x != null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>(),
				new CachePage(pageIndex, pageSize)
			);
		}

		public IPagedList<User> FindByUserName(string userNameToMatch, int pageIndex, int pageSize) {
			return _cacheService.GetItems<IPagedList<User>, User>(
				string.Format("FindUsersByName:{0}", userNameToMatch),
				() => {
					int totalUserCount;
					var usersCollection = _membershipProvider.FindUsersByName(userNameToMatch, pageIndex, pageSize, out totalUserCount);
					return new PagedList<User>(GetUsers(usersCollection), pageIndex, pageSize, totalUserCount);
				},
				x => x != null ? x.GetCacheDependencyItems() : Enumerable.Empty<ICacheable>(),
				new CachePage(pageIndex, pageSize)
			);
		}

		public User Get(string userName) {
			return Get(userName, false);
		}

		public User Get(string userName, bool isAuthenticated) {
			return _cacheService.GetItem(
				string.Format("GetUser-Name:{0}-IsAuthenticated:{1}", userName, isAuthenticated),
				() => GetUser(_membershipProvider.GetUser(userName, false), isAuthenticated)
			);
		}

		public User Get(int id) {
			return Get(id, false);
		}

		public User Get(int id, bool isAuthenticated) {
			return _cacheService.GetItem(
				string.Format("GetUser-Id:{0}-IsAuthenticated:{1}", id, isAuthenticated),
				() => GetUser(_membershipProvider.GetUser(_userDescriptionService.GetUserDescription(id).Name, false), isAuthenticated)
			);
		}

		public User Get(object providerUserKey) {
			return GetUser(_membershipProvider.GetUser(providerUserKey, false), false);
		}

		public void Update(User user) {
			_membershipProvider.UpdateUser(GetMembershipUser(user));
		}

		public void Delete(User user) {
			_membershipProvider.DeleteUser(user.Name, false);
		}

		public void Delete(User user, bool deleteAllRelatedData) {
			_membershipProvider.DeleteUser(user.Name, deleteAllRelatedData);
		}

		public User Touch(User user) {
			return GetUser(_membershipProvider.GetUser(user.Name, true), false);
		}

		public User Touch(string userName) {
			return GetUser(_membershipProvider.GetUser(userName, true), false);
		}

		public User Touch(object providerUserKey) {
			return GetUser(_membershipProvider.GetUser(providerUserKey, true), false);
		}

		public int TotalUsers {
			get {
				int totalUsers;
				_membershipProvider.GetAllUsers(1, 1, out totalUsers);
				return totalUsers;
			}
		}

		public int UsersOnline {
			get { return _membershipProvider.GetNumberOfUsersOnline(); }
		}

		private IEnumerable<User> GetUsers(MembershipUserCollection membershipUsers) {
			// convert from MembershipUserColletion to PagedList<MembershipUser> and return
			var usersList = new EnumerableToEnumerableTConverter<MembershipUserCollection, MembershipUser>()
				.ConvertTo<IEnumerable<MembershipUser>>(membershipUsers)
				.ToArray();

			var users = new User[usersList.Length];
			for (int i = 0; i < usersList.Length; i++) {
				users[i] = GetUser(usersList[i], false);
			}
			return users;
		}

		private static MembershipUser GetMembershipUser(User user) {
			var membershipUser = user as OxLetMembershipUser;
			if (membershipUser != null) {
				return membershipUser.InternalUser;
			}
			throw new ArgumentException("OxLetMemberhsipProvider expects membership user instance");
		}

		private User GetUser(MembershipUser membershipUser, bool isAuthenticated) {
			var userDescription = _userDescriptionService.GetUserDescription(membershipUser.UserName)
				?? _userDescriptionService.Add(new UserDescription(0) {
					DisplayName = membershipUser.UserName,
					Email = membershipUser.Email,
					EmailHash = membershipUser.Email.ComputeHash(),
					Name = membershipUser.UserName
			}).Item;

			// fill roles of the user
			userDescription.Roles = _roleService.FindByUserName(membershipUser.UserName).ToArray();

			return new OxLetMembershipUser(membershipUser, userDescription, isAuthenticated);
		}

		public bool ValidateUser(string name, string password) {
			return _membershipProvider.ValidateUser(name, password);
		}


		#endregion

	}
}