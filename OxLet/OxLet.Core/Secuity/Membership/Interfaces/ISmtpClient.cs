﻿using System.Net.Mail;

namespace OxLet.Core.Secuity.Membership.Interfaces {
	public interface ISmtpClient {
		void Send(MailMessage mailMessage);
	}
}