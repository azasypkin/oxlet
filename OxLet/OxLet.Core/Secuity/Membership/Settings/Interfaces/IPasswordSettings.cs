﻿using System.Web.Security;

namespace OxLet.Core.Secuity.Membership.Settings.Interfaces {
	public interface IPasswordSettings {
		IPasswordResetRetrievalSettings ResetOrRetrieval { get; }
		int MinimumLength { get; }
		int MinimumNonAlphanumericCharacters { get; }
		string RegularExpressionToMatch { get; }
		MembershipPasswordFormat StorageFormat { get; }
	}
}