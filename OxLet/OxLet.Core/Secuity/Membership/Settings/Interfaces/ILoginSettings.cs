﻿namespace OxLet.Core.Secuity.Membership.Settings.Interfaces {
	public interface ILoginSettings {
		int MaximumInvalidPasswordAttempts { get; }
		int PasswordAttemptWindowInMinutes { get; }
	}
}