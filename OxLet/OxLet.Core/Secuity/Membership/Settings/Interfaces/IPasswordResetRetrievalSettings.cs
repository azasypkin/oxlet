﻿namespace OxLet.Core.Secuity.Membership.Settings.Interfaces {
	public interface IPasswordResetRetrievalSettings {
		bool CanReset { get; }
		bool CanRetrieve { get; }
		bool RequiresQuestionAndAnswer { get; }
	}
}