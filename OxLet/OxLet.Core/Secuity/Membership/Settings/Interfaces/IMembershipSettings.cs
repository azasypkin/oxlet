﻿namespace OxLet.Core.Secuity.Membership.Settings.Interfaces {
	public interface IMembershipSettings {
		IRegistrationSettings Registration { get; }
		IPasswordSettings Password { get; }
		ILoginSettings Login { get; }
	}
}