﻿namespace OxLet.Core.Secuity.Membership.Settings.Interfaces {
	public interface IRegistrationSettings {
		bool RequiresUniqueEmailAddress { get; }
	}
}