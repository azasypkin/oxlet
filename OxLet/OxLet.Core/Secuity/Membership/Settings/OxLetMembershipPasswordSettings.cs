﻿using System.Web.Security;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity.Membership.Settings {
	public class OxLetMembershipPasswordSettings : IPasswordSettings {
		public OxLetMembershipPasswordSettings(IPasswordResetRetrievalSettings resetOrRetrieval, int minimumLength,
		                        int minimumNonAlphanumericCharacters, string regularExpressionToMatch,
		                        MembershipPasswordFormat storageFormat) {
			ResetOrRetrieval = resetOrRetrieval;
			MinimumLength = minimumLength;
			MinimumNonAlphanumericCharacters = minimumNonAlphanumericCharacters;
			RegularExpressionToMatch = regularExpressionToMatch;
			PasswordFormat passwordFormat;
			StorageFormat = PasswordFormat.TryParse(storageFormat.ToString(), out passwordFormat) ? passwordFormat : PasswordFormat.Encrypted;
		}

		#region IPasswordSettings Members

		public IPasswordResetRetrievalSettings ResetOrRetrieval { get; private set; }
		public int MinimumLength { get; private set; }
		public int MinimumNonAlphanumericCharacters { get; private set; }
		public string RegularExpressionToMatch { get; private set; }
		public PasswordFormat StorageFormat { get; private set; }

		#endregion
	}
}