﻿using System.Web.Security;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Secuity.Membership.Settings {
	public class OxLetMembershipSecuritySettings : ISecuritySettings {
		public OxLetMembershipSecuritySettings(MembershipProvider provider) : this(
			new RegistrationSettings(provider.RequiresUniqueEmail),
			new OxLetMembershipPasswordSettings(
				new PasswordResetRetrievalSettings(
					provider.EnablePasswordReset,
					provider.EnablePasswordRetrieval,
					provider.RequiresQuestionAndAnswer
				),
				provider.MinRequiredPasswordLength,
				provider.MinRequiredNonAlphanumericCharacters,
				provider.PasswordStrengthRegularExpression,
				provider.PasswordFormat),
			new LoginSettings(
				provider.MaxInvalidPasswordAttempts,
				provider.PasswordAttemptWindow
			)
		) {}

		public OxLetMembershipSecuritySettings(IRegistrationSettings registration, IPasswordSettings password, ILoginSettings login) {
			Registration = registration;
			Password = password;
			Login = login;
		}

		#region ISecuritySettings Members

		public IRegistrationSettings Registration { get; private set; }
		public IPasswordSettings Password { get; private set; }
		public ILoginSettings Login { get; private set; }

		#endregion
	}
}