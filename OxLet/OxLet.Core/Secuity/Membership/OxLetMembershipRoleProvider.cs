﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Secuity.Interfaces;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Secuity.Membership {
	public class OxLetMembershipRoleProvider : IRolesService {
		private readonly RoleProvider _roleProvider;
		private readonly ICacheService _cacheService;
		private readonly IUserDescriptionService _userDescriptionService;

		public OxLetMembershipRoleProvider(RoleProvider roleProvider, ICacheService cacheService, IUserDescriptionService userDescriptionService) {
			_roleProvider = roleProvider;
			_cacheService = cacheService;
			_userDescriptionService = userDescriptionService;
		}

		#region IRolesService Members

		public IEnumerable<string> FindAll() {
			return _cacheService.GetItems<IEnumerable<string>, string>(
				"FindAllRoles",
				()=>_roleProvider.GetAllRoles(),
				x => Enumerable.Empty<ICacheable>()
			);
		}

		public IEnumerable<string> FindByUser(User user) {
			return _cacheService.GetItems<IEnumerable<string>, string>(
				string.Format("User:{0}-Roles", user.Id),
				() => _roleProvider.GetRolesForUser(user.Name),
				x => user.GetCacheDependencyItems()
			);
		}

		public IEnumerable<string> FindByUserName(string userName) {
			var user = _userDescriptionService.GetUserDescription(userName);
			return _cacheService.GetItems<IEnumerable<string>, string>(
				string.Format("UserName:{0}-Roles", userName),
				() => _roleProvider.GetRolesForUser(userName),
				x => user.GetCacheDependencyItems()
			);
		}

		public IEnumerable<string> FindUserNamesByRole(string roleName) {
			return _cacheService.GetItems<IEnumerable<string>, string>(
				string.Format("FindUserByRole:{0}", roleName),
				() => _roleProvider.GetUsersInRole(roleName),
				x => Enumerable.Empty<ICacheable>()
			);
		}

		public void AddToRole(User user, string roleName) {
			_roleProvider.AddUsersToRoles(new[] {user.Name}, new[] {roleName});
		}

		public void RemoveFromRole(User user, string roleName) {
			_roleProvider.RemoveUsersFromRoles(new[] {user.Name}, new[] {roleName});
		}

		public bool IsInRole(User user, string roleName) {
			return _cacheService.GetItem(
				string.Format("IsUser:{0}-InRole:{1}", user.Id, roleName),
				()=>_roleProvider.IsUserInRole(user.Name, roleName)
			);
		}

		public void Create(string roleName) {
			_roleProvider.CreateRole(roleName);
		}

		public void Delete(string roleName) {
			_roleProvider.DeleteRole(roleName, false);
		}

		#endregion
	}
}