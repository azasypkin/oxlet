﻿using System.Web.Security;
using OxLet.Core.Models;

namespace OxLet.Core.Secuity.Membership {
	public class OxLetMembershipUser : User {
		public OxLetMembershipUser(MembershipUser user, UserDescription userDescription, bool isAuthenticated) : base(userDescription, isAuthenticated) {
				InternalUser = user;
		}

		public MembershipUser InternalUser { get; private set; }
	}
}
