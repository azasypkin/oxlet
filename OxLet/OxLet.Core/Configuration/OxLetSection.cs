﻿using System;
using System.Configuration;
using System.Linq;
using OxLet.Core.Tools;

namespace OxLet.Core.Configuration {
	public class OxLetSection : ConfigurationSection {
		[ConfigurationProperty("antiForgeryCookieTtl", IsRequired = true)]
		public int AntiForgeryCookieTtl {
			get { return (int) this["antiForgeryCookieTtl"]; }
			set { this["antiForgeryCookieTtl"] = value; }
		}

		[ConfigurationProperty("fileCachePath", IsRequired = true)]
		public string FileCachePath {
			get { return (string) this["fileCachePath"]; }
			set { this["fileCachePath"] = value; }
		}

		[ConfigurationProperty("siteName", IsRequired = true)]
		public string SiteName {
			get { return (string)this["siteName"]; }
			set { this["siteName"] = value; }
		}

		[ConfigurationProperty("cacheExpirationType", IsRequired = false)]
		public CacheExpiration CacheExpiration {
			get { return (CacheExpiration) this["cacheExpirationType"]; }
			set { this["cacheExpirationType"] = value; }
		}

		[ConfigurationProperty("mode", IsRequired = true)]
		public SiteMode Mode {
			get { return (SiteMode)this["mode"]; }
			set { this["mode"] = value; }
		}

		[ConfigurationProperty("absoluteExpirationTime", IsRequired = false)]
		public DateTime AbsoluteExpirationTime {
			get { return (DateTime) this["absoluteExpirationTime"]; }
			set { this["absoluteExpirationTime"] = value; }
		}

		[ConfigurationProperty("slidingExpirationTimeSpan", IsRequired = false)]
		public int SlidingExpirationTimeSpan {
			get { return (int) this["slidingExpirationTimeSpan"]; }
			set { this["slidingExpirationTimeSpan"] = value; }
		}

		[ConfigurationProperty("siteModules", IsDefaultCollection = true)]
		public SiteModuleElementCollection SiteModules {
			get { return (SiteModuleElementCollection) this["siteModules"]; }
			set { this["siteModules"] = value; }
		}

		[ConfigurationProperty("settings", IsDefaultCollection = true)]
		public SettingsElementCollection Settings {
			get { return (SettingsElementCollection)this["settings"]; }
			set { this["settings"] = value; }
		}

		#region Methods

		public TValue GetValue<TValue>(string name) {
			return GetValue(name, default(TValue));
		}

		public TValue GetValue<TValue>(string name, TValue defaultValue) {
			return GenericStringValueRetriever.GetValue(Settings, x => x.First(s=>s.Name == name).Value, defaultValue);
		}

		public TValue[] GetValueArray<TValue>(string name, string separator) {
			return GetValueArray<TValue>(name, separator, null);
		}

		public TValue[] GetValueArray<TValue>(string name, string separator, TValue[] defaultValueList) {
			return GenericStringValueRetriever.GetValueArray(Settings, x => x.First(s => s.Name == name).Value, separator, defaultValueList);
		}

		#endregion
	}
}
