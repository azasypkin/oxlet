﻿using System.Configuration;

namespace OxLet.Core.Configuration {
	public class SiteModuleElement : ConfigurationElement, IConfigurationElementCollectionElement {


		[ConfigurationProperty("name", IsRequired = true, IsKey = true)]
		public string Name {
			get {
				return (string)this["name"];
			}
			set {
				this["name"] = value;
			}
		}

		[ConfigurationProperty("areaName", IsRequired = false)]
		public string AreaName {
			get {
				return (string)this["areaName"];
			}
			set {
				this["areaName"] = value;
			}
		}

		[ConfigurationProperty("controllerNamespaces")]
		public ControllerNamespaceElementCollection ControllerNamespaces {
			get {
				return (ControllerNamespaceElementCollection)this["controllerNamespaces"];
			}
			set {
				this["controllerNamespaces"] = value;
			}
		}

		public string ElementKey {
			get { return Name; }
		}
	}
}