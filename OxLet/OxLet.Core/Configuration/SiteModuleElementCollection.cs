﻿using System.Configuration;

namespace OxLet.Core.Configuration {
	[ConfigurationCollection(typeof(SiteModuleElement), AddItemName = "siteModule",
		 CollectionType = ConfigurationElementCollectionType.BasicMap)]
	public class SiteModuleElementCollection : BaseConfigurationElementCollection<SiteModuleElement> {
		//public override ConfigurationElementCollectionType CollectionType {
		//    get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
		//}

		protected override string ElementName {
			get { return "siteModules"; }
		}
	}
}
