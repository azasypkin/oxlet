﻿using System.Configuration;

namespace OxLet.Core.Configuration {
	[ConfigurationCollection(typeof(SettingsElement), AddItemName = "setting",
		 CollectionType = ConfigurationElementCollectionType.BasicMap)]
	public class SettingsElementCollection : BaseConfigurationElementCollection<SettingsElement> {
		protected override string ElementName {
			get { return "settings"; }
		}
	}
}
