﻿using System.Collections.Specialized;
using OxLet.Core.Tools;

namespace OxLet.Core.Configuration {
	public class AppSettingsHelper {

		#region Fields

		private readonly NameValueCollection _appSettings;

		#endregion

		#region Constructor

		public AppSettingsHelper(NameValueCollection appSettings) {
			_appSettings = appSettings;
		}

		#endregion

		#region Methods

		public TValue GetValue<TValue>(string name) {
			return GetValue(name, default(TValue));
		}

		public TValue GetValue<TValue>(string name, TValue defaultValue) {
			return GenericStringValueRetriever.GetValue(_appSettings, x => x[name], defaultValue);
		}

		public TValue[] GetValueArray<TValue>(string name, string separator) {
			return GetValueArray<TValue>(name, separator, null);
		}

		public TValue[] GetValueArray<TValue>(string name, string separator, TValue[] defaultValueList) {
			return GenericStringValueRetriever.GetValueArray(_appSettings, x => x[name], separator, defaultValueList);
		}

		#endregion
	}
}
