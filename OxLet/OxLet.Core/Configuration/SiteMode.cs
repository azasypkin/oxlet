﻿namespace OxLet.Core.Configuration {
	public enum SiteMode : byte {
		Local = 0,
		Production
	}
}
