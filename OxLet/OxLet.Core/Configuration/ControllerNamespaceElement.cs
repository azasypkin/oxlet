﻿using System.Configuration;

namespace OxLet.Core.Configuration {
	public class ControllerNamespaceElement: ConfigurationElement, IConfigurationElementCollectionElement{
		[ConfigurationProperty("value", IsRequired = true, IsKey = true)]
		public string Value {
			get {
				return (string)this["value"];
			}
			set {
				this["value"] = value;
			}
		}
		public string ElementKey {
			get { return Value; }
		}
	}
}
