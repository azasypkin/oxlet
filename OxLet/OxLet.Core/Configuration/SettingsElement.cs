﻿using System.Configuration;

namespace OxLet.Core.Configuration {
	public class SettingsElement : ConfigurationElement, IConfigurationElementCollectionElement {


		[ConfigurationProperty("name", IsRequired = true, IsKey = true)]
		public string Name {
			get {
				return (string)this["name"];
			}
			set {
				this["name"] = value;
			}
		}

		[ConfigurationProperty("value", IsRequired = true, IsKey = false)]
		public string Value {
			get {
				return (string)this["value"];
			}
			set {
				this["value"] = value;
			}
		}

		public string ElementKey {
			get { return Name; }
		}
	}
}