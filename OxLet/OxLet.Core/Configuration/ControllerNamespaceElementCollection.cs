﻿using System.Configuration;

namespace OxLet.Core.Configuration {
	public class ControllerNamespaceElementCollection : BaseConfigurationElementCollection<ControllerNamespaceElement> {
		public override ConfigurationElementCollectionType CollectionType {
			get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
		}

		protected override string ElementName {
			get { return "siteModules"; }
		}
	}
}
