﻿namespace OxLet.Core.Configuration {
	public enum CacheExpiration {
		Default = 0,
		Absolute = 1,
		Sliding = 2
	}
}
