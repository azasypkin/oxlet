﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace OxLet.Core.Tools {
	public class GenericStringValueRetriever {

		private static readonly Dictionary<Type, TypeConverter> _typeConverters = new Dictionary<Type, TypeConverter>();

		public static TValue GetValue<TValue, TSource>(TSource source, Func<TSource, string> valueExtractor) {
			return GetValue(source, valueExtractor, default(TValue));
		}

		public static TValue GetValue<TValue, TSource>(TSource source, Func<TSource, string> valueExtractor, TValue defaultValue) {
			return GetValue(valueExtractor(source), defaultValue);
		}

		public static TValue GetValue<TValue>(string value) {
			return GetValue(value, default(TValue));
		}

		public static TValue GetValue<TValue>(string value, TValue defaultValue) {

			// if stickness wasn't found then returns default value
			if (string.IsNullOrEmpty(value)) {
				return defaultValue;
			}

			try {
				var converter = GetTypeConverter(typeof(TValue));
				if (converter != null && converter.CanConvertFrom(typeof(string))) {
					return (TValue)converter.ConvertFrom(value);
				}
			} catch { }

			return defaultValue;
		}

		public static TValue[] GetValueArray<TValue, TSource>(TSource source, Func<TSource, string> valueExtractor, string separator) {
			return GetValueArray(source, valueExtractor, separator, new TValue[0]);
		}

		public static TValue[] GetValueArray<TValue, TSource>(TSource source, Func<TSource, string> valueExtractor, string separator, TValue[] defaultValueList) {
			return GetValueArray(valueExtractor(source), separator, defaultValueList);
		}

		public static TValue[] GetValueArray<TValue>(string value, string separator) {
			return GetValueArray(value, separator, new TValue[0]);
		}

		public static TValue[] GetValueArray<TValue>(string value, string separator, TValue[] defaultValueList) {

			if (string.IsNullOrEmpty(value)) {
				return defaultValueList;
			}

			try {
				var converter = GetTypeConverter(typeof(TValue));
				if (converter != null) {
					var array = value.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
					return array
						.Where(item => converter.CanConvertFrom(typeof(string)))
						.Select(item => (TValue)converter.ConvertFrom(item))
						.ToArray();
				}
			} catch { }

			return defaultValueList;
		}

		private static TypeConverter GetTypeConverter(Type type) {
			if (_typeConverters.ContainsKey(type)) {
				return _typeConverters[type];
			}
			var converter = TypeDescriptor.GetConverter(type);
			if (converter != null) {
				_typeConverters[type] = converter;
				return converter;
			}
			return null;
		}

	}
}
