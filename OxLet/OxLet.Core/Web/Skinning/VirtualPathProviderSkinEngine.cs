﻿using OxLet.Core.Models;

namespace OxLet.Core.Web.Skinning {
	public class VirtualPathProviderSkinEngine : ISkinEngine {

		private readonly Site _site;

		public VirtualPathProviderSkinEngine(Site site) {
			_site = site;
		}

		public string[] GetMasterLocationFormats() {
			return new[] {
				string.Format("~/Skins/{0}/Views/{{1}}/{{0}}.master", _site.SkinDefault),
				string.Format("~/Skins/{0}/Views/Shared/{{0}}.master", _site.SkinDefault),
				string.Format("~/Views/{0}/{{0}}.master", _site.SkinDefault),
				"~/Views/Shared/{0}.master"
			};
		}

		public string[] GetAreaMasterLocationFormats() {
			return new [] {
				string.Format("~/Areas/{{2}}/Skins/{0}/Views/{{1}}/{{0}}.master", _site.SkinDefault),
				string.Format("~/Areas/{{2}}/Skins/{0}/Views/Shared/{{0}}.master", _site.SkinDefault),
				"~/Areas/{2}/Views/{1}/{0}.master",
				"~/Areas/{2}/Views/Shared/{0}.master"
			};
		}

		public string[] GetViewLocationFormats() {
			return new[] {
				string.Format("~/Skins/{0}/Views/{{1}}/{{0}}.aspx", _site.SkinDefault),
				string.Format("~/Skins/{0}/Views/{{1}}/{{0}}.ascx", _site.SkinDefault),
				string.Format("~/Skins/{0}/Views/Shared/{{0}}.aspx", _site.SkinDefault),
				string.Format("~/Skins/{0}/Views/Shared/{{0}}.ascx", _site.SkinDefault),
				"~/Views/{1}/{0}.aspx",
				"~/Views/{1}/{0}.ascx",
				"~/Views/Shared/{0}.aspx",
				"~/Views/Shared/{0}.ascx"
			};
		
		}

		public string[] GetAreaViewLocationFormats() {
			return new[] {
				string.Format("~/Areas/{{2}}/Skins/{0}/Views/{{1}}/{{0}}.aspx", _site.SkinDefault),
				string.Format("~/Areas/{{2}}/Skins/{0}/Views/{{1}}/{{0}}.ascx", _site.SkinDefault),
				string.Format("~/Areas/{{2}}/Skins/{0}/Views/Shared/{{0}}.aspx", _site.SkinDefault),
				string.Format("~/Areas/{{2}}/Skins/{0}/Views/Shared/{{0}}.ascx", _site.SkinDefault),
				"~/Areas/{2}/Views/{1}/{0}.aspx",
				"~/Areas/{2}/Views/{1}/{0}.ascx",
				"~/Areas/{2}/Views/Shared/{0}.aspx",
				"~/Areas/{2}/Views/Shared/{0}.ascx"
			};
		}

		public string[] GetPartialViewLocationFormats() {
			return GetViewLocationFormats();
		}

		public string[] GetAreaPartialViewLocationFormats() {
			return GetAreaViewLocationFormats();
		}

		public string[] GetFileExtensions() {
			return new[] { "aspx", "ascx", "master" };
		}
	}
}
