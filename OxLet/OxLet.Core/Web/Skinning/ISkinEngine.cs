﻿namespace OxLet.Core.Web.Skinning {
	public interface ISkinEngine {
		string[] GetMasterLocationFormats();
		string[] GetAreaMasterLocationFormats();
		string[] GetViewLocationFormats();
		string[] GetAreaViewLocationFormats();
		string[] GetPartialViewLocationFormats();
		string[] GetAreaPartialViewLocationFormats();
		string[] GetFileExtensions();
	}
}
