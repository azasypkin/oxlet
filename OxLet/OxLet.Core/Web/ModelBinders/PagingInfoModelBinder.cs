﻿using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Core.Models;

namespace OxLet.Core.Web.ModelBinders {
	public class PagingInfoModelBinder : IModelBinder {
		private const string RouteDataName = "pageNumber";

		private readonly Regex _pageNumberRegex = new Regex(
			@"(?:(?<=^|/)Page(?<number>\d+)(?=$|/))?",
			RegexOptions.Compiled | RegexOptions.IgnoreCase
		);

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var result = bindingContext.ValueProvider.GetValue(RouteDataName);

			if (result == null) {
				string routeUrl = "";

				if (controllerContext.RouteData.Route is Route){
					routeUrl = ((Route) controllerContext.RouteData.Route).Url;
				}

				throw new InvalidOperationException(
					string.Format(
						"{0} requires a parameter of type {1} but the matching route url '{2}' does not contain {3} route data.",
						controllerContext.Controller.GetType().FullName, typeof (PagingInfo).FullName, routeUrl,
						RouteDataName));
			}

			var pageNumber = result.RawValue as string;
			int index = 0;

			if (!string.IsNullOrEmpty(pageNumber)) {
				Match pageNumberMatch = _pageNumberRegex.Match(pageNumber);

				if (pageNumberMatch.Groups["number"].Success && int.TryParse(pageNumberMatch.Groups["number"].Value, out index))
					index = index > 0 ? index - 1 : 0;
			}

			return new PagingInfo(index, 10);
		}
	}
}