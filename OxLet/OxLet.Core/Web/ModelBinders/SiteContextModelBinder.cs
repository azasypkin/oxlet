﻿using System.Web.Mvc;
using Microsoft.Practices.Unity;
using OxLet.Core.Infrastructure;

namespace OxLet.Core.Web.ModelBinders {
	public class SiteContextModelBinder : IModelBinder {
		private readonly IUnityContainer _container;
		public SiteContextModelBinder(IUnityContainer container) {
			_container = container;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			return _container.Resolve<SiteContext>();
		}
	}
}
