﻿using System.Web.Helpers;
using System.Web.Mvc;
using OxLet.Core.Models;

namespace OxLet.Core.Web.ModelBinders {
	public class SearchCriteriaModelBinder : IModelBinder {
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			
			var criteria = new SearchCriteria {
				Term = controllerContext.HttpContext.Request.Unvalidated().QueryString.Get("term")
			};

			return criteria;
		}
	}
}
