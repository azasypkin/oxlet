﻿using System.Web.Mvc;
using OxLet.Core.Models.Input;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;
using OxLet.Core.Web.Extensions;

namespace OxLet.Core.Web.ModelBinders {
	public class ContentItemInputModelBinder : IModelBinder {

		private readonly IContentItemService _contentItemService;
		private readonly IModelValidator<ContentItemInput> _validator;

		public ContentItemInputModelBinder(IContentItemService contentItemService, IModelValidator<ContentItemInput> validator) {
			_contentItemService = contentItemService;
			_validator = validator;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var valueProvider = bindingContext.ValueProvider;

			var comment = new ContentItemInput();
			//ValueProviderResult providerResult = valueProvider.GetValue("ContentItemInput.Body");
			//if (providerResult != null) {
			//    comment.Body = providerResult.GetValue<string>();
			//}
			string name = null;
			ValueProviderResult providerResult = valueProvider.GetValue("Item.Name");
			if (providerResult != null) {
				name = providerResult.GetValue<string>();
			}

			string displayName = null;
			providerResult = valueProvider.GetValue("Item.DisplayName");
			if (providerResult != null) {
				displayName = providerResult.GetValue<string>();
			}

			string slug = null;
			providerResult = valueProvider.GetValue("Item.Slug");
			if (providerResult != null) {
				slug = providerResult.GetValue<string>();
			}
			bool isFolder = false;
			providerResult = valueProvider.GetValue("Item.IsFolder");
			if (providerResult != null) {
				isFolder = providerResult.GetValue<string>().Contains("true");
			}
			int id = 0;
			providerResult = valueProvider.GetValue("Item.Id");
			if (providerResult != null) {
				id = providerResult.GetValue<int>();
			}
			int languageId = 0;
			providerResult = valueProvider.GetValue("Item.Language");
			if (providerResult != null) {
				languageId = providerResult.GetValue<int>();
			}

			// check for parent
			int? parentId = null;
			providerResult = valueProvider.GetValue("Item.Parent");
			if (providerResult != null) {
				parentId = providerResult.GetValue<int>();
			}

			// bind value
			providerResult = valueProvider.GetValue("Item.Value.File");
			FileResourceInput fileResourceInput = null;
			if(providerResult != null) {
				var file = controllerContext.RequestContext.HttpContext.Request.Files[0];
				if(file != null) {
					var content = new byte[file.InputStream.Length];
					file.InputStream.Read(content, 0, content.Length);
					fileResourceInput = new FileResourceInput(0, file.FileName, file.ContentType, content);
				}
			}

			return new ContentItemInput {
				Id = id,
				DisplayName = displayName,
				Name = name,
				Slug = slug,
				IsFolder = isFolder,
				Language = languageId,
				Parent = parentId,
				Value = new ContentItemResourceInput(0, fileResourceInput, null)
			};
		}
	}
}
