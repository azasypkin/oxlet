﻿using System.Web.Mvc;
using OxLet.Core.Extensions;
using OxLet.Core.Models;

namespace OxLet.Core.Web.ModelBinders {
	public class ContentItemPathModelBinder : IModelBinder {
		private const string ContentItemPathRouteDataName = "contentItemPath";
		private const string ContentItemPathRootRouteDataName = "contentItemPathRoot";

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var contentItemPathSegment = bindingContext.ValueProvider.GetValue(ContentItemPathRouteDataName);
			var contentItemPathRoot = bindingContext.ValueProvider.GetValue(ContentItemPathRootRouteDataName);

			var rootPathSegment = contentItemPathRoot != null ? contentItemPathRoot.AttemptedValue : null;
			var itemPathSegment = contentItemPathSegment != null ? contentItemPathSegment.AttemptedValue : null;

			if(string.IsNullOrEmpty(rootPathSegment) && string.IsNullOrEmpty(itemPathSegment)) {
				return null;
			}

			var path = !string.IsNullOrEmpty(rootPathSegment) && !string.IsNullOrEmpty(itemPathSegment)
				? string.Format("/{0}/{1}/", rootPathSegment, itemPathSegment)
				: rootPathSegment ?? itemPathSegment;

			return ContentItemPath.Parse(path.ClearContentItemMode());
		}

		
	}
}