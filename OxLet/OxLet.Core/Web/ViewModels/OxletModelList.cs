﻿using System.Collections.Generic;

namespace OxLet.Core.Web.ViewModels {
	public class OxletModelList<T> : OxletModel {
		public IList<T> List { get; set; }

		public OxletModelItem<T> GetModelItem(int index) {
			var item = new OxletModelItem<T> {
				AntiForgeryToken = AntiForgeryToken,
				Container = Container,
				Context = Context,
				Item = List[index],
				SkinName = SkinName
			};
			item.AddModelItems(GetModelItems());
			return item;
		}
	}
}
