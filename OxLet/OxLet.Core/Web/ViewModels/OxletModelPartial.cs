﻿namespace OxLet.Core.Web.ViewModels {
	public class OxletModelPartial<T> : OxletModel {
		public OxletModelPartial(OxletModel rootModel, T partialModel) {
			RootModel = rootModel;
			PartialModel = partialModel;
			AntiForgeryToken = rootModel.AntiForgeryToken;
			Context = rootModel.Context;
			SkinName = rootModel.SkinName;
		}

		public OxletModel RootModel { get; set; }
		public T PartialModel { get; set; }
	}
}
