﻿namespace OxLet.Core.Web.ViewModels {
	public class NavigationEntity {
		public string Area { get; set; }
		public string Controller { get; set; }
		public string Action { get; set; }
		private string _key;

		public string GetKey() {
			if (string.IsNullOrEmpty(_key)) {
				if (!string.IsNullOrEmpty(Area)) {
					if (!string.IsNullOrEmpty(Controller)) {
						_key = !string.IsNullOrEmpty(Action) 
							? string.Format("{0}:{1}:{2}", Area, Controller, Action) 
							: string.Format("{0}:{1}", Area, Controller);
					}
					else {
						_key = Area;
					}
				}
				else if (!string.IsNullOrEmpty(Controller)) {
					_key = !string.IsNullOrEmpty(Action)
						? string.Format("{0}:{1}", Controller, Action)
						: Controller;
				}
				else {
					_key = "Unknown";
				}
			}
			return _key;
		}
	}
}
