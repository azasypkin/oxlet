﻿using System;
using System.Collections.Generic;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Validation;
using OxLet.Core.Web.Infrastructure;

namespace OxLet.Core.Web.ViewModels {
	public class OxletModel {
		public AntiForgeryToken AntiForgeryToken { get; set; }
		public INamedEntity Container { get; set; }
		public SiteContext Context { get; set; }
		public NavigationEntity Navigation { get; set; }
		public IValidationService ValidationService { get; set; }

		private string _skinName;

		public string SkinName {
			get {
				if (string.IsNullOrEmpty(_skinName)){
					return Context.Site.SkinDefault;
				}

				return _skinName;
			}

			set { _skinName = value; }
		}

		private readonly Dictionary<Type, object> _modelItems = new Dictionary<Type, object>();

		public void AddModelItem<T>(T modelItem) where T : class {
			_modelItems[typeof (T)] = modelItem;
		}

		public T GetModelItem<T>() where T : class {
			if (_modelItems.ContainsKey(typeof (T))){
				return _modelItems[typeof (T)] as T;
			}
			return null;
		}

		public Dictionary<Type, object> GetModelItems() {
			return _modelItems;
		}

		public void AddModelItems(Dictionary<Type, object> modelItems) {
			AddModelItems(modelItems, false);
		}

		public void AddModelItems(Dictionary<Type, object> modelItems, bool isOverride) {
			foreach (var modelItem in modelItems) {
				if(!_modelItems.ContainsKey(modelItem.Key) || isOverride) {
					_modelItems[modelItem.Key] = modelItem.Value;
				}
			}
		}
	}
}