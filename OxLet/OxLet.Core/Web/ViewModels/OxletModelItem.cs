﻿namespace OxLet.Core.Web.ViewModels {
	public class OxletModelItem<T> : OxletModel {
		public T Item { get; set; }
	}
}
