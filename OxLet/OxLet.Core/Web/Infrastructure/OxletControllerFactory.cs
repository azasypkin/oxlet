﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Async;
using Microsoft.Practices.Unity;
using OxLet.Core.Web.Controllers;

namespace OxLet.Core.Web.Infrastructure {
	public class OxletControllerFactory : DefaultControllerFactory {
		private readonly IUnityContainer _container;

		public OxletControllerFactory(IUnityContainer container) {
			_container = container;
		}

		protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType) {
			IController icontroller;
			//try {
				icontroller = _container.Resolve(controllerType) as IController;
				if (typeof(AsyncController).IsAssignableFrom(controllerType)) {
					var controller = icontroller as AsyncController;
					if (controller != null) {
						controller.ActionInvoker = _container.Resolve<IAsyncActionInvoker>();
					}
					return icontroller;
				}
				if (typeof (Controller).IsAssignableFrom(controllerType)) {
					var controller = icontroller as Controller;
					if (controller != null) {
						controller.ActionInvoker = _container.Resolve<IActionInvoker>();
					}
					return icontroller;
				}
			//}
			//catch {
			//    icontroller = new ErrorController();
			//    ((Controller) icontroller).ActionInvoker = _container.Resolve<IActionInvoker>();
			//}

			return icontroller;
		}
	}
}
