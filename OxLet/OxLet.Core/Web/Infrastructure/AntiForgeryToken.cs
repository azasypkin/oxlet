﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace OxLet.Core.Web.Infrastructure {
	public class AntiForgeryToken {
		public static readonly string TicksName = "__AntiForgeryTicks";
		public static readonly string TokenName = "__AntiForgeryToken";

		private readonly string _salt;

		public AntiForgeryToken(string salt) {
			_salt = salt;
			Ticks = GetTicks();
		}

		public long Ticks { get; set; }

		public string Value {
			get { return GetHash(_salt, Ticks.ToString()); }
		}

		private static long GetTicks() {
			return DateTime.UtcNow.Ticks;
		}

		public static string GetHash(string salt, string ticks) {
			byte[] bytesToHash = Encoding.UTF8.GetBytes(string.Concat(salt, ticks));

			SHA512 hasher = new SHA512Managed();
			byte[] hashed = hasher.ComputeHash(bytesToHash);

			return Convert.ToBase64String(hashed);
		}
	}
}