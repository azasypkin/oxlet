﻿using System.Web.Mvc;
using System.Web.Mvc.Async;
using OxLet.Core.Models;
using OxLet.Core.Web.Filters;
using OxLet.Core.Web.Filters.Interfaces;
using OxLet.Core.Web.Results;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Infrastructure {
	public class OxLetAsyncControllerActionInvoker : AsyncControllerActionInvoker {
		private readonly IActionFilterRegistry _registry;

		public OxLetAsyncControllerActionInvoker(IActionFilterRegistry registry) {
			_registry = registry;
		}

		protected override ActionResult CreateActionResult(ControllerContext controllerContext, ActionDescriptor actionDescriptor, object actionReturnValue) {
			if (actionReturnValue == null) {
				controllerContext.Controller.ViewData.Model = new OxletModel {Container = new NotFoundPageContainer()};
				return new NotFoundResult();
			}

			if (typeof(ActionResult).IsAssignableFrom(actionReturnValue.GetType())) {
				return actionReturnValue as ActionResult;
			}

			controllerContext.Controller.ViewData.Model = actionReturnValue;

			return new ViewResult {
				ViewData = controllerContext.Controller.ViewData,
				TempData = controllerContext.Controller.TempData
			};
		}

		protected override FilterInfo GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor) {
			FilterInfo baseFilters = base.GetFilters(controllerContext, actionDescriptor);

			FilterInfo registeredFilters = _registry.GetFilters(
				new ActionFilterRegistryContext(controllerContext, actionDescriptor)
			);

			foreach (IActionFilter actionFilter in registeredFilters.ActionFilters) {
				baseFilters.ActionFilters.Insert(0, actionFilter);
			}
			foreach (IAuthorizationFilter authorizationFilter in registeredFilters.AuthorizationFilters){
				baseFilters.AuthorizationFilters.Add(authorizationFilter);
			}
			foreach (IExceptionFilter exceptionFilter in registeredFilters.ExceptionFilters){
				baseFilters.ExceptionFilters.Add(exceptionFilter);
			}
			foreach (IResultFilter resultFilter in registeredFilters.ResultFilters){
				baseFilters.ResultFilters.Add(resultFilter);
			}

			return baseFilters;
		}
	}
}