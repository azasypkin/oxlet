﻿using System.Web.Mvc;

namespace OxLet.Mvc.Infrastructure {
	public class OxLetWebFormViewBase : WebFormView{

		public OxLetWebFormViewBase(ControllerContext controllerContext, string viewPath) : base(controllerContext, viewPath) {
		}

		public OxLetWebFormViewBase(ControllerContext controllerContext, string viewPath, IViewPageActivator viewPageActivator) : base(controllerContext, viewPath, null, viewPageActivator) {
		}

		public OxLetWebFormViewBase(ControllerContext controllerContext, string viewPath, string masterPath, IViewPageActivator viewPageActivator)
			: base(controllerContext, viewPath, masterPath, viewPageActivator) {
		}
	}
}
