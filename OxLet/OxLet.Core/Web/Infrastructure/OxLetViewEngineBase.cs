﻿using System.Web.Mvc;
using OxLet.Core.Web.Skinning;
using OxLet.Mvc.Infrastructure;

namespace OxLet.Core.Web.Infrastructure {
	public class OxLetViewEngineBase : BuildManagerViewEngine {
		
		public OxLetViewEngineBase(ISkinEngine skinEngine) {
			MasterLocationFormats = skinEngine.GetMasterLocationFormats();
			AreaMasterLocationFormats = skinEngine.GetAreaMasterLocationFormats();
			ViewLocationFormats = skinEngine.GetViewLocationFormats();
			AreaViewLocationFormats = skinEngine.GetAreaViewLocationFormats();
			PartialViewLocationFormats = skinEngine.GetPartialViewLocationFormats();
			AreaPartialViewLocationFormats = skinEngine.GetAreaPartialViewLocationFormats();
			FileExtensions = skinEngine.GetFileExtensions();
		}

		protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath) {
			return new OxLetWebFormViewBase(controllerContext, partialPath, null, ViewPageActivator);
		}

		protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath) {
			return new OxLetWebFormViewBase(controllerContext, viewPath, masterPath, ViewPageActivator);
		}

		public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache) {
			var view = base.FindView(controllerContext, viewName, masterName, useCache);
			return view;
		}
	}
}
