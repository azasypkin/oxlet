﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using OxLet.Core.Infrastructure;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Models;
using OxLet.Core.Models.Validation;
using OxLet.Core.Modules.Interfaces;
using OxLet.Core.Secuity;
using OxLet.Core.Secuity.Interfaces;
using OxLet.Core.Secuity.Membership;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Unity.LifetimeManagers;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.Filters;
using OxLet.Core.Web.Filters.Interfaces;
using OxLet.Core.Web.Infrastructure;
using OxLet.Core.Configuration;

namespace OxLet.Core.Web {
	public class OxLetApplication : HttpApplication {
		public OxLetApplication() {
			BeginRequest += OxletApplication_BeginRequest;
			AcquireRequestState += OxletApplication_AcquireRequestState;
		}

		private void OxletApplication_AcquireRequestState(object sender, EventArgs e) {
			var handler = Context.Handler as MvcHandler;

			if (handler != null) {
				RequestContext requestContext = handler.RequestContext;

				if (requestContext != null) {
					var container = ((IUnityContainer) Application["container"]);

					if (container != null) {

						Context.Items[typeof (Site).FullName] = container.Resolve<ISiteService>().GetSite();
						Context.Items[typeof (IUser).FullName] = GetCurrentUser(requestContext, container);
						Context.Items[typeof (RequestContext).FullName] = requestContext;

						// set up current culture
						Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = container.Resolve<ILocalizationService>().GetCulture();
					}
				}
			}
		}

		private void OxletApplication_BeginRequest(object sender, EventArgs e) {
			//Site site = GetSite();
			var siteContext = GetContainer().Resolve<SiteContext>();

			// If it is a first site run then redirect to site setup page only if it's not resource(css, image, js) request)
			if (siteContext.Site.Id == 0) {
				string setupUrl = new UrlHelper(new RequestContext(new HttpContextWrapper(Context), new RouteData())).Site();
				if (!Request.RawUrl.EndsWith(setupUrl, StringComparison.OrdinalIgnoreCase) &&
					Request.RawUrl.IndexOf("/skins", StringComparison.OrdinalIgnoreCase) == -1 &&
					!Request.RawUrl.StartsWith("/Content", StringComparison.OrdinalIgnoreCase)) {
					Response.Redirect(setupUrl, true);
				}
			}

			// If site is requested by the url which isn't matched to the configured value let's check for allowed site redirects
			if (siteContext.Site.Id != 0 && !HasSameHostAsRequest(siteContext.Site.Host)) {
				Uri hostAlias = siteContext.Site.HostRedirects.Where(HasSameHostAsRequest).FirstOrDefault();

				if (hostAlias != null) {
					Response.RedirectLocation = MakeHostMatchRequest(siteContext.Site.Host).ToString();
					Response.StatusCode = 301;
					Response.End();
				}
			}
		}

		protected virtual void OnStart() {
			SetupContainer();

			SetupModules();

			RegisterViewEngines();

			RegisterControllerFactory();

			SetupValidationProviders();
		}

		private void SetupValidationProviders() {
			ModelValidatorProviders.Providers.Clear();
			ModelValidatorProviders.Providers.Add(new ConventionValidatorProvider(GetContainer()));
		}

		private void SetupContainer() {
			IUnityContainer parentContainer = new UnityContainer();

			parentContainer
				.RegisterInstance(new AppSettingsHelper(ConfigurationManager.AppSettings))
				.RegisterInstance((OxLetSection) ConfigurationManager.GetSection("oxlet"))
				.RegisterInstance(RouteTable.Routes)
				.RegisterInstance(ViewEngines.Engines)
				.RegisterInstance(System.Web.Mvc.ModelBinders.Binders)
				.RegisterInstance(HostingEnvironment.VirtualPathProvider)
				.RegisterInstance(Membership.Provider)
				.RegisterInstance(Roles.Provider)
				.RegisterType<IUserService, OxLetMembershipProvider>()
				.RegisterType<IRolesService, OxLetMembershipRoleProvider>()
				.RegisterInstance("RegisterRoutesHandler", typeof (MvcRouteHandler));

			foreach (ConnectionStringSettings connectionString in ConfigurationManager.ConnectionStrings) {
				parentContainer.RegisterInstance(connectionString.Name, connectionString.ConnectionString);
			}

			var configurationSection = (UnityConfigurationSection) ConfigurationManager.GetSection("unity");

			IUnityContainer lastContainer = parentContainer.CreateChildContainer();
			foreach (var container in configurationSection.Containers) {
				lastContainer = lastContainer.CreateChildContainer();
				configurationSection.Configure(lastContainer, container.Name);
			}
			lastContainer.RegisterType<Site>(new FactoryLifetimeManager(
					() => HttpContext.Current.Items[typeof(Site).FullName] as Site ?? lastContainer.Resolve<ISiteService>().GetSite())
				)
				.RegisterType<User>(new FactoryLifetimeManager(
					() => {
						var user = HttpContext.Current.Items[typeof (IUser).FullName] as User;
						
						if(user != null && user.IsAuthenticated) {
							return user;
						}
						var identity = HttpContext.Current.User;
						if (identity != null && identity.Identity.IsAuthenticated) {
							return lastContainer.Resolve<IUserService>().Get(identity.Identity.Name, identity.Identity.IsAuthenticated);
						} 

						if(user != null) {
							return user;
						}
						return new UserAnonymous();
					})
				)
				.RegisterType<RequestContext>(new FactoryLifetimeManager(
					() => HttpContext.Current.Items[typeof (RequestContext).FullName] as RequestContext ?? new RequestContext(new HttpContextWrapper(HttpContext.Current), new RouteData()))
				);

			Application.Add("container", lastContainer.RegisterInstance(lastContainer));
		}

		private void SetupModules() {
			IUnityContainer container = GetContainer();
			// setup modules
			var siteModules = container.ResolveAll<ISiteModule>().Reverse();
			var routes = container.Resolve<RouteCollection>();
			var registry = container.Resolve<ActionFilterRegistry>();
			var binders = GetContainer().Resolve<ModelBinderDictionary>();
			if (siteModules.Count() > 0) {
				registry.Clear();
				routes.Clear();
				foreach (var siteModule in siteModules) {
					// register in the container
					siteModule.RegisterInContainer(container);
					// lets config modules
					siteModule.RegisterRoutes(routes);
					// register action filters
					siteModule.RegisterActionFilters(registry);
					// register model binders
					siteModule.RegisterModelBinders(binders);
				}
				container.RegisterInstance<IActionFilterRegistry>(registry);
			}
		}

		private IUser GetCurrentUser(RequestContext context, IUnityContainer container) {

			foreach (var module in container.ResolveAll<IAuthenticationModule>()) {
				var user = module.Authentificate(context);
				if (user != null) {
					return user;
				}
			}

			return new UserAnonymous(container.Resolve<IUserDescriptionService>().GetUserDescription("Anonymous"));
		}

		private void RegisterViewEngines() {
			var viewEngines = GetContainer().Resolve<ViewEngineCollection>();
			viewEngines.Clear();
			viewEngines.Add(GetContainer().Resolve<IViewEngine>());
		}

		private void RegisterControllerFactory() {
			ControllerBuilder.Current.SetControllerFactory(GetContainer().Resolve<OxletControllerFactory>());
		}

		private IUnityContainer GetContainer() {
			return (IUnityContainer) Application["container"];
		}

		private bool HasSameHostAsRequest(Uri url) {
			if (!string.Equals(url.Scheme, Request.Url.Scheme, StringComparison.OrdinalIgnoreCase)) {
				return false;
			}

			if (!string.Equals(url.Host, Request.Url.Host, StringComparison.OrdinalIgnoreCase)) {
				return false;
			}

			if (url.Port != Request.Url.Port) {
				return false;
			}

			return true;
		}

		private Uri MakeHostMatchRequest(Uri configuredSiteUrl) {
			if (configuredSiteUrl == null) {
				return null;
			}

			var requestUriBuilder = new UriBuilder(Request.Url);
			var siteUriBuilder = new UriBuilder(configuredSiteUrl);

			requestUriBuilder.Scheme = siteUriBuilder.Scheme;
			requestUriBuilder.Host = siteUriBuilder.Host;
			requestUriBuilder.Port = siteUriBuilder.Port;

			return requestUriBuilder.Uri;
		}

		protected void Application_Start() {
			OnStart();
		}
	}
}