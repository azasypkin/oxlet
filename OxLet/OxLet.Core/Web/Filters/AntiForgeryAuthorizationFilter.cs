﻿using System;
using System.Web.Mvc;
using OxLet.Core.Infrastructure;
using OxLet.Core.Web.Infrastructure;

namespace OxLet.Core.Web.Filters {
	public class AntiForgeryAuthorizationFilter : IAuthorizationFilter {
		private readonly SiteContext _context;

		public AntiForgeryAuthorizationFilter(SiteContext context) {
			_context = context;
		}

		public void OnAuthorization(AuthorizationContext filterContext) {
			if (!(filterContext.RouteData.Values["validateAntiForgeryToken"] is bool
			      && (bool) filterContext.RouteData.Values["validateAntiForgeryToken"]
				  && filterContext.HttpContext.Request.HttpMethod == "POST")) {
			      //&& filterContext.RequestContext.HttpContext.Request.IsAuthenticated)) {
				return;
			}

			string salt = _context.Site.Id.ToString();

			var antiForgeryToken = new AntiForgeryToken(salt);

			long ticks;

			var antiForgeryCookie = filterContext.HttpContext.Request.Cookies.Get(AntiForgeryToken.TicksName);

			string formTicks = antiForgeryCookie != null ? antiForgeryCookie.Value : null;

			string formHash = filterContext.HttpContext.Request.Form[AntiForgeryToken.TokenName];

			if (string.IsNullOrEmpty(formTicks) || !long.TryParse(formTicks, out ticks) || string.IsNullOrEmpty(formHash)) {
				throw new HttpAntiForgeryException("Bad Anti-Forgery Token");
			}

			var timeOffset = new TimeSpan(antiForgeryToken.Ticks - ticks);

			//todo: (nheskew)drop the time span into some configurable property
			// and handle the "exception" better than just throwing one. ideally we should give the form back, populated with the same data, with a 
			// message saying something like "fall asleep at your desk? writing a novel? try that submission again!"
			if (
				!(AntiForgeryToken.GetHash(salt, ticks.ToString()) == formHash &&
				  timeOffset.TotalMinutes < _context.Configuration.AntiForgeryCookieTtl)) {
				throw new HttpAntiForgeryException("Bad Anti-Forgery Token");
			}
		}
	}
}