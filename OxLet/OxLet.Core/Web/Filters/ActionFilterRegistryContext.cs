﻿using System.Web.Mvc;

namespace OxLet.Core.Web.Filters {
	public class ActionFilterRegistryContext {
		public ActionFilterRegistryContext(ControllerContext controllerContext, ActionDescriptor actionDescriptor) {
			ActionDescriptor = actionDescriptor;
			ControllerContext = controllerContext;
		}

		public ActionDescriptor ActionDescriptor { get; set; }
		public ControllerContext ControllerContext { get; set; }
	}
}