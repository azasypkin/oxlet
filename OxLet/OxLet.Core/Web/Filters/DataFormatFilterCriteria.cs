﻿using System;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Web.Filters {
	public class DataFormatFilterCriteria : IActionFilterCriteria {
		private readonly string format;

		public DataFormatFilterCriteria(string format) {
			this.format = format;
		}

		#region IFilterCriteria Members

		public bool Match(ActionFilterRegistryContext context) {
			return string.Equals(format, context.ControllerContext.RouteData.Values["dataFormat"] as string, StringComparison.OrdinalIgnoreCase);
		}

		#endregion
	}
}
