﻿using System.Web.Mvc;
using OxLet.Core.Infrastructure;
using OxLet.Core.Web.Helpers;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Filters {
	public class NavigationActionFilter : IActionFilter {
		private readonly SiteContext _siteContext;

		public NavigationActionFilter(SiteContext siteContext) {
			_siteContext = siteContext;
		}

		#region IActionFilter Members

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model != null) {

				model.Navigation = new NavigationEntity {
					Area = NavigationHelpers.GetAreaName(filterContext.RouteData),
					Controller = filterContext.Controller.ValueProvider.GetValue("controller").RawValue as string,
					Action = filterContext.Controller.ValueProvider.GetValue("action").RawValue as string,
				};
			}
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}

		#endregion
	}
}
