﻿using System;
using System.Collections.Generic;
using System.Linq;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Web.Filters {
	public class ActionFilterRecord {
		private readonly List<IActionFilterCriteria> _criteria;
		public Type FilterType { get; private set; }

		public ActionFilterRecord(IEnumerable<IActionFilterCriteria> criteria, Type filterType) {
			_criteria = new List<IActionFilterCriteria>(criteria);
			FilterType = filterType;
		}

		public bool Match(ActionFilterRegistryContext context) {
			return _criteria.Aggregate(true, (prev, f) => prev ? f.Match(context) : false);
		}
	}
}