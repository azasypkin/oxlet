﻿using System.Web.Mvc;
using OxLet.Core.Configuration;

namespace OxLet.Core.Web.Filters {
	public class PageSizeActionFilter : IActionFilter {

		private readonly int _defaultPageSize;

		public PageSizeActionFilter(OxLetSection section) {
			_defaultPageSize = section.GetValue<int>("PageSize", 10);
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
			if (filterContext.ActionParameters.ContainsKey("pageSize")) {
				filterContext.ActionParameters["pageSize"] = _defaultPageSize;
			}
		}
	}
}
