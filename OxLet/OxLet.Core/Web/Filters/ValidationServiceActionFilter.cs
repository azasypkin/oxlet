﻿using System.Web.Mvc;
using OxLet.Core.Validation;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Filters {
	public class ValidationServiceActionFilter : IActionFilter {
		private readonly IValidationService _validationService;

		public ValidationServiceActionFilter(IValidationService validationService) {
			_validationService = validationService;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model != null) {
				model.ValidationService = _validationService;
			}
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
