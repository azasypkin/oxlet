﻿using System.Web.Mvc;
using OxLet.Core.Infrastructure;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Filters {
	public class SiteContextActionFilter : IActionFilter {
		private readonly SiteContext _context;

		public SiteContextActionFilter(SiteContext siteContext) {
			_context = siteContext;
		}

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model != null) {
				model.Context = _context;
			}
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}
	}
}
