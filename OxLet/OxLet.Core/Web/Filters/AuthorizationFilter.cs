﻿using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Core.Infrastructure;
using OxLet.Core.Web.Extensions;

namespace OxLet.Core.Web.Filters {
	public class AuthorizationFilter : IAuthorizationFilter {
		private readonly RouteCollection _routes;
		private readonly SiteContext _context;

		public AuthorizationFilter(SiteContext context, RouteCollection routes) {
			_routes = routes;
			_context = context;
		}

		public void OnAuthorization(AuthorizationContext filterContext) {
			if (!_context.User.IsAuthenticated) {
				var helper = new UrlHelper(filterContext.RequestContext, _routes);
				filterContext.Result = new RedirectResult(helper.SignIn(filterContext.HttpContext.Request.Url.AbsolutePath));
			}
		}
	}
}
