﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace OxLet.Core.Web.Filters.Interfaces {
	public interface IActionFilterRegistry {
		void Clear();

		void Add(IEnumerable<IActionFilterCriteria> criteria, Type filterType);

		FilterInfo GetFilters(ActionFilterRegistryContext context);
	}
}