﻿namespace OxLet.Core.Web.Filters.Interfaces {
	public interface IActionFilterCriteria {
		bool Match(ActionFilterRegistryContext context);
	}
}