﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Web.Filters {
	public class ActionFilterRegistry : IActionFilterRegistry {
		private readonly IUnityContainer _container;
		private readonly List<ActionFilterRecord> _registry;

		public ActionFilterRegistry(IUnityContainer container) {
			_container = container;
			_registry = new List<ActionFilterRecord>();
		}

		public void Clear() {
			_registry.Clear();
		}

		public void Add(IEnumerable<IActionFilterCriteria> criteria, Type filterType) {
			_registry.Add(new ActionFilterRecord(criteria, filterType));
		}

		public FilterInfo GetFilters(ActionFilterRegistryContext context) {
			var filters = new FilterInfo();

			foreach (ActionFilterRecord record in _registry) {
				if (record.Match(context)) {
					object filterObject = _container.Resolve(record.FilterType);

					if (filterObject is IActionFilter){
						filters.ActionFilters.Add((IActionFilter)filterObject);
					}

					if (filterObject is IAuthorizationFilter){
						filters.AuthorizationFilters.Add((IAuthorizationFilter)filterObject);
					}

					if (filterObject is IExceptionFilter){
						filters.ExceptionFilters.Add((IExceptionFilter)filterObject);
					}

					if (filterObject is IResultFilter){
						filters.ResultFilters.Add((IResultFilter)filterObject);
					}
				}
			}

			return filters;
		}
	}
}
