﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Web.Filters {
	public class ControllerActionCriteria : IActionFilterCriteria {
		private readonly List<ReflectedActionDescriptor> _methods = new List<ReflectedActionDescriptor>();

		public void AddMethod<T>(Expression<Func<T, object>> method) {
			var methodCall = method.Body as MethodCallExpression;

			if (methodCall == null) {
				throw new InvalidOperationException();
			}

			_methods.Add(new ReflectedActionDescriptor(
				methodCall.Method,
				methodCall.Method.Name,
				new ReflectedControllerDescriptor(methodCall.Object.Type)
			));
		}

		public bool Match(ActionFilterRegistryContext context) {
			var reflectedDescriptor = context.ActionDescriptor as ReflectedActionDescriptor;
			if (reflectedDescriptor != null) {
				return _methods.Any(a => a.MethodInfo == reflectedDescriptor.MethodInfo);
			}

			return _methods.Any(
					a => a.ControllerDescriptor.FindAction(context.ControllerContext, context.ActionDescriptor.ActionName) != null
			);
		}
	}
}
