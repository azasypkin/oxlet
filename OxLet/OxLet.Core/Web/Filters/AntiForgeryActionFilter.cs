﻿using System.Web.Mvc;
using OxLet.Core.Models;
using OxLet.Core.Web.Infrastructure;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Filters {
	public class AntiForgeryActionFilter : IActionFilter {
		private readonly Site _site;

		public AntiForgeryActionFilter(Site site) {
			_site = site;
		}

		#region IActionFilter Members

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			var model = filterContext.Controller.ViewData.Model as OxletModel;

			if (model != null) {
				model.AntiForgeryToken = new AntiForgeryToken(_site.Id.ToString());
			}
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}

		#endregion
	}
}
