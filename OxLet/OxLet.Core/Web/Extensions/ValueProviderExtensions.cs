﻿using System.Web.Mvc;
using OxLet.Core.Tools;

namespace OxLet.Core.Web.Extensions {
	public static class ValueProviderExtensions {
		public static TValue GetValue<TValue>(this ValueProviderResult providerResult, TValue defaultValue) {
			return GenericStringValueRetriever.GetValue(providerResult, x => x.AttemptedValue, defaultValue);
		}

		public static TValue GetValue<TValue>(this ValueProviderResult providerResult) {
			return GetValue(providerResult, default(TValue));
		}
	}
}
