﻿using System;
using System.Threading;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Models.Extensions;
using OxLet.Core.Secuity;
using OxLet.Core.Web.ViewModels;
using OxLet.Core.Web.Helpers;

namespace OxLet.Core.Web.Extensions {
	public static class UrlHelperExtensions {

		private const string AreaTemplateName = "{area}";
		private const string SkinTemplateName = "{skin}";

		public static string OxiLet(this UrlHelper urlHelper) {
			return "http://OxiLet.com";
		}

		public static string Home(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("Posts");
		}

		public static string ContentItem(this UrlHelper urlHelper, ContentItemPath path) {
			return urlHelper.RouteOxiLetUrl("AdminContentItem", new {contentItemPath = path});
		}

		public static string ContentItemFolder(this UrlHelper urlHelper, ContentItemPath path) {
			return urlHelper.RouteOxiLetUrl("AdminContentItemFolder", new { contentItemPath = path });
		}

		public static string AbsolutePath(this UrlHelper urlHelper, string relativeUrl) {
			Uri url = urlHelper.RequestContext.HttpContext.Request.Url;
			var uriBuilder = new UriBuilder(url.Scheme, url.Host, url.Port) {Path = relativeUrl};
			return uriBuilder.Uri.ToString();
		}

		public static string AppPath(this UrlHelper urlHelper, string relativePath) {
			if (relativePath == null) {
				return null;
			}

			return VirtualPathUtility.ToAbsolute(relativePath, urlHelper.RequestContext.HttpContext.Request.ApplicationPath);
		}

		public static string ImagePath(this UrlHelper urlHelper, OxletModel model) {
			return ImagePath(urlHelper, model, true);
		}

		public static string ImagePath(this UrlHelper urlHelper, OxletModel model, bool isRoot) {
			return ImagePath(urlHelper, model, "", true);
		}

		public static string ImagePath(this UrlHelper urlHelper, OxletModel model, string relativePath, bool isRoot) {
			if (!string.IsNullOrEmpty(relativePath) && !relativePath.StartsWith("/")) {
				relativePath = "/" + relativePath;
			}
			return urlHelper.AppPath(
				GetFormatedPath(
					NavigationHelpers.GetAreaName(urlHelper.RequestContext.RouteData),
					model.SkinName,
					model.Context.Site.ImagesPath,
					isRoot
				)
			) + relativePath;
		}

		public static string CssPath(this UrlHelper urlHelper, OxletModel model) {
			return CssPath(urlHelper, model, true);
		}

		public static string CssPath(this UrlHelper urlHelper, OxletModel model, bool isRoot) {
			return CssPath(urlHelper, model, "", isRoot);
		}

		public static string CssPath(this UrlHelper urlHelper, OxletModel model, string relativePath, bool isRoot) {
			if (!string.IsNullOrEmpty(relativePath) && !relativePath.StartsWith("/")) {
				relativePath = "/" + relativePath;
			}
			return urlHelper.AppPath(
				GetFormatedPath(
					NavigationHelpers.GetAreaName(urlHelper.RequestContext.RouteData), 
					model.SkinName,
					model.Context.Site.CssPath,
					isRoot
				)
			) + relativePath;
		}

		public static string ScriptPath(this UrlHelper urlHelper, OxletModel model) {
			return ScriptPath(urlHelper, model, true);
		}

		public static string ScriptPath(this UrlHelper urlHelper, OxletModel model, bool isRoot) {
			return ScriptPath(urlHelper, model, "", isRoot);
		}

		public static string ScriptPath(this UrlHelper urlHelper, OxletModel model, string relativePath, bool isRoot) {
			if (!string.IsNullOrEmpty(relativePath) && !relativePath.StartsWith("/")) {
				relativePath = "/" + relativePath;
			}
			return urlHelper.AppPath(
				GetFormatedPath(
					NavigationHelpers.GetAreaName(urlHelper.RequestContext.RouteData),
					model.SkinName,
					model.Context.Site.ScriptsPath,
					isRoot
				)
			) + relativePath; 
		}

		private static string GetFormatedPath(string area, string skinName, string path, bool isRoot) {

			// apply skin template
			if(path.IndexOf(SkinTemplateName) > 0) {
				path = path.Replace(SkinTemplateName, skinName);
			}

			// if area is presented in the template
			var areaTemplateIndex = path.IndexOf(AreaTemplateName);

			// if area template is set but current context hasn't area then remove area template
			// with leading slash if presented
			if(areaTemplateIndex > 0) {
				
				// if area doesn't exist in the current context or styles set as root 
				// then remove area template
				if(string.IsNullOrEmpty(area) || isRoot) {
					path = path[areaTemplateIndex - 1] == '/'
						? path.Remove(areaTemplateIndex - 1, AreaTemplateName.Length + 1)
						: path.Remove(areaTemplateIndex, AreaTemplateName.Length);
				}
				else if(!string.IsNullOrEmpty(area)) {
					path = path.Replace(AreaTemplateName, string.Format("areas/{0}", area));
				}
			}
			return path;
		}

		public static string OpenSearch(this UrlHelper urlHelper) {
			return urlHelper.RouteUrl("OpenSearch");
		}

		public static string SiteMapIndex(this UrlHelper urlHelper) {
			//return urlHelper.RouteOxiLetUrl("SiteMapIndex");
			return urlHelper.RouteOxiLetUrl("SiteMap");
		}

		public static string SiteMap(this UrlHelper urlHelper, int year, int month) {
			return urlHelper.RouteOxiLetUrl("SiteMap", new {year, month});
		}

		public static string SignIn(this UrlHelper urlHelper, string returnUrl) {
			return urlHelper.RouteOxiLetUrl("SignIn", new { ReturnUrl = returnUrl });
		}

		public static string SignOut(this UrlHelper urlHelper, string returnUrl) {
			return urlHelper.RouteUrl("SignOut", new { ReturnUrl = returnUrl });
		}

		public static string ComputeHash(this UrlHelper urlHelper) {
			return urlHelper.RouteUrl("ComputeHash");
		}

		public static string Site(this UrlHelper urlHelper) {
			return urlHelper.RouteUrl("Site");
		}

		public static string ManageBlog(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("ManageBlog");
		}

		public static string ManageSite(this UrlHelper urlHelper) {
			return urlHelper.RouteUrl("ManageSite");
		}

		public static string ManageUsers(this UrlHelper urlHelper) {
			return urlHelper.RouteUrl("ManageUsers");
		}

		public static string UserChangePassword(this UrlHelper urlHelper) {
			return urlHelper.RouteUrl("UserChangePassword");
		}

		//public static string Plugins(this UrlHelper urlHelper) {
		//    return urlHelper.RouteUrl("Plugins");
		//}

		//public static string Plugin(this UrlHelper urlHelper, IPlugin plugin) {
		//    return urlHelper.RouteUrl("Plugin", new {pluginID = plugin.Id.ToString("N")});
		//}

		public static string Admin(this UrlHelper urlHelper) {
			return urlHelper.RouteOxiLetUrl("Admin");
		}

		public static string AdminUser(this UrlHelper urlHelper, User user) {
			return user.GetAdminUrl(urlHelper.RequestContext, urlHelper.RouteCollection);
		}

		public static string AdminUserChangePassword(this UrlHelper urlHelper, User user) {
			return user.GetAdminChangePasswordUrl(urlHelper.RequestContext, urlHelper.RouteCollection);
		}

		public static bool IsHome(this UrlHelper urlHelper) {
			var controller = urlHelper.RequestContext.RouteData.Values["controller"] as string;
			var action = urlHelper.RequestContext.RouteData.Values["action"] as string;

			return
				!string.IsNullOrEmpty(controller) &&
				string.Compare(controller, "Post", true) == 0 &&
				!string.IsNullOrEmpty(action) &&
				string.Compare(action, "List", true) == 0;
		}

		public static bool IsPagePath(this UrlHelper urlHelper, string pagePath) {
			var controller = urlHelper.RequestContext.RouteData.Values["controller"] as string;
			var pagePathValue = urlHelper.RequestContext.RouteData.Values["pagePath"] as string;

			return
				!string.IsNullOrEmpty(controller) &&
				string.Compare(controller, "Page", true) == 0 &&
				!string.IsNullOrEmpty(pagePath) &&
				string.Compare(pagePathValue, pagePath, true) == 0;
		}

		public static bool IsAdmin(this UrlHelper urlHelper) {
			var controller = urlHelper.RequestContext.RouteData.Values["controller"] as string;

			//TODO: (erikpo) Once the admin is refactored, change this to look at the url to determine if we're in the admin or not
			return
				!string.IsNullOrEmpty(controller) &&
				string.Compare(controller, "Admin", true) == 0;
		}

		public static string ChangeLanguage(this UrlHelper urlHelper, string languageCode, string returnUrl) {
			return urlHelper.RouteUrl("ChangeLanguage", new { langCode = languageCode, returnUrl });
		}

		private static string GetLanguageRouteValue(IUnityContainer container) {
			return Thread.CurrentThread.CurrentCulture.Name.StartsWith(
				container != null ? container.Resolve<SiteContext>().Site.DefaultLanguage.Name.ToLower() : "en-us",
				StringComparison.InvariantCultureIgnoreCase
			)
				? null
				: Thread.CurrentThread.CurrentCulture.Name.ToLower();
		}

		public static string RouteOxiLetUrl(this UrlHelper urlHelper, string routeName) {
			return urlHelper.RouteOxiLetUrl(routeName, null);
		}

		public static string RouteOxiLetUrl(this UrlHelper urlHelper, string routeName, object routeValues) {
			var routeValuesDictionary = routeValues != null
				? new RouteValueDictionary(routeValues)
				: new RouteValueDictionary();

			var language = GetLanguageRouteValue(((IUnityContainer)urlHelper.RequestContext.HttpContext.Application["container"]));
			if(!string.IsNullOrEmpty(language)) {
				routeValuesDictionary.Add("lang", language);
			}
			return urlHelper.RouteUrl(
				!string.IsNullOrEmpty(language)
					? string.Format("{0}Localized", routeName)
					: routeName,
				routeValuesDictionary);
		}

		public static string LocalizedUrl(this UrlHelper urlHelper, string language, string path) {
			var currentCulture = Thread.CurrentThread.CurrentCulture.Name.ToLower();
			bool isDefualtRequired = language.StartsWith(
				"en-us",
				StringComparison.InvariantCultureIgnoreCase
			);

			if (isDefualtRequired) {
				if (path.IndexOf(currentCulture, StringComparison.InvariantCultureIgnoreCase) >= 0) {
					path = path.Remove(1, path.Length > 6 ? 6 : 5);
				}
			} else {
				path = path.IndexOf(currentCulture, StringComparison.InvariantCultureIgnoreCase) >= 0
					? path.Replace(currentCulture, language)
					: string.Format("/{0}{1}", language, path);
			}
			return path;
		}
	}
}
