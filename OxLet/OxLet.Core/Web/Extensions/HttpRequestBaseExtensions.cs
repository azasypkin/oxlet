﻿using System;
using System.Net;
using System.Web;
using OxLet.Core.Extensions;

namespace OxLet.Core.Web.Extensions {
	public static class HttpRequestBaseExtensions {
		public static IPAddress GetUserIpAddress(this HttpRequestBase request) {
			IPAddress address;

			if (!IPAddress.TryParse(request.UserHostAddress, out address)) {
				address = null;
			}

			return address;
		}

		public static DateTime? IfModifiedSince(this HttpRequestBase request) {
			string ifModifiedSinceValue = request.Headers["If-Modified-Since"];
			DateTime ifModifiedSince;

			if (!string.IsNullOrEmpty(ifModifiedSinceValue) && DateTime.TryParse(ifModifiedSinceValue, out ifModifiedSince)) {
				return ifModifiedSince;
			}

			return null;
		}

		public static string GenerateAntiForgeryToken(this HttpRequestBase request, string key, string salt) {
			return (key + salt + request.UserAgent).ComputeHash();
		}

		public static string GenerateSiteHost(this HttpRequestBase request) {
			return string.Format("{0}://{1}{2}{3}", request.Url.Scheme, request.Url.Host,
								 request.Url.Port != 80 ? ":" + request.Url.Port : "",
								 request.ApplicationPath.Length > 1 ? request.ApplicationPath.Substring(1) : "");
		}
	}
}