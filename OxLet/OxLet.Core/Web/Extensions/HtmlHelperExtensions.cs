﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Mvc.Html;
using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Core.Configuration;
using OxLet.Core.Extensions;
using OxLet.Core.Models;
using OxLet.Core.Web.Infrastructure;
using OxLet.Core.Web.ViewModels;
using System.Web;

namespace OxLet.Core.Web.Extensions {
	public static class HtmlHelperExtensions {
		#region OxiletAntiForgeryToken

		public static string OxiletAntiForgeryToken<TModel>(this HtmlHelper<TModel> htmlHelper,
		                                                   Func<TModel, AntiForgeryToken> getToken) where TModel : class {
			AntiForgeryToken token = getToken(htmlHelper.ViewData.Model);

			if (token == null) return "";

			htmlHelper.ViewContext.HttpContext.Response.Cookies.Add(new HttpCookie(AntiForgeryToken.TicksName,
			                                                                                  token.Ticks.ToString()));

			return string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\" />", AntiForgeryToken.TokenName, token.Value);
		}

		#endregion

		#region CheckBox

		public static string CheckBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, bool> getIsChecked,
		                                      string labelInnerHtml) where TModel : OxletModel {
			return CheckBox(htmlHelper, name, getIsChecked, labelInnerHtml, true);
		}

		public static string CheckBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, bool> getIsChecked,
		                                      string labelInnerHtml, bool enabled) where TModel : OxletModel {
			return CheckBox(htmlHelper, name, getIsChecked, null, enabled, null);
		}

		public static string CheckBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, bool> getIsChecked,
		                                      string labelInnerHtml, object htmlAttributes) where TModel : OxletModel {
			return CheckBox(htmlHelper, name, getIsChecked, labelInnerHtml, true, htmlAttributes);
		}

		public static string CheckBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, bool> getIsChecked,
		                                      string labelInnerHtml, bool enabled, object htmlAttributes)
			where TModel : OxletModel {
			bool value = htmlHelper.ViewContext.HttpContext.Request.Form.GetValues(name) != null
			             	? htmlHelper.ViewContext.HttpContext.Request.Form.IsTrue(name)
			             	: getIsChecked(htmlHelper.ViewData.Model);

			var attributes = new RouteValueDictionary(htmlAttributes);

			if (!enabled)
				attributes.Add("disabled", "disabled");

			return FieldWithLabel(htmlHelper, name, () => htmlHelper.CheckBox(name, value, attributes).ToHtmlString(), labelInnerHtml);
		}

		#endregion

		#region ConvertToLocalTime

		public static DateTime ConvertToLocalTime<TModel>(this HtmlHelper<TModel> htmlHelper, DateTime dateTime)
			where TModel : OxletModel {
			return ConvertToLocalTime(htmlHelper, dateTime, htmlHelper.ViewData.Model);
		}

		public static DateTime ConvertToLocalTime(this HtmlHelper htmlHelper, DateTime dateTime, OxletModel model) {
			if (model.Context.User == null) {
				if (model.Context.Site.TimeZoneOffset != 0)
					return dateTime.Add(TimeSpan.FromHours(model.Context.Site.TimeZoneOffset));

				return dateTime;
			}

			return dateTime; //TODO: (erikpo) Get the timezone offset from the current user and apply it
		}

		#endregion

		#region Gravatar

		public static string Gravatar<TModel>(this HtmlHelper<TModel> htmlHelper, string size) where TModel : OxletModel {
			OxletModel model = htmlHelper.ViewData.Model;

			return htmlHelper.Gravatar(
				model.Context.User != null ? (model.Context.User.EmailHash ?? model.Context.User.Email.ComputeHash()).CleanAttribute() : null,
				model.Context.User != null ? (model.Context.User.DisplayName ?? model.Context.User.Name).CleanAttribute() : null,
				size,
				model.Context.Site.GravatarDefault
				);
		}

		public static string Gravatar<TModel>(this HtmlHelper<TModel> htmlHelper, UserDescription user, string size)
			where TModel : OxletModel {
			OxletModel model = htmlHelper.ViewData.Model;

			return htmlHelper.Gravatar(
				user != null ? user.EmailHash.CleanAttribute() : null,
				user != null ? (user.DisplayName ?? user.Name).CleanAttribute() : null,
				size,
				model.Context.Site.GravatarDefault
				);
		}

		public static string Gravatar(this HtmlHelper htmlHelper, OxletModel model, UserDescription user, string size) {
			return htmlHelper.Gravatar(
				user != null ? user.EmailHash.CleanAttribute() : null,
				user != null ? (user.DisplayName ?? user.Name).CleanAttribute() : null,
				size,
				model.Context.Site.GravatarDefault
				);
		}

		public static string Gravatar(this HtmlHelper htmlHelper, string id, string name, string size, string defaultImage) {
			return htmlHelper.Image(
				string.Format("http://www.gravatar.com/avatar/{0}?s={1}&d={2}", id ?? "@", size, defaultImage),
				string.Format("{0} (gravatar)", name ?? "Mrs. Gravatar"),
				new RouteValueDictionary(new {width = size, height = size, @class = "gravatar"})
				);
		}

		#endregion

		#region HeadLink

		public static string HeadLink(this HtmlHelper htmlHelper, string rel, string href, string type, string title) {
			return htmlHelper.HeadLink(rel, href, type, title, null);
		}

		public static string HeadLink(this HtmlHelper htmlHelper, string rel, string href, string type, string title,
		                              object htmlAttributes) {
			var tagBuilder = new TagBuilder("link");

			tagBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
			if (!string.IsNullOrEmpty(rel)) {
				tagBuilder.MergeAttribute("rel", rel);
			}
			if (!string.IsNullOrEmpty(href)) {
				tagBuilder.MergeAttribute("href", href);
			}
			if (!string.IsNullOrEmpty(type)) {
				tagBuilder.MergeAttribute("type", type);
			}
			if (!string.IsNullOrEmpty(title)) {
				tagBuilder.MergeAttribute("title", title);
			}

			return tagBuilder.ToString(TagRenderMode.SelfClosing);
		}

		#endregion

		#region Image

		public static string Image(this HtmlHelper helper, string src, string alt, IDictionary<string, object> htmlAttributes) {
			var url = new UrlHelper(helper.ViewContext.RequestContext);
			string imageUrl = url.Content(src);
			var imageTag = new TagBuilder("img");

			if (!string.IsNullOrEmpty(imageUrl)) {
				imageTag.MergeAttribute("src", imageUrl);
			}

			if (!string.IsNullOrEmpty(alt)) {
				imageTag.MergeAttribute("alt", alt);
			}

			imageTag.MergeAttributes(htmlAttributes, true);

			if (imageTag.Attributes.ContainsKey("alt") && !imageTag.Attributes.ContainsKey("title")) {
				imageTag.MergeAttribute("title", imageTag.Attributes["alt"] ?? "");
			}

			return imageTag.ToString(TagRenderMode.SelfClosing);
		}

		#endregion

		#region Input

		public static string DropDownList(this HtmlHelper htmlHelper, string name, SelectList selectList,
		                                  object htmlAttributes, bool isEnabled) {
			var htmlAttributeDictionary = new RouteValueDictionary(htmlAttributes);

			if (!isEnabled) {
				htmlAttributeDictionary["disabled"] = "disabled";

				var inputItemBuilder = new StringBuilder();
				inputItemBuilder.AppendLine(htmlHelper.DropDownList(string.Format("{0}_view", name), selectList,
				                                                    htmlAttributeDictionary).ToHtmlString());
				inputItemBuilder.AppendLine(htmlHelper.Hidden(name, selectList.SelectedValue).ToHtmlString());
				return inputItemBuilder.ToString();
			}

			return htmlHelper.DropDownList(name, selectList, htmlAttributeDictionary).ToHtmlString();
		}

		public static string TextBox(this HtmlHelper htmlHelper, string name, object value, object htmlAttributes,
		                             bool isEnabled) {
			var htmlAttributeDictionary = new RouteValueDictionary(htmlAttributes);

			if (!isEnabled) {
				htmlAttributeDictionary["disabled"] = "disabled";

				var inputItemBuilder = new StringBuilder();

				inputItemBuilder.Append(htmlHelper.TextBox(string.Format("{0}_view", name), value, htmlAttributeDictionary));
				inputItemBuilder.Append(htmlHelper.Hidden(name, value));

				return inputItemBuilder.ToString();
			}

			return htmlHelper.TextBox(name, value, htmlAttributeDictionary).ToHtmlString();
		}

		public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name, TEnum selectedValue) {
			IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum))
				.Cast<TEnum>();

			IEnumerable<SelectListItem> items =
				from value in values
				select new SelectListItem {
					Text = value.ToString(),
					Value = value.ToString(),
					Selected = (value.Equals(selectedValue))
				};

			return htmlHelper.DropDownList(
				name,
				items
			);
		}

		public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IEnumerable<TEnum> excludes) {
			ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
			IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

			IEnumerable<SelectListItem> items =
				values.Select(value => new SelectListItem {
					Text = excludes != null && excludes.Any(x=>value.ToString() == x.ToString()) ? "" : value.ToString(),
					Value = value.ToString(),
					Selected = value.Equals(metadata.Model)
				});

			return htmlHelper.DropDownListFor(
				expression,
				items
			);
		}

		public static string RadioButton(this HtmlHelper htmlHelper, string name, object value, bool isChecked,
		                                 object htmlAttributes, bool isEnabled) {
			var htmlAttributeDictionary = new RouteValueDictionary(htmlAttributes);

			if (!isEnabled) {
				htmlAttributeDictionary["disabled"] = "disabled";

				var inputItemBuilder = new StringBuilder();

				inputItemBuilder.AppendLine(htmlHelper.RadioButton(string.Format("{0}_view", name), value, isChecked,
				                                                   htmlAttributeDictionary).ToHtmlString());

				if (isChecked) {
					inputItemBuilder.AppendLine(htmlHelper.Hidden(name, value).ToHtmlString());
				}

				return inputItemBuilder.ToString();
			}

			return htmlHelper.RadioButton(name, value, isChecked, htmlAttributeDictionary).ToHtmlString();
		}

		public static string Button(this HtmlHelper htmlHelper, string name, string buttonContent, object htmlAttributes) {
			return htmlHelper.Button(name, buttonContent, new RouteValueDictionary(htmlAttributes));
		}

		public static string Button(this HtmlHelper htmlHelper, string name, string buttonContent,
		                            IDictionary<string, object> htmlAttributes) {
			var tagBuilder = new TagBuilder("button") {InnerHtml = buttonContent};

			tagBuilder.MergeAttributes(htmlAttributes);

			return tagBuilder.ToString(TagRenderMode.Normal);
		}

		#endregion

		#region Link

		public static string Link(this HtmlHelper htmlHelper, string linkText, string href) {
			return htmlHelper.Link(linkText, href, null);
		}

		public static string Link(this HtmlHelper htmlHelper, string linkText, string href, object htmlAttributes) {
			return htmlHelper.Link(linkText, href, new RouteValueDictionary(htmlAttributes));
		}

		public static string Link(this HtmlHelper htmlHelper, string linkText, string href,
		                          IDictionary<string, object> htmlAttributes) {
			var tagBuilder = new TagBuilder("a") {
			                                            	InnerHtml = linkText
			                                            };
			tagBuilder.MergeAttributes(htmlAttributes);
			tagBuilder.MergeAttribute("href", href);
			return tagBuilder.ToString(TagRenderMode.Normal);
		}

		#endregion

		#region LinkOrDefault

		public static string LinkOrDefault(this HtmlHelper htmlHelper, string linkContents, string href) {
			return htmlHelper.LinkOrDefault(linkContents, href, null);
		}

		public static string LinkOrDefault(this HtmlHelper htmlHelper, string linkContents, string href, object htmlAttributes) {
			return htmlHelper.LinkOrDefault(linkContents, href, new RouteValueDictionary(htmlAttributes));
		}

		public static string LinkOrDefault(this HtmlHelper htmlHelper, string linkContents, string href,
		                                   IDictionary<string, object> htmlAttributes) {
			if (!string.IsNullOrEmpty(href)) {
				var tagBuilder = new TagBuilder("a") {
				                                            	InnerHtml = linkContents
				                                            };
				tagBuilder.MergeAttributes(htmlAttributes);
				tagBuilder.MergeAttribute("href", href);
				linkContents = tagBuilder.ToString(TagRenderMode.Normal);
			}

			return linkContents;
		}

		#endregion

		#region PageKeywords

		public static string RobotsMetaTags<TModel>(this HtmlHelper<TModel> htmlHelper) where TModel : OxletModel {
			return htmlHelper.RobotsMetaTags(null);
		}

		public static string RobotsMetaTags<TModel>(this HtmlHelper<TModel> htmlHelper, string value) where TModel : OxletModel {
			if (value == null) {
				value = "index,follow";
			}
			return string.Format("<meta name=\"robots\" content=\"{0}\" />", value.CleanHtmlTags().CleanHref());
		}

		public static string PageKeywords<TModel>(this HtmlHelper<TModel> htmlHelper) where TModel : OxletModel {
			return htmlHelper.PageKeywords(null);
		}

		public static string PageKeywords<TModel>(this HtmlHelper<TModel> htmlHelper, params string[] keywords) where TModel : OxletModel {
			return string.Format(
				"<meta name=\"keywords\" content=\"{0}\" />",
				string.Join(", ", keywords.Select(x => x.CleanHtmlTags().CleanHref()))
			);
		}

		public static string PageDescription<TModel>(this HtmlHelper<TModel> htmlHelper) where TModel : OxletModel {
			return htmlHelper.PageDescription(null);
		}

		public static string PageDescription<TModel>(this HtmlHelper<TModel> htmlHelper, string description) where TModel : OxletModel {
			if (description == null) {
				description = htmlHelper.Encode(htmlHelper.ViewData.Model.Context.Site.Description);
			}
			description = description.CleanHtmlTags().CleanHref();
			if(description.Length > 150) {
				description = description.Substring(0, 150);
			}
			return string.Format("<meta name=\"description\" content=\"{0}\" />", description);
		}

		#endregion

		#region PageTitle

		public static string PageTitle<TModel>(this HtmlHelper<TModel> htmlHelper, params string[] items)
			where TModel : OxletModel {
			OxletModel model = htmlHelper.ViewData.Model;

			if (items == null || items.Length == 0){
				return model.Context.Site.DisplayName;
			}

			var sb = new StringBuilder(50);
			var itemList = new List<string>(items);

			itemList.RemoveAll(s => s == null);

			itemList.Insert(0, model.Context.Site.DisplayName);

			for (int i = itemList.Count - 1; i >= 0; i--) {
				sb.Append(itemList[i]);

				if (i > 0){
					sb.Append(model.Context.Site.PageTitleSeparator);
				}
			}

			return htmlHelper.Encode(sb.ToString());
		}

		#endregion

		#region PageState

		public static string PageState<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, string template) {
			if (pageOfAList.TotalPageCount > 1)
				return string.Format(
					"<div class=\"pageState\">{0}</div>",
					string.Format(
						template,
						(pageOfAList.PageIndex*pageOfAList.PageSize) + 1,
						(pageOfAList.PageIndex*pageOfAList.PageSize) + pageOfAList.Count,
						pageOfAList.TotalItemCount
					)
				);
			return "";
		}

		#endregion


		#region Password

		public static string Password<TModel>(this HtmlHelper<TModel> htmlHelper, string name, object value,
		                                      string labelInnerHtml) where TModel : OxletModel {
			return Password(htmlHelper, name, value, labelInnerHtml, true);
		}

		public static string Password<TModel>(this HtmlHelper<TModel> htmlHelper, string name, object value,
		                                      string labelInnerHtml, bool enabled) where TModel : OxletModel {
			return Password(htmlHelper, name, value, labelInnerHtml, enabled, null);
		}

		public static string Password<TModel>(this HtmlHelper<TModel> htmlHelper, string name, object value,
		                                      string labelInnerHtml, object htmlAttributes) where TModel : OxletModel {
			return Password(htmlHelper, name, value, labelInnerHtml, true, htmlAttributes);
		}

		public static string Password<TModel>(this HtmlHelper<TModel> htmlHelper, string name, object value,
		                                      string labelInnerHtml, bool enabled, object htmlAttributes)
			where TModel : OxletModel {
			var attributes = new RouteValueDictionary(htmlAttributes);

			if (!enabled)
				attributes.Add("disabled", "disabled");

			return FieldWithLabel(htmlHelper, name, () => htmlHelper.Password(name, value, attributes).ToHtmlString(), labelInnerHtml);
		}

		#endregion

		#region RenderCssFile

		public static void RenderCssFile<TModel>(this HtmlHelper<TModel> htmlHelper, string path) where TModel : OxletModel {
			htmlHelper.RenderCssFile(path, null);
		}

		public static void RenderCssFile<TModel>(this HtmlHelper<TModel> htmlHelper, string path, string releasePath) where TModel : OxletModel {
			htmlHelper.RenderCssFile(path, releasePath, true);
		}

		public static void RenderCssFile<TModel>(this HtmlHelper<TModel> htmlHelper, string path, bool isRoot) where TModel : OxletModel {
			htmlHelper.RenderCssFile(path, null, isRoot);
		}

		public static void RenderCssFile<TModel>(this HtmlHelper<TModel> htmlHelper, string path, string releasePath, bool isRoot)
			where TModel : OxletModel {
			if (string.IsNullOrEmpty(path)) {
				throw new ArgumentNullException("path");
			}

			// if it is production mode we should load from release path
			var requiredPath = htmlHelper.ViewData.Model.Context.Configuration.Mode == SiteMode.Production && !string.IsNullOrEmpty(releasePath)
				? releasePath
				: path;

			if (!(requiredPath.StartsWith("http://") || requiredPath.StartsWith("https://"))) {
				var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

				requiredPath = !requiredPath.StartsWith("/")
					? urlHelper.CssPath(htmlHelper.ViewData.Model, requiredPath, isRoot)
					: urlHelper.AppPath(requiredPath);
			}

			htmlHelper.ViewContext.HttpContext.Response.Write(
				htmlHelper.HeadLink("stylesheet", requiredPath, "text/css", "")
			);
		}

		#endregion

		#region RenderFavIcon

		public static void RenderFavIcon<TModel>(this HtmlHelper<TModel> htmlHelper) where TModel : OxletModel {
			OxletModel model = htmlHelper.ViewData.Model;

			if (!string.IsNullOrEmpty(model.Context.Site.FavIconUrl)) {
				var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

				htmlHelper.ViewContext.HttpContext.Response.Write(htmlHelper.HeadLink(
					"shortcut icon",
					urlHelper.AppPath(model.Context.Site.FavIconUrl),
					null,
					null
				));
			}
		}

		#endregion

		#region RenderFeedDiscovery

		public static void RenderFeedDiscoveryRss(this HtmlHelper htmlHelper, string title, string url) {
			htmlHelper.RenderFeedDiscovery(title, url, "application/rss+xml");
		}

		public static void RenderFeedDiscoveryAtom(this HtmlHelper htmlHelper, string title, string url) {
			htmlHelper.RenderFeedDiscovery(title, url, "application/atom+xml");
		}

		public static void RenderFeedDiscovery(this HtmlHelper htmlHelper, string title, string url, string type) {
			htmlHelper.ViewContext.HttpContext.Response.Write(
				htmlHelper.HeadLink("alternate", url, type, title)
				);
		}

		#endregion

		#region RenderLiveWriterManifest

		public static void RenderLiveWriterManifest(this HtmlHelper htmlHelper) {
			var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

			htmlHelper.RenderLiveWriterManifest(urlHelper.AppPath("~/LiveWriterManifest.xml"));
		}

		public static void RenderLiveWriterManifest(this HtmlHelper htmlHelper, string path) {
			htmlHelper.ViewContext.HttpContext.Response.Write(
				htmlHelper.HeadLink(
					"wlwmanifest",
					path,
					"application/wlwmanifest+xml",
					""
					)
				);
		}

		#endregion

		#region RenderOpenSearch

		public static void RenderOpenSearch<TModel>(this HtmlHelper<TModel> htmlHelper, string searchFormat) where TModel : OxletModel {
			OxletModel model = htmlHelper.ViewData.Model;

			if (model.Context.Site.IncludeOpenSearch) {
				var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

				htmlHelper.ViewContext.HttpContext.Response.Write(htmlHelper.HeadLink(
					"search",
					urlHelper.AbsolutePath(urlHelper.OpenSearch()),
					"application/opensearchdescription+xml",
					string.Format(searchFormat, model.Context.Site.DisplayName)
				));
			}
		}

		#endregion

		#region RenderPartialFromSkin

		public static void RenderPartialFromSkin(this HtmlHelper htmlHelper, string partialViewName) {
			htmlHelper.RenderPartialFromSkin(partialViewName, null, htmlHelper.ViewData);
		}

		public static void RenderPartialFromSkin(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData) {
			htmlHelper.RenderPartialFromSkin(partialViewName, null, viewData);
		}

		public static void RenderPartialFromSkin(this HtmlHelper htmlHelper, string partialViewName, object model) {
			htmlHelper.RenderPartialFromSkin(partialViewName, model, htmlHelper.ViewData);
		}

		private static void RenderPartialFromSkin(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData) {
			ViewDataDictionary newViewData;

			if (model == null){
				newViewData = viewData == null ? new ViewDataDictionary(htmlHelper.ViewData) : new ViewDataDictionary(viewData);
			}
			else {
				newViewData = viewData == null ? new ViewDataDictionary(model) : new ViewDataDictionary(viewData) { Model = model };
			}

			ViewEngineResult result = ViewEngines.Engines.FindPartialView(htmlHelper.ViewContext, partialViewName);

			if (result.View != null) {

				result.View.Render(
					new ViewContext(
						htmlHelper.ViewContext,
						htmlHelper.ViewContext.View,
						newViewData,
						htmlHelper.ViewContext.TempData,
						htmlHelper.ViewContext.HttpContext.Response.Output
					),
					htmlHelper.ViewContext.HttpContext.Response.Output
				);
			}

			//if (result.SearchedLocations.Count() > 0) {
			//    var locationsText = new StringBuilder();

			//    foreach (string location in result.SearchedLocations) {
			//        locationsText.AppendLine();
			//        locationsText.Append(location);
			//    }

			//    throw new InvalidOperationException(string.Format("The partial view '{0}' could not be found. The following locations were searched:{1}", partialViewName, locationsText));
			//}
		}

		#endregion

		#region RenderScriptTag

		public static void RenderScriptTag<TModel>(this HtmlHelper<TModel> htmlHelper, string path) where TModel : OxletModel {
			htmlHelper.RenderScriptTag(path, null);
		}

		public static void RenderScriptTag<TModel>(this HtmlHelper<TModel> htmlHelper, string path, string releasePath) where TModel : OxletModel {
			htmlHelper.RenderScriptTag(path, releasePath, true);
		}

		public static void RenderScriptTag<TModel>(this HtmlHelper<TModel> htmlHelper, string path, bool isRoot) where TModel : OxletModel {
			htmlHelper.RenderScriptTag(path, null, isRoot);
		}

		public static void RenderScriptTag<TModel>(this HtmlHelper<TModel> htmlHelper, string path, string releasePath, bool isRoot)
			where TModel : OxletModel {
			if (string.IsNullOrEmpty(path)) {
				throw new ArgumentNullException("path");
			}

			// if it is production mode we should load from release path
			var requiredPath = htmlHelper.ViewData.Model.Context.Configuration.Mode == SiteMode.Production && !string.IsNullOrEmpty(releasePath)
				? releasePath
				: path;

			if (!(requiredPath.StartsWith("http://") || requiredPath.StartsWith("https://"))) {
				var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

				requiredPath = !requiredPath.StartsWith("/")
					? urlHelper.ScriptPath(htmlHelper.ViewData.Model, requiredPath, isRoot)
					: urlHelper.AppPath(requiredPath);
			}

			htmlHelper.ViewContext.HttpContext.Response.Write(htmlHelper.ScriptBlock("text/javascript", requiredPath));
		}

		#endregion

		#region RenderScriptVariable

		public static void RenderScriptVariable(this HtmlHelper htmlHelper, string name, object value) {
			const string scriptVariableFormat = "window.{0} = {1};";
			string script;

			if (value != null) {
				var dcjs = new DataContractJsonSerializer(value.GetType());

				using (var ms = new MemoryStream()) {
					dcjs.WriteObject(ms, value);

					script = string.Format(scriptVariableFormat, name, Encoding.Default.GetString(ms.ToArray()));

					ms.Close();
				}
			}
			else {
				script = string.Format(scriptVariableFormat, name, "null");
			}

			htmlHelper.ViewContext.HttpContext.Response.Write(script);
		}

		#endregion

		#region ScriptBlock

		public static string ScriptBlock(this HtmlHelper htmlHelper, string type, string src) {
			return htmlHelper.ScriptBlock(type, src, null);
		}

		public static string ScriptBlock(this HtmlHelper htmlHelper, string type, string src, object htmlAttributes) {
			var tagBuilder = new TagBuilder("script");

			tagBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
			if (!string.IsNullOrEmpty(type)) {
				tagBuilder.MergeAttribute("type", type);
			}
			if (!string.IsNullOrEmpty(src)) {
				tagBuilder.MergeAttribute("src", src);
			}

			return tagBuilder.ToString(TagRenderMode.Normal);
		}

		#endregion

		#region SkinImage

		public static string SkinImage<TModel>(this HtmlHelper<TModel> htmlHelper, string src, string alt, object htmlAttributes, bool isRoot) where TModel : OxletModel {
			var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

			src = urlHelper.ImagePath(htmlHelper.ViewData.Model, src, isRoot);

			return htmlHelper.Image(src, alt, new RouteValueDictionary(htmlAttributes));
		}

		#endregion

		#region TextArea

		public static string TextArea<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, string> getValue,
		                                      int rows, int columns, string labelInnerHtml) where TModel : OxletModel {
			return TextArea(htmlHelper, name, getValue, rows, columns, labelInnerHtml, true);
		}

		public static string TextArea<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, string> getValue,
		                                      int rows, int columns, string labelInnerHtml, bool enabled)
			where TModel : OxletModel {
			return TextArea(htmlHelper, name, getValue, rows, columns, labelInnerHtml, enabled, null);
		}

		public static string TextArea<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, string> getValue,
		                                      int rows, int columns, string labelInnerHtml, object htmlAttributes)
			where TModel : OxletModel {
			return TextArea(htmlHelper, name, getValue, rows, columns, labelInnerHtml, true, htmlAttributes);
		}

		public static string TextArea<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, string> getValue,
		                                      int rows, int columns, string labelInnerHtml, bool enabled,
		                                      object htmlAttributes) where TModel : OxletModel {
			string value = htmlHelper.ViewContext.HttpContext.Request.Form[name] ?? getValue(htmlHelper.ViewData.Model) ?? "";

			var attributes = new RouteValueDictionary(htmlAttributes);

			if (!enabled)
				attributes.Add("disabled", "disabled");

			return FieldWithLabel(htmlHelper, name, () => htmlHelper.TextArea(name, value, rows, columns, attributes).ToHtmlString(),
			                      labelInnerHtml);
		}

		#endregion

		#region TextBox

		public static string TextBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, object> getValue,
		                                     string labelInnerHtml) where TModel : OxletModel {
			return TextBox(htmlHelper, name, getValue, labelInnerHtml, true);
		}

		public static string TextBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, object> getValue,
		                                     string labelInnerHtml, bool enabled) where TModel : OxletModel {
			return TextBox(htmlHelper, name, getValue, labelInnerHtml, enabled, null);
		}

		public static string TextBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, object> getValue,
		                                     string labelInnerHtml, object htmlAttributes) where TModel : OxletModel {
			return TextBox(htmlHelper, name, getValue, labelInnerHtml, true, htmlAttributes);
		}

		public static string TextBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<TModel, object> getValue,
		                                     string labelInnerHtml, bool enabled, object htmlAttributes)
			where TModel : OxletModel {
			object value = htmlHelper.ViewContext.HttpContext.Request.Form[name] ?? getValue(htmlHelper.ViewData.Model);

			var attributes = new RouteValueDictionary(htmlAttributes);

			if (!enabled)
				attributes.Add("disabled", "disabled");

			return FieldWithLabel(htmlHelper, name, () => htmlHelper.TextBox(name, value, attributes).ToHtmlString(), labelInnerHtml);
		}

		#endregion

		#region TextBoxWithValidation

		public static string TextBoxWithValidation<TModel>(this HtmlHelper<TModel> htmlHelper, string validationKey, string validationMessage,
														   string name, string labelInnerHtml) where TModel : OxletModel {
			return TextBoxWithValidation(htmlHelper, validationKey, validationMessage, name, labelInnerHtml, null);
		}

		public static string TextBoxWithValidation<TModel>(this HtmlHelper<TModel> htmlHelper, string validationKey, string validationMessage,
														   string name, string labelInnerHtml, object value) where TModel : OxletModel {
			return TextBoxWithValidation(htmlHelper, validationKey, validationMessage, name, labelInnerHtml, value, null);
		}

		public static string TextBoxWithValidation<TModel>(this HtmlHelper<TModel> htmlHelper, string validationKey, string validationMessage,
														   string name, string labelInnerHtml, object value, object htmlAttributes)
			where TModel : OxletModel {

			return htmlHelper.TextBox(name, x=>value, labelInnerHtml, htmlAttributes) +
				   htmlHelper.ValidationMessage(validationKey, validationMessage);
		}

		public static string TextBoxWithValidation<TModel>(this HtmlHelper<TModel> htmlHelper, string validationKey, string validationMessage,
														   string name, string labelInnerHtml, object value, object htmlAttributes, bool isEnabled)
			where TModel : OxletModel {
			var htmlAttributeDictionary = new RouteValueDictionary(htmlAttributes);

			if (!isEnabled) {
				htmlAttributeDictionary["disabled"] = "disabled";

				var inputItemBuilder = new StringBuilder();

				inputItemBuilder.Append(TextBoxWithValidation(htmlHelper, validationKey, validationMessage, string.Format("{0}_view", name), labelInnerHtml, value,
															  htmlAttributeDictionary));
				inputItemBuilder.Append(htmlHelper.Hidden(name, value));

				return inputItemBuilder.ToString();
			}

			return htmlHelper.TextBox(name, value, htmlAttributeDictionary).ToHtmlString();
		}

		#endregion

		#region UnorderedList

		public static string UnorderedList<T>(this HtmlHelper htmlHelper, IEnumerable<T> items,
		                                      Func<T, string> generateContent) {
			return UnorderedList(htmlHelper, items, (t, i) => generateContent(t));
		}

		public static string UnorderedList<T>(this HtmlHelper htmlHelper, IEnumerable<T> items,
		                                      Func<T, string> generateContent, string cssClass) {
			return UnorderedList(htmlHelper, items, (t, i) => generateContent(t), cssClass, null, null);
		}

		public static string UnorderedList<T>(this HtmlHelper htmlHelper, IEnumerable<T> items,
		                                      Func<T, int, string> generateContent) {
			return UnorderedList(htmlHelper, items, generateContent, null);
		}

		public static string UnorderedList<T>(this HtmlHelper htmlHelper, IEnumerable<T> items,
		                                      Func<T, int, string> generateContent, string cssClass) {
			return UnorderedList(htmlHelper, items, generateContent, cssClass, null, null);
		}

		public static string UnorderedList<T>(this HtmlHelper htmlHelper, IEnumerable<T> items,
		                                      Func<T, int, string> generateContent, string cssClass, string itemCssClass,
		                                      string alternatingItemCssClass) {
			if (items == null || items.Count() == 0) {
				return "";
			}

			var sb = new StringBuilder(100);
			int counter = 0;

			sb.Append("<ul");
			if (!string.IsNullOrEmpty(cssClass)) {
				sb.AppendFormat(" class=\"{0}\"", cssClass);
			}
			sb.Append(">");
			foreach (T item in items) {
				var sbClass = new StringBuilder(40);

				if (counter == 0) {
					sbClass.Append("first ");
				}
				if (item.Equals(items.Last())) {
					sbClass.Append("last ");
				}

				if (counter%2 == 0 && !string.IsNullOrEmpty(itemCssClass)) {
					sbClass.AppendFormat("{0} ", itemCssClass);
				}
				else if (counter%2 != 0 && !string.IsNullOrEmpty(alternatingItemCssClass)) {
					sbClass.AppendFormat("{0} ", alternatingItemCssClass);
				}

				sb.Append("<li");
				if (sbClass.Length > 0) {
					sb.AppendFormat(" class=\"{0}\"", sbClass.Remove(sbClass.Length - 1, 1));
				}
				sb.AppendFormat(">{0}</li>", generateContent(item, counter));

				counter++;
			}
			sb.Append("</ul>");

			return sb.ToString();
		}

		#endregion

		#region OxiletEditorFor

		public static string OxiLetEditor<TModel>(
			this HtmlHelper<TModel> htmlHelper,
			IEnumerable<ModelClientValidationRule> validationRules,
			object value, object htmlAttributes, bool isEnabled) {

			return htmlHelper.OxiLetEditor(validationRules, value, htmlAttributes, isEnabled, false);
		}

		public static string OxiLetEditor<TModel>(
			this HtmlHelper<TModel> htmlHelper,
			IEnumerable<ModelClientValidationRule> validationRules,
			object value, object htmlAttributes, bool isEnabled, bool multiLine)  {
		
			var tagBuilder = new TagBuilder(multiLine ? "textarea":"input");

			tagBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

			//var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
			if (!multiLine) {
				tagBuilder.MergeAttribute("value", value != null ? value.ToString() : "");
			}
			else {
				tagBuilder.InnerHtml = value != null ? value.ToString() : "";
			}

			if(!isEnabled) {
				tagBuilder.MergeAttribute("disabled", "disabled");
			}

			if (validationRules != null && validationRules.Count() > 0) {
				tagBuilder.MergeAttributes(
					new RouteValueDictionary(GetUnobtrusiveValidationAttributes(htmlHelper, validationRules))
				);
			}
			return tagBuilder.ToString(multiLine ? TagRenderMode.Normal: TagRenderMode.SelfClosing);
		}

		public static MvcHtmlString OxiletValidationMessage(this HtmlHelper htmlHelper, string modelName) {
			return htmlHelper.OxiletValidationMessage(modelName, null, null);
		}

		public static MvcHtmlString OxiletValidationMessage(this HtmlHelper htmlHelper, string modelName, string validationMessage, IDictionary<string, object> htmlAttributes) {
			if (modelName == null) {
				throw new ArgumentNullException("modelName");
			}

			return ValidationMessageHelper(
				htmlHelper,
				modelName,
				validationMessage,
				htmlAttributes
			);
		}


		private static MvcHtmlString ValidationMessageHelper(this HtmlHelper htmlHelper, string modelName, string validationMessage, IDictionary<string, object> htmlAttributes) {

			//if (!htmlHelper.ViewData.ModelState.ContainsKey(modelName)) {
			//    return null;
			//}

			ModelState modelState = htmlHelper.ViewData.ModelState[modelName];
			ModelErrorCollection modelErrors = (modelState == null) ? null : modelState.Errors;
			ModelError modelError = (((modelErrors == null) || (modelErrors.Count == 0))
				? null
				: modelErrors.FirstOrDefault(m => !String.IsNullOrEmpty(m.ErrorMessage)) ?? modelErrors[0]);

			//if (modelError == null) {
			//    return null;
			//}

			var builder = new TagBuilder("span");

			builder.MergeAttributes(htmlAttributes);
			builder.AddCssClass(HtmlHelper.ValidationMessageCssClassName);

			if (!string.IsNullOrEmpty(validationMessage)) {
				builder.SetInnerText(validationMessage);
			} else if (modelError != null && !string.IsNullOrEmpty(modelError.ErrorMessage)) {
				builder.SetInnerText(modelError.ErrorMessage);
			}

			bool replaceValidationMessageContents = String.IsNullOrEmpty(validationMessage);

			if (htmlHelper.ViewContext.UnobtrusiveJavaScriptEnabled) {
				builder.MergeAttribute("data-valmsg-for", modelName);
				builder.MergeAttribute("data-valmsg-replace", replaceValidationMessageContents.ToString().ToLowerInvariant());
			}
			
			return new MvcHtmlString(builder.ToString(TagRenderMode.Normal));
		}

		//public static string OxiLetItemEditorFor<TModel, TItem, TValue>(
		//    this HtmlHelper<TModel> htmlHelper,
		//    Expression<Func<TModel, TValue>> expression,
		//    string label,
		//    object htmlAttributes, bool multiLine) where TModel : OxletModelItem<TItem> {

		//    //var item = htmlHelper.ViewData.Model.Item;
		//    //var name = ExpressionHelper.GetExpressionText(expression);
		//    //var value = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

		//    var validationRules = htmlHelper.ViewData.Model.GetValidatorFor<TItem>().GetClientValidationRules(
		//        ExpressionHelper.GetExpressionText(expression)
		//    );
		//    if (validationRules != null && validationRules.Count() > 0) {
		//        var attributes = new RouteValueDictionary(GetUnobtrusiveValidationAttributes(htmlHelper, validationRules));
		//        tagBuilder.MergeAttributes(
		//            new RouteValueDictionary(GetUnobtrusiveValidationAttributes(htmlHelper, validationRules))
		//        );
		//    }
		//    var result = htmlHelper.LabelFor(expression, label).ToHtmlString()
		//        + (!multiLine 
		//            ? htmlHelper.TextBoxFor(expression, htmlAttributes)
		//            : htmlHelper.TextAreaFor(expression, htmlAttributes))
		//        + htmlHelper.ValidationMessageFor(expression);

		//    return result;
		//}

		//private static void MergeAttribute(string key, string value) {
		//    MergeAttribute(key, value, replaceExisting: false);
		//}

		//private static void MergeAttribute(string key, string value, bool replaceExisting) {
		//    if (replaceExisting || !Attributes.ContainsKey(key)) {
		//        Attributes[key] = value;
		//    }
		//}

		//public void MergeAttributes<TKey, TValue>(IDictionary<TKey, TValue> attributes) {
		//    MergeAttributes(attributes, replaceExisting: false);
		//}

		//public void MergeAttributes<TKey, TValue>(IDictionary<TKey, TValue> attributes, bool replaceExisting) {
		//    if (attributes != null) {
		//        foreach (var entry in attributes) {
		//            string key = Convert.ToString(entry.Key, CultureInfo.InvariantCulture);
		//            string value = Convert.ToString(entry.Value, CultureInfo.InvariantCulture);
		//            MergeAttribute(key, value, replaceExisting);
		//        }
		//    }
		//}

		//private static MvcHtmlString OxiLetItemValidationMessageFor<TModel, TItem, TValue>(
		//    this HtmlHelper<TModel> htmlHelper,
		//    Expression<Func<TModel, TValue>> expression) where TModel : OxletModelItem<TItem> {

		//    string modelName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));


		//    ModelState modelState = htmlHelper.ViewData.ModelState[modelName];
		//    ModelErrorCollection modelErrors = (modelState == null) ? null : modelState.Errors;
		//    ModelError modelError = (((modelErrors == null) || (modelErrors.Count == 0)) ? null : modelErrors.FirstOrDefault(m => !String.IsNullOrEmpty(m.ErrorMessage)) ?? modelErrors[0]);


		//    TagBuilder builder = new TagBuilder("span");
		//    builder.MergeAttributes(htmlAttributes);
		//    builder.AddCssClass((modelError != null) ? HtmlHelper.ValidationMessageCssClassName : HtmlHelper.ValidationMessageValidCssClassName);

		//    if (!String.IsNullOrEmpty(validationMessage)) {
		//        builder.SetInnerText(validationMessage);
		//    } else if (modelError != null) {
		//        builder.SetInnerText(GetUserErrorMessageOrDefault(htmlHelper.ViewContext.HttpContext, modelError, modelState));
		//    }

		//    if (formContext != null) {
		//        bool replaceValidationMessageContents = String.IsNullOrEmpty(validationMessage);

		//        if (htmlHelper.ViewContext.UnobtrusiveJavaScriptEnabled) {
		//            builder.MergeAttribute("data-valmsg-for", modelName);
		//            builder.MergeAttribute("data-valmsg-replace", replaceValidationMessageContents.ToString().ToLowerInvariant());
		//        } else {
		//            FieldValidationMetadata fieldMetadata = ApplyFieldValidationMetadata(htmlHelper, modelMetadata, modelName);
		//            // rules will already have been written to the metadata object
		//            fieldMetadata.ReplaceValidationMessageContents = replaceValidationMessageContents; // only replace contents if no explicit message was specified

		//            // client validation always requires an ID
		//            builder.GenerateId(modelName + "_validationMessage");
		//            fieldMetadata.ValidationMessageId = builder.Attributes["id"];
		//        }
		//    }

		//    return builder.ToMvcHtmlString(TagRenderMode.Normal);
		//}

		#endregion

		#region Private Methods

		private static string FieldWithLabel<TModel>(this HtmlHelper<TModel> htmlHelper, string name, Func<string> renderText,
		                                             string labelInnerHtml) where TModel : OxletModel {
			var output = new StringBuilder(200);

			if (!string.IsNullOrEmpty(labelInnerHtml)) {
				var builder = new TagBuilder("label");

				builder.Attributes["for"] = name;
				builder.InnerHtml = labelInnerHtml;

				output.Append(builder.ToString());
				output.Append(" ");
			}

			output.Append(renderText());

			return output.ToString();
		}

		#endregion

		/// <summary>
		/// Only render attributes if unobtrusive client-side validation is enabled, and then only if we've
		/// never rendered validation for a field with this name in this form. Also, if there's no form context,
		/// then we can't render the attributes (we'd have no <form> to attach them to).</form>
		/// </summary>
		/// <param name="htmlHelper"></param>
		/// <param name="clientRules"></param>
		/// <returns></returns>
		private static IDictionary<string, object> GetUnobtrusiveValidationAttributes<TModel>(HtmlHelper<TModel> htmlHelper, IEnumerable<ModelClientValidationRule> clientRules){
			var results = new Dictionary<string, object>();
			// The ordering of these 3 checks (and the early exits) is for performance reasons.
			if (!htmlHelper.ViewContext.UnobtrusiveJavaScriptEnabled) {
				return results;
			}

			//IEnumerable<ModelClientValidationRule> clientRules = model.GetClientValidationRules(
			//    metadata,
			//    htmlHelper.ViewContext.Controller.ControllerContext
			//);

			bool renderedRules = false;

			foreach (ModelClientValidationRule rule in clientRules) {
				renderedRules = true;
				string ruleName = "data-val-" + rule.ValidationType;

				results.Add(ruleName, HttpUtility.HtmlEncode(rule.ErrorMessage ?? String.Empty));
				ruleName += "-";

				foreach (var kvp in rule.ValidationParameters) {
					results.Add(ruleName + kvp.Key, kvp.Value ?? String.Empty);
				}
			}

			if (renderedRules) {
				results.Add("data-val", "true");
			}

			return results;
		}

		public static string SimplePager<T>(this HtmlHelper htmlHelper, IPagedList<T> pageOfAList, string routeName,
			object values, string previousText, string nextText, bool alwaysShowPreviousAndNext) {

			if (pageOfAList.TotalPageCount < 2) {
				return "";
			}

			var sb = new StringBuilder(75);
			ViewContext viewContext = htmlHelper.ViewContext;
			var rvd = new RouteValueDictionary();

			foreach (KeyValuePair<string, object> item in viewContext.RouteData.Values) {
				rvd.Add(item.Key, item.Value);
			}

			var urlHelper = new UrlHelper(viewContext.RequestContext);

			rvd.Remove("controller");
			rvd.Remove("action");
			rvd.Remove("id");
			rvd.Remove("dataFormat");

			if (values != null) {
				var rvd2 = new RouteValueDictionary(values);

				foreach (KeyValuePair<string, object> item in rvd2) {
					rvd[item.Key] = item.Value;
				}
			}

			sb.Append("<div class=\"pager\">");

			if (pageOfAList.PageIndex < pageOfAList.TotalPageCount - 1 || alwaysShowPreviousAndNext) {
				rvd["pageNumber"] = pageOfAList.PageIndex + 2;

				sb.AppendFormat("<a href=\"{1}{2}\" class=\"next\">{0}</a>", nextText,
								urlHelper.RouteUrl(routeName, rvd),
								viewContext.HttpContext.Request.QueryString.ToQueryString());
			}

			if (pageOfAList.PageIndex > 0 || alwaysShowPreviousAndNext) {
				rvd["pageNumber"] = pageOfAList.PageIndex;

				sb.AppendFormat("<a href=\"{1}{2}\" class=\"previous\">{0}</a>", previousText,
								urlHelper.RouteUrl(routeName, rvd),
								viewContext.HttpContext.Request.QueryString.ToQueryString());
			}

			sb.Append("</div>");

			return sb.ToString();
		}
	}
}
