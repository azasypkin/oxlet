﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace OxLet.Core.Web.Extensions {
	public static class ModelStateDictionaryExtensions {
		public static void AddModelErrors(this ModelStateDictionary modelStateDictionary,
			IEnumerable<ModelValidationResult> validationResults) {

			foreach (var validationResult in validationResults) {
				modelStateDictionary.AddModelError(validationResult.MemberName, validationResult.Message);

				//modelStateDictionary.SetModelValue(
				//    key,
				//    new ValueProviderResult(
				//        validationError.AttemptedValue,
				//        validationError.AttemptedValue as string,
				//        CultureInfo.CurrentCulture
				//    )
				//);
			}
		}
	}
}
