﻿using System.Web.Mvc;
using System.Web.Routing;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Helpers {
	public static class NavigationHelpers {
		public static string GetAreaName(RouteBase route) {
			var routeWithArea = route as IRouteWithArea;
			if (routeWithArea != null) {
				return routeWithArea.Area;
			}

			var castRoute = route as Route;
			if (castRoute != null && castRoute.DataTokens != null) {
				return castRoute.DataTokens["area"] as string;
			}

			return null;
		}

		public static string GetAreaName(RouteData routeData) {
			object area;
			if (routeData.DataTokens.TryGetValue("area", out area)) {
				return area as string;
			}

			return GetAreaName(routeData.Route);
		}

		public static string GetControllerName(RouteData routeData) {
			object controller;
			if (routeData.DataTokens.TryGetValue("controller", out controller)) {
				return controller as string;
			}

			var castRoute = routeData.Route as Route;
			if (castRoute != null && castRoute.DataTokens != null) {
				return castRoute.DataTokens["controller"] as string;
			}

			return null;
		}

		public static string GetActionName(RouteData routeData) {
			object controller;
			if (routeData.DataTokens.TryGetValue("action", out controller)) {
				return controller as string;
			}

			var castRoute = routeData.Route as Route;
			if (castRoute != null && castRoute.DataTokens != null) {
				return castRoute.DataTokens["action"] as string;
			}

			return null;
		}

		public static NavigationEntity GetNavigationEntity(RouteData routeData) {
			return new NavigationEntity {
				Area = GetAreaName(routeData),
				Controller = GetControllerName(routeData),
				Action = GetControllerName(routeData)
			};
		}
	}
}
