﻿using System.Web.Mvc;
using OxLet.Core.Extensions;
using OxLet.Core.Web.Extensions;

namespace OxLet.Core.Web.Controllers {
	public class UtilityController : Controller {

		[AcceptVerbs(HttpVerbs.Post)]
		public virtual ContentResult ComputeHash(string value) {
			return Content(value.ComputeHash(), "text/plain");
		}

		//public ViewResult NotFound(string url) {
		//    return null;
		//}

		[ActionName("RedirectToAction"), AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Redirect(string controllerName, string actionName) {
			return RedirectToActionPermanent(actionName, controllerName);
		}

		[ActionName("RedirectToRoot"), AcceptVerbs(HttpVerbs.Get)]
		public ActionResult RedirectToRoot(string rootName, object parameters) {
			return parameters != null 
				? Redirect(Url.RouteOxiLetUrl(rootName, parameters)) 
				: Redirect(Url.RouteOxiLetUrl(rootName));
		}


	}
}
