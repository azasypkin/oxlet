﻿using System;
using System.Web.Mvc;
using OxLet.Core.Models.SEO;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Controllers {
	public class SEOController : Controller {

		public virtual ViewResult OpenSearch() {
			return View(new OxletModel());
		}

		public virtual ViewResult OpenSearchOSDX() {
			throw new Exception("Oleg see below!");
			//return View(new OxletModel());
		}


		public virtual OxletModel SiteMap() {
			return new OxletModelItem<SiteMap>{Item = new SiteMap()};
		}

		public virtual ContentResult RobotsTxt() {
			return Content(string.Format("SiteMap: {0}", Url.AbsolutePath(Url.SiteMapIndex())), "text/plain");
		}

		//public virtual OxletModelList<DateTime> SiteMapIndex() {
		//    IList<DateTime> postDateGroups = _postService.GetPostDateGroups();

		//    return new OxletModelList<DateTime> {List = postDateGroups};
		//}

	}
}
