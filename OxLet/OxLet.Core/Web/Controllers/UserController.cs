﻿using System;
using System.Web.Mvc;
using OxLet.Core.Models;
using OxLet.Core.Secuity;
using OxLet.Core.Secuity.Interfaces;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Controllers {
	public class UserController : Controller {
		private readonly IFormsAuthentication _formsAuth;
		private readonly IUserService _userService;

		public UserController(IFormsAuthentication formsAuth, IUserService userService) {
			_formsAuth = formsAuth;
			_userService = userService;
		}

		public virtual OxletModel SignIn() {
			return new OxletModel {Container = new SignInPageContainer()};
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public object SignIn(string username, string password, bool rememberMe, string returnUrl) {
			if (string.IsNullOrEmpty(username)) {
				ModelState.AddModelError("username", "You must specify a username.");
			}

			if (string.IsNullOrEmpty(password)) {
				ModelState.AddModelError("password", "You must specify a password.");
			}

			if (ViewData.ModelState.IsValid) {
				if (_userService.ValidateUser(username, password)) {
					_formsAuth.SetAuthCookie(username, rememberMe);

					if (!string.IsNullOrEmpty(returnUrl) && returnUrl.StartsWith("/")) {
						return Redirect(returnUrl);
					}

					return Redirect(Url.AppPath(Url.Home()));
				}

				ModelState.AddModelError("_FORM", "The username or password provided is incorrect.");
			}

			return SignIn();
		}

		public virtual ActionResult SignOut(string returnUrl) {
			_formsAuth.SignOut();
			if (!string.IsNullOrEmpty(returnUrl) && returnUrl.StartsWith("/")) {
				return Redirect(returnUrl);
			}
			return Redirect(Url.AppPath("/"));
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public virtual OxletModelItem<User> ChangePassword(User currentUser) {
			//return new OxletModelItem<User>();
			throw new NotImplementedException();
		}

		[ActionName("ChangePassword"), AcceptVerbs(HttpVerbs.Post)]
		public virtual object ChangePasswordSave(User currentUser, FormCollection form) {
			//string password = form["userPassword"];
			//string confirmPassword = form["userPasswordConfirm"];
			//ValidationStateDictionary validationState;

			//if (string.IsNullOrEmpty(password)) {
			//    ModelState.AddModelError("userPassword", "New Password is not set.");
			//}
			//if (string.IsNullOrEmpty(confirmPassword)) {
			//    ModelState.AddModelError("userPasswordConfirm", "New Password (Confirm) is not set.");
			//}

			//if (ModelState.IsValid) {
			//    if (password != confirmPassword) {
			//        ModelState.AddModelError("userPasswordConfirm", "New Password (Confirm) does not match New Password");
			//    }
			//}

			//if (ModelState.IsValid) {
			////	currentUser.Password = password;

			//    _userService.EditUser(currentUser, out validationState);

			//    if (!validationState.IsValid) {
			//        ModelState.AddModelErrors(validationState);
			//    }
			//}

			//if (!ModelState.IsValid) {
			//    ModelState.SetModelValue("userPassword", new ValueProviderResult(password, password, CultureInfo.CurrentCulture));
			//    ModelState.SetModelValue("userPasswordConfirm",
			//                             new ValueProviderResult(confirmPassword, confirmPassword, CultureInfo.CurrentCulture));
			//    return ChangePassword(currentUser);
			//}

			//return Redirect(Url.AppPath(Url.ManageUsers()));
			throw new NotImplementedException();
		}
	}
}
