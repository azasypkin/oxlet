﻿using System.IO;
using System.Web.Mvc;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Web.Controllers {
	public class ResourceForwarderController : Controller {
		private readonly IFileResourceService _fileResourceService;

		public ResourceForwarderController(IFileResourceService fileResourceService) {
			_fileResourceService = fileResourceService;
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public virtual ActionResult Forward(string type, string skin, string path) {

			// check if resource is exists and returns file
			string filePath = Request.MapPath(ControllerContext.HttpContext.Request.FilePath);

			if (System.IO.File.Exists(filePath)) {
				return new FileStreamResult(new FileStream(filePath, FileMode.Open), MimeTypeByExtension(path.Substring(path.LastIndexOf('.') + 1)));
			}
			var parentPath = string.Format(
				"{0}Skins/{1}/{2}/{3}",
				ControllerContext.HttpContext.Request.ApplicationPath,
				skin,
				type,
				path
			);
			return RedirectPermanent(parentPath);
			//return new FileStreamResult(new FileStream(Request.MapPath(parentPath), FileMode.Open), MimeTypeByExtension(path.Substring(path.LastIndexOf('.') + 1)));
		}

		private string MimeType(string filename) {
			string mime = "application/octetstream";
			string ext = System.IO.Path.GetExtension(filename).ToLower();
			Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
			if (rk != null && rk.GetValue("Content Type") != null){
				mime = rk.GetValue("Content Type").ToString();
			}
			return mime;
		}

		private string MimeTypeByExtension(string extension) {
			string mime = "application/octetstream";
			Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(extension);
			if (rk != null && rk.GetValue("Content Type") != null) {
				mime = rk.GetValue("Content Type").ToString();
			}
			return mime;
		}  
	}
}
