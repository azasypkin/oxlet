﻿using System;
using System.Threading;
using System.Web.Mvc;
using OxLet.Core.Infrastructure;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Localization.Models.Input;

namespace OxLet.Core.Web.Controllers {
	public class LocalizationController : Controller {
		private readonly ILocalizationService _localizationService;
		private readonly SiteContext _context;

		public LocalizationController(ILocalizationService localizationService, SiteContext context) {
			_localizationService = localizationService;
			_context = context;
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public virtual ActionResult ChangeLanguage(SiteContext siteContext, LanguageInput languageInput, string returnUrl) {

			bool isDefualtRequired = languageInput.Culture.Name.StartsWith(
				_context.Site.DefaultLanguage.Name,
				StringComparison.InvariantCultureIgnoreCase
			);

			if(isDefualtRequired) {
				if (returnUrl.IndexOf(Thread.CurrentThread.CurrentCulture.Name, StringComparison.InvariantCultureIgnoreCase) >= 0) {
					returnUrl = returnUrl.Remove(1, returnUrl.Length > 6 ? 6 : 5);
				}
			} else {
				returnUrl = returnUrl.IndexOf(Thread.CurrentThread.CurrentCulture.Name, StringComparison.InvariantCultureIgnoreCase) >= 0
					? returnUrl.Replace(Thread.CurrentThread.CurrentCulture.Name, languageInput.Culture.Name) 
					: string.Format("/{0}{1}", languageInput.Culture.Name, returnUrl);
			}

			// check culture
			if(languageInput.Culture.Name != _localizationService.GetCulture().Name) {
				_localizationService.SetCulture(languageInput.Culture);
			}

			return new RedirectResult(returnUrl);
		}
	}
}
