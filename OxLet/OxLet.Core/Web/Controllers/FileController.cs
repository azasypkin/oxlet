﻿using System;
using System.IO;
using System.Web.Mvc;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Web.Controllers {
	public class FileController : Controller {
		private readonly IFileResourceService _fileResourceService;

		public FileController(IFileResourceService fileResourceService) {
			_fileResourceService = fileResourceService;
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public virtual ActionResult Load(SiteContext siteContext, int fileId) {
			var result = _fileResourceService.GetFileResource(fileId);
			if(result != null) {
				return new FileStreamResult(new FileStream(result.GetPhysicalPath(siteContext), FileMode.Open), result.MimeType);
			}
			return new HttpNotFoundResult();
		}
	}
}
