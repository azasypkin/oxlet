﻿using System;
using System.Web.Mvc;
using OxLet.Core.Extensions;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;
using OxLet.Core.Models.Input;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Web.Extensions;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Controllers {
	public class ContentItemController : Controller {
		private readonly IContentItemService _contentItemService;
		private readonly SiteContext _context;
		private readonly ILanguageService _languageService;

		public ContentItemController(
			IContentItemService contentItemService,
			SiteContext siteContext, 
			IRegularExpressionService expressionService,
			ILanguageService languageService
			) {

			_contentItemService = contentItemService;
			_context = siteContext;
			_languageService = languageService;
			ValidateRequest = false;
		}

		public virtual OxletModelList<ContentItem> List(int? pageNumber, int pageSize, ContentItemPath path) {
			int pageIndex = pageNumber.HasValue ? pageNumber.Value - 1 : 0;
			return GetContentItemList(
				new SiteContainer(0, "ContentItems", null),
				() => _contentItemService.GetItems(path, new PagingInfo(pageIndex, pageSize))
			);
		}

		public virtual OxletModelItem<ContentItem> Item(ContentItemPath contentItemPath) {

			if (contentItemPath == null) {
				return null;
			}

			var contentItem = _contentItemService.GetItem(contentItemPath);

			if(contentItem == null) {
				return null;
			}

			var model =  new OxletModelItem<ContentItem> {
				Container = contentItem.Parent != null ? _contentItemService.GetItem(contentItem.Parent.Id) : null,
				Item = _contentItemService.GetItem(contentItem.Id)
			};

		

			return model;
		}

		[ActionName("ItemAdd"), AcceptVerbs(HttpVerbs.Get)]
		public virtual OxletModelItem<ContentItemInput> Add(ContentItemPath parentPath) {
			var parent = parentPath != null ? _contentItemService.GetItem(parentPath) : null;
			var item = new ContentItemInput();
			if(parent != null) {
				item.Parent = parent.Id;
			}
			var model = new OxletModelItem<ContentItemInput> {
				Container = parent,
				Item = item
			};
			model.AddModelItem(new[] { _languageService.GetLanguage("en-us"), _languageService.GetLanguage("ru-ru") });

			return model;
		}

		[ActionName("ItemAdd"), AcceptVerbs(HttpVerbs.Post)]
		public virtual object SaveAdd(ContentItemInput contentItemInput) {

			var result = _contentItemService.Add(contentItemInput, EntityState.Normal);

			//todo: (nheskew) need to do more than just return another action method because it's likely different actions will need different filters applied to it
			if (!result.IsValid) {
				ModelState.AddModelErrors(result.ValidationResults);
				var model = new OxletModelItem<ContentItemInput> {
					Container = contentItemInput.Parent != null ? _contentItemService.GetItem(contentItemInput.Parent.Value) : null,
					Item = contentItemInput
				};
				model.AddModelItem(new[] { _languageService.GetLanguage("en-us"), _languageService.GetLanguage("ru-ru") });
				return model;
			}

			var itemPath = result.Item.GetPath();

			return result.Item.IsFolder ? Redirect(Url.ContentItemFolder(itemPath)) : Redirect(Url.ContentItem(itemPath));
		}

		//[ActionName("ItemEdit"), AcceptVerbs(HttpVerbs.Get)]
		//public virtual OxletModelItem<ContentItem> Edit(Models.Blog blog, ContentItem contentItem) {

		//    if (contentItem == null || blog == null) {
		//        return null;
		//    }

		//    return new OxletModelItem<ContentItem> {
		//        Container = blog,
		//        Item = contentItem
		//    };
		//}

		//[ValidateInput(false)]
		//[ActionName("ItemEdit"), AcceptVerbs(HttpVerbs.ContentItem)]
		//public virtual object SaveEdit(Models.Blog blog, ContentItem contentItem, ContentItemInput contentItemInput) {

		//    if (contentItem == null || blog == null) {
		//        return null;
		//    }

		//    var result = _contentItemService.EditContentItem(blog, contentItem, contentItemInput, EntityState.Normal);

		//    //todo: (nheskew) need to do more than just return another action method because it's likely different actions will need different filters applied to it
		//    if (!result.IsValid) {
		//        ModelState.AddModelErrors(result.ValidationState);
		//        return Edit(blog, result.Item);
		//    }

		//    return Redirect(Url.ContentItem(contentItem));
		//}

		//[AcceptVerbs(HttpVerbs.ContentItem)]
		//public virtual ActionResult Remove(Models.Blog blog, ContentItem contentItemBaseInput, string returnUri) {

		//    ContentItem contentItem = _contentItemService.GetContentItem(blog, contentItemBaseInput.Slug);

		//    _contentItemService.Remove(contentItem);

		//    return Redirect(returnUri);
		//}

		private static OxletModelList<ContentItem> GetContentItemList(INamedEntity container, Func<IPagedList<ContentItem>> serviceCall) {
			return new OxletModelList<ContentItem> {
				Container = container,
				List = serviceCall()
			};
		}
	}
}
