﻿using System.Web.Mvc;
using OxLet.Core.Models;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Controllers {
	public class ErrorController : Controller {
		public OxletModel NotFound() {
			return new OxletModel {Container = new NotFoundPageContainer()};
		}
	}
}
