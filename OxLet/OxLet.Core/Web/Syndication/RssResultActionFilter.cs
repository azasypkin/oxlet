﻿namespace OxLet.Core.Web.Syndication {
	public class RssResultActionFilter : FeedResultActionFilter {
		public RssResultActionFilter() : base(FeedType.Rss) {
		}
	}
}
