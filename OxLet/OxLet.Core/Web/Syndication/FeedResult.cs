﻿using System.Web.Mvc;

namespace OxLet.Core.Web.Syndication {
	public class FeedResult : ViewResult {
		public bool IsClientCached { get; set; }
		private readonly FeedType _feedType;
		public FeedResult(FeedType feedType, string viewName, bool isClientCached) {
			_feedType = feedType;
			ViewName = viewName;
			IsClientCached = isClientCached;
		}

		public override void ExecuteResult(ControllerContext context) {
			TempData = context.Controller.TempData;
			ViewData = context.Controller.ViewData;

			base.ExecuteResult(context);

			if (!IsClientCached) {
				context.HttpContext.Response.ContentType = _feedType == FeedType.Atom 
					? "application/atom+xml"
					: "application/rss+xml";
			}
			else {
				context.HttpContext.Response.StatusCode = 304;
				context.HttpContext.Response.SuppressContent = true;
			}
		}
	}
}