﻿using System;

namespace OxLet.Core.Web.Syndication {
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
	public sealed class FeedViewSettingsAttribute : Attribute {
		public FeedViewSettingsAttribute(string viewName, FeedType type) {
			ViewName = viewName;
			Type = type;
		}

		public string ViewName { get; private set; }
		public FeedType Type { get; private set; }
	}
}
