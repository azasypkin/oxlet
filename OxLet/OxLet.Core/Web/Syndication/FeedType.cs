﻿namespace OxLet.Core.Web.Syndication {
	public enum FeedType : byte {
		Rss,
		Atom
	}
}
