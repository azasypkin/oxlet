﻿namespace OxLet.Core.Web.Syndication {
	public class AtomResultActionFilter : FeedResultActionFilter {
		public AtomResultActionFilter() : base(FeedType.Atom) {
		}
	}
}
