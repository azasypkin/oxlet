﻿using System.Linq;
using System.Web.Mvc;
using OxLet.Core.Web.ViewModels;

namespace OxLet.Core.Web.Syndication {
	public abstract class FeedResultActionFilter : IActionFilter {
		private readonly FeedType _feedType;

		protected FeedResultActionFilter(FeedType feedType) {
			_feedType = feedType;
		}

		#region IActionFilter Members

		public void OnActionExecuted(ActionExecutedContext filterContext) {
			object model = filterContext.Controller.ViewData.Model;

			if (model.GetType().GetGenericTypeDefinition() == typeof(OxletModelList<>)) {
				object list = model.GetType().GetProperty("List").GetValue(model, null);

				var count = (int)list.GetType().GetProperty("Count").GetValue(list, null);

				// check feed attributes
				var feedAttribute = filterContext.ActionDescriptor
					.GetCustomAttributes(typeof (FeedViewSettingsAttribute), true)
					.Cast<FeedViewSettingsAttribute>()
					.SingleOrDefault(x => x.Type == _feedType);

					filterContext.Result = new FeedResult(
						_feedType,
						feedAttribute != null ? feedAttribute.ViewName : _feedType.ToString(),
						count == 0
					);
			}
		}

		public void OnActionExecuting(ActionExecutingContext filterContext) {
		}

		#endregion
	}
}
