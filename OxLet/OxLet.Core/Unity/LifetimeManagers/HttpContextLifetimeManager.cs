﻿using System;
using Microsoft.Practices.Unity;

namespace OxLet.Core.Unity.LifetimeManagers {
	public class FactoryLifetimeManager : LifetimeManager, IDisposable {
		private readonly Func<object> _getFunc;
		private readonly Action _removeFunc;

		public FactoryLifetimeManager(Func<object> getFunc):this(getFunc,null) {
		}

		public FactoryLifetimeManager(Func<object > getFunc, Action removeFunc) {
			_getFunc = getFunc;
			_removeFunc = removeFunc;
		}

		public override object GetValue() {
			return _getFunc();
		}

		public override void RemoveValue() {
			if(_removeFunc != null) {
				_removeFunc();
			}
		}

		public void Dispose() {
			RemoveValue();
		}
		
		public override void SetValue(object newValue) {
		}

		//public override object GetValue() {
		//    return HttpContext.Current.Items[typeof (T).AssemblyQualifiedName];
		//}

		//public override void RemoveValue() {
		//    HttpContext.Current.Items.Remove(typeof (T).AssemblyQualifiedName);
		//}

		//public override void SetValue(object newValue) {
		//    HttpContext.Current.Items[typeof (T).AssemblyQualifiedName]
		//        = newValue;
		//}

		
	}
}
