﻿using System.Linq;
using OxLet.Core.Models;

namespace OxLet.Core.Extensions {
	public static class IQueryableExtensions {
		public static PagedList<T> GetPage<T>(this IQueryable<T> query, PagingInfo pagingInfo) {
			return new PagedList<T>(query.Skip(pagingInfo.Index * pagingInfo.Size).Take(pagingInfo.Size), pagingInfo.Index, pagingInfo.Size, query.Count());
		}
	}
}
