﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Serialization;

namespace OxLet.Core.Extensions {
	public static class ObjectExtensions {
		public static string ToJson(this object o) {
			string serializedObject = string.Empty;

			if (o != null) {
				var jsonSerializer = new DataContractJsonSerializer(o.GetType());

				using (var ms = new MemoryStream()) {
					jsonSerializer.WriteObject(ms, o);
					ms.Position = 0;
					serializedObject = new StreamReader(ms).ReadToEnd();
					ms.Close();
				}
			}

			return serializedObject;
		}

		public static object FromJson(this Type type, string serializedObject) {
			object filledObject = null;

			if (!string.IsNullOrEmpty(serializedObject)) {
				var dcjs = new DataContractJsonSerializer(type);

				using (var ms = new MemoryStream()) {
					var writer = new StreamWriter(ms, Encoding.Default);
					writer.Write(serializedObject);
					writer.Flush();

					ms.Position = 0;

					try {
						filledObject = dcjs.ReadObject(ms);
					}
					catch (SerializationException) {
						filledObject = null;
					}
					ms.Close();
				}
			}

			return filledObject;
		}

		public static string ToXml(this object o) {
			string serializedObject = string.Empty;

			if (o != null) {
				var xmlSerializer = new XmlSerializer(o.GetType());

				using (var ms = new MemoryStream()) {
					xmlSerializer.Serialize(ms, o);
					ms.Position = 0;
					serializedObject = new StreamReader(ms).ReadToEnd();
					ms.Close();
				}
			}

			return serializedObject;
		}
	}
}
