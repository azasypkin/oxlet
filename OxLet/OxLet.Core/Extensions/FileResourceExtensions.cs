﻿using System.IO;
using OxLet.Core.Infrastructure;
using OxLet.Core.Models;

namespace OxLet.Core.Extensions {
	public static class FileResourceExtensions {
		/// <summary>
		/// Save file resource to disk and return path to it
		/// </summary>
		/// <param name="file">File resource to save</param>
		/// <param name="context">Site context</param>
		/// <returns>Path on the local disk where file was saved</returns>
		public static string SaveToDisk(this FileResource file, SiteContext context) {
			var filePath = string.Format(
				"{0}{1}\\{2}",
				context.HttpContext.Request.PhysicalApplicationPath,
				context.Configuration.FileCachePath,
				file.Id
			);

			//[TODO]:Rewrite for checking if file exists
			File.WriteAllBytes(filePath, file.Data);

			return filePath;
		}

		public static string GetPhysicalPath(this FileResource file, SiteContext context) {
			return string.Format(
				"{0}\\{1}\\{2}",
				context.HttpContext.Request.PhysicalApplicationPath,
				context.Configuration.FileCachePath,
				file.Id
			);
		}

	}
}
