﻿using System;

namespace OxLet.Core.Extensions {
	public static class DateTimeExtensions {
		public static string ToStringForEdit(this DateTime dateTime) {
			return string.Format("{0} {1}", dateTime.ToShortDateString(), dateTime.ToShortTimeString());
		}

		public static string ToStringForFeed(this DateTime dateTime) {
			return dateTime.ToString("R");
		}
	}
}