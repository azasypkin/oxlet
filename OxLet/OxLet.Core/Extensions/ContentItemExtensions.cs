﻿using OxLet.Core.Models;
using OxLet.Core.Models.Input;
using OxLet.Core.Secuity.Interfaces;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Extensions {
	public static class ContentItemExtensions {

		public static IPagedList<ContentItem> FillCreator(this IPagedList<ContentItem> contentItems, IUserService userService) {
			if (contentItems != null) {
				foreach (var contentItem in contentItems) {
					contentItem.SetCreator(userService.Get(contentItem.Creator.Id).GetDescription());
				}
			}
			return contentItems;
		}

		public static ContentItem FillCreator(this ContentItem contentItem, IUserService userService) {
			if (contentItem != null) {
				contentItem.SetCreator(userService.Get(contentItem.Creator.Id).GetDescription());
			}
			return contentItem;
		}

		public static IPagedList<ContentItem> FillResources(this IPagedList<ContentItem> contentItems, IFileResourceService fileResourceService) {
			if (contentItems != null) {
				foreach (var contentItem in contentItems) {
					if(contentItem.Thumbnail != null && contentItem.Thumbnail.File != null) {
						fileResourceService.GetFileResource(contentItem.Thumbnail.File.Id);
					}
					if (contentItem.Value != null && contentItem.Value.File != null) {
						fileResourceService.GetFileResource(contentItem.Value.File.Id);
					}
				}
			}
			return contentItems;
		}

		public static ContentItem FillResources(this ContentItem contentItem, IFileResourceService fileResourceService) {
			if (contentItem != null) {
				if (contentItem.Thumbnail != null && contentItem.Thumbnail.File != null) {
					fileResourceService.GetFileResource(contentItem.Thumbnail.File.Id);
				}
				if (contentItem.Value != null && contentItem.Value.File != null) {
					fileResourceService.GetFileResource(contentItem.Value.File.Id);
				}
			}
			return contentItem;
		}

		public static ContentItem Apply(this ContentItem contentItem, ContentItemInput input, EntityState state, UserDescription creator) {
			return new ContentItem(
				contentItem.Id,
				input.Name,
				input.DisplayName,
				input.Slug,
				contentItem.ParentSlug,
				contentItem.Parent,
				input.Value != null ? input.Value.ToContentItemResource(creator, state) : null,
				input.Thumbnail != null ? input.Thumbnail.ToContentItemResource(creator, state) : null,
				contentItem.Created,
				contentItem.Modified,
				input.Published,
				state,
				new Language(input.Language, null, null), 
				input.IsFolder,
				creator,
				null,
				null
			);
		}

		public static ContentItemPath GetPath(this ContentItem item) {
			return ContentItemPath.Parse(string.Format("{0}{1}/", item.ParentSlug, item.Slug));
		}
	}
}
