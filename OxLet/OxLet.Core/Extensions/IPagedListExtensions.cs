﻿using System;
using System.Linq;
using OxLet.Core.Models;

namespace OxLet.Core.Extensions {
	public static class IPagedListExtensions {
		public static IPagedList<T> Since<T>(this IPagedList<T> pageOfItems, Func<T, DateTime> getCompareDateTime, DateTime? sinceDate) {
			if (sinceDate.HasValue)
				return new PagedList<T>(
					pageOfItems.Where(i => getCompareDateTime(i) > sinceDate.Value),
					pageOfItems.PageIndex, 
					pageOfItems.PageSize, pageOfItems.TotalItemCount
				);

			return pageOfItems;
		}
	}
}
