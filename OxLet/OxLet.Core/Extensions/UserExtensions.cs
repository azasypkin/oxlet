﻿using OxLet.Core.Secuity;

namespace OxLet.Core.Extensions {
	public static class UserExtensions {
		public static bool HasAdminAccess(this User user) {
			return user != null && user.IsAuthenticated && user.IsInRole("Administrator");
		}
	}
}
