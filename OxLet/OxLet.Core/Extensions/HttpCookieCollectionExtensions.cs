﻿using System;
using System.Web;
using OxLet.Core.Secuity;

namespace OxLet.Core.Extensions {
	public static class HttpCookieCollectionExtensions {
		private const string AnonymousUserCookieName = "anonUser";

		public static void ClearAnonymousUser(this HttpCookieCollection cookies) {
			cookies.Add(new HttpCookie(AnonymousUserCookieName) { Expires = DateTime.Now.AddDays(-1) });
		}

		public static UserAnonymous GetAnonymousUser(this HttpCookieCollection cookies) {
			HttpCookie cookie = cookies[AnonymousUserCookieName];

			if (cookie != null) {
				try {
					var userCookieProxy = typeof(UserCookieProxy).FromJson(cookie.Value) as UserCookieProxy;

					if (userCookieProxy != null) {
						return userCookieProxy.ToUserAnonymous();
					}
						
				} catch { }
			}

			return null;
		}

		public static void SetAnonymousUser(this HttpCookieCollection cookies, UserAnonymous user) {
			var cookie = new HttpCookie(AnonymousUserCookieName, new UserCookieProxy(user).ToJson()) {
				Expires = DateTime.Now.AddDays(14)
			};

			cookies.Remove(AnonymousUserCookieName);

			cookies.Add(cookie);
		}
	}
}
