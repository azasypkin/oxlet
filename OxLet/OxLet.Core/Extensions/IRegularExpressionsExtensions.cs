﻿using System.Text.RegularExpressions;
using OxLet.Core.Services;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Extensions {
	public static class IRegularExpressionServiceExtensions {
		public static string Slugify(this IRegularExpressionService expressionService, string value) {
			string slug = "";

			if (!string.IsNullOrEmpty(value)) {
				Regex regex = expressionService.GetExpression("SlugReplace");

				slug = value.Trim();
				slug = slug.Replace(' ', '-');
				slug = slug.Replace("---", "-");
				slug = slug.Replace("--", "-");
				if (regex != null) {
					slug = regex.Replace(slug, "");
				}

				if (slug.Length*2 < value.Length) {
					return "";
				}

				if (slug.Length > 100) {
					slug = slug.Substring(0, 100);
				}
			}

			return slug;
		}

		public static bool IsUrl(this IRegularExpressionService expressionService, string value) {
			if (!(value.StartsWith("http://") || value.StartsWith("https://"))) {
				value = string.Format("http://{0}", value);
			}

			return expressionService.IsMatch("IsUrl", value);
		}
	}
}
