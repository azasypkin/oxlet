﻿using OxLet.Core.Models;
using OxLet.Core.Models.Input;

namespace OxLet.Core.Extensions {
	public static class SiteExtensions {

		public static Site Apply(this Site site, SiteInput input) {

			return new Site(
				site.Id,
				input.HostRedirects,
				input.Host,
				input.Name,
				input.DisplayName,
				input.Description,
				input.DefaultLanguage,
				input.TimeZoneOffset,
				input.PageTitleSeparator,
				input.FavIconUrl,
				input.ScriptsPath,
				input.CssPath,
				input.ImagesPath,
				input.IncludeOpenSearch,
				input.GravatarDefault,
				input.SkinDefault,
				input.RouteUrlPrefix
			);
		}
	}
}
