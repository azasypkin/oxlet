﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace OxLet.Core.Extensions {
	public static class ModelValidationResultExtensions {
		public static bool IsValid(this IEnumerable<ModelValidationResult> results) {
			return results == null || results.Count() == 0;
		}
	}
}
