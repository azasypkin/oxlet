﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Mail;

namespace OxLet.Core.Email {
	/// <summary>
	/// Interface for email sender contains only two methods
	/// </summary>
	/// <typeparam name="TEmailMessageInfo"></typeparam>
	public interface IEmailSender<TEmailMessageInfo> where TEmailMessageInfo : EmailMessageInfo {

		IDictionary<TEmailMessageInfo, Exception> SendEmail(TEmailMessageInfo[] emailMessageInfos);

		void SendEmailAsync(TEmailMessageInfo[] emailMessageInfos);

		event SendEmailCompletedEventHandler<TEmailMessageInfo> SendEmailCompleted;
	}

	#region Asynchronous staff

	/// <summary>
	/// Delegate for handling event of the send method
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void SendEmailCompletedEventHandler<TEmailMessageInfo>(object sender, SendEmailCompletedEventArgs<TEmailMessageInfo> e) where TEmailMessageInfo : EmailMessageInfo;

	/// <summary>
	/// User defined async state for SendEmailAsync method
	/// </summary>
	public class SendAsyncState<TEmailMessageInfo> where TEmailMessageInfo : EmailMessageInfo {
		#region Properties

		/// <summary>
		/// Message info for current <see cref="SmtpClient"/>
		/// </summary>
		public TEmailMessageInfo EmailMessageInfo { get; private set; }

		/// <summary>
		/// Message content for the current message
		/// </summary>
		public MailMessage EmailMessage { get; private set; }

		#endregion

		#region Methods

		public SendAsyncState(TEmailMessageInfo emailMessageInfo, MailMessage emailMessage) {
			EmailMessageInfo = emailMessageInfo;
			EmailMessage = emailMessage;
		}

		#endregion
	}

	/// <summary>
	/// <see cref="EventArgs"/> inheritor for SendAsync method of <see cref="EmailSenderBase{TEmailMessageInfo}"/> class
	/// </summary>
	public class SendEmailCompletedEventArgs<TEmailMessageInfo> : AsyncCompletedEventArgs where TEmailMessageInfo : EmailMessageInfo {
		#region Properties

		public SendEmailCompletedEventArgs(
			Exception error,
			bool canceled,
			object userState,
			IDictionary<TEmailMessageInfo, Exception> result)
			: base(error, canceled, userState) {
			Result = result;
		}

		/// <summary>
		/// Result of the send email method contains error for every send iteration
		/// </summary>
		public IDictionary<TEmailMessageInfo, Exception> Result { get; set; }

		#endregion
	}

	#endregion
}
