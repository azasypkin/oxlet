﻿namespace OxLet.Core.Email {
	public class EmailContact {
		public string Address { get; set; }
		public string DisplayName { get; set; }
	}
}
