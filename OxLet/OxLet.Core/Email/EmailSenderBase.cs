﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using OxLet.Core.Configuration;

namespace OxLet.Core.Email {

	/// <summary>
	/// Sends emails with attachments
	/// </summary>
	public class EmailSenderBase<TEmailMessageInfo>:IEmailSender<TEmailMessageInfo> where TEmailMessageInfo : EmailMessageInfo {
		#region Fields

		private const string EmailSmtpHostConfigName = "EmailSmtpHost";
		private const string EmailSmtpPortConfigName = "EmailSmtpPort";
		private const string EmailSmtpLoginNameConfigName = "EmailSmtpLoginName";
		private const string EmailSmtpPasswordConfigName = "EmailSmtpPassword";
		private const string EmailEnableSSLConfigName = "EnableSSL";

		/// <summary>
		/// Number of the recipients to whom necessary to send email
		/// Necessary for asynchronous execution
		/// </summary>
		private int _recipientCount;

		/// <summary>
		/// Result of the operation execution
		/// </summary>
		private readonly IDictionary<TEmailMessageInfo, Exception> _result = new Dictionary<TEmailMessageInfo, Exception>();

		private bool IsAlreadyRun{get;set;}

		private readonly OxLetSection _config;

		#endregion

		#region Events

		/// <summary>
		/// Event for SendAsync method
		/// </summary>
		public event SendEmailCompletedEventHandler<TEmailMessageInfo> SendEmailCompleted;

		#endregion

		#region Constructor

		public EmailSenderBase(OxLetSection config) {
			_config = config;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sends email with or without attachments
		/// </summary>
		/// <param name="emailMessageInfos">Information about message to send</param>
		public IDictionary<TEmailMessageInfo, Exception> SendEmail(TEmailMessageInfo[] emailMessageInfos) {
			using (SmtpClient smtpClient = GetSmtpClient()) {
				foreach (var emailMessageInfo in emailMessageInfos) {
					MailMessage mailMessage = null;
					try {
						mailMessage = GetMailMessage(emailMessageInfo);
						smtpClient.Send(mailMessage);
						// No exception
						_result[emailMessageInfo] = null;
					}
					catch (Exception exception) {
						Trace.TraceError(
							"An error has occurred while creating mail message to \"{0}\" with subject \"{1}\". \n Message:{2}",
								emailMessageInfo.Recipients != null && emailMessageInfo.Recipients.Length > 0
									? string.Join(", ", emailMessageInfo.Recipients.Select(x => x.Address))
									: "Unknown recipient",
								emailMessageInfo.Subject,
								exception.Message
						);
						_result[emailMessageInfo] = exception;
					}
					finally {
						// Disposing of the email message calls disposing of all views, body and attachments
						if (mailMessage != null) {
							mailMessage.Dispose();
						}
					}
				}
			}
			return _result;
		}

		/// <summary>
		/// Sends email with or without attachments
		/// </summary>
		/// <param name="emailMessageInfos">Information about message to send</param>
		public void SendEmailAsync(TEmailMessageInfo[] emailMessageInfos){
			if(IsAlreadyRun) {
				throw new InvalidOperationException("EmailSenderBase doesn't support multiple concurrent invocations.");
			}
			IsAlreadyRun = true;
			_recipientCount = emailMessageInfos.Length;
			// SmtpClient instance must be created for every SendAsync request because of limitation to one SendAsync for one SmtpClient simultaneously
			foreach (var emailMessageInfo in emailMessageInfos){
				SmtpClient smtpClient = GetSmtpClient();
				smtpClient.SendCompleted += SmtpClientSendCompleted;
				MailMessage mailMessage = null;
				try {
					mailMessage = GetMailMessage(emailMessageInfo);
					smtpClient.SendAsync(
						mailMessage,
						new SendAsyncState<TEmailMessageInfo>(emailMessageInfo, mailMessage)
					);
				} catch (Exception exception){
					Trace.TraceError(
							"An error has occurred while creating mail message to \"{0}\" with subject \"{1}\". \n Message:{3}",
								emailMessageInfo.Recipients != null && emailMessageInfo.Recipients.Length > 0
									? string.Join(", ", emailMessageInfo.Recipients.Select(x => x.Address))
									: "Unknown recipient",
								emailMessageInfo.Subject,
								exception.Message
					);
					_result[emailMessageInfo] = exception;
					// Disposing of the email message calls disposing of all views, body and attachments
					if(mailMessage != null) {
						mailMessage.Dispose();
					}
					smtpClient.Dispose();
				}
			}
		}

		/// <summary>
		/// Returns <see cref="SmtpClient"/> with SMTP setting from configuration
		/// </summary>
		/// <returns><see cref="SmtpClient"/> instance</returns>
		private SmtpClient GetSmtpClient(){
			var smtpClient = new SmtpClient(
				_config.GetValue<string>(EmailSmtpHostConfigName),
				_config.GetValue<int>(EmailSmtpPortConfigName)
			) {
				EnableSsl = _config.GetValue<bool>(EmailEnableSSLConfigName),
				Timeout = 10000,
				DeliveryMethod = SmtpDeliveryMethod.Network,
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(
					_config.GetValue<string>(EmailSmtpLoginNameConfigName),
					_config.GetValue<string>(EmailSmtpPasswordConfigName)
				)
			};
			return smtpClient;
		}

		/// <summary>
		/// Formats message body
		/// </summary>
		/// <param name="emailMessageInfo">message item</param>
		/// <returns></returns>
		protected virtual string FormatMessageBodyText(TEmailMessageInfo emailMessageInfo) {
			return emailMessageInfo.BodyText;
		}

		/// <summary>
		/// Returns subject for email
		/// </summary>
		/// <param name="emailMessageInfo">Email to send</param>
		/// <returns>string representation of the email subject</returns>
		protected virtual string FormatMessageSubjectText(TEmailMessageInfo emailMessageInfo) {
			return emailMessageInfo.Subject;
		}

		/// <summary>
		/// Forms email attachment
		/// </summary>
		/// <param name="emailMessageAttachmentInfo">Email attachment info</param>
		/// <returns>Complete attachment</returns>
		protected virtual Attachment GetMessageAttachment(EmailMessageAttachmentInfo emailMessageAttachmentInfo) {
			try {
				var attachment = new Attachment(emailMessageAttachmentInfo.Path);
				attachment.ContentDisposition.Inline = emailMessageAttachmentInfo.IsInline;
				attachment.ContentDisposition.DispositionType = emailMessageAttachmentInfo.IsInline
					? DispositionTypeNames.Inline
					: DispositionTypeNames.Attachment;
				// generate the contentID
				attachment.ContentId = emailMessageAttachmentInfo.ContentId;
				attachment.ContentType.MediaType = emailMessageAttachmentInfo.MediaType;
				attachment.Name = emailMessageAttachmentInfo.Name;
				return attachment;

			}
			catch (Exception exception) {
				Trace.TraceError(
					"An error has occurred while creating an {0} with path \"{1}\" and name \"{2}\". \n Message:{3}",
					emailMessageAttachmentInfo.IsInline ? "inline attachment" : "attachment",
					emailMessageAttachmentInfo.Path,
					emailMessageAttachmentInfo.Name,
					exception.Message
				);
				throw;
			}

		}

		/// <summary>
		/// Prepares e-mail message before sending
		/// </summary>
		/// <param name="emailMessageInfo">Information about item to send</param>
		/// <returns></returns>
		private MailMessage GetMailMessage(TEmailMessageInfo emailMessageInfo) {

			var encoding = emailMessageInfo.Encoding ?? System.Text.Encoding.UTF8;

			var mailMessage = new MailMessage {
				From = new MailAddress(emailMessageInfo.Sender.Address, emailMessageInfo.Sender.DisplayName, encoding),
				Subject = FormatMessageSubjectText(emailMessageInfo),
			};

			if (emailMessageInfo.ReplyTo != null && emailMessageInfo.ReplyTo.Length > 0) {
				Array.ForEach(
					emailMessageInfo.ReplyTo,
					x => mailMessage.ReplyToList.Add(new MailAddress(x.Address, x.DisplayName, encoding))
				);
			}

			if(emailMessageInfo.Recipients != null && emailMessageInfo.Recipients.Length > 0) {
				Array.ForEach(
					emailMessageInfo.Recipients,
					x =>mailMessage.To.Add(new MailAddress(x.Address, x.DisplayName, encoding))
				);
			}

			if (emailMessageInfo.CC != null && emailMessageInfo.CC.Length > 0) {
				Array.ForEach(
					emailMessageInfo.CC,
					x => mailMessage.CC.Add(new MailAddress(x.Address, x.DisplayName, encoding))
				);
			}

			if (emailMessageInfo.Bcc != null && emailMessageInfo.Bcc.Length > 0) {
				Array.ForEach(
					emailMessageInfo.Bcc,
					x => mailMessage.Bcc.Add(new MailAddress(x.Address, x.DisplayName, encoding))
				);
			}

			// Format body 
			string body = FormatMessageBodyText(emailMessageInfo);

			mailMessage.AlternateViews.Add(
				AlternateView.CreateAlternateViewFromString(
					body,
					new ContentType(string.Format("text/html; charset={0}", encoding.BodyName))
				)
			);

			// Add attachments
			if (emailMessageInfo.Attachments != null && emailMessageInfo.Attachments.Length > 0) {
				emailMessageInfo.Attachments.Select(GetMessageAttachment).ToList().ForEach(mailMessage.Attachments.Add);
			}

			mailMessage.BodyEncoding = mailMessage.SubjectEncoding = encoding;

			return mailMessage;
		}

		#endregion

		#region Event handlers

		private void SmtpClientSendCompleted(object sender, AsyncCompletedEventArgs e){
			var smtpClient = (SmtpClient) sender;
			var userAsyncState = (SendAsyncState<TEmailMessageInfo>) e.UserState;
			smtpClient.SendCompleted -= SmtpClientSendCompleted;
			_result.Add(userAsyncState.EmailMessageInfo, e.Error);
			
			// Trace error
			if(e.Error != null) {
				Trace.TraceError(
					"An error has occurred while creating mail message to \"{0}\" with subject \"{1}\". \n Message:{2}",
					userAsyncState.EmailMessageInfo.Recipients != null && userAsyncState.EmailMessageInfo.Recipients.Length > 0
						? string.Join(", ", userAsyncState.EmailMessageInfo.Recipients.Select(x => x.Address))
						: "Unknown recipient",
					userAsyncState.EmailMessageInfo.Subject,
					e.Error.Message
				);
			}

			// Disposing of the email message calls disposing of all views, body and attachments
			if(userAsyncState.EmailMessage != null) {
				userAsyncState.EmailMessage.Dispose();
			}

			// disposing email client
			smtpClient.Dispose();

			// If all emails were sent let's finish operation
			if (--_recipientCount == 0){
				OnSendCompleted(new SendEmailCompletedEventArgs<TEmailMessageInfo>(null, false, null, _result));
				IsAlreadyRun = false;
			}
		}

		/// <summary>
		/// Fires <see cref="SendEmailCompleted"/> event with supplied arguments.
		/// </summary>
		/// <param name="args">Event arguments.</param>
		protected virtual void OnSendCompleted(SendEmailCompletedEventArgs<TEmailMessageInfo> args){
			var handler = SendEmailCompleted;
			if (handler != null){
				handler(this, args);
			}
		}

		#endregion
	}
	
}
