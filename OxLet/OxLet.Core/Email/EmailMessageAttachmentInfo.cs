﻿namespace OxLet.Core.Email {
	/// <summary>
	/// Represents info about attachment
	/// </summary>
	public class EmailMessageAttachmentInfo {
		/// <summary>
		/// Path to the attachment
		/// </summary>
		public string Path { get; set; }

		/// <summary>
		/// True if attachment should be inline in the message
		/// </summary>
		public bool IsInline { get; set; }

		/// <summary>
		/// Media type of the email attachment
		/// </summary>
		public string MediaType { get; set; }

		/// <summary>
		/// Unique identifier of the attachment, by this identifier it's possible to refer to inline attachment in message body
		/// </summary>
		public string ContentId { get; set; }

		/// <summary>
		/// Name of the attachment file
		/// </summary>
		public string Name { get; set; }

	}
}
