﻿using System.Text;

namespace OxLet.Core.Email {
	/// <summary>
	/// Encapsulate information about sender and receiver of the message
	/// </summary>
	public class EmailMessageInfo {
		#region Properties

		/// <summary>
		/// Subject of the message to send
		/// </summary>
		public string Subject { get; set; }

		/// <summary>
		/// Text of the mail message body
		/// </summary>
		public string BodyText { get; set; }

		/// <summary>
		/// Sender contact details will be used for FROM field filling.
		/// </summary>
		public EmailContact Sender { get; set; }

		/// <summary>
		/// Recipients contact details which will be used for TO field filling.
		/// </summary>
		public EmailContact[] Recipients { get; set; }

		/// <summary>
		/// Contact details which will be used for ReplyTo field filling.
		/// </summary>
		public EmailContact[] ReplyTo { get; set; }

		/// <summary>
		/// Contact details which will be used for CC field filling.
		/// </summary>
		public EmailContact[] CC { get; set; }

		/// <summary>
		/// Contact details which will be used for Bcc field filling.
		/// </summary>
		public EmailContact[] Bcc { get; set; }

		/// <summary>
		/// Encoding of the message wich will be used for Body, Subject and all adress display names
		/// By default UTF-8 is used.
		/// </summary>
		public Encoding Encoding { get; set; }

		/// <summary>
		/// Attachments to email
		/// </summary>
		public EmailMessageAttachmentInfo[] Attachments { get; set; }

		#endregion
	}
}
