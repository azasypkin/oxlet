﻿using System.Web;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Core.Models;

namespace OxLet.Core.Routing {
	public class SkinConstraint : IRouteConstraint {
		private readonly IUnityContainer _container;

		public SkinConstraint(IUnityContainer container) {
			_container = container;
		}

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			return _container.Resolve<Site>().SkinDefault == (string)values[parameterName];
		}
	}
}

