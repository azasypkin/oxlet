﻿using System.Web.Routing;

namespace OxLet.Core.Routing {
	public class RouteDirectionConstraint : IRouteConstraint {
		private readonly RouteDirection _routeDirection;

		public RouteDirectionConstraint(RouteDirection routeDirection) {
			_routeDirection = routeDirection;
		}

		public bool Match(System.Web.HttpContextBase httpContext, Route route, string parameterName,
		                  RouteValueDictionary values, RouteDirection routeDirection) {
			return _routeDirection == routeDirection;
		}
	}
}
