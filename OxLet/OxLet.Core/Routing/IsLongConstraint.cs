﻿using System.Web;
using System.Web.Routing;

namespace OxLet.Core.Routing {
	public class IsLong : IRouteConstraint {
		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
		                  RouteDirection routeDirection) {
			long tryLong;
			return long.TryParse(values[parameterName].ToString(), out tryLong);
		}
	}
}