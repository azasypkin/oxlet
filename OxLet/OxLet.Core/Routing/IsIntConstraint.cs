﻿using System.Web;
using System.Web.Routing;

namespace OxLet.Core.Routing {
	public class IsIntegerConstraint : IRouteConstraint {
		public IsIntegerConstraint () {
		}

		public IsIntegerConstraint (int minValue, int maxValue)
			: this() {
			MinValue = minValue;
			MaxValue = maxValue;
		}

		public IsIntegerConstraint (bool isOptional)
			: this() {
			IsOptional = isOptional;
		}

		public IsIntegerConstraint(int minValue, int maxValue, bool isOptional)
			: this(minValue, maxValue) {
			IsOptional = isOptional;
		}

		protected int MinValue { get; set; }
		protected int MaxValue { get; set; }
		protected bool IsOptional { get; set; }

		#region IRouteConstraint Members

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
		                  RouteDirection routeDirection) {
			int tryInt;

			return (
			       	values[parameterName] != null
			       	&& int.TryParse(values[parameterName].ToString(), out tryInt)
			       	&& ((MinValue < 1 && MaxValue < 1) || (MinValue <= tryInt && tryInt <= MaxValue))
			       )
			       || (IsOptional && int.Parse(values[parameterName].ToString()) == 0);
		}

		#endregion
	}
}