﻿using System.Web;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Routing {
	public class LanguageConstraint : IRouteConstraint {
	private readonly IUnityContainer _container;

	public LanguageConstraint(IUnityContainer container) {
			_container = container;
		}

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			string languageName = values[parameterName].ToString();
			return string.IsNullOrEmpty(languageName)
				? true
				: _container.Resolve<ILanguageService>().GetLanguage(languageName) != null;
		}
	}
}

