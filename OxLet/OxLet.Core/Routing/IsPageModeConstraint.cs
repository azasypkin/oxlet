﻿using System;
using System.Web;
using System.Web.Routing;

namespace OxLet.Core.Routing {
	public class IsPageMode : IRouteConstraint {
		private readonly PageMode _mode;

		public IsPageMode(PageMode mode) {
			_mode = mode;
		}

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			if (values[parameterName] == null) return false;

			string pagePath = values[parameterName].ToString();

			if (_mode == PageMode.Add && pagePath.EndsWith("/" + PageMode.Add, StringComparison.OrdinalIgnoreCase)) {
				return true;
			}

			if (_mode == PageMode.Edit && pagePath.EndsWith("/" + PageMode.Edit, StringComparison.OrdinalIgnoreCase)) {
				return true;
			}

			if (_mode == PageMode.Remove && pagePath.EndsWith("/" + PageMode.Remove, StringComparison.OrdinalIgnoreCase)) {
				return true;
			}

			return false;
		}
	}
}
