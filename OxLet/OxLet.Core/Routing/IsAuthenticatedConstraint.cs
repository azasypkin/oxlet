﻿using System.Web;
using System.Web.Routing;

namespace OxLet.Core.Routing {
	public class IsAuthenticated : IRouteConstraint {
		#region IRouteConstraint Members

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
		                  RouteDirection routeDirection) {
			return httpContext.Request.IsAuthenticated;
		}

		#endregion
	}
}