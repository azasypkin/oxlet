﻿using System;
using System.Web;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using OxLet.Core.Models;
using OxLet.Core.Services.Interfaces;

namespace OxLet.Core.Routing {
	public class IsContentItemPathConstraint : IRouteConstraint {

		private readonly IUnityContainer _container;
		private readonly ContentItemPathMatch _pathMatch;
		private readonly ContentItemMode _mode;
		private readonly bool _isFolder;
		private const string ContentItemPathRootRouteDataName = "contentItemPathRoot";

		/// <summary>
		/// Requires main site container and path mathx
		/// </summary>
		/// <param name="container">Main site container</param>
		/// <param name="pathMatch">Path match which checks if current root value match to pathMatch</param>
		/// <param name="mode">Mode of the content item</param>
		/// <param name="isFolder"></param>
		public IsContentItemPathConstraint(IUnityContainer container, ContentItemPathMatch pathMatch, ContentItemMode mode, bool isFolder) {
			_container = container;
			_pathMatch = pathMatch;
			_mode = mode;
			_isFolder = isFolder;
		}

		/// <summary>
		/// Requires main site container and path mathx
		/// </summary>
		/// <param name="container">Main site container</param>
		/// <param name="pathMatch">Path match which checks if current root value match to pathMatch</param>
		/// <param name="isFolder"></param>
		public IsContentItemPathConstraint(IUnityContainer container, ContentItemPathMatch pathMatch, bool isFolder) : this(container, pathMatch, ContentItemMode.NotSet, isFolder) {
		}

		public IsContentItemPathConstraint(IUnityContainer container, bool isFolder) : this(container, null, isFolder){
		}

		public IsContentItemPathConstraint(IUnityContainer container, ContentItemMode mode, bool isFolder)
			: this(container, null, mode, isFolder) {
		}

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			var itemPathSegment = values[parameterName] != null
				? values[parameterName].ToString()
				: null;

			var rootPathSegment = values[ContentItemPathRootRouteDataName] != null && routeDirection != RouteDirection.UrlGeneration
				? values[ContentItemPathRootRouteDataName].ToString()
				: null;

			if (string.IsNullOrEmpty(rootPathSegment) && string.IsNullOrEmpty(itemPathSegment)) {
				return _pathMatch == null;
			}

			var pathString = !string.IsNullOrEmpty(rootPathSegment) && !string.IsNullOrEmpty(itemPathSegment)
				? string.Format("/{0}/{1}/", rootPathSegment, itemPathSegment)
				: rootPathSegment ?? itemPathSegment;

			if(_mode != ContentItemMode.NotSet) {
				if (!pathString.EndsWith(string.Format("/{0}/", _mode), StringComparison.OrdinalIgnoreCase)) {
					return false;
				}
				pathString = pathString.Substring(0, pathString.Length - (1 + _mode.ToString().Length));
			}

			ContentItemPath path = ContentItemPath.Parse(pathString);

			// try find item by path
			var contentItem = _container.Resolve<IContentItemService>().GetItem(path);

			if (contentItem != null && _pathMatch != null) {
				return _pathMatch.IsMatch(path) && contentItem.IsFolder == _isFolder;
			}

			return contentItem != null && contentItem.IsFolder == _isFolder;

		}
	}
}