﻿using System;
using System.Web;
using System.Web.Routing;
using OxLet.Core.Extensions;

namespace OxLet.Core.Routing {
	public class IsGuidConstraint : IRouteConstraint {
		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
			Guid result;
			return values[parameterName].ToString().GuidTryParse(out result);
		}
	}
}
