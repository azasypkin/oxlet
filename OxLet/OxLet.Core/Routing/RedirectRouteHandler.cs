﻿using System.Web;
using System.Web.Routing;
using OxLet.Core.Handlers;

namespace OxLet.Core.Routing {
	public class RedirectRouteHandler : IRouteHandler {
		private readonly string _url;

		public RedirectRouteHandler(string url) {
			_url = url;
		}

		#region IRouteHandler Members

		public IHttpHandler GetHttpHandler(RequestContext requestContext) {
			string targetUrl = _url;

			if (requestContext.RouteData.Values.ContainsKey("path") &&
			    requestContext.RouteData.Values.ContainsKey("rootPath")) {
				targetUrl = _url + requestContext.RouteData.Values["rootPath"] + "/" +
				            requestContext.RouteData.Values["path"];
			}

			return new RedirectHttpHandler(targetUrl);
		}

		#endregion
	}
}