﻿using System.Web.Routing;

namespace OxLet.Core.Routing {
	public static class RouteCollectionExtensions {
		public static string GetUrl(this RouteCollection routes, RequestContext context, string route, object values) {
			VirtualPathData pathData = routes[route].GetVirtualPath(context, new RouteValueDictionary(values));
			string path = "";

			if (pathData != null) {
				path = pathData.VirtualPath;

				if (!path.StartsWith("/")) {
					path = "/" + path;
				}

				string appPath = context.HttpContext.Request.ApplicationPath;

				if (appPath != null)
					if (!path.StartsWith(appPath)) {
						path = appPath + path;
					}
			}

			return path;
		}
	}
}