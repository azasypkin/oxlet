﻿using System;

namespace OxLet.Core.Models {
	public class ContentItemResource : EntityBase {
		public ContentItemResource(int id, FileResource file, string path, EntityState state, DateTime created, DateTime modified) : base(id) {
			File = file;
			Path = path;
			State = state;
			Created = created;
			Modified = modified;
		}

		public FileResource File { get; private set; }
		public string Path { get; private set; }
		public EntityState State{ get; private set; }
		public DateTime Created{ get; private set; }
		public DateTime Modified{ get; private set; }
	}
}
