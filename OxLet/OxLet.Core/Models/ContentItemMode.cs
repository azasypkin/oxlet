﻿namespace OxLet.Core.Models {
	public enum ContentItemMode {
		NotSet,
		Add,
		AddContent,
		Edit,
		EditContent,
		Remove
	}
}
