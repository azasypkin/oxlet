﻿namespace OxLet.Core.Models {
	public class SearchPageContainer : NamedEntity {
		public SearchPageContainer(int id, string name, string displayName) : base(id, name, displayName) {
		}

		public SearchPageContainer() : base(0, null, null) {
		}
	}
}
