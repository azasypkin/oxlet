﻿using System.Collections.Generic;

namespace OxLet.Core.Models {
	public interface IPagedList<T> : IList<T> {
		int PageIndex { get; }
		int PageSize { get; }
		int TotalPageCount { get; }
		int TotalItemCount { get; }
	}
}
