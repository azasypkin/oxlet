﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace OxLet.Core.Models {
	public class ModelResult<T> : ModelResult {
		public ModelResult(IEnumerable<ModelValidationResult> validationResults)
			: base(validationResults) {
		}

		public ModelResult(T result, IEnumerable<ModelValidationResult> validationResults)
			: base(validationResults) {
			Item = result;
		}

		public T Item { get; private set; }
	}
}
