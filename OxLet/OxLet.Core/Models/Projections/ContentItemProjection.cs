﻿namespace OxLet.Core.Models.Projections {
	public class ContentItemProjection : EntityBase {

		public ContentItemProjection(int id, string displayName, string slug, string parentSlug, bool isFolder)
			: base(id) {
			DisplayName = displayName;
			Slug = slug;
			ParentSlug = parentSlug;
			IsFolder = isFolder;
		}


		#region Properties

		public string DisplayName { get; private set; }
		public string Slug{ get; private set; }
		public bool IsFolder { get; private set; }
		public string ParentSlug { get; private set; }
		#endregion

		public override string GetCacheItemKey() {
			return string.Format("ContentItem:{0}", Id);
		}
	}
}
