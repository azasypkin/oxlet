﻿namespace OxLet.Core.Models.Projections {
	public class SiteProjection : EntityBase {
		public SiteProjection(int id) : base(id) {
		}

		#region ICacheEntity Members

		public override string GetCacheItemKey() {
			return string.Format("Site:{0}", Id);
		}

		#endregion
	}
}
