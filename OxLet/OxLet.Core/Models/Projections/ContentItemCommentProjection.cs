﻿using System.Collections.Generic;
using OxLet.Core.Cache.Interfaces;

namespace OxLet.Core.Models.Projections {
	public class ContentItemCommentProjection : EntityBase {

		public ContentItemCommentProjection(int id, string slug, int? parentId)
			: base(id) {
			ParentId = parentId;
			Slug = slug;
		}

		public int? ParentId { get; private set; }
		public string Slug { get; private set; }

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			return new ICacheable[] {this};
		}
	}
}
