﻿using System.Collections.Generic;

namespace OxLet.Core.Models {
	public class ContentItemPath{

		public ContentItemPath(ContentItemPath parent, string slug) {
			Slug = slug.ToLower();
			Parent = parent;
		}

		public string Slug { get; private set; }
		public ContentItemPath Child { get; private set; }
		public ContentItemPath Parent { get; private set; }

		public static ContentItemPath Parse(string path) {
			var elements = path.Trim('/').ToLower().Split('/');
			return elements.Length == 0 ? null : GetItemPath(null, elements, 0);
		}

		private static ContentItemPath GetItemPath(ContentItemPath parent,  IList<string> elements, int currentIndex) {
			var parentPath = new ContentItemPath(parent, elements[currentIndex].ToLower());
			if (currentIndex + 1 < elements.Count) {
				parentPath.AddChild(GetItemPath(parentPath, elements, currentIndex + 1));
			}
			return parentPath; 
		}

		public override string ToString() {
			return GetFullPath(string.Empty, this);
		}

		public string GetFullPath(string source, ContentItemPath itemPath) {
			source += "/" + itemPath.Slug;
			return itemPath.Child != null ? GetFullPath(source, itemPath.Child) : source;
		}

		public ContentItemPath AddChild(ContentItemPath child) {
			return Child = child;
		}
	}
}
