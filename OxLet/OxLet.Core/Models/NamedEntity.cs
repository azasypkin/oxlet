﻿using System.Runtime.Serialization;

namespace OxLet.Core.Models {
	[DataContract]
	public class NamedEntity : EntityBase, INamedEntity {
		public NamedEntity(int id, string name, string displayName) : base(id) {
			Name = name;
			DisplayName = displayName;
		}

		[DataMember]
		public string Name { get; private set; }

		public string DisplayName { get; private set; }
	}
}
