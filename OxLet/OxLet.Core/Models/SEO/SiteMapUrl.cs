using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace OxLet.Core.Models.SEO {

	[GeneratedCode("System.Xml", "4.0.30319.225")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")]
	[XmlRoot(Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9", IsNullable = true)]
	public class SiteMapUrl {
		[XmlElement(DataType = "anyURI", Order = 0, ElementName = "loc")]
		public string Location { get; set; }

		[XmlElement(Order = 1, ElementName = "lastmod")]
		public string LastModified { get; set; }

		[XmlElement(Order = 3, ElementName = "priority")]
		public decimal Priority { get; set; }

		[XmlElement(Order = 2, ElementName = "changefreq")]
		public ChangeFrequency ChangeFrequency { get; set; }
	}
}
