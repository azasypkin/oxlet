using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace OxLet.Core.Models.SEO {
	[GeneratedCode("System.Xml", "4.0.30319.225")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")]
	[XmlRootAttribute(Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9", IsNullable = false, ElementName = "urlset")]
	public class SiteMap {
		public SiteMap() {
			SiteMapUrls = new List<SiteMapUrl>(0);
		}

		[XmlElement("url", Order = 0)]
		public List<SiteMapUrl> SiteMapUrls { get; set; }
	}
}
