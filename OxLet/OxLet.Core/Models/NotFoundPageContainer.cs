﻿namespace OxLet.Core.Models {
	public class NotFoundPageContainer : NamedEntity {
		public NotFoundPageContainer(int id, string name, string displayName) : base(id, name, displayName) {
		}
		public NotFoundPageContainer():this(0, null, null) {
		
		}
	}
}
