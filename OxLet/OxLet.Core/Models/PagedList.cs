﻿using System;
using System.Collections.Generic;

namespace OxLet.Core.Models {
	public class PagedList<T> : List<T>, IPagedList<T> {
		public PagedList(IEnumerable<T> items, int pageIndex, int pageSize, int totalItemCount) {
			AddRange(items);
			PageIndex = pageIndex;
			PageSize = pageSize;
			TotalItemCount = totalItemCount;
			TotalPageCount = (int) Math.Ceiling(totalItemCount/(double) pageSize);
		}

		#region IPagedList<T> Members

		public int PageIndex { get; set; }
		public int PageSize { get; set; }
		public int TotalItemCount { get; set; }
		public int TotalPageCount { get; private set; }

		#endregion
	}
}
