﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using OxLet.Core.Cache.Interfaces;

namespace OxLet.Core.Models {
	[DataContract]
	public class EntityBase:ICacheable {

		public EntityBase(int id) {
			Id = id;
		}

		public int Id { get; private set; }

		public virtual string GetCacheItemKey() {
			return string.Format("{0}:{1}", GetType().Name, Id.ToString());
		}

		public virtual IEnumerable<ICacheable> GetCacheDependencyItems() {
			return Enumerable.Empty<ICacheable>();
		}
	}
}
