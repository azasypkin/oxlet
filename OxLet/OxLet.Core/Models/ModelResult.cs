﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace OxLet.Core.Models {
	public class ModelResult {
		public ModelResult() : this(null) {
		}

		public ModelResult(IEnumerable<ModelValidationResult> validationResults) {
			ValidationResults = validationResults;
			IsValid = validationResults == null || validationResults.Count() == 0;
		}

		public IEnumerable<ModelValidationResult> ValidationResults { get; private set; }
		public bool IsValid { get; private set; }

	}
}
