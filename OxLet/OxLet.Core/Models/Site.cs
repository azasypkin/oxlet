﻿using System;
using System.Collections.Generic;

namespace OxLet.Core.Models {
	public class Site:EntityBase {
		public Site(int id, IList<Uri> hostRedirects, Uri host, string name, string displayName, string description, Language defaultLanguage, double timeZoneOffset, string pageTitleSeparator, string favIconUrl, string scriptsPath, string cssPath, string imagesPath, bool includeOpenSearch, string gravatarDefault, string skinDefault, string routeUrlPrefix) : base(id) {
			HostRedirects = hostRedirects;
			Host = host;
			Name = name;
			DisplayName = displayName;
			Description = description;
			DefaultLanguage = defaultLanguage;
			TimeZoneOffset = timeZoneOffset;
			PageTitleSeparator = pageTitleSeparator;
			FavIconUrl = favIconUrl;
			ScriptsPath = scriptsPath;
			CssPath = cssPath;
			ImagesPath = imagesPath;
			IncludeOpenSearch = includeOpenSearch;
			GravatarDefault = gravatarDefault;
			SkinDefault = skinDefault;
			RouteUrlPrefix = routeUrlPrefix;
		}

		public IList<Uri> HostRedirects { get; private set; }
		public Uri Host { get; private set; }
		public string Name { get; private set; }
		public string DisplayName { get; private set; }
		public string Description { get; private set; }
		public Language DefaultLanguage { get; private set; }
		public double TimeZoneOffset { get; private set; }
		public string PageTitleSeparator { get; private set; }
		public string FavIconUrl { get; private set; }
		public string ScriptsPath { get; private set; }
		public string CssPath { get; private set; }
		public string ImagesPath { get; private set; }
		public bool IncludeOpenSearch { get; private set; }
		public string GravatarDefault { get; private set; }
		public string SkinDefault { get; private set; }
		public string RouteUrlPrefix { get; private set; }
	}
}
