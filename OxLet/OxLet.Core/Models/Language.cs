﻿namespace OxLet.Core.Models {
	public class Language : NamedEntity {
		public Language(int id, string name, string displayName) : base(id, name, displayName) {
		}
	}
}
