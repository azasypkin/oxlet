﻿using System.Collections.Generic;
using OxLet.Core.Cache.Interfaces;

namespace OxLet.Core.Models {
	public class UserDescription : EntityBase {
		public UserDescription(int id) : base(id) {
		}

		public string Name { get; set; }

		public string DisplayName { get; set; }

		public string Email { get; set; }

		public string EmailHash { get; set; }

		public string[] Roles { get; set; }

		public string Url { get; set; }

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			return new[] {this};
		}
	}
}
