﻿namespace OxLet.Core.Models {
	public class SearchCriteria {
		public string Term { get; set; }

		public bool HasCriteria() {
			return !string.IsNullOrEmpty(Term);
		}
	}
}
