﻿using System;
using System.Collections.Generic;
using OxLet.Core.Cache.Interfaces;

namespace OxLet.Core.Models {
	public class ContentItemComment : EntityBase {

		public ContentItemComment(int id, IEnumerable<ContentItemComment> children) : base(id) {
		}

		public ContentItemComment(int id, UserDescription creator, long creatorIp, string creatorUserAgent, Language language, string body, EntityState state, DateTime? created, DateTime? modified, ContentItem contentItem, ContentItemComment parent, IEnumerable<ContentItemComment> children, string slug) : base(id) {
			Creator = creator;
			CreatorIp = creatorIp;
			CreatorUserAgent = creatorUserAgent;
			Language = language;
			Body = body;
			State = state;
			Created = created;
			Modified = modified;
			ContentItem = contentItem;
			Parent = parent;
			Children = children;
			Slug = slug;
		}

		public UserDescription Creator { get; private set; }
		public long CreatorIp { get; private set; }
		public string CreatorUserAgent { get; private set; }
		public Language Language { get; private set; }
		public string Body { get; private set; }
		public EntityState State { get; private set; }
		public DateTime? Created { get; private set; }
		public DateTime? Modified { get; private set; }
		public ContentItem ContentItem { get; private set; }
		public ContentItemComment Parent { get; private set; }
		public IEnumerable<ContentItemComment> Children { get; private set; }
		public string Slug { get; private set; }

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			return new ICacheable[] {this, ContentItem};
		}
	}
}
