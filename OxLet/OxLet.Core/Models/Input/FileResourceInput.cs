﻿using System;

namespace OxLet.Core.Models.Input {
	public class FileResourceInput {
		public FileResourceInput(int id, string name, string mimeType, byte[] data) {
			Id = id;
			Name = name;
			MimeType = mimeType;
			Data = data;
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public string MimeType { get; set; }
		public byte[] Data { get; set; }

		public FileResource ToFileResource(UserDescription creator) {
			return new FileResource(
				Id,
				Name,
				MimeType,
				Data,
				creator,
				DateTime.MinValue,
				DateTime.MinValue
			);
		}
	}
}
