﻿using System;
using OxLet.Core.Models.Projections;

namespace OxLet.Core.Models.Input {
	public class ContentItemInput {
		#region Properties

		public int Id { get; set; }
		public string Name { get; set; }
		public string DisplayName { get; set; }
		public string Slug { get; set; }

		public int? Parent { get; set; }

		public ContentItemResourceInput Value { get; set; }
		public ContentItemResourceInput Thumbnail { get; set; }

		public DateTime? Published { get; set; }

		public int Language { get; set; }

		public bool IsFolder { get; set; }

		#endregion

		public ContentItem ToContentItem(UserDescription creator, EntityState state) {
			return new ContentItem(
				Id,
				Name,
				DisplayName,
				Slug,
				null,
				Parent != null ? new ContentItemProjection(Parent.Value, null, null, null, false) : null,
				Value != null ? Value.ToContentItemResource(creator, state) : null,
				Thumbnail != null ? Thumbnail.ToContentItemResource(creator, state) : null,
				DateTime.MinValue,
				DateTime.MinValue,
				Published,
				state,
				new Language(Language, null, null),
				IsFolder,
				creator,
				null,
				null
				);
		}
	}
}