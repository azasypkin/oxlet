﻿using System;
using System.Collections.Generic;

namespace OxLet.Core.Models.Input {
	public class SiteInput {
		public IList<Uri> HostRedirects { get; set; }
		public Uri Host { get; set; }
		public string Name { get; set; }
		public string DisplayName { get; set; }
		public string Description { get; set; }
		public Language DefaultLanguage { get; set; }
		public double TimeZoneOffset { get; set; }
		public string PageTitleSeparator { get; set; }
		public string FavIconUrl { get; set; }
		public string ScriptsPath { get; set; }
		public string CssPath { get; set; }
		public string ImagesPath { get; set; }
		public bool IncludeOpenSearch { get; set; }
		public string GravatarDefault { get; set; }
		public string SkinDefault { get; set; }
		public string RouteUrlPrefix { get; set; }

		public Site ToSite() {
			//return new Site {
			//    CssPath = CssPath,
			//    Description = Description,
			//    DisplayName = DisplayName,
			//    FavIconUrl = FavIconUrl,
			//    GravatarDefault = GravatarDefault,
			//    Host = Host,
			//    HostRedirects = HostRedirects,
			//    IncludeOpenSearch = IncludeOpenSearch,
			//    DefaultLanguage = DefaultLanguage,
			//    Name = Name,
			//    PageTitleSeparator = PageTitleSeparator,
			//    RouteUrlPrefix = RouteUrlPrefix,
			//    ScriptsPath = ScriptsPath,
			//    SkinDefault = SkinDefault,
			//    TimeZoneOffset = TimeZoneOffset
			//};
			throw new NotImplementedException();
		}
	}
}
