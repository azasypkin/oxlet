﻿using System;

namespace OxLet.Core.Models.Input {
	public class ContentItemResourceInput{
		public ContentItemResourceInput(int id, FileResourceInput file, string path) {
			Id = id;
			File = file;
			Path = path;
		}

		public int Id { get; set; }
		public FileResourceInput File { get; private set; }
		public string Path { get; private set; }

		public ContentItemResource ToContentItemResource(UserDescription creator, EntityState state) {
			return new ContentItemResource(
				Id,
				File != null ? File.ToFileResource(creator) : null,
				Path,
				state,
				DateTime.MinValue,
				DateTime.MinValue
			);
		}

	}
}
