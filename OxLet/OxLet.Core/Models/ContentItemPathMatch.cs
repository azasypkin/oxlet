﻿using System;

namespace OxLet.Core.Models {
	public class ContentItemPathMatch {

		public ContentItemPath Path { get; private set; }
		public ContentItemPathMatchType MatchType { get; private set; }

		public ContentItemPathMatch(string path, ContentItemPathMatchType matchType) {
			Path = ContentItemPath.Parse(path);
			MatchType = matchType;
		}

		public ContentItemPathMatch(ContentItemPath path, ContentItemPathMatchType matchType) {
			Path = path;
			MatchType = matchType;
		}

		public bool IsMatch(ContentItemPath otherPath) {

			if(otherPath == null) {
				return false;
			}

			var pathString = Path.ToString().Trim('\\', '/', ' ');
			var otherPathString = otherPath.ToString().Trim('\\', '/', ' ');
			
			switch (MatchType) {
				case ContentItemPathMatchType.Exact:
					return string.Compare(pathString, otherPathString, StringComparison.CurrentCultureIgnoreCase) == 0;
				case ContentItemPathMatchType.StartsWith:
					return otherPathString.StartsWith(pathString, StringComparison.CurrentCultureIgnoreCase);
				default:
					return false;
			}
		}
	}
}
