﻿using System;

namespace OxLet.Core.Models {
	public class FileResource : EntityBase, INamedEntity {

		public FileResource(int id)
			: base(id) {
		}

		public FileResource(int id, string name, string mimeType, byte[] data, UserDescription creator, DateTime created, DateTime modified) : base(id) {
			Name = name;
			MimeType = mimeType;
			Data = data;
			Creator = creator;
			Created = created;
			Modified = modified;
		}

		public string Name { get; private set; }

		public string DisplayName {
			get { return Name; }
		}

		public UserDescription Creator { get; private set; }

		public string MimeType { get; private set; }
		public byte[] Data { get; private set; }
		public DateTime Created { get; private set; }
		public DateTime Modified { get; private set; }

		public FileResource FreeUpBytes() {
			Data = null;
			return this;
		}
	}
}
