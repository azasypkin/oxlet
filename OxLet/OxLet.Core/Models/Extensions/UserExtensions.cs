﻿using System.Web.Routing;
using OxLet.Core.Routing;
using OxLet.Core.Secuity;

namespace OxLet.Core.Models.Extensions {
	public static class UserExtensions {
		public static string GetAdminUrl(this User user, RequestContext context, RouteCollection routes) {
			return routes.GetUrl(context, "AdminUsersEdit", new {userID = user.Id});
		}

		public static string GetAdminChangePasswordUrl(this User user, RequestContext context, RouteCollection routes) {
			return routes.GetUrl(context, "AdminUsersChangePassword", new {userID = user.Id});
		}
	}
}
