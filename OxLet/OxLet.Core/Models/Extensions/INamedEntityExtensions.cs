﻿namespace OxLet.Core.Models.Extensions {
	public static class INamedEntityExtensions {
		public static string GetDisplayName(this INamedEntity entity) {
			if (!string.IsNullOrEmpty(entity.DisplayName))
				return entity.DisplayName;

			return entity.Name;
		}
	}
}
