﻿using System.Text.RegularExpressions;

namespace OxLet.Core.Models {
	public class RegularExpressionDescription {
		public string Name { get; set; }
		public string Expression { get; set; }
		public RegexOptions Options { get; set; }
	}
}
