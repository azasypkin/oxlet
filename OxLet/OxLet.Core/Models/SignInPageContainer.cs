﻿namespace OxLet.Core.Models {
	public class SignInPageContainer : NamedEntity {
		public SignInPageContainer(int id, string name, string displayName) : base(id, name, displayName) {
		}

		public SignInPageContainer() : this(0, null, null) {
		}
	}
}
