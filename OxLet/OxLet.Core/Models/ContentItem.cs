﻿using System;
using System.Collections.Generic;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Models.Projections;

namespace OxLet.Core.Models {
	public class ContentItem : EntityBase, INamedEntity {

		public ContentItem(int id)
			: base(id) {
		}

		public ContentItem(int id, string name, string displayName, string slug, string parentSlug, ContentItemProjection parent, ContentItemResource value, ContentItemResource thumbnail, DateTime created, DateTime modified, DateTime? published, EntityState state, Language language, bool isFolder, UserDescription creator, IEnumerable<ContentItemCommentProjection> comments, IEnumerable<ContentItemProjection> children)
			: base(id) {
			Name = name;
			DisplayName = displayName;
			Slug = slug;
			ParentSlug = parentSlug;
			Parent = parent;
			Value = value;
			Thumbnail = thumbnail;
			Created = created;
			Modified = modified;
			Published = published;
			State = state;
			Language = language;
			IsFolder = isFolder;
			Creator = creator;
			Comments = comments;
			Children = children;
		}


		#region Properties

		public string Name { get; private set; }
		public string DisplayName { get; private set; }
		public string Slug{ get; private set; }
		public string ParentSlug { get; private set; }

		public ContentItemProjection Parent { get; private set; }

		public ContentItemResource Value{ get; private set; }
		public ContentItemResource Thumbnail{ get; private set; }
		
		public DateTime Created{ get; private set; }
		public DateTime Modified{ get; private set; }
		public DateTime? Published{ get; private set; }

		public EntityState State{ get; private set; }

		public Language Language { get; private set; }

		public bool IsFolder { get; private set; }

		public UserDescription Creator { get; private set; }

		public IEnumerable<ContentItemCommentProjection> Comments { get; private set; }

		public IEnumerable<ContentItemProjection> Children{ get; private set; }

		#endregion

		public void SetCreator(UserDescription user) {
			Creator = user;
		}

		public override IEnumerable<ICacheable> GetCacheDependencyItems() {
			if(Parent != null && Parent.Id > 0) {
				yield return Parent;
			}
		}
	}
}
