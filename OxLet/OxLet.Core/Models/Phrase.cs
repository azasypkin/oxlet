﻿namespace OxLet.Core.Models {
	public class Phrase {
		public string Key { get; set; }
		public string Value { get; set; }
		public string Language { get; set; }

		public override string ToString() {
			return Value;
		}
	}
}
