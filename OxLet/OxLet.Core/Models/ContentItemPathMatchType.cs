﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OxLet.Core.Models {
	public enum ContentItemPathMatchType: byte {
		StartsWith = 0,
		Exact
	}
}
