﻿namespace OxLet.Core.Models {
	public interface INamedEntity {
		string Name { get; }
		string DisplayName { get; }
	}
}
