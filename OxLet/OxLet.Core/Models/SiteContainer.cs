﻿namespace OxLet.Core.Models {
	public class SiteContainer : NamedEntity {
		public SiteContainer(int id, string name, string displayName) : base(id, name, displayName) {
		}

		public SiteContainer() : this(0, null, null) {
		}
	}
}
