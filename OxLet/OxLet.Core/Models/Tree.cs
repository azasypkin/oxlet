﻿namespace OxLet.Core.Models {
	public class Tree<TParent, TChild> {
		public TParent Parent { get; set; }
		public TChild Child { get; set; }
	}
}
