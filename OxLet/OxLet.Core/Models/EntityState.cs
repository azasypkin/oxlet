﻿namespace OxLet.Core.Models {
	public enum EntityState : byte {
		NotSet = 0,
		Normal = 1,
		PendingApproval = 2,
		Removed = 3
	}
}