﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Core.Validation;

namespace OxLet.Core.Models.Validation {
	public class ModelValidatorWrapper : ModelValidator {
		public ModelValidatorWrapper(ModelMetadata metadata, ControllerContext controllerContext, IModelValidator validator)
			: base(metadata, controllerContext) {
			Validator = validator;
		}
		private IModelValidator Validator { get; set; }
		public override IEnumerable<ModelValidationResult> Validate(object container) {
			return Validator.Validate(container);
		}

		public override IEnumerable<ModelClientValidationRule> GetClientValidationRules() {
			return Metadata != null ? Validator.GetClientValidationRules(Metadata.PropertyName) : Enumerable.Empty<ModelClientValidationRule>();
		}
	}
}
