﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Models.Input;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;

namespace OxLet.Core.Models.Validation {
	public class ContentItemInputModelValidator : ModelValidatorBase<ContentItemInput> {
		private const string LocalizationStore = "CommentValidationMessages";


		public ContentItemInputModelValidator(ILocalizationService localizationService, IRegularExpressionService regularExpressionService) 
			: base(localizationService, regularExpressionService) {
		}

		public override IEnumerable<ModelValidationResult> Validate(ContentItemInput model) {
			if (string.IsNullOrEmpty(model.Name)) {
				yield return new ModelValidationResult {
					MemberName = "Name",
					Message = LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Body_ValidationMessage").Value
				};
			}
		}

		public override IEnumerable<ModelClientValidationRule> GetClientValidationRules(string propertyName) {

			if (new[] { "Slug", "DisplayName", "Name" }.Contains(propertyName)) {
				yield return
					new ModelClientValidationRequiredRule(
						LocalizationService.GetPhrase(LocalizationStore, "Comment_Form_Body_ValidationMessage").Value);
			}
		}
	}
}