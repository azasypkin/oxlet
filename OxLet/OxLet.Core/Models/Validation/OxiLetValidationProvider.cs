﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using OxLet.Core.Validation;

namespace OxLet.Core.Models.Validation {
	public class ConventionValidatorProvider : ModelValidatorProvider {
		private readonly IUnityContainer _container;
		public ConventionValidatorProvider(IUnityContainer container) {
			_container = container;
		}

		public override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, ControllerContext context) {

			// try get validator
			if(metadata != null && metadata.ContainerType != null) {
				var genericValidatorType = typeof (IModelValidator<>).MakeGenericType(new[] {metadata.ContainerType});
				try {
					return new[] {
						new ModelValidatorWrapper(metadata, context, _container.Resolve(genericValidatorType) as IModelValidator)
					};
				}
				catch {
				}
			}
			return Enumerable.Empty<ModelValidator>();
		}
	}
}
