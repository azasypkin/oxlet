﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Models.Input;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Validation;

namespace OxLet.Core.Models.Validation {
	public class FileResourceInputModelValidator : ModelValidatorBase<FileResourceInput> {

		private const string LocalizationStore = "FileResourceValidationMessages";

		public FileResourceInputModelValidator(ILocalizationService localizationService, IRegularExpressionService regularExpressionService) : base(localizationService, regularExpressionService) {
		}

		public override IEnumerable<ModelValidationResult> Validate(FileResourceInput model) {
			if (model == null) {
				throw new ArgumentNullException("model");
			}


			if (model.Data == null || model.Data.Length == 0) {
				yield return new ModelValidationResult {
					MemberName = "Data",
					Message = LocalizationService.GetPhrase(LocalizationStore, "FileResource_Data_ValidationMessage").Value
				};
			}

			model.MimeType = model.MimeType.Trim();
			if (string.IsNullOrEmpty(model.MimeType)) {
				yield return new ModelValidationResult {
					MemberName = "MimeType",
					Message = LocalizationService.GetPhrase(LocalizationStore, "FileResource_MimeType_ValidationMessage_Required").Value
				};
			} else {
				if (model.MimeType.Length > 25) {
					yield return new ModelValidationResult {
						MemberName = "MimeType",
						Message = string.Format(LocalizationService.GetPhrase(LocalizationStore, "FileResource_MimeType_ValidationMessage_Length").Value, 25)
					};
				}
			}

			model.Name = model.Name.Trim();
			if (string.IsNullOrEmpty(model.Name)) {
				yield return new ModelValidationResult {
					MemberName = "Name",
					Message = LocalizationService.GetPhrase(LocalizationStore, "FileResource_Name_ValidationMessage_Required").Value
				};
			} else {
				if (model.Name.Length > 255) {
					yield return new ModelValidationResult {
						MemberName = "Name",
						Message = string.Format(LocalizationService.GetPhrase(LocalizationStore, "FileResource_Name_ValidationMessage_Length").Value, 255)
					};
				}
			}
		}
	}
}
