﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;
using OxLet.Core.Plugins.Interfaces;

namespace OxLet.Core.Plugins {
	public class PluginService : IPluginService {
		private readonly IEnumerable<IPlugin> _plugins;
		public PluginService(IUnityContainer container) {
			_plugins = container.ResolveAll<IPlugin>();
		}

		#region IPluginService Members

		public IEnumerable<IPlugin> GetPlugins() {
			return _plugins;
		}

		public void Save(IPlugin plugin) {
			throw new NotImplementedException();
		}

		#endregion
	}
}
