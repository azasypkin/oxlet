﻿using System;
using OxLet.Core.Plugins.Interfaces;

namespace OxLet.Core.Plugins {
	public class LambdaPluginParametersBinder<TOut>:IPluginParametersBinder<TOut> {

		private readonly Func<object, TOut> _labmda;

		public LambdaPluginParametersBinder(Func<object, TOut> lambda) {
			_labmda = lambda;
		}

		public TOut Bind(object parameters) {
			if(_labmda != null) {
				return _labmda(parameters);
			}
			return default(TOut);
		}
	}
}
