﻿using System;
using System.Collections.Generic;
using OxLet.Core.Plugins.Interfaces;
using System.Linq;

namespace OxLet.Core.Plugins {
	public class PluginEngine : IPluginEngine {

		private readonly IPluginService _pluginService;
		private static IDictionary<string, List<ISubscribeable>> _subscribedPlugins;

		public PluginEngine(IPluginService pluginService) {
			_pluginService = pluginService;
		}

		private static void InitializeSubscribeablePlugins(IPluginService pluginService) {
			_subscribedPlugins = new Dictionary<string, List<ISubscribeable>>();
			// register all ISubscribeable plugins
			var subscribeablePluginList = pluginService.GetPlugins().OfType<ISubscribeable>();
			foreach (var subscribeable in subscribeablePluginList) {
				var targetEvents = subscribeable.GetEvents().Select(x => x.ToUpper());
				foreach (var targetEvent in targetEvents) {
					if (_subscribedPlugins.ContainsKey(targetEvent)) {
						_subscribedPlugins[targetEvent].Add(subscribeable);
					} else {
						_subscribedPlugins[targetEvent] = new List<ISubscribeable> { subscribeable };
					}
				}
			}
		}

		public IEnumerable<IPlugin> GetPlugins() {
			throw new NotImplementedException();
		}

		public void Fire(string targetEvent, object parameters) {
			targetEvent = targetEvent.ToUpper();
			if (_subscribedPlugins == null) {
				InitializeSubscribeablePlugins(_pluginService);
			}

			if(_subscribedPlugins.ContainsKey(targetEvent)) {
				_subscribedPlugins[targetEvent].ForEach(x=>x.Fire(targetEvent, parameters));
			}
		}
	}
}
