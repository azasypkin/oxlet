﻿using System.Collections.Generic;

namespace OxLet.Core.Plugins.Interfaces {
	public interface IPluginService {
		IEnumerable<IPlugin> GetPlugins();
		void Save(IPlugin plugin);
	}
}
