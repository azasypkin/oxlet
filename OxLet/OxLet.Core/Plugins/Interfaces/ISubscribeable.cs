﻿using System.Collections.Generic;

namespace OxLet.Core.Plugins.Interfaces {
	public interface ISubscribeable {
		IEnumerable<string> GetEvents();
		void Fire(string targetEvent, object parameters);
	}
}
