﻿using System.Collections.Generic;

namespace OxLet.Core.Plugins.Interfaces {
	public interface IPluginEngine {
		IEnumerable<IPlugin> GetPlugins();
		void Fire(string targetEvent, object parameters);
	}
}
