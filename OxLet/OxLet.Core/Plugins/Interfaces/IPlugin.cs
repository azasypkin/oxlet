﻿namespace OxLet.Core.Plugins.Interfaces {
	public interface IPlugin {
		string Description { get;}
		string Name { get;}
		bool Enabled { get; set; }
	}
}
