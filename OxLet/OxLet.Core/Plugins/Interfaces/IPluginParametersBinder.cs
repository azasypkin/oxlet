﻿namespace OxLet.Core.Plugins.Interfaces {
	public interface IPluginParametersBinder<out TOut> {
		TOut Bind(object parameters);
	}
}
