﻿using OxLet.Core.Plugins.Interfaces;

namespace OxLet.Core.Plugins {
	public class SimplePluginParametersBinder<TOut>:IPluginParametersBinder<TOut> {
		public TOut Bind(object parameters) {
			if(parameters is TOut) {
				return (TOut) parameters;
			}
			return default(TOut);
		}
	}
}
