﻿using System;
using System.Collections.Generic;

namespace OxLet.Core.Cache.Interfaces {
	public interface ICacheService {

		void Insert(string key, object value);

		TList GetItems<TList, TItem>(
			string key,
			Func<TList> getData,
			Func<TItem, IEnumerable<ICacheable>> getDependencies
		) where TList : IEnumerable<TItem>;

		TList GetItems<TList, TItem>(
			string key,
			Func<TList> getData,
			Func<TItem, IEnumerable<ICacheable>> getDependencies,
			CachePage cachePage
		) where TList : IEnumerable<TItem>;

		T GetItem<T>(
			string key,
			Func<T> getData,
			Func<T, IEnumerable<ICacheable>> getDependencies
		);

		T GetItem<T>(
			string key,
			Func<T> getData
		);

		void InvalidateItem(ICacheable item);

		void Invalidate<T>() where T : ICacheable;

		void Invalidate(string key);
	}
}
