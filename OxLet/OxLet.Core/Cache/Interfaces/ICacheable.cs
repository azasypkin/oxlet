﻿using System.Collections.Generic;

namespace OxLet.Core.Cache.Interfaces {
	public interface ICacheable {
		string GetCacheItemKey();
		IEnumerable<ICacheable> GetCacheDependencyItems();
	}
}
