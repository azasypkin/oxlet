﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OxLet.Core.Cache.Interfaces;
using OxLet.Core.Configuration;

namespace OxLet.Core.Cache {
	public class AspNetCacheService : ICacheService {

		#region Fields

		private readonly TimeSpan _slidingExpirationTimeSpan;
		private readonly DateTime _absoluteExpirationTime;

		#endregion

		public AspNetCacheService(OxLetSection config) {
			switch (config.CacheExpiration) {
				case CacheExpiration.Absolute:
					_absoluteExpirationTime = config.AbsoluteExpirationTime;
					_slidingExpirationTimeSpan = System.Web.Caching.Cache.NoSlidingExpiration;
					break;
				case CacheExpiration.Sliding:
					_slidingExpirationTimeSpan = TimeSpan.FromMinutes(config.SlidingExpirationTimeSpan);
					_absoluteExpirationTime = System.Web.Caching.Cache.NoAbsoluteExpiration;
					break;
				default:
					_slidingExpirationTimeSpan = TimeSpan.FromMinutes(config.SlidingExpirationTimeSpan);
					_absoluteExpirationTime = System.Web.Caching.Cache.NoAbsoluteExpiration;
					break;
			}
		}


		public void Insert(string key, object value) {
			InsertItemIntoCache(HttpContext.Current, key, value);
		}

		public TList GetItems<TList, TItem>(string key, Func<TList> getData, Func<TItem, IEnumerable<ICacheable>> getDependencies) where TList : IEnumerable<TItem> {
			return GetItems(key, getData, getDependencies, null);
		}

		public TList GetItems<TList, TItem>(string key, Func<TList> getData, Func<TItem, IEnumerable<ICacheable>> getDependencies, CachePage cachePage) where TList : IEnumerable<TItem> {
			HttpContext context = HttpContext.Current;
			string fullKey = cachePage != null ? key + cachePage : key;

			var cacheItem = (TList)context.Cache[fullKey];

			if (cacheItem != null) {
				return cacheItem;
			}

			TList items = getData();

			if (items != null) {

				InsertItemIntoCache(context, fullKey, items);

				// paging
				if (cachePage != null) {
					var pageList = context.Cache[key] as CacheDependencyList;

					if (pageList == null){
						InsertItemIntoCache(context, key, pageList = new CacheDependencyList());
					}

					string keyAndPage = key + cachePage;

					if (!pageList.Contains(keyAndPage)){
						pageList.Add(keyAndPage);
					}
				}

				// dependencies
				if (getDependencies != null)
					if (items.Count() > 0){
						foreach (TItem item in items){
							// add dependencies for each item
							foreach (ICacheable dependency in getDependencies(item)){
								CacheDependency(key, context, dependency);
							}
						}
					}
					else{
						// allow the caller to add dependencies for dependent objects even though no items were found
						foreach (ICacheable dependency in getDependencies(default(TItem))){
							CacheDependency(key, context, dependency);
						}
					}
			}

			return items;
		}

		public TItem GetItem<TItem>(string key, Func<TItem> getData) {
			return GetItem(key, getData, null);
		}

		public TItem GetItem<TItem>(string key, Func<TItem> getData, Func<TItem, IEnumerable<ICacheable>> getDependencies) {
			HttpContext context = HttpContext.Current;
			var cacheItem = (TItem)context.Cache[key];

			if (cacheItem != null) {
				return cacheItem;
			}

			TItem item = getData();

			if (item != null) {
				InsertItemIntoCache(context, key, item);

				if (getDependencies != null) {
					foreach (ICacheable dependency in getDependencies(item)) {
						CacheDependency(key, context, dependency);
					}
				}
			}

			return item;
		}

		public void InvalidateItem(ICacheable item) {
			HttpContext context = HttpContext.Current;

			InvalidateDependency(context, item.GetCacheItemKey());

			foreach (ICacheable cacheItem in item.GetCacheDependencyItems()){
				InvalidateDependency(context, cacheItem.GetCacheItemKey());
			}
		}

		public void Invalidate<T>() where T : ICacheable {
			HttpContext context = HttpContext.Current;
			var dependencyList = context.Cache[typeof(T).Name] as CacheDependencyList;

			if (dependencyList != null) {
				foreach (string dependency in dependencyList) {
					InvalidateDependency(context, dependency);
				}
			}
		}

		public void Invalidate(string key) {
			InvalidateDependency(HttpContext.Current, key);
		}

		#region Private methods

		private void InsertItemIntoCache(HttpContext context, string key, object value) {
			context.Cache.Insert(key, value, null, _absoluteExpirationTime, _slidingExpirationTimeSpan);
		}

		private void CacheDependency(string key, HttpContext context, ICacheable item) {
			string dependencyKey = item.GetCacheItemKey();
			var dependencyList = context.Cache[dependencyKey] as CacheDependencyList;

			if (dependencyList == null) {
				InsertItemIntoCache(context, dependencyKey, dependencyList = new CacheDependencyList());
			}

			if (!dependencyList.Contains(key)){
				dependencyList.Add(key);
			}

			string typeDependencyKey = item.GetType().Name;
			var typeDependencyList = context.Cache[typeDependencyKey] as CacheDependencyList;

			if (typeDependencyList == null){
				InsertItemIntoCache(context, typeDependencyKey, typeDependencyList = new CacheDependencyList());
			}

			if (!typeDependencyList.Contains(dependencyKey)){
				typeDependencyList.Add(dependencyKey);
			}
		}

		private static void InvalidateDependency(HttpContext context, string dependencyKey) {
			var dependencyList = context.Cache[dependencyKey] as CacheDependencyList;

			if (dependencyList != null) {
				foreach (string dependency in dependencyList) {
					var dependencyItems = context.Cache[dependency] as CacheDependencyList;

					if (dependencyItems != null){
						foreach (string dependencyItem in dependencyItems){
							InvalidateByKey(context, dependencyItem);
						}
					}

					InvalidateByKey(context, dependency);
				}
			}

			InvalidateByKey(context, dependencyKey);
		}

		private static void InvalidateByKey(HttpContext context, string key) {
			context.Cache.Remove(key);
		}

		#endregion
	}
}
