﻿using System;
using System.Collections.Generic;

namespace OxLet.Core.Localization {
	public interface IResourceMapper {
		Type GetResourceType(string resourceName);
		IEnumerable<Type> GetRegisteredResourceTypes();
		IResourceMapper AddResourceMapping(string alias, Type resourceType);
	}
}
