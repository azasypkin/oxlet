﻿using System;
using System.Globalization;
using System.Web;
using System.Web.Routing;
using OxLet.Core.Infrastructure;
using OxLet.Core.Localization.Interfaces;
using OxLet.Core.Localization.Repositories;
using OxLet.Core.Models;

namespace OxLet.Core.Localization {
	public class LocalizationService : ILocalizationService {
		private readonly ILocalizationRepository _repository;
		private readonly RequestContext _context;
		private const string CookieName = "lang";
		private readonly SiteContext _siteContext;

		public LocalizationService(ILocalizationRepository repository, RequestContext context, SiteContext siteContext) {
			_repository = repository;
			_context = context;
			_siteContext = siteContext;
		}

		public CultureInfo GetCulture() {
			// first check route values
			CultureInfo currentCulture = _context.RouteData.Values.ContainsKey("lang") 
				? new CultureInfo(_context.RouteData.Values["lang"].ToString())
				: new CultureInfo(_siteContext.Site.DefaultLanguage.Name);
			return currentCulture;
		}

		public void SetCulture(CultureInfo culture) {
			if (culture == null) {
				throw new ArgumentNullException("culture");
			}
			_context.HttpContext.Response.Cookies.Add(new HttpCookie(CookieName, culture.Name));
		}

		public Phrase GetPhrase(string key) {
			return _repository.GetPhrase(key);
		}

		public Phrase GetPhrase(string localizationStore, string key) {
			return _repository.GetPhrase(localizationStore, key);
		}

		public void AddPhrase(Phrase phrase) {
			throw new NotImplementedException();
		}
	}
}
