﻿using System.Globalization;
using OxLet.Core.Models;

namespace OxLet.Core.Localization.Interfaces {
	public interface ILocalizationService {
		CultureInfo GetCulture();
		void SetCulture(CultureInfo culture);
		Phrase GetPhrase(string key);
		Phrase GetPhrase(string localizationStore, string key);
		void AddPhrase(Phrase phrase);
	}
}
