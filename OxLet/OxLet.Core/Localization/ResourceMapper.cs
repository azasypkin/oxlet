﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OxLet.Core.Localization {
	public class ResourceMapper : IResourceMapper {

		private static readonly Dictionary<string, Type> _mapping = new Dictionary<string, Type>();

		public Type GetResourceType(string resourceName) {
			return _mapping.ContainsKey(resourceName) ? _mapping[resourceName] : null;
		}

		public IEnumerable<Type> GetRegisteredResourceTypes() {
			return _mapping.Values.Distinct();
		}

		public IResourceMapper AddResourceMapping(string alias, Type resourceType) {
			_mapping[alias] = resourceType;
			return this;
		}
	}
}