﻿using OxLet.Core.Models;

namespace OxLet.Core.Localization.Repositories {
	public interface ILocalizationRepository {
		Phrase GetPhrase(string key);
		Phrase GetPhrase(string localizationStore, string key);
	}
}
