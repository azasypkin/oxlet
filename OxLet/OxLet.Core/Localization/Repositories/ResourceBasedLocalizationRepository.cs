﻿using System;
using System.Linq;
using System.Threading;
using OxLet.Core.Models;

namespace OxLet.Core.Localization.Repositories {
	public class ResourceBasedLocalizationRepository : ILocalizationRepository {
		private readonly IResourceMapper _resourceMapper;
		public ResourceBasedLocalizationRepository(IResourceMapper resourceMapper) {
			_resourceMapper = resourceMapper;
		}

		public Phrase GetPhrase(string key) {
			return _resourceMapper
				.GetRegisteredResourceTypes()
				.Select(mappingType => ExtractPhraseFromResources(mappingType, key))
				.FirstOrDefault(phrase => phrase != null);
		}

		public Phrase GetPhrase(string localizationStore, string key) {
			var mappingType = _resourceMapper.GetResourceType(localizationStore);
			return mappingType != null ? ExtractPhraseFromResources(mappingType, key) : null;
		}
		private static Phrase ExtractPhraseFromResources(Type resourceType, string key) {
			// check for an entry in the resource file
			var resourceValueProperty = resourceType
				.GetProperties()
				.FirstOrDefault(p => p.Name.Equals(key));
			if (resourceValueProperty != null) {
				return new Phrase {
					Key = key,
					Value = resourceValueProperty.GetValue(null, null).ToString(),
					Language = Thread.CurrentThread.CurrentCulture.Name
				};
			}
			return null;
		}
	}
}
