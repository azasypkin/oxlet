﻿using System.Web.Mvc;
using OxLet.Core.Localization.Models.Input;
using OxLet.Core.Web.Extensions;

namespace OxLet.Core.Localization.ModelBinders.Input {
	public class LanguageInputModelBinder : IModelBinder {

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

			var valueProvider = bindingContext.ValueProvider;

			ValueProviderResult providerResult = valueProvider.GetValue("langCode");
			if (providerResult != null) {
				return new LanguageInput(providerResult.GetValue<string>());
			}

			return null;
		}
	}
}
