﻿using System.Web.Mvc;
using OxLet.Core.Infrastructure;
using OxLet.Core.Services.Interfaces;
using OxLet.Core.Web.Extensions;

namespace OxLet.Core.Localization.ModelBinders {
	public class LanguageModelBinder : IModelBinder {
		private readonly ILanguageService _languageService;
		private readonly SiteContext _siteContext;

		public LanguageModelBinder(ILanguageService languageService, SiteContext siteContext) {
			_languageService = languageService;
			_siteContext = siteContext;
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			
			ValueProviderResult providerResult = bindingContext.ValueProvider.GetValue("langName");


			if (providerResult != null && !string.IsNullOrEmpty(providerResult.GetValue<string>())) {
				return _languageService.GetLanguage(providerResult.GetValue<string>());
			}

			return null;
		}
	}
}
