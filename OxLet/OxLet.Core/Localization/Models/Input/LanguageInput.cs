﻿using System.Globalization;

namespace OxLet.Core.Localization.Models.Input {
	public class LanguageInput {

		public LanguageInput(string languageCode) {
			Culture = new CultureInfo(languageCode);
		}

		public LanguageInput(CultureInfo culture) {
			Culture = culture;
		}

		public CultureInfo Culture { get; private set; }
	}
}
