﻿using System.Web.Mvc;

namespace OxLet.Core.Infrastructure.XmlRpc.ModelBinders {
	public class XmlRpcRequestModelBinder<TValue> : IModelBinder {
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
			return (TValue)controllerContext.RouteData.Values["XmlRpcRequest"];
		}
	}
}
