﻿using System.Web.Routing;
using OxLet.Core.Infrastructure.XmlRpc.Models;
using OxLet.Core.Secuity.Interfaces;

namespace OxLet.Core.Infrastructure.XmlRpc {
	class XmlRpcAuthenticationModule : IAuthenticationModule {

		private readonly IUserService _userService;

		public XmlRpcAuthenticationModule(IUserService userService) {
			_userService = userService;
		}

		public IUser Authentificate(RequestContext context) {
			// check if it is xml rpc request
			if (context.RouteData.DataTokens.ContainsKey("IsXmlRpc") && (bool)context.RouteData.DataTokens["IsXmlRpc"]) {
				var xmlRpcSecureItem = context.RouteData.Values["XmlRpcRequest"] as IXmlRpcSecureItem;
				if (xmlRpcSecureItem != null) {
					if (_userService.ValidateUser(xmlRpcSecureItem.Credentials.Name, xmlRpcSecureItem.Credentials.Password)) {
						return _userService.Get(xmlRpcSecureItem.Credentials.Name, true);
					}
				}
			}
			return null;
		}
	}
}
