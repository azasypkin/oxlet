using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OxLet.Core.Infrastructure.XmlRpc.Models;

namespace OxLet.Core.Infrastructure.XmlRpc {
	public static class XmlRpcParameterMapper {
		public static IDictionary<string, object> Map(ParameterDescriptor[] methodParameters,
		                                              IList<XmlRpcParameter> rpcParameters) {
			var mappedValues = new Dictionary<string, object>();

			for (int i = 0; i < rpcParameters.Count; i++)
				mappedValues.Add(methodParameters[i].ParameterName, rpcParameters[i].AsType(methodParameters[i].ParameterType));

			return mappedValues;
		}

		public static object ConvertToXmlRpcRequest(string action, IList<XmlRpcParameter> rpcParameters) {
			switch (action) {
				case "blogger.getUsersBlogs":
					return new XmlRpcGetUserBlogs(rpcParameters.ToArray());
				case "metaWeblog.getCategories":
					return new XmlRpcGetCategories(rpcParameters.ToArray());
				case "metaWeblog.newPost":
					return new XmlRpcNewPost(rpcParameters.ToArray());
				case "metaWeblog.newMediaObject":
					return new XmlRpcNewMediaObject(rpcParameters.ToArray());
				case "metaWeblog.getRecentPosts":
					return new XmlRpcGetRecentPosts(rpcParameters.ToArray());
				case "metaWeblog.getPost":
					return new XmlRpcGetPost(rpcParameters.ToArray());
				case "metaWeblog.editPost":
					return new XmlRpcEditPost(rpcParameters.ToArray());
				case "blogger.deletePost":
					return new XmlRpcDeletePost(rpcParameters.ToArray());
				default: return null;
			}
		}
	}
}
