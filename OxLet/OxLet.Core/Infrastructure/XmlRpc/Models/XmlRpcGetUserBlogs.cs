﻿namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcGetUserBlogs : IXmlRpcSecureItem {

		public XmlRpcGetUserBlogs(XmlRpcParameter[] parameters) {
			ApiKey = parameters[0].AsString();
			Credentials = new XmlRpcCredentials { Name = parameters[1].AsString(), Password = parameters[2].AsString() };
		}

		public string ApiKey { get; private set; }

		public XmlRpcCredentials Credentials { get; private set; }
	}
}
