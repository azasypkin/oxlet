﻿namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcGetRecentPosts : IXmlRpcSecureItem {

		public XmlRpcGetRecentPosts(XmlRpcParameter[] parameters) {
			BlogName = parameters[0].AsString();
			Credentials = new XmlRpcCredentials { Name = parameters[1].AsString(), Password = parameters[2].AsString() };
			PageSize = parameters[3].AsInt();
		}

		public string BlogName { get; private set; }

		public int? PageSize { get; set; }

		public XmlRpcCredentials Credentials { get; private set; }
	}
}
