﻿namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public interface IXmlRpcSecureItem {
		XmlRpcCredentials Credentials { get;}
	}
}
