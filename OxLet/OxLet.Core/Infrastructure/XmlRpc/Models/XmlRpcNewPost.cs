﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcNewPost: IXmlRpcSecureItem {

		public XmlRpcNewPost(XmlRpcParameter[] parameters) {
			BlogId = parameters[0].AsString();
			Credentials = new XmlRpcCredentials { Name = parameters[1].AsString(), Password = parameters[2].AsString() };

			FillExtendedPostProperties(parameters[3].AsDictionary());

			IsPublish = parameters[4].AsBool();
		}

		private void FillExtendedPostProperties(IDictionary<string, object> postProperties) {
			Title = postProperties["title"] as string;
			Description = postProperties["description"] as string;
			Excerpt = postProperties.ContainsKey("mt_excerpt") ? postProperties["mt_excerpt"] as string : "";
			Categories = (postProperties["categories"] as object[]).OfType<string>().ToArray();
			BaseName = postProperties.ContainsKey("mt_basename")?postProperties["mt_basename"] as string : "";

			if (postProperties.ContainsKey("date_created_gmt")) {
				Created = postProperties["date_created_gmt"] as DateTime?;
			}
			//if(postProperties.ContainsKey("dateTime.iso8601")) {
			//    Created = DateTime.ParseExact(
			//        postProperties["dateTime.iso8601"] as string,
			//        new[] {"r", "s", "u", "yyyyMMddTHHmmss", "yyyyMMddTHH:mm:ss", "yyyy-MM-ddTHH:mm:ss"},
			//        CultureInfo.InvariantCulture,
			//        DateTimeStyles.AdjustToUniversal
			//    );
			//}
		}

		public string BlogId { get; private set; }

		public string Title { get; private set; }

		public string Excerpt { get; private set; }

		public string BaseName { get; private set; }

		public string Description { get; private set; }

		public string[] Categories { get; private set; }

		public bool? IsPublish { get; set; }

		public DateTime? Created{ get; private set; }

		public XmlRpcCredentials Credentials { get; private set; }
	}
}
