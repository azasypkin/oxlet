﻿namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcCredentials {
		public string Name { get; set; }
		public string Password { get; set; }
	}
}
