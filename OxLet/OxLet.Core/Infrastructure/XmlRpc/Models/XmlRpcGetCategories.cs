﻿namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcGetCategories : IXmlRpcSecureItem {

		public XmlRpcGetCategories(XmlRpcParameter[] parameters) {
			BlogId = parameters[0].AsString();
			Credentials = new XmlRpcCredentials { Name = parameters[1].AsString(), Password = parameters[2].AsString() };
		}

		public string BlogId { get; private set; }

		public XmlRpcCredentials Credentials { get; private set; }
	}
}
