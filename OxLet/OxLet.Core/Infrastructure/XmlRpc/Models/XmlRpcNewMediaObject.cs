﻿using System.Collections.Generic;

namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcNewMediaObject : IXmlRpcSecureItem {

		public XmlRpcNewMediaObject(XmlRpcParameter[] parameters) {
			BlogId = parameters[0].AsString();
			Credentials = new XmlRpcCredentials { Name = parameters[1].AsString(), Password = parameters[2].AsString() };
			FillExtendedPostProperties(parameters[3].AsDictionary());

		}

		private void FillExtendedPostProperties(IDictionary<string, object> postProperties) {
			MimeType = postProperties["type"] as string;
			Name = postProperties["name"] as string;
			Content = postProperties["bits"] as byte[];
		}

		public string BlogId { get; private set; }

		public string MimeType { get; private set; }

		public string Name { get; private set; }

		public byte[] Content { get; private set; }

		public XmlRpcCredentials Credentials { get; private set; }
	}
}
