﻿namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcDeletePost : IXmlRpcSecureItem {

		public XmlRpcDeletePost(XmlRpcParameter[] parameters) {
			AppKey = parameters[0].AsString();
			Id = parameters[1].AsString();
			Credentials = new XmlRpcCredentials { Name = parameters[2].AsString(), Password = parameters[3].AsString() };
			IsPublish = parameters[4].AsBool();
		}

		public string AppKey { get; private set; }
		public string Id { get; private set; }
		public bool? IsPublish { get; set; }
		public XmlRpcCredentials Credentials { get; private set; }
	}
}
