﻿namespace OxLet.Core.Infrastructure.XmlRpc.Models {
	public class XmlRpcGetPost : IXmlRpcSecureItem {

		public XmlRpcGetPost(XmlRpcParameter[] parameters) {
			PostId = parameters[0].AsString();
			Credentials = new XmlRpcCredentials { Name = parameters[1].AsString(), Password = parameters[2].AsString() };
		}

		public string PostId { get; private set; }

		public XmlRpcCredentials Credentials { get; private set; }
	}
}
