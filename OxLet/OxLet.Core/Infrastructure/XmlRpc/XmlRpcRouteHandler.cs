﻿using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml.Linq;

namespace OxLet.Core.Infrastructure.XmlRpc {
	public class XmlRpcRouteHandler : IRouteHandler {
		#region IRouteHandler Members

		/// <summary>
		/// Provides the object that processes the request.
		/// </summary>
		/// <returns>
		/// An object that processes the request.
		/// </returns>
		/// <param name="requestContext">An object that encapsulates information about the request.</param>
		public IHttpHandler GetHttpHandler(RequestContext requestContext) {
			try {
				XDocument document = XDocument.Load(new StreamReader(requestContext.HttpContext.Request.InputStream));
				XElement methodCall = document.Element("methodCall");
				requestContext.RouteData.Values["action"] = methodCall.Element("methodName").Value;
				if (methodCall != null) {

					XElement parameters = methodCall.Element("params");

					if (parameters != null) {
						var xmlRpcParametersList = parameters.Elements("param").Select(e => new XmlRpcParameter(e)).ToList();
						requestContext.RouteData.Values["parameters"] = xmlRpcParametersList;
						// map to xmlRpcRequest
						requestContext.RouteData.Values.Add(
							"XmlRpcRequest",
							XmlRpcParameterMapper.ConvertToXmlRpcRequest(requestContext.RouteData.Values["action"].ToString(), xmlRpcParametersList)
						);
					}
				}


				requestContext.RouteData.DataTokens.Add("IsXmlRpc", true);

				return new MvcHandler(requestContext);
			}
			catch {
				return new XmlRpcFaultHttpHandler(new XmlRpcFaultResult(0, "Error parsing request"));
			}
		}

		#endregion
	}
}
