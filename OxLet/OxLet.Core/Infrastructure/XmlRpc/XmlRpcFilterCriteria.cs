using OxLet.Core.Web.Filters;
using OxLet.Core.Web.Filters.Interfaces;

namespace OxLet.Core.Infrastructure.XmlRpc {
	public class XmlRpcFilterCriteria : IActionFilterCriteria {
		public bool Match(ActionFilterRegistryContext context) {
			return context.ControllerContext.RouteData.DataTokens.ContainsKey("IsXmlRpc") &&
			       ((bool) context.ControllerContext.RouteData.DataTokens["IsXmlRpc"]);
		}
	}
}
