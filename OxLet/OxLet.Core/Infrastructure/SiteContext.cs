﻿using System.Web;
using System.Web.Routing;
using OxLet.Core.Configuration;
using OxLet.Core.Models;
using OxLet.Core.Secuity;

namespace OxLet.Core.Infrastructure {
	public class SiteContext {
		public SiteContext(RequestContext requestContext, RouteCollection routes, OxLetSection configuration, Site site, User user) {
			HttpContext = requestContext.HttpContext;
			RequestContext = requestContext;
			Routes = routes;
			Site = site;
			User = user;
			Configuration = configuration;
		}

		public SiteContext(SiteContext context): this(context.RequestContext, context.Routes, context.Configuration, context.Site, context.User) {
		}

		public HttpContextBase HttpContext { get; private set; }
		public RequestContext RequestContext { get; private set; }
		public RouteCollection Routes { get; private set; }
		public Site Site { get; private set; }
		public User User { get; private set; }
		public OxLetSection Configuration { get; private set; }
	}
}
