﻿using System;
using System.ComponentModel;

namespace OxLet.Core.TypeConverters {
	public class StringTypeConverter : TypeConverter{

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
			return ((sourceType == typeof(string)) || base.CanConvertFrom(context, sourceType));
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value) {
			var typeName = value as string;
			if (typeName != null) {
				return Type.GetType(typeName, true, true);
			}
			return base.ConvertFrom(context, culture, value);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType) {
			return destinationType != typeof(string) 
				? base.ConvertTo(context, culture, value, destinationType) 
				: ((Type)value).AssemblyQualifiedName;
		}
	}
}
