<Query Kind="Expression">
  <Connection>
    <ID>e8ef5843-36ba-4984-821d-2af6616f5e93</ID>
    <Persist>true</Persist>
    <Server>(local)</Server>
    <Database>OxLet</Database>
  </Connection>
</Query>

from c in Oxlet_Comments
				join p in Oxlet_Posts on c.PostId equals p.Id
				join b in Oxlet_Blogs on p.BlogId equals b.Id
				join u in Oxlet_Users on c.CreatorUserId equals u.Id into ucr
				join anu in Oxlet_CommentAnonymous on c.Id equals anu.Id into aucr
				from uc in ucr.DefaultIfEmpty()
				from auc in aucr.DefaultIfEmpty()
				where p.PublishedDate <= DateTime.UtcNow
				orderby c.CreatedDate descending
				select new {Comment = c, Post = p, User = uc, AnonymousUser = auc}