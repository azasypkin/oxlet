-- SQL Manager 2010 for SQL Server 3.6.0.1
-- ---------------------------------------
-- Host      : OXYCOM-PC
-- Database  : OxiLet
-- Version   : Microsoft SQL Server  10.50.1600.1


CREATE DATABASE [OxiLet1]
ON PRIMARY
  ( NAME = [OxiLet1],
    FILENAME = N'E:\Projects\Oxlet\trunk\Data\OxiLet1.mdf',
    SIZE = 25088 KB,
    MAXSIZE = UNLIMITED,
    FILEGROWTH = 1 MB )
LOG ON
  ( NAME = [OxiLet1_log],
    FILENAME = N'E:\Projects\Oxlet\trunk\Data\OxiLet1_log.LDF',
    SIZE = 3136 KB,
    MAXSIZE = 2097152 MB,
    FILEGROWTH = 10 % )
COLLATE Cyrillic_General_CI_AS
GO

USE [OxiLet1]
GO

SET NOCOUNT ON
GO

--
-- Definition for table oxlet_Language : 
--

CREATE TABLE [dbo].[oxlet_Language] (
  [Id] int NOT NULL,
  [Name] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [DisplayName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Site : 
--

CREATE TABLE [dbo].[oxlet_Site] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [SiteHost] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [SiteName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [SiteDisplayName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [SiteDescription] nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [DefaultLanguageId] int NOT NULL,
  [TimeZoneOffset] float NOT NULL,
  [PageTitleSeparator] nvarchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [FavIconUrl] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [ScriptsPath] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [CssPath] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [IncludeOpenSearch] bit NOT NULL,
  [AuthorAutoSubscribe] bit NOT NULL,
  [PostEditTimeout] smallint NOT NULL,
  [GravatarDefault] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [SkinDefault] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [ServiceRetryCountDefault] tinyint CONSTRAINT [DF_oxlet_Site_ServiceRetryCountDefault] DEFAULT 3 NOT NULL,
  [RouteUrlPrefix] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS CONSTRAINT [DF_oxlet_Site_RouteUrlPrefix] DEFAULT N'' NOT NULL,
  [ImagesPath] varchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Blog : 
--

CREATE TABLE [dbo].[oxlet_Blog] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [SiteId] int NOT NULL,
  [BlogName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [BlogDisplayName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [BlogDescription] nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [AuthorAutoSubscribe] bit NOT NULL,
  [PostEditTimeout] smallint NOT NULL,
  [SkinDefault] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [CommentingDisabled] bit CONSTRAINT [DF_oxlet_Blog_AllowComments] DEFAULT 0 NOT NULL,
  [CreatedDate] datetime DEFAULT getdate() NOT NULL,
  [ModifiedDate] datetime DEFAULT getdate() NOT NULL,
  [CommentStateDefault] tinyint NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Role : 
--

CREATE TABLE [dbo].[oxlet_Role] (
  [ParentRoleId] int NOT NULL,
  [Id] int IDENTITY(1, 1) NOT NULL,
  [RoleName] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_BlogRoleRelationship : 
--

CREATE TABLE [dbo].[oxlet_BlogRoleRelationship] (
  [BlogId] int NOT NULL,
  [RoleId] int NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_User : 
--

CREATE TABLE [dbo].[oxlet_User] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [Name] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [DisplayName] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Email] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [EmailHash] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Password] nvarchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [PasswordSalt] nvarchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [DefaultLanguageId] int NOT NULL,
  [Status] tinyint NOT NULL,
  [Url] varchar(255) COLLATE Cyrillic_General_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Post : 
--

CREATE TABLE [dbo].[oxlet_Post] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [CreatorUserId] int NOT NULL,
  [Title] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Body] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [BodyShort] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Slug] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [CommentingDisabled] bit CONSTRAINT [DF_oxlet_Post_AllowComments] DEFAULT 0 NOT NULL,
  [CreatedDate] datetime NOT NULL,
  [ModifiedDate] datetime NOT NULL,
  [PublishedDate] datetime NULL,
  [SearchBody] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [BlogId] int NOT NULL,
  [State] tinyint NOT NULL,
  [LanguageId] int NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Comment : 
--

CREATE TABLE [dbo].[oxlet_Comment] (
  [PostId] int NOT NULL,
  [Id] int IDENTITY(1, 1) NOT NULL,
  [CreatorUserId] int NULL,
  [LanguageId] int NOT NULL,
  [CreatorIp] bigint NOT NULL,
  [UserAgent] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Body] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [State] tinyint NOT NULL,
  [CreatedDate] datetime NOT NULL,
  [ModifiedDate] datetime NOT NULL,
  [Slug] nvarchar(100) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_CommentAnonymous : 
--

CREATE TABLE [dbo].[oxlet_CommentAnonymous] (
  [CommentId] int NOT NULL,
  [Name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Email] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [EmailHash] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Url] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_FileResource : 
--

CREATE TABLE [dbo].[oxlet_FileResource] (
  [SiteId] int NOT NULL,
  [Id] int IDENTITY(1, 1) NOT NULL,
  [Name] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [CreatorUserId] int NOT NULL,
  [Data] varbinary(max) NULL,
  [ContentType] varchar(25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [State] tinyint NOT NULL,
  [CreatedDate] datetime NOT NULL,
  [ModifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_ContentItemResource : 
--

CREATE TABLE [dbo].[oxlet_ContentItemResource] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [FileResourceId] int NULL,
  [Path] nvarchar(256) COLLATE Cyrillic_General_CI_AS NULL,
  [State] tinyint NOT NULL,
  [Created] datetime NOT NULL,
  [Modified] datetime NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_ContentItem : 
--

CREATE TABLE [dbo].[oxlet_ContentItem] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [ParentId] int NULL,
  [ValueResourceId] int NULL,
  [ThumbnailResourceId] int NULL,
  [Slug] nvarchar(256) COLLATE Cyrillic_General_CI_AS NOT NULL,
  [Created] datetime NOT NULL,
  [Modified] datetime NOT NULL,
  [Published] datetime NULL,
  [Name] nvarchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
  [DispalyName] nvarchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
  [State] tinyint NOT NULL,
  [LanguageId] int NOT NULL,
  [SiteId] int NOT NULL,
  [IsFolder] bit NOT NULL,
  [CreatorId] int NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_ContentItemComment : 
--

CREATE TABLE [dbo].[oxlet_ContentItemComment] (
  [ContentItemId] int NOT NULL,
  [Id] int IDENTITY(1, 1) NOT NULL,
  [CreatorUserId] int NULL,
  [LanguageId] int NOT NULL,
  [CreatorIp] bigint NOT NULL,
  [UserAgent] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Body] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [State] tinyint NOT NULL,
  [CreatedDate] datetime NOT NULL,
  [ModifiedDate] datetime NOT NULL,
  [Slug] nvarchar(100) COLLATE Cyrillic_General_CI_AS NOT NULL,
  [ParentId] int NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_ContentItemAnonymousComment : 
--

CREATE TABLE [dbo].[oxlet_ContentItemAnonymousComment] (
  [ContentItemCommentId] int NOT NULL,
  [Name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Email] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [EmailHash] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Url] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Message : 
--

CREATE TABLE [dbo].[oxlet_Message] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [CreatorUserId] int NULL,
  [CreatorIp] bigint NOT NULL,
  [UserAgent] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Body] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [CreatedDate] datetime NOT NULL,
  [CreatorName] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [CreatorEmail] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [CreatorUrl] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Plugin : 
--

CREATE TABLE [dbo].[oxlet_Plugin] (
  [SiteId] int NOT NULL,
  [Id] int IDENTITY(1, 1) NOT NULL,
  [PluginName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [PluginCategory] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Enabled] bit NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_PluginSetting : 
--

CREATE TABLE [dbo].[oxlet_PluginSetting] (
  [SiteId] int NOT NULL,
  [PluginId] int NOT NULL,
  [PluginSettingName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [PluginSettingValue] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Podcast : 
--

CREATE TABLE [dbo].[oxlet_Podcast] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [ContentItemId] int NOT NULL,
  [Duration] tinyint NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_Tag : 
--

CREATE TABLE [dbo].[oxlet_Tag] (
  [ParentId] int NOT NULL,
  [Id] int IDENTITY(1, 1) NOT NULL,
  [Name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
  [CreatedDate] datetime NOT NULL,
  [DisplayName] nvarchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
  [IsService] bit DEFAULT 0 NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_PostTagRelationship : 
--

CREATE TABLE [dbo].[oxlet_PostTagRelationship] (
  [PostId] int NOT NULL,
  [TagId] int NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_RecentUpdate : 
--

CREATE TABLE [dbo].[oxlet_RecentUpdate] (
  [Id] int IDENTITY(1, 1) NOT NULL,
  [CreatorUserId] int NOT NULL,
  [Category] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [ItemValue] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ModifiedDate] datetime NOT NULL,
  [SiteId] int NOT NULL,
  [State] tinyint NOT NULL,
  [ItemId] int NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_SiteRedirect : 
--

CREATE TABLE [dbo].[oxlet_SiteRedirect] (
  [SiteId] int NOT NULL,
  [SiteRedirect] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_StringResource : 
--

CREATE TABLE [dbo].[oxlet_StringResource] (
  [StringResourceKey] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Language] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Version] smallint NOT NULL,
  [StringResourceValue] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [CreatorUserId] int NOT NULL,
  [CreatedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_StringResourceVersion : 
--

CREATE TABLE [dbo].[oxlet_StringResourceVersion] (
  [StringResourceKey] nvarchar(256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Language] varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [Version] smallint NOT NULL,
  [StringResourceValue] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [CreatorUserId] int NOT NULL,
  [CreatedDate] datetime NOT NULL,
  [State] tinyint NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_UserFileResourceRelationship : 
--

CREATE TABLE [dbo].[oxlet_UserFileResourceRelationship] (
  [UserId] int NOT NULL,
  [FileResourceId] int NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_UserLanguage : 
--

CREATE TABLE [dbo].[oxlet_UserLanguage] (
  [UserId] int NOT NULL,
  [LanguageId] int NOT NULL
)
ON [PRIMARY]
GO

--
-- Definition for table oxlet_UserRoleRelationship : 
--

CREATE TABLE [dbo].[oxlet_UserRoleRelationship] (
  [UserId] int NOT NULL,
  [RoleId] int NOT NULL
)
ON [PRIMARY]
GO

--
-- Data for table dbo.oxlet_Language  (LIMIT 0,500)
--

INSERT INTO [dbo].[oxlet_Language] ([Id], [Name], [DisplayName])
VALUES 
  (1, N'en-US', N'English')
GO

INSERT INTO [dbo].[oxlet_Language] ([Id], [Name], [DisplayName])
VALUES 
  (2, N'ru-RU', N'Russian')
GO

--
-- Data for table dbo.oxlet_Site  (LIMIT 0,500)
--

INSERT INTO [dbo].[oxlet_Site] ([SiteHost], [SiteName], [SiteDisplayName], [SiteDescription], [DefaultLanguageId], [TimeZoneOffset], [PageTitleSeparator], [FavIconUrl], [ScriptsPath], [CssPath], [IncludeOpenSearch], [AuthorAutoSubscribe], [PostEditTimeout], [GravatarDefault], [SkinDefault], [ServiceRetryCountDefault], [RouteUrlPrefix], [ImagesPath])
VALUES 
  (N'http://oxletsite/', N'OxLet', N'OxLet', N'', 1, -8, N' |', N'~/Images/favicon.ico', N'~/{area}/Skins/{skin}/Scripts', N'~/{area}/Skins/{skin}/Styles', 1, 1, 24, N'http://www.gravatar.com/avatar/1009d53423e81d879775e6c802d43d72.png', N'Default', 0, N'', N'~/{area}/Skins/{skin}/Images')
GO

--
-- Data for table dbo.oxlet_Blog  (LIMIT 0,500)
--

INSERT INTO [dbo].[oxlet_Blog] ([SiteId], [BlogName], [BlogDisplayName], [BlogDescription], [AuthorAutoSubscribe], [PostEditTimeout], [SkinDefault], [CommentingDisabled], [CreatedDate], [ModifiedDate], [CommentStateDefault])
VALUES 
  (1, N'OxLetBlog', N'OxLet Blog', N'This is my blog  - OxLet blog.', 1, 0, N'Default', 0, '20110225 01:37:16.547', '20110225 01:37:16.547', 1)
GO

--
-- Data for table dbo.oxlet_User  (LIMIT 0,500)
--

INSERT INTO [dbo].[oxlet_User] ([Name], [DisplayName], [Email], [EmailHash], [Password], [PasswordSalt], [DefaultLanguageId], [Status], [Url])
VALUES 
  (N'Anonymous', N'', N'', N'', N'', N'', 1, 1, NULL)
GO

INSERT INTO [dbo].[oxlet_User] ([Name], [DisplayName], [Email], [EmailHash], [Password], [PasswordSalt], [DefaultLanguageId], [Status], [Url])
VALUES 
  (N'OxyGen', N'OxyGen', N'oxycommail@gmail.com', N'641e53b227d5b1aa32da3c6bc474ed3e', N'gyjBnCt1eBC18PvZo36p/4RKIIEXCMwOJC2oTn1AbNg=', N'fc9ce49f645849be800860f94690480b', 1, 1, NULL)
GO

--
-- Definition for indices : 
--

ALTER TABLE [dbo].[oxlet_Language]
ADD CONSTRAINT [PK_oxlet_Language] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Site]
ADD CONSTRAINT [PK_oxlet_Site] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Blog]
ADD CONSTRAINT [PK_oxlet_Blog] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Role]
ADD CONSTRAINT [IX_oxlet_RoleName] 
UNIQUE NONCLUSTERED ([RoleName])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Role]
ADD CONSTRAINT [PK_oxlet_Role] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_BlogRoleRelationship]
ADD CONSTRAINT [PK_oxlet_AreaRoleRelationship] 
PRIMARY KEY CLUSTERED ([BlogId], [RoleId])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_User]
ADD CONSTRAINT [IX_oxlet_Username] 
UNIQUE NONCLUSTERED ([Name])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_User]
ADD CONSTRAINT [PK_oxlet_User] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_oxlet_Post] ON [dbo].[oxlet_Post]
  ([Slug])
WITH (
  PAD_INDEX = OFF,
  DROP_EXISTING = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  SORT_IN_TEMPDB = OFF,
  ONLINE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Post]
ADD CONSTRAINT [PK_oxlet_Post] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Comment]
ADD CONSTRAINT [PK_oxlet_Comment] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_CommentAnonymous]
ADD CONSTRAINT [PK_oxlet_CommentAnonymous] 
PRIMARY KEY CLUSTERED ([CommentId])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_FileResource]
ADD CONSTRAINT [PK_oxlet_FileResource] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_ContentItemResource]
ADD PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD CONSTRAINT [oxlet_ContentItem_uq] 
UNIQUE NONCLUSTERED ([ParentId], [Slug])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_ContentItemComment]
ADD CONSTRAINT [PK_oxlet_PodcastComment] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_ContentItemAnonymousComment]
ADD CONSTRAINT [PK_oxlet_ContentItemAnonymousComment] 
PRIMARY KEY CLUSTERED ([ContentItemCommentId])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Message]
ADD CONSTRAINT [PK_oxlet_Message] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Plugin]
ADD CONSTRAINT [PK_oxlet_Plugin] 
PRIMARY KEY CLUSTERED ([SiteId], [Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_PluginSetting]
ADD CONSTRAINT [PK_oxlet_PluginSetting] 
PRIMARY KEY CLUSTERED ([SiteId], [PluginId], [PluginSettingName])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Podcast]
ADD PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_Tag]
ADD CONSTRAINT [PK_oxlet_Tag] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_PostTagRelationship]
ADD CONSTRAINT [PK_oxlet_PostTagRelationship] 
PRIMARY KEY CLUSTERED ([PostId], [TagId])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_RecentUpdate]
ADD CONSTRAINT [PK_oxlet_RecentUpdate] 
PRIMARY KEY CLUSTERED ([Id])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_SiteRedirect]
ADD CONSTRAINT [PK_oxlet_SiteRedirect] 
PRIMARY KEY CLUSTERED ([SiteId], [SiteRedirect])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_StringResource]
ADD CONSTRAINT [PK_oxlet_StringResource] 
PRIMARY KEY CLUSTERED ([StringResourceKey])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_StringResourceVersion]
ADD CONSTRAINT [PK_oxlet_StringResourceVersion] 
PRIMARY KEY CLUSTERED ([StringResourceKey], [Language], [Version])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_UserFileResourceRelationship]
ADD CONSTRAINT [PK_oxlet_UserFileResourceRelationship] 
PRIMARY KEY CLUSTERED ([UserId], [FileResourceId])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_UserLanguage]
ADD CONSTRAINT [PK_oxlet_UserLanguage] 
PRIMARY KEY CLUSTERED ([UserId], [LanguageId])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[oxlet_UserRoleRelationship]
ADD CONSTRAINT [PK_oxlet_UserRoleRelationship] 
PRIMARY KEY CLUSTERED ([UserId], [RoleId])
WITH (
  PAD_INDEX = OFF,
  IGNORE_DUP_KEY = OFF,
  STATISTICS_NORECOMPUTE = OFF,
  ALLOW_ROW_LOCKS = ON,
  ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

--
-- Definition for foreign keys : 
--

ALTER TABLE [dbo].[oxlet_Site]
ADD CONSTRAINT [oxlet_Site_DefaultLanguageId_Language_Id] FOREIGN KEY ([DefaultLanguageId]) 
  REFERENCES [dbo].[oxlet_Language] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Blog]
ADD CONSTRAINT [FK_oxlet_Blog_SiteId_Site_Id] FOREIGN KEY ([SiteId]) 
  REFERENCES [dbo].[oxlet_Site] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Role]
ADD CONSTRAINT [FK_oxlet_Role_oxlet_Role] FOREIGN KEY ([ParentRoleId]) 
  REFERENCES [dbo].[oxlet_Role] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_BlogRoleRelationship]
ADD CONSTRAINT [FK_oxlet_AreaRoleRelationship_oxlet_Role] FOREIGN KEY ([RoleId]) 
  REFERENCES [dbo].[oxlet_Role] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_BlogRoleRelationship]
ADD CONSTRAINT [FK_oxlet_BlogRoleRelationship_oxlet_Blog] FOREIGN KEY ([BlogId]) 
  REFERENCES [dbo].[oxlet_Blog] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_User]
ADD CONSTRAINT [FK_oxlet_User_oxlet_Language] FOREIGN KEY ([DefaultLanguageId]) 
  REFERENCES [dbo].[oxlet_Language] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Post]
ADD CONSTRAINT [FK_oxlet_Post_BlogId_Blog_Id] FOREIGN KEY ([BlogId]) 
  REFERENCES [dbo].[oxlet_Blog] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Post]
ADD CONSTRAINT [FK_oxlet_Post_LanguageId_Language_Id] FOREIGN KEY ([LanguageId]) 
  REFERENCES [dbo].[oxlet_Language] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Post]
ADD CONSTRAINT [FK_oxlet_Post_oxlet_User] FOREIGN KEY ([CreatorUserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Comment]
ADD CONSTRAINT [FK_oxlet_Comment_oxlet_Language] FOREIGN KEY ([LanguageId]) 
  REFERENCES [dbo].[oxlet_Language] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Comment]
ADD CONSTRAINT [FK_oxlet_Comment_oxlet_Post] FOREIGN KEY ([PostId]) 
  REFERENCES [dbo].[oxlet_Post] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Comment]
ADD CONSTRAINT [FK_oxlet_Comment_oxlet_User] FOREIGN KEY ([CreatorUserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_CommentAnonymous]
ADD CONSTRAINT [FK_oxlet_CommentAnonymous_oxlet_Comment] FOREIGN KEY ([CommentId]) 
  REFERENCES [dbo].[oxlet_Comment] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_FileResource]
ADD CONSTRAINT [FK_oxlet_FileResource_oxlet_User] FOREIGN KEY ([CreatorUserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_FileResource]
ADD CONSTRAINT [FK_oxlet_FileResource_SiteId_Site_Id] FOREIGN KEY ([SiteId]) 
  REFERENCES [dbo].[oxlet_Site] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItemResource]
ADD CONSTRAINT [oxlet_ContentItemResource_FileResourceId_FileResource_Id] FOREIGN KEY ([FileResourceId]) 
  REFERENCES [dbo].[oxlet_FileResource] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD CONSTRAINT [oxlet_ContentItem_CreatorId_User_Id] FOREIGN KEY ([CreatorId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD CONSTRAINT [oxlet_ContentItem_LanguageId_Language_Id] FOREIGN KEY ([LanguageId]) 
  REFERENCES [dbo].[oxlet_Language] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD CONSTRAINT [oxlet_ContentItem_ParentId_Id] FOREIGN KEY ([ParentId]) 
  REFERENCES [dbo].[oxlet_ContentItem] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD CONSTRAINT [oxlet_ContentItem_SiteId_Site_Id] FOREIGN KEY ([SiteId]) 
  REFERENCES [dbo].[oxlet_Site] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD CONSTRAINT [oxlet_ContentItem_ThumbnailResourceId_ContentItemResource_Id] FOREIGN KEY ([ThumbnailResourceId]) 
  REFERENCES [dbo].[oxlet_ContentItemResource] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItem]
ADD CONSTRAINT [oxlet_ContentItem_ValueResourceId_ContentItemResource_Id] FOREIGN KEY ([ValueResourceId]) 
  REFERENCES [dbo].[oxlet_ContentItemResource] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItemComment]
ADD CONSTRAINT [FK_oxlet_ContentItemComment_ContentItemId_ContentItem_Id] FOREIGN KEY ([ContentItemId]) 
  REFERENCES [dbo].[oxlet_ContentItem] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItemComment]
ADD CONSTRAINT [FK_oxlet_PodcastComment_oxlet_Language] FOREIGN KEY ([LanguageId]) 
  REFERENCES [dbo].[oxlet_Language] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItemComment]
ADD CONSTRAINT [FK_oxlet_PodcastComment_oxlet_User] FOREIGN KEY ([CreatorUserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItemComment]
ADD CONSTRAINT [oxlet_ContentItemComment_ParentId_Id] FOREIGN KEY ([ParentId]) 
  REFERENCES [dbo].[oxlet_ContentItemComment] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_ContentItemAnonymousComment]
ADD CONSTRAINT [FK_oxlet_ContentItemAnonymousComment_ContentItemCommentId_ContentItemComment_Id] FOREIGN KEY ([ContentItemCommentId]) 
  REFERENCES [dbo].[oxlet_ContentItemComment] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Message]
ADD CONSTRAINT [FK_oxlet_Message_oxlet_User] FOREIGN KEY ([CreatorUserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Plugin]
ADD CONSTRAINT [FK_oxlet_Plugin_oxlet_Site] FOREIGN KEY ([SiteId]) 
  REFERENCES [dbo].[oxlet_Site] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_PluginSetting]
ADD CONSTRAINT [FK_oxlet_PluginSetting_oxlet_Plugin] FOREIGN KEY ([SiteId], [PluginId]) 
  REFERENCES [dbo].[oxlet_Plugin] ([SiteId], [Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_PluginSetting]
ADD CONSTRAINT [FK_oxlet_PluginSetting_oxlet_Site] FOREIGN KEY ([SiteId]) 
  REFERENCES [dbo].[oxlet_Site] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Podcast]
ADD CONSTRAINT [oxlet_Podcast_ContentItemId_ContentItem_Id] FOREIGN KEY ([ContentItemId]) 
  REFERENCES [dbo].[oxlet_ContentItem] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_Tag]
ADD CONSTRAINT [FK_oxlet_Tag_oxlet_Tag] FOREIGN KEY ([ParentId]) 
  REFERENCES [dbo].[oxlet_Tag] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_PostTagRelationship]
ADD CONSTRAINT [FK_oxlet_PostTagRelationship_oxlet_Post] FOREIGN KEY ([PostId]) 
  REFERENCES [dbo].[oxlet_Post] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_PostTagRelationship]
ADD CONSTRAINT [FK_oxlet_PostTagRelationship_oxlet_Tag] FOREIGN KEY ([TagId]) 
  REFERENCES [dbo].[oxlet_Tag] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_RecentUpdate]
ADD CONSTRAINT [FK_oxlet_RecentUpdate_oxlet_User] FOREIGN KEY ([CreatorUserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_RecentUpdate]
ADD CONSTRAINT [FK_oxlet_RecentUpdate_SiteId_Site_Id] FOREIGN KEY ([SiteId]) 
  REFERENCES [dbo].[oxlet_Site] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_SiteRedirect]
ADD CONSTRAINT [FK_oxlet_SiteRedirect_oxlet_Site] FOREIGN KEY ([SiteId]) 
  REFERENCES [dbo].[oxlet_Site] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_StringResourceVersion]
ADD CONSTRAINT [FK_oxlet_StringResourceVersion_oxlet_StringResource] FOREIGN KEY ([StringResourceKey]) 
  REFERENCES [dbo].[oxlet_StringResource] ([StringResourceKey]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_UserFileResourceRelationship]
ADD CONSTRAINT [FK_oxlet_UserFileResourceRelationship_oxlet_FileResource] FOREIGN KEY ([FileResourceId]) 
  REFERENCES [dbo].[oxlet_FileResource] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_UserFileResourceRelationship]
ADD CONSTRAINT [FK_oxlet_UserFileResourceRelationship_oxlet_User] FOREIGN KEY ([UserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_UserLanguage]
ADD CONSTRAINT [FK_oxlet_UserLanguage_oxlet_Language] FOREIGN KEY ([LanguageId]) 
  REFERENCES [dbo].[oxlet_Language] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_UserLanguage]
ADD CONSTRAINT [FK_oxlet_UserLanguage_oxlet_User] FOREIGN KEY ([UserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_UserRoleRelationship]
ADD CONSTRAINT [FK_oxlet_UserRoleRelationship_oxlet_Role] FOREIGN KEY ([RoleId]) 
  REFERENCES [dbo].[oxlet_Role] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE [dbo].[oxlet_UserRoleRelationship]
ADD CONSTRAINT [FK_oxlet_UserRoleRelationship_oxlet_User] FOREIGN KEY ([UserId]) 
  REFERENCES [dbo].[oxlet_User] ([Id]) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO




