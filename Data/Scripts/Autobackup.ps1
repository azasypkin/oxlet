[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null

Import-Module PSCX

$dbname = "Oxilet1"
	
$a = Get-Date
$b = $dbname + "_" + $a.Year + "-" + $a.Month + "-" + $a.Day + ".bak.zip"

Remove-Item E:\Projects\Oxlet\trunk\Data\Bak\*.zip
Remove-Item E:\Projects\Oxlet\trunk\Data\Bak\*.bak

$server = New-Object ("Microsoft.SqlServer.Management.Smo.Server") ("(local)")

$dbBackup = new-Object ("Microsoft.SqlServer.Management.Smo.Backup")
$dbRestore = new-object ("Microsoft.SqlServer.Management.Smo.Restore")

$dbBackup.Database = "Oxilet1"
$dbBackup.Devices.AddDevice("E:\Projects\Oxlet\trunk\Data\Bak\oxilet1.bak", "File")
$dbBackup.Action="Database"
$dbBackup.Initialize = $TRUE

$dbBackup.SqlBackup($server)

if(!(Test-Path E:\Projects\Oxlet\trunk\Data\Bak\oxilet1.bak)){
  $smtp = new-object Net.Mail.SmtpClient("emailserver")
  $smtp.Send("from", "to", "Backups not working", "Action required immediately for Full Backup")
  Exit
}

$dbRestore.Devices.AddDevice("E:\Projects\Oxlet\trunk\Data\Bak\oxilet1.bak", "File")
if (!($dbRestore.SqlVerify($server))){
 $smtp = new-object Net.Mail.SmtpClient("emailserver")
 $smtp.Send("from", "to", "Backups not valid", "Action required immediately for Full Backup")
 Exit
}

Write-Zip E:\Projects\Oxlet\trunk\Data\Bak\oxilet1.bak -OutputPath E:\Projects\Oxlet\trunk\Data\Bak\$b